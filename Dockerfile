# FROM node:12-slim
# RUN mkdir -p usr/src/app
# WORKDIR /usr/src/app
# COPY . .
# RUN npm install -g serve
# RUN npm install
# RUN npm run build
# EXPOSE 8080
# CMD ["serve", "-s", "-l", "8080", "./build"]


# FROM node:14

# WORKDIR /usr/src/app

# COPY package.json ./
# RUN npm install

# COPY . .
# COPY .env.staging .

# ENV PORT 80
# ENV HOST 0.0.0.0
# EXPOSE 80

# RUN npm run build

# # CMD [ "npm", "run", "start" ]
# CMD ["serve", "-s", "-l", "8080", "./build"]


# build environment
FROM node:12-alpine as react-build
WORKDIR /app
COPY . ./
#COPY .env .
RUN npm install
RUN npm run build

# server environment
FROM nginx:alpine
COPY nginx.conf /etc/nginx/conf.d/configfile.template

COPY --from=react-build /app/build /usr/share/nginx/html

ENV PORT 8080
ENV HOST 0.0.0.0

ARG REACT_APP_API_URL=https://versatile-age-284708.et.r.appspot.com
ENV REACT_APP_API_URL=$REACT_APP_API_URL

EXPOSE 8080
CMD sh -c "envsubst '\$PORT' < /etc/nginx/conf.d/configfile.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"