import { formatMoney } from 'accounting-js';

export function formatPrice(value = 0, currency = 'Rp. ') {
  return formatMoney(value, { symbol: currency, thousand: '.', precision: 0 });
}
