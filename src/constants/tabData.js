const TAB_DATA = [
  {
    id: 0,
    name: 'semua',
  },
  {
    id: 61,
    name: 'popular',
  },
  {
    id: 11,
    name: 'film & TV',
  },
  {
    id: 59,
    name: 'International',
  },
  {
    id: 51,
    name: 'athlete',
  },
  {
    id: 22,
    name: 'musician',
  },
  {
    id: 2,
    name: 'influencer',
  },
  {
    id: 52,
    name: 'new',
  },
  {
    id: 67,
    name: 'xoshi',
  },
  {
    id: 57,
    name: 'indonesian idol',
  },
  {
    id: 62,
    name: 'cosplayer',
  },
  {
    id: 63,
    name: 'youtuber',
  },
  {
    id: 64,
    name: 'tiktoker',
  },
  {
    id: 56,
    name: 'drag queen',
  },
  {
    id: 58,
    name: 'magician',
  },
];

export default TAB_DATA;
