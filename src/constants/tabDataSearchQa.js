export const TAB_DATA = {
  data: [
    {
      id: 1,
      tabTitle: 'Semua',
      orderList: false,
      fanQuestions: true,
    },
    {
      id: 2,
      tabTitle: 'Popular',
      orderList: false,
      fanQuestions: false,
    },
    {
      id: 3,
      tabTitle: 'Film & TV',
      orderList: true,
      fanQuestions: false,
    },

    {
      id: 4,
      tabTitle: 'Athlete',
      orderList: true,
      fanQuestions: false,
    },
    {
      id: 5,
      tabTitle: 'Gamer',
      orderList: true,
      fanQuestions: false,
    },
  ],
};
