import request from 'superagent';
import { getApiBaseUrl } from '../app/helpers';

export async function getDetailTalent(params) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Talent/TalentDetail`)
      .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function getDetailVideo(params) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Talent/TalentDetailVideo`)
      .set('Content-Range', 'bytes 0-288955/2521832')
      .set('Access-Control-Allow-Headers', 'Range')
      .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
