/* eslint-disable linebreak-style */
import request from 'superagent';
import { getApiBaseUrl } from '../app/helpers';

export async function searchTalent() {
  return new Promise((resolve, reject) => {
    request.get(`${getApiBaseUrl()}/api/Home/AllTalent`).end((err, res) => {
      if (err) {
        reject(err);
        return false;
      }
      return resolve(res.body);
    });
  });
}

export async function searchTalentCategoryById(params) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/home/index`)
      .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
