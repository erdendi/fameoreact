import request from 'superagent';
import { getApiBaseUrl } from '../app/helpers';

export async function searchVideo(params) {
  console.log(params, 'param apis');
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Talent/ListTalentVideo`)
      .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
