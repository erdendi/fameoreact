import request from 'superagent';
import { getApiBaseUrl } from '../app/helpers';

export async function getQuestion(params) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/QA/GetAllQuestion`)
      .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
