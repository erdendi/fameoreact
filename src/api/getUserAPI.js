import request from 'superagent';
import { getApiBaseUrl } from '../app/helpers';

export async function getUser(params) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Account/Profile`)
      .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
