   
import request from 'superagent';
import { getApiBaseUrl } from '../helpers';

export async function getOrderList(params) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Account/WorkSpace`)
      .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function getFormOrder(query) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Booking/Hire`)
      .query(query)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function CancelledBooking(body) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Booking/CancelledBooking`)
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function postFormOrder(body) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Booking/PostHire`)
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function getConfirmation(params) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Booking/DataConfirm`)
      .send(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body.result);
      });
  });
}

export async function getCategory() {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Booking/getcategory`)
      // .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function postVoucher(body) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/booking/PostVoucherCode`)
      .query(body)
      // .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function VerifyVoucher(body) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/booking/VerifyVoucherCode`)
      .query(body)
      // .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function postUpdateCompletedBooking(body) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/booking/UpdatCompletedBookingStatus`)
      // .query(body)
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function DeclineOrderBook(body) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/booking/DeclineOrderBook`)
      // .query(body)
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function postUpdatePayment(body) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/booking/UpdateSuccessPaymentBooking`)
      // .query(body)
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function postUpdateBookingAmount(body) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/booking/UpdateBookingAmount`)
      // .query(body)
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function getVoucerlist(body) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Booking/GetVoucherCodeByTalent`)
      .query(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
