   
import request from 'superagent';
import { getApiBaseUrl } from '../helpers';

export async function postHelp(body) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Account/Support`)
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
