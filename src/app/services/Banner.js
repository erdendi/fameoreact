import request from 'superagent';
import { getApiBaseUrl } from '../helpers';

export async function GetBannerData(params) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Banner/GetBanner`)
      .send(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
