import request from 'superagent';
import { getApiBaseUrl } from '../helpers';

export async function getOrderList(params) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Account/WorkSpace`)
      .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
