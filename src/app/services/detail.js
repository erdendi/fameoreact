import request from 'superagent';
import { getApiBaseUrl } from '../helpers';

export async function getDetailTalent(params) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Talent/TalentDetail`)
      .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function postLike(params) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Booking/AddToWishlist`)
      .send(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
export async function postUnlike(params) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Booking/RemoveFromWishlist`)
      .send(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function getUser(params) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Account/Profile`)
      .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
