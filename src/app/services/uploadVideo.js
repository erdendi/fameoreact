import request from 'superagent';
import { getApiBaseUrl } from '../helpers';

export async function UploadVideoFiles(params) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Files/UploadVideoFiles`)
      // .set('Content-Type', 'multipart/form-data')
      // .type('form')
      // .attach('Files', path)
      .send(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function UploadMaterialFileByBookId(params) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Booking/UpdateBookingMaterialVideo`)
      // .set('Content-Type', 'multipart/form-data')
      // .type('form')
      // .attach('Files', path)
      .send(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function UploadMaterialReactionByBookId(params) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Booking/UpdateBookingReactionVideo`)
      // .set('Content-Type', 'multipart/form-data')
      // .type('form')
      // .attach('Files', path)
      .send(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function UpdateMaterialSent(params) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Booking/UpdateMaterialSent`)
      // .set('Content-Type', 'multipart/form-data')
      .send(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
