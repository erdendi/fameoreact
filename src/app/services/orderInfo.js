import request from 'superagent';
import { getApiBaseUrl } from '../helpers';

export async function getOrderInfo(params) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Booking/OrderDetail`)
      .send(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body.result);
      });
  });
}
