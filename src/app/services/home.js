   
import request from 'superagent';
import { getApiBaseUrl } from '../helpers';

export async function getDataHome(body) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/home/index`)
      // .set(customRequestHeader())
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        //   if (res.body.status !== 2000) {
        //     reject(res.body.message);
        //     return;
        //   }
        return resolve(res.body);
      });
  });
}

export async function GetBannerData(params) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Banner/GetBanner`)
      .send(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function getTalentCategory(body) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/home/GetTalentCategory`)
      // .set(customRequestHeader())
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function getAllQuestion(body) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/QA/GetAllQuestion?answered=false`)
      // .set(customRequestHeader())
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function getCategoryByID(params) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/home/index`)
      .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
