/* eslint-disable jsx-a11y/aria-props */
import request from 'superagent';
import { getApiBaseUrl } from '../helpers';
const ProgressPromise = require('progress-promise');

export async function getEditProfile(params) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Account/GetEditProfile/${params}`)
      // .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function getBank() {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Account/GetBank`)
      // .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export function postEditProfile(params) {
  return new ProgressPromise((resolve, reject, progress) => {
    request
      .post(`${getApiBaseUrl()}/api/Account/PostEditProfile`)
      .send(params)
      .on('progress', function (e) {
        progress(Number(e.percent));
      })
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
export async function postChangePassword(params) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Account/ChangePassword`)
      // .set('Content-Type', 'application/json')
      .send(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
