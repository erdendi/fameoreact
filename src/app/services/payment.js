import request from 'superagent';
import { getApiBaseUrl } from '../helpers';

export async function postPaymentOvo(body) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Payment/PayFasPay`)
      // .query(body)
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function postPaymentVA(body) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Payment/PayFasPay`)
      // .query(body)
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}

export async function postPaymentGopay(body) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Booking/GetMidTransToken`)
      // .query(body)
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
