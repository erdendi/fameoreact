import request from 'superagent';
import { getApiBaseUrl } from '../helpers';

export async function postWithdraw(body) {
  return new Promise((resolve, reject) => {
    request
      .post(`${getApiBaseUrl()}/api/Account/PostClaim`)
      .send(body)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
export async function getDetail(params) {
  return new Promise((resolve, reject) => {
    request
      .get(`${getApiBaseUrl()}/api/Account/Profile`)
      .query(params)
      .end((err, res) => {
        if (err) {
          reject(err);
          return false;
        }
        return resolve(res.body);
      });
  });
}
