/* eslint-disable no-dupe-keys */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable func-names */
/* eslint-disable jsx-a11y/no-static-element-interactions */

/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import Header from '../components/Header';
import SingleButton from '../components/Button';
import Back from '../assets/images/icon-back.svg';
import History from '../assets/images/history-coin.png';
import Coinx from '../assets/images/coinxx.svg';

const MyCoin = () => {
  document.getElementById('id-header-right').addEventListener('click', () => {
    window.location.replace('/#/history-coin');
  });

  return (
    <div className="coin-wrapper">
      <Header
        content={{
          title: 'Koin Kamu',
          BtnLeftIcon: Back,
          BtnRightIcon: History,
        }}
      />
      <div className="box-coin">
        <div className="section-1">Total Koin</div>
        <div className="section-2">
          <img src={Coinx} style={{ verticalAlign: 'sub' }} alt="coin" /> 2000
        </div>
      </div>
      <div className="after-heading-wrapper">
        <div className="container">
          <div className="box-text">
            <h3>Tentang Koin Fameo</h3>
            <p>
              Nulla maximus pharetra interdum. Ut in justo libero. Sed feugiat,
              sem ut interdum pharetra, sem ante condimentum magna, vel
              convallis diam dolor venenatis dui. Ut finibus et dolor bibendum
              varius.
            </p>
            <a
              onClick={function () {
                window.location.href = '/fameoapp/#/about-fameo-coin';
              }}
            >
              Selengkapnya
            </a>
          </div>
          <div className="box-text">
            <h3>Cara mendapatkan Koin Fameo</h3>
            <p>
              Nulla maximus pharetra interdum. Ut in justo libero. Sed feugiat,
              sem ut interdum pharetra, sem ante condimentum magna, vel
              convallis diam dolor venenatis dui. Ut finibus et dolor bibendum
              varius.
            </p>
            <a
              onClick={function () {
                window.location.href = '/fameoapp/#/about-fameo-coin';
              }}
            >
              Selengkapnya
            </a>
          </div>
          <div className="update-form-wrapper">
            <SingleButton
              content={{
                url: '#',
                text: 'BELI KOIN FAMEO',
                color: 'pink',
                url: '/detail-store',
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyCoin;
