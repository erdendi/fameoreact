import React, { useState, useEffect } from 'react';
import { Link, useParams, useHistory } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, Button } from 'reactstrap';
import { SocialIcon } from 'react-social-icons';
import SweetAlert from 'react-bootstrap-sweetalert';
import PlayButton from '../assets/images/play.svg';
import Heart from '../assets/images/heart.png';
import Share from '../assets/images/share.png';
import Question from '../assets/images/icon-question-msg.svg';
import LeftArrow from '../assets/images/icon-left-arrow.svg';
import VideoA from '../assets/images/detail/left_arrow_video.png';
import { LikesVideo } from '../services/video';
import VideoC from '../assets/images/detail/close.png';
import VideoD from '../assets/images/video/share.png';
import VideoE from '../assets/images/video/chat.png';
import VideoG from '../assets/images/video/nice.png';
import VideoH from '../assets/images/video/charz.svg';
import VideoI from '../assets/images/video/origami.png';
import VideoCardSwiper from '../components/VideoCardSwiper';
import TaskBar from '../components/TaskBar';
import SwiperQuestions from './FanQuestions';
import { getDetailTalent, postLike } from '../services/detail';
import Loading from '../components/Loading';
import { getCookie } from '../helpers';
import { formatPrice } from '../../utils/price';
// import { VideoPlayer } from '../components/VideoPlayer/Loadable';
import { Player } from '../components/Player';
import 'videojs-plus/dist/plugins/unload';

const contentSwiperData = {
  headingData: {
    heading: 'Video Terbaru',
    headingColor: 'white',
    linkName: 'Lihat Semua',
    url: '/',
  },
};

const contentSwiperData2 = {
  headingData: {
    heading: '',
    headingColor: 'white',
    linkName: 'Lihat Semua',
    url: '/',
  },
};

const playerOptions = {};

const Detail = () => {
  const history = useHistory();
  const [talents, setTalents] = useState([]);
  const [userId] = useState(parseInt(getCookie('UserId'), 10));
  const [IsNotUserTalent, setIsNotUserTalent] = useState(false);
  const [notif, setnotif] = useState(false);
  const [modal, setModal] = useState(false);
  const [modalShare, setModalShare] = useState(false);
  const [modalShareVid, setModalShareVid] = useState(false);
  const [notifUnavailable, setnotifUnavailable] = useState(false);
  // const [IsUserTalent, setIsUserTalent] = useState(false);
  // const [totalLike, setTotalLike] = useState();

  const [player, setPlayer] = useState(null);
  const [video, setVideo] = useState(null);

  // useEffect(() => {
  //   if (player) {
  //     player.unload({ loading: true });
  //     getTalents();
  //   }
  // }, [player]);

  useEffect(() => {
    if (player && video) {
      player.src(video.sources);
    }
  }, [video, player]);

  const toggle = () => {
    setModal(!modal);
  };
  const toggleShare = () => {
    setModalShare(!modalShare);
  };

  const toggleShareVid = () => {
    setModalShareVid(!modalShareVid);
  };
  const handlePopup = () => {
    setnotif(true);
  };

  const handleClick = () => {
    if (!userId) {
      setnotif(true);
    } else if (
      !isNaN(userId) &&
      talents.IsAvailable &&
      talents.IsTalentAcceptOrder === true
    ) {
      history.push(`/order-personal/${talentId}`);
    } else if (
      (!isNaN(userId) && !talents.IsAvailable) ||
      !talents.IsTalentAcceptOrder ||
      (!isNaN(userId) && !talents.IsAvailable && !talents.IsTalentAcceptOrder)
    ) {
      setnotifUnavailable(true);
    }
  };

  const askbtn = e => {
    history.push(`/asktalent/${e}`);

    // di Comment Sementara sampe fitur talent answer video udh jadi
    // if(show == false)
    // {
    //   setShow(true);
    // }
    // else
    // {
    //   setShow(false);
    // }
  };

  const LikesVideosModel = e => {
    if (isNaN(userId)) {
      setnotif(true);
    } else {
      const params = {
        UserId: userId,
        FileId: e,
      };
      LikesVideo(params).then(resp => {
        if (resp.Status === 'OK') {
          document.getElementById('TotalLikes').innerHTML = resp.CountLikes;
        }
      });
    }
  };

  function myFunction() {
    const element = document.getElementById('taskbar');
    if (element.getAttribute('class') === 'taskbar w-100') {
      element.classList.add('hidden');
    } else {
      element.classList.remove('hidden');
      // stopvideo();
    }
  }

  function askbtnshow() {
    const element = document.getElementById('custom-ask-on-video');
    if (element.getAttribute('class') === 'custom-ask-on-video-hidden') {
      element.classList.add('show');
    } else {
      element.classList.remove('show');
    }
  }

  const { talentId } = useParams();

  const getTalents = async () => {
    const params = {
      TalentId: talentId,
    };
    getDetailTalent(params).then(response => {
      const { result } = response;
      setTalents(result);

      if (result.UserId !== userId) {
        setIsNotUserTalent(true);
      }

      const videoObj = {
        title: result.TalentNm,
        // poster:
        //   'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg',
        sources: [
          {
            src: result.VideoIntro,
            type: 'video/mp4',
          },
        ],
      };

      setVideo(videoObj);
      // alert(resp.UserId)
    });
  };

  const handleLike = () => {
    const data = {
      TalentId: talentId,
      UserId: userId,
    };
    postLike(data).then(resp => {
      if (resp.Status === 'OK') {
        document.getElementById('FavouriteCounts').innerHTML =
          resp.FavouriteCount;
      }
    });
  };

  useEffect(() => {
    window.scroll(0, 0);
    getTalents();
    if (player) {
      player.unload({ loading: true });
      // getTalents();
    }
  }, [player]);
  return (
    <>
      <div className="detail-wrapper">
        {talents.Status === 0 ? (
          <div Style="height: 100vh">
            <div className="back-wrapper">
              <Button className="back-wrapper" onClick={() => history.goBack()}>
                <img src={LeftArrow} className="back-arrow" alt="fameo-back" />
              </Button>
            </div>
            <div
              Style="display: flex;
              flex-direction: column;
              justify-content: center;
              align-items: center;
              text-align: center;
              min-height: 100vh;"
            >
              <div className="c-white">{talents.Message}</div>
            </div>
          </div>
        ) : (
          <>
            <div className="container px-0">
              <div
                onClick={() => history.goBack()}
                className="idol-banner"
                aria-hidden="true"
              >
                {!talents.LinkImg && <Loading />}
                <img src={LeftArrow} className="back-arrow" alt="fameo-back" />
                {talents.LinkImg && (
                  <img src={talents.LinkImg} className="idol-image" alt="." />
                )}
              </div>
            </div>

            <div className="container">
              <div className="action-list">
                <div className="heart-wrapper">
                  {/* <input id="heartbox" type="checkbox" /> */}
                  <Button
                    style={{
                      border: 0,
                      backgroundColor: 'transparent',
                      padding: 0,
                    }}
                    htmlFor="heart"
                    onClick={!userId ? handlePopup : handleLike}
                  >
                    <img
                      src={Heart}
                      className="action-icon heart"
                      alt="fameo-heart"
                    />
                  </Button>
                </div>
                <button
                  type="button"
                  className="btn-round-white"
                  onClick={() => {
                    toggle();
                    myFunction();
                  }}
                >
                  <img src={PlayButton} alt="fameo-play" />
                </button>
                {/* <Link to="/"> */}
                <img
                  onClick={toggleShare}
                  src={Share}
                  className="action-icon share"
                  alt="fameo-share"
                  aria-hidden="true"
                />

                <Modal
                  isOpen={modalShare}
                  toggle={toggleShare}
                  contentClassName="modal-share"
                >
                  <ModalHeader toggle={toggleShare} charCode="X">
                    Share talent on
                  </ModalHeader>
                  <ModalBody>
                    <div className="modal-body-container">
                      <div className="modal-icon">
                        <SocialIcon
                          url={`https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.fameoapp.com%2Fdetail%2F${talentId}`}
                          target="_blank"
                        />
                      </div>
                      <div className="modal-icon">
                        <SocialIcon
                          network="whatsapp"
                          url={`https://wa.me/?text=https%3A%2F%2Fwww.fameoapp.com%2Fdetail%2F${talentId}`}
                          target="_blank"
                        />
                      </div>
                    </div>
                  </ModalBody>
                </Modal>
              </div>
              <div className="count-wrapper-likes">
                {/* <span Style={{marginLeft: 50}} className="like-counter c-petal-off">&emsp;{totalLike.FavoriteCount}</span> */}
                <label htmlFor="heart" id="FavouriteCounts">
                  {talents.FavoriteCount}
                </label>
              </div>
              <h2 className="idol-name text-center text-white">
                {talents.TalentNm}
              </h2>
              <div className="row idol-category">
                <div className="badge-list mx-auto text-white">
                  <span className="badge badge-one">
                    {talents.CategoryName}
                  </span>
                  <span className="badge badge-two">{talents.Profesion}</span>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="idol-message">
                <p className="detail-paragraph heading text-white">
                  {talents.Bio}
                </p>
                <div className="row pills-section">
                  <div className="pills-wrapper mx-auto">
                    {IsNotUserTalent && (
                      <div>
                        <Link
                          to={`/AskTalent/${talentId}`}
                          className="pill pill-purple"
                        >
                          <p className="pill-text">Ask</p>
                          <img
                            src={Question}
                            className="question-tick"
                            alt="question"
                          />
                          {/* <p className="pill-text font-weight-bold">{talents.FirstName}</p> */}
                        </Link>
                        <Link
                          onClick={handleClick}
                          className="pill pill-pink 1"
                        >
                          {talents.SalePrice > 0 ? (
                            <>
                              {/* <p className="pill-text"><span class="diskon">{"Rp " + talents.PriceAmount}</span></p> */}
                              <p className="pill-text">
                                {formatPrice(talents.SalePrice)}
                                <span
                                  style={{
                                    textDecoration: 'line-through',
                                    color: '#707070',
                                    marginLeft: '8px',
                                  }}
                                >
                                  {formatPrice(talents.PriceAmount, '')}
                                </span>
                              </p>
                            </>
                          ) : (
                            <p className="pill-text">
                              Order {formatPrice(talents.PriceAmount)}
                            </p>
                          )}
                        </Link>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>

            <div className="section-live mt-4">
              {!!talents.ListTalentVideo && talents.ListTalentVideo.length > 0 && (
                <>
                  {/* <Heading content={contentSwiperData.headingData} userId={talentId}/> */}
                  <div className="section-heading">
                    <div className="d-flex justify-content-between">
                      <p className="section-title section-title-white">
                        Video Terbaru
                      </p>
                      <Link
                        // to={{pathname: `/search`, talent: talents.TalentNm}}
                        to={`/search?talentId=${talents.Id}&talentname=${talents.TalentNm}&Category=Ucapan`}
                        className="see-all-link"
                      >
                        Lihat Semua
                      </Link>
                    </div>
                  </div>
                  <VideoCardSwiper
                    contentSwiper={contentSwiperData}
                    head
                    contents={talents.ListTalentVideo}
                    content2={talents}
                  />
                </>
              )}
              {!talents.ListTalentVideo && <Loading />}
            </div>

            <div className="section-live mt-5">
              {!!talents.ReactionVideoList &&
                talents.ReactionVideoList.length > 0 && (
                  <>
                    <div className="section-heading">
                      <div className="d-flex justify-content-between">
                        <p className="section-title section-title-white">
                          Video Reaksi
                        </p>
                        <Link
                          // to={{pathname: `/search`, talent: talents.TalentNm}}
                          to={`/search?talentId=${
                            talents.Id
                          }&talentname=${talents.TalentNm.replace(
                            ' ',
                            '-',
                          )}&Category=Reaksi`}
                          className="see-all-link"
                        >
                          Lihat Semua
                        </Link>
                      </div>
                    </div>

                    <VideoCardSwiper
                      contentSwiper={contentSwiperData2}
                      contents={talents.ReactionVideoList}
                      content2={talents}
                    />
                  </>
                )}
              {!talents.ListTalentVideo && <Loading />}
            </div>
            {/* <div className="section-live mt-5">
        <VideoCardSwiper contentSwiper={contentSwiperData2} />
      </div> */}
            {!!talents.QuestionList && !!talents.QuestionList.length > 0 && (
              <div className="section-questions mt-5">
                {/* <Heading content={questionHeadingData.headingData} /> */}
                <SwiperQuestions content={talents.QuestionList} />
              </div>
            )}

            <Modal isOpen={modal} toggle={toggle} id="ModalVideoPlayer">
              <ModalBody>
                <div className="box-video--">
                  <div className="row">
                    <div className="col">
                      <div
                        className="btn-close--"
                        aria-hidden="true"
                        onClick={() => {
                          toggle();
                          myFunction();
                        }}
                      >
                        <img src={VideoA} alt="" />
                      </div>
                    </div>
                    <div className="col-8">
                      <div className="media">
                        <img
                          style={{ marginLeft: '15px' }}
                          src={talents.LinkImg}
                          className="align-self-center mr-3"
                          alt=""
                        />
                        <div
                          className="media-body"
                          style={{ marginTop: '8px' }}
                        >
                          <h4>{talents.TalentNm}</h4>
                          {talents.ListTalentVideo &&
                            talents.ListTalentVideo.map(item => (
                              <p key={item.id}>{item.CustomerName}</p>
                            ))}
                        </div>
                      </div>
                    </div>
                    <div className="col">
                      <div
                        aria-hidden="true"
                        onClick={() => {
                          toggle();
                          myFunction();
                        }}
                      >
                        <img className="vid-logo-det" src={VideoC} alt="" />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="wrapper_video">
                  {/* <VideoPlayer source={talents.VideoIntro} /> */}
                  <Player
                    playerOptions={playerOptions}
                    onPlayerInit={setPlayer}
                    onPlayerDispose={setPlayer}
                  />
                  {/* <video
                    width="100%"
                    height="100%"
                    src={talents.VideoIntro}
                    className="video_video"
                    id="video"
                    type="video/mp4"
                    onClick={stopvideo}
                  >
                    <track kind="captions" />
                  </video>
                  <div
                    className="playpause_video"
                    id="playpause_video_id"
                    onClick={playvideo}
                    aria-hidden="true"
                  /> */}
                </div>
                <div className="box-video--bottom">
                  <div className="row">
                    <div className="col-2 col-md-2">
                      <div className="btn-bottom-icon">
                        <img
                          src={VideoD}
                          alt="share"
                          onClick={toggleShareVid}
                          aria-hidden="true"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-2 col-md-2">
                      <div
                        onClick={() => askbtn(talents.Id)}
                        className="btn-bottom-icon"
                        id="ask-btn-show"
                        aria-hidden="true"
                      >
                        <img src={VideoE} style={{ padding: '6px' }} alt="" />
                      </div>
                    </div>
                  </div>
                  {/* <div className="row">
                    <div className="col-2 col-md-2">
                      <div className="btn-bottom-icon">
                        <img src={VideoF} alt="" />
                      </div>
                    </div>
                  </div> */}
                  <div className="row">
                    <div className="col-2 col-md-2">
                      <div
                        onClick={() => LikesVideosModel(talents.VideoIntroId)}
                        className="btn-bottom-icon"
                        aria-hidden="true"
                      >
                        <img className="m-0" src={VideoG} alt="" />
                        <div style={{ color: 'white' }}>
                          <label id="TotalLikes">
                            {talents.VideoIntroTotalLikes}
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* <div id="custom-seekbar">
                  <span id="custom-seekbar-span" />
                  <div id="custom-seekbar-div" />
                </div> */}
                <div
                  id="custom-ask-on-video"
                  className="custom-ask-on-video-hidden"
                >
                  <div className="col-12 col-md-12">
                    <div
                      className="line----"
                      onClick={askbtnshow}
                      aria-hidden="true"
                    />
                    <p className="text-center">
                      <img className="btn-chat----" src={VideoH} alt="" />{' '}
                      Pertanyaan untuk
                      <b>Michelle Ziudith</b>
                    </p>
                    <input
                      type="text"
                      className
                      placeholder="Ajukan pertanyaan Kamu"
                    />
                    <div href="#">
                      <img src={VideoI} alt="" />
                    </div>
                  </div>
                </div>
              </ModalBody>
            </Modal>

            <TaskBar />
          </>
        )}
      </div>

      <Modal
        isOpen={modalShareVid}
        toggle={toggleShareVid}
        contentClassName="modal-share"
      >
        <ModalHeader toggle={toggleShareVid} charCode="X">
          Share Video on
        </ModalHeader>
        <ModalBody>
          <div className="modal-body-container">
            <div className="modal-icon">
              <SocialIcon
                url={`https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.fameoapp.com%2Fvideo%2F${talents.VideoIntroId}`}
              />
            </div>
            <div className="modal-icon">
              <SocialIcon
                network="whatsapp"
                url={`https://wa.me/?text=https%3A%2F%2Fwww.fameoapp.com%2Fvideo%2F${talents.VideoIntroId}`}
              />
            </div>
          </div>
        </ModalBody>
      </Modal>

      <SweetAlert
        warning
        show={notif}
        confirmBtnText="Login"
        title=""
        onConfirm={() => {
          setnotif(false);
          window.location.href = '/signin';
        }}
        onCancel={() => {
          setnotif(false);
        }}
        onEscapeKey={() => {
          setnotif(false);
          // window.location.href = `/detail/${talentId}`;
        }}
        onOutsideClick={() => {
          setnotif(false);
          // window.location.href = `/detail/${talentId}`;
        }}
      >
        <p style={{ fontSize: '13pt' }}>Silahkan login terlebih dahulu</p>
      </SweetAlert>

      <SweetAlert
        warning
        show={notifUnavailable}
        confirmBtnText="OK"
        title=""
        onConfirm={() => {
          setnotifUnavailable(false);
        }}
        onCancel={() => {
          setnotifUnavailable(false);
        }}
        onEscapeKey={() => {
          setnotifUnavailable(false);
          // window.location.href = `/detail/${talentId}`;
        }}
        onOutsideClick={() => {
          setnotifUnavailable(false);
          // window.location.href = `/detail/${talentId}`;
        }}
      >
        <p style={{ fontSize: '13pt' }}>
          Mohon maaf {talents.TalentNm} saat ini tidak bisa menerima order
        </p>
      </SweetAlert>
    </>
  );
};

export default Detail;
