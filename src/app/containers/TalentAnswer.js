/* eslint-disable jsx-a11y/alt-text */

import React from 'react';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import { Link } from 'react-router-dom';

import Header from '../components/Header';

import Back from '../assets/images/icon-left-arrow-two.svg';
import download from '../assets/images/search/download.svg';
import videoz from '../assets/images/search/video.svg';
import Settings from '../assets/images/icon-more.svg';

const TalentAnswer = () => (
  <div className="talentanswer-wrapper">
    <Header
      content={{
        title: 'Talent Answer',
        BtnLeftIcon: Back,
        BtnRightIcon: Settings,
        hasSubtitle: true,
        subtitle: 'No. Order Saya ORD-2019000184',
      }}
    />
    <div className="after-heading-wrapper">
      <div className="container">
        <div className="update-form-wrapper">
          <p className="beri-jawaban">
            Beri jawaban kamu untuk pertanyaan dari fans.
          </p>
          <Form>
            <FormGroup>
              <Label className="title-textbox">Nama Fans</Label>
              <Input
                type="text"
                name="nama-fans"
                className="textbox"
                value="Wilda"
              />
            </FormGroup>
            <FormGroup>
              <Label className="title-textbox">Pertanyaan Fans</Label>
              <textarea name="pertanyaan-fans">
                Hi..Kak selama pandemi corona ngapain aja?
              </textarea>
            </FormGroup>
          </Form>
        </div>
        <div className="row">
          <div className="col-6">
            <Link to="/">
              <div className="box-btn-on-talent mt-3">
                <img src={download} />
              </div>
              <p className="box-btn-on-talent-2">UPLOAD FILE</p>
            </Link>
          </div>
          <div className="col-6">
            <Link to="/talent-answer-finish">
              <div className="box-btn-on-talent mt-3">
                <img src={videoz} />
              </div>
              <p className="box-btn-on-talent-2">REKAM VIDEO</p>
            </Link>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default TalentAnswer;
