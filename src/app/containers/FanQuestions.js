import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Heading from '../components/Heading';
import Nice from '../assets/images/icon-nice.svg';
import Chat from '../assets/images/icon-chat-purple.svg';

const contentSwiperData = {
  headingData: {
    heading: 'Pertanyaan Fans',
    headingColor: 'violet',
    // headingLetters: 'uppercase',
    linkName: 'Lihat Semua',
    url: '/search-qa',
  },
};
// const params = {
//   slidesPerView: 2.1,
//   slidesPerGroup: 2.1,
//   spaceBetween: 5,
//   rebuildOnUpdate: true,
// }

export const NoData = headingData => {
  return (
    <div>
      <Heading content={headingData} />
      <div
        Style="display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        text-align: center;
    "
      >
        <div style={{ color: '#707070' }}>(Belum ada pertanyaan) </div>
      </div>
    </div>
  );
};

function FanQuestions({ content }) {
  return (
    <div className="fan-questions">
      <Heading content={contentSwiperData.headingData} />
      {content.length === 0 ? (
        <NoData headingData={contentSwiperData.headingData} />
      ) : (
        <div className="d-wrap">
          {content.map((item, i) => {
            if (item.Question !== null) {
              return (
                <div key={i}>
                  <div className="slider-fan-question">
                    <div className="fan-data">
                      <img
                        src={item.UserProfPicLink}
                        className="fan-image"
                        alt="fameo-user-icon"
                      />
                      <h5 className="fan-name">{item.UserName.slice(0, 11)}</h5>
                    </div>
                    <div className="fan-data">
                      <h5 className="fan-name">
                        To.
                        <Link
                          to={`/detail/${item.TalentId}`}
                          className="read-more-text"
                        >
                          {item.TalentName.split(' ').slice(0, -1).join(' ')}
                        </Link>
                      </h5>
                    </div>
                    <p className="fan-message">
                      {`"${item.Question.slice(0, 35)} ... "`}
                      <br />
                      <Link
                        to={`/details-qa/${item.QuestionId}`}
                        className="read-more-text"
                      >
                        Read more
                      </Link>
                    </p>
                    <Link
                      to={`/details-qa/${item.QuestionId}`}
                      className="read-more-text"
                    >
                      <div className="message-action-list">
                        <button className="btn-vote text-center">
                          <div className="vote-wrapper">
                            <img
                              src={Nice}
                              className="fan-nice"
                              alt="fameo-nice"
                            />
                            <span className="fan-votes text-center">
                              {item.TotalLike} vote
                            </span>
                          </div>
                        </button>
                        <img src={Chat} className="fan-chat" alt="fameo-chat" />
                      </div>
                    </Link>
                  </div>
                </div>
              );
            }
          })}
        </div>
      )}
    </div>
  );
}

FanQuestions.propTypes = {
  content: PropTypes.array.isRequired,
};

export default React.memo(FanQuestions);
