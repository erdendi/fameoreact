import React, { useState, useEffect } from 'react';
import { Form, FormGroup, Input } from 'reactstrap';
import { Helmet } from 'react-helmet-async';
import { getQuestion } from '../services/searchQA';
import Tabs from './Tabs';
import Header from '../components/Header';
import Heading from '../components/Heading';
import TaskBar from '../components/TaskBar';
import Sort from '../assets/images/icon-sort.svg';

const FansQeustions = {
  headingData: {
    heading: 'Pertanyaan Fans',
    headingColor: 'white',
  },
};

const tabContent = {
  data: [
    {
      id: 1,
      tabTitle: 'Semua',
      orderList: false,
      fanQuestions: true,
    },
    {
      id: 2,
      tabTitle: 'Popular',
      orderList: false,
      fanQuestions: false,
    },
    {
      id: 3,
      tabTitle: 'Film & TV',
      orderList: true,
      fanQuestions: false,
    },

    {
      id: 4,
      tabTitle: 'Athlete',
      orderList: true,
      fanQuestions: false,
    },
    {
      id: 5,
      tabTitle: 'Gamer',
      orderList: true,
      fanQuestions: false,
    },
  ],
};

const SearchQA = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [isLoading, setIsLoading] = useState(true);
  const [limitGet, setLimitGet] = useState(20);

  const handleChange = event => {
    setSearchTerm(event.target.value);
    const params = {
      type: 1,
      talentname: event.target.value,
      limitvideo: 20,
    };
    fectData(params);
  };

  const fectData = params => {
    getQuestion(params)
      .then(resp => {
        setSearchResults(resp);
        setIsLoading(false);
      })
      .catch(err => {
        // setError(true);
        console.log(err);
      });
  };

  const getsearchQA = () => {
    const params = {
      limitall: limitGet,
    };
    fectData(params);
  };
  // eslint-disable-next-line no-unused-vars
  const load = () => {
    fectData({ limitall: limitGet + 10 });
    setLimitGet(limitGet + 10);
  };
  useEffect(() => {
    window.scroll(0, 0);
    getsearchQA();
  }, []);

  const [activeTab, setActiveTab] = useState('1');

  // eslint-disable-next-line no-unused-vars
  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  return (
    <>
      <Helmet>
        <title>Search Question - Fameo</title>
        <meta name="description" content="Reset Password" />
      </Helmet>
      <div className="search-qa-wrapper">
        <Header content={{ title: 'TANYA IDOLA' }} />
        <div className="after-heading-wrapper">
          <div className="section-gift mt-4">
            {/* <CardSwiper contentSwiper={contentSwiperData} /> */}
          </div>
          <div className="mt-4">
            <Heading content={FansQeustions.headingData} />
            <div className="container">
              <div className="search-wrapper mb-4">
                <Form className="search-bar">
                  <FormGroup>
                    <Input
                      type="search"
                      name="search"
                      value={searchTerm}
                      onChange={handleChange}
                      id="exampleSearch"
                      placeholder="Cari berdasarkan nama talent"
                    />
                    <span className="search-icon" />
                  </FormGroup>
                </Form>
                <img src={Sort} alt="fameo-sort" />
              </div>
              <Tabs
                items={tabContent}
                question={searchResults.getCurrentData}
              />
            </div>
          </div>
        </div>
        <TaskBar active="tanyajawab" />
      </div>
    </>
  );
};

export default SearchQA;
