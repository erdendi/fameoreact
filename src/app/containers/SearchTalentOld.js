/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import { Form, FormGroup, Input } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';

// Components
import Header from '../components/Header';
import TaskBar from '../components/TaskBar';

// import Sort from '../assets/images/icon-sort.svg'
// import Imgcard from '../assets/images/image_14.png'
import { searchTalent } from '../services/search-talent';
import { getCategoryByID } from '../services/home';
import Loading from '../components/Loading';
import NormalCard from '../components/NormalCard';
// import EntertainmentTabs2 from '../components/EntertainmentTabs2'

// const entertainmentContent = {
//   data: [
//     {
//       id: 52,
//       tabTitle: 'Popular',
//       icon: require('../assets/images/icon-popular.svg'),
//       iconName: 'icon popular',
//     },
//     {
//       id: 11,
//       tabTitle: 'Film & TV',
//       icon: require('../assets/images/icon-film.svg'),
//       iconName: 'icon film',
//     },
//     {
//       id: 51,
//       tabTitle: 'Athlete',
//       icon: require('../assets/images/basketball.svg'),
//       iconName: 'icon basketball',
//     },
//     {
//       id: 22,
//       tabTitle: 'Musicianas',
//       icon: require('../assets/images/icon-sing.svg'),
//       iconName: 'icon musician',
//     },
//     {
//       id: 2,
//       tabTitle: 'Influencer',
//       icon: require('../assets/images/icon-tv.svg'),
//       iconName: 'icon influencer',
//     },
//     {
//       id: 6,
//       tabTitle: 'Gamer',
//       icon: require('../assets/images/icon-joystick.svg'),
//       iconName: 'icon joystick',
//     },
//   ],
//   card: false
// };

const SearchTalent = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [content] = useState([]);
  // const [entertainmentContent_, setEntertainmentContent_] = useState(entertainmentContent);
  // const [loading, setLoading] = useState(false);
  // const [error, setError] = useState(false);
  // const [childLoading, setChildLoading] = useState(true);

  const handleChange = e => {
    if (e.key === 'Enter') {
      fetchData();
    } else {
      setSearchTerm(e.target.value);
      fetchData();
    }

    // let params = {
    //   name: event.target.value,
    // }
    // fetchData(params)
  };

  const doSearch = () => {
    fetchData();
  };
  const fetchData = () => {
    // alert(searchTerm)

    const params = {};
    if (searchTerm !== '') {
      params.talentname = searchTerm;
    }
    searchTalent(params)
      .then(resp => {
        setSearchResults(resp);
        setIsLoading(false);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const getByCategory = async params => {
    await getCategoryByID(params)
      .then(resp => {
        // setContent(resp.ListTalentCategoryModel);
        setSearchResults(resp);
        setIsLoading(false);
      })
      .catch(err => {
        // setError(true);
        console.log(err);
      });
  };

  useEffect(() => {
    // window.scroll(0, 0);

    const urlParams = new URLSearchParams(window.location.search);
    const searchParam = urlParams.get('search');
    const idParam = urlParams.get('id');

    if (searchParam !== '' && searchParam != null) {
      setSearchTerm(searchParam);
      const params = {};

      params.talentname = searchParam;

      searchTalent(params)
        .then(resp => {
          setSearchResults(resp);
          setIsLoading(false);
        })
        .catch(err => {
          console.log(err);
        });
    } else if (idParam !== '' && idParam != null) {
      toggle(idParam);
    } else {
      fetchData();
    }
  }, []);

  const [activeTab, setActiveTab] = useState('0');
  const toggle = id => {
    setIsLoading(true);
    if (activeTab !== Number(id)) setActiveTab(Number(id));
    // Toastnotif(tab);

    if (id === 0) {
      fetchData();
    } else {
      const params = {
        categoryid: id,
        // limit: 10
      };
      if (searchTerm !== '') {
        params.talentname = searchTerm;
      }
      getByCategory(params);
    }
  };

  const tabData = [
    {
      id: 0,
      name: 'semua',
    },
    {
      id: 61,
      name: 'popular',
    },
    {
      id: 11,
      name: 'film & TV',
    },
    {
      id: 51,
      name: 'athlete',
    },
    {
      id: 22,
      name: 'musician',
    },
    {
      id: 2,
      name: 'influencer',
    },
    {
      id: 52,
      name: 'new',
    },
    {
      id: 62,
      name: 'cosplayer',
    },
    {
      id: 63,
      name: 'youtuber',
    },
    {
      id: 64,
      name: 'tiktoker',
    },
    {
      id: 56,
      name: 'drag queen',
    },
    {
      id: 58,
      name: 'magician',
    },
  ];

  return (
    <>
      {/* <Header content={{ title: 'List Talent', BtnLeftIcon: Toggler, BtnRightIcon: Sort }} /> */}
      <Header content={{ title: 'TEMUKAN IDOLAMU' }} />
      <div className="search">
        <div className="container">
          <Form className="search-bar">
            <FormGroup>
              <Input
                type="search"
                name="search"
                id="exampleSearch"
                value={searchTerm}
                onChange={e => handleChange(e)}
                onKeyDown={e => handleChange(e)}
                placeholder="Cari berdasarkan nama talent"
              />
              <button
                type="button"
                className="search-icon"
                onKeyPress={() => doSearch()}
                onClick={() => doSearch()}
                aria-label="search"
              />
            </FormGroup>
          </Form>
          <div className="filter-holder">
            <ul className="nav nav-tabs">
              {tabData.map(item => (
                <li className="nav-item" key={item.id}>
                  <NavLink
                    className={classnames('nav-link', {
                      active: activeTab === item.id,
                    })}
                    onClick={() => {
                      toggle(item.id);
                    }}
                    to={{
                      pathname: '/search-talent',
                      search: `?id=${item.id}`,
                    }}
                    activeClassName="selectedLink"
                  >
                    {item.name}
                  </NavLink>
                </li>
              ))}
            </ul>
          </div>

          <div className="height-100">
            {isLoading && (
              <div className="height-100">
                <Loading />
              </div>
            )}
            {!!searchResults.ListTalentCategoryModel &&
              searchResults.ListTalentCategoryModel.map(item => (
                <div className="w-50 d-inline-block p-1" key={item.UserId}>
                  <NormalCard content={item} contentCategory={content} />
                </div>
              ))}
          </div>
          {/* <TabContent className="results-holder" activeTab={activeTab}>
          {isLoading && <div className="height-100"><Loading/></div>}
            <TabPane className="tab-content -m-1" tabId="1">
            {isLoading && <div className="height-100"><Loading /></div>}
            {!!searchResults.ListTalentCategoryModel &&
                searchResults.ListTalentCategoryModel.map((item, i) => (
                  <div className="w-50 d-inline-block p-1" key={i}>
                    <NormalCard content={item} contentCategory={content}/>
                  </div>
                ))
              }
            </TabPane>
            <TabPane className="tab-content -m-1" tabId="2">
            </TabPane>
            <TabPane className="tab-content -m-1" tabId="3"></TabPane>
            <TabPane className="tab-content -m-1" tabId="4"></TabPane>
            <TabPane className="tab-content -m-1" tabId="5"></TabPane>
            <TabPane className="tab-content -m-1" tabId="6"></TabPane>
            <TabPane className="tab-content -m-1" tabId="7"></TabPane>
            <TabPane className="tab-content -m-1" tabId="8"></TabPane>
          </TabContent> */}
        </div>
        <TaskBar active="search-talent" />
      </div>
    </>
  );
};

export default SearchTalent;
