/* eslint-disable react/no-array-index-key */

/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-shadow */
/* eslint-disable radix */

import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useDropzone } from 'react-dropzone';
import Header from '../components/Header';
// import { Input } from 'reactstrap'
import {
  UploadVideoFiles,
  UploadMaterialReactionByBookId,
} from '../services/uploadVideo';
import { getCookie } from '../helpers';
import SingleButton from '../components/Button';
import Back from '../assets/images/icon-left-arrow-two.svg';
import Settings from '../assets/images/icon-settings.svg';
import Upload from '../assets/images/upload-two.svg';
import LoadingFull from '../components/LoadingFull';

const ProfileUpload = () => {
  const [userID] = useState(Number(getCookie('UserId')));
  const { bookId } = useParams();
  const [loading, setLoading] = useState(false);
  // const [filenya, setFilenya] = useState();
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    accept: 'video/*',
  });

  const files = acceptedFiles.map((file, i) => (
    <li className="file-data" key={i}>
      {file.path} -{file.size} bytes
    </li>
  ));

  // const changeUploadVideo = (e) => {
  //   setFilenya(e.target.files[0])
  // }

  const uploadVideo = e => {
    e.preventDefault();
    setLoading(true);
    const formData = new FormData();
    // formData.append("Files", filenya);
    formData.append('Files', acceptedFiles[0]);
    formData.append('UserId', userID);

    UploadVideoFiles(formData).then(resp => {
      const r = {
        UserID: userID,
        BookId: parseInt(bookId),
        ReactionFileId: resp,
        Id: bookId,
      };
      UploadMaterialReactionByBookId(r).then(resp => {
        setLoading(false);
        if (resp.Status === 'OK') {
          // history.push('/order-video-review/resp.BookId')
          window.location.href = `/orderReview/${resp.BookId}`;
        }
      });
    });
  };

  useEffect(() => {
    window.scroll(0, 0);
  }, []);

  return (
    <>
      {loading && <LoadingFull />}
      <div className="file-upload-wrapper">
        <Header
          content={{
            title: 'Michelle Ziudith',
            BtnLeftIcon: Back,
            BtnRightIcon: Settings,
          }}
        />
        <div className="after-heading-wrapper">
          <div className="container order-container">
            <h2 className="section-title mb-3">Video Reaksi</h2>
            <div className="form-upload">
              <div {...getRootProps({ className: 'dropzone' })}>
                <input {...getInputProps()} />
                {/* <input onChange={(e)=> changeUploadVideo(e)} type="file" enctype="multipart/form-data" accept='video/*'/> */}
                <div className="dropzone-data">
                  <img
                    src={Upload}
                    className="fameo-upload"
                    alt="fameo-upload"
                  />
                  <p className="lbl-upload mb-2">Select file to upload</p>
                  <ul className="file-list">{files}</ul>
                </div>
              </div>
              <SingleButton
                onClick={e => uploadVideo(e)}
                content={{ text: 'UPLOAD VIDEO REAKSI', color: 'pink' }}
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProfileUpload;
