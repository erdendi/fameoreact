/* eslint-disable react/no-unused-state */
/* eslint-disable react/prop-types */

/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
// import { Link } from 'react-router-dom'
import { Row, Col } from 'reactstrap';
import { formatPrice } from 'utils/price';
import Header from '../components/Header';
import { getOrderInfo } from '../services/orderInfo';
import { postPaymentVA } from '../services/payment';
import Back from '../assets/images/icon-back.svg';
import Settings from '../assets/images/icon-more.svg';
import LoadingFull from '../components/LoadingFull';
import { getCookie } from '../helpers';

class OrderRincian extends React.Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      bankType: this.props.match.params.banktype,
      bookId: this.props.match.params.bookId,
      totalPay: '',
      orderNo: '',
      CustomerName: '',
      Email: '',
      PriceAmount: '',
      ProjectNm: '',
      BookedBy: Number(getCookie('UserId')),
      Potongan: '',
      loading: false,
    };
  }

  componentDidMount() {
    const params = {
      Id: this.props.match.params.bookId,
      UserId: this.state.BookedBy,
    };
    getOrderInfo(params).then(resp => {
      // setConfirm(resp);
      this.setState({
        orderNo: resp.OrderNo,
        totalPay: resp.TotalPay,
        BankSelected: this.props.match.params.banktype,
        CustomerName: resp.CustomerName,
        Email: resp.Email,
        PriceAmount:
          resp.PriceAmount == null ? resp.TotalPay : resp.PriceAmount,
        ProjectNm: resp.ProjectNm,
        BookedBy: resp.BookedBy,
        Potongan: resp.Potongan || 0,
      });
      // setLoading(false)
    });
  }

  onSubmit(e) {
    this.setState({ Loading: true });
    e.preventDefault();

    // setTimeout(() => {

    // }, 2);

    const params = {
      BankSelected: this.props.match.params.banktype,
      CustomerName: this.state.CustomerName,
      Email: this.state.Email,
      OrderNo: this.state.orderNo,
      TotalPay: this.state.totalPay,
      PriceAmount: this.state.PriceAmount,
      ProjectNm: this.state.ProjectNm,
      BookedBy: this.state.BookedBy,
      Potongan: this.state.Potongan,
      Id: this.props.match.params.bookId,
      UserId: this.state.BookedBy,
    };

    postPaymentVA(params).then(() => {
      this.setState({ Loading: false });
      this.props.history.push(
        `/order-rincian-how-payment/${this.props.match.params.bookId}`,
      );
    });
  }

  render() {
    // const [loading, setLoading] = useState(true);
    // alert(this.props.match.params.banktype);
    // rest of the code

    const bankType = this.props.match.params.banktype;
    // const { bookId } = this.props.match.params;

    //  const [confirm, setConfirm] = useState(false);

    // useEffect(() => {
    //   window.scroll(0, 0)
    //   let params = {
    //     BookId: bookId
    //   }
    //   getOrderInfo(params)
    //     .then((resp) => {
    //       //setConfirm(resp);
    //     //  setLoading(false)
    //     })
    // }, [])

    return (
      <>
        {this.state.Loading && <LoadingFull />}
        <Header
          content={{
            title: 'Pilih Metode Pembayaran',
            BtnLeftIcon: Back,
            BtnRightIcon: Settings,
            hasSubtitle: true,
            subtitle: 'No. Pesanan Fameo ORD-2019000184',
          }}
        />
        <div className="order order-confirmation">
          <div className="container">
            <form onSubmit={this.onSubmit}>
              <div className="content-wrapper">
                <section className="content-pembayaran">
                  <Row>
                    <Col xs="12">
                      {/* <div className="warning-selesaikan mb-4">
                    Selesaikan pembayaran dalam 09 mnt 11 dtk
                    <img src={Timeimg} className="profile pl-2" alt="" />
                  </div> */}
                      {/* <div className="box-promo-pembayaran">
                    <Row>
                      <Col xs="6">
                        <p>Kode Promo</p>
                      </Col>
                      <Col xs="6" className="text-right">
                        <a onClick={function(){ window.location.href = "/fameoapp/#/order-promo-code"; }} >TAMBAH</a>
                      </Col>
                    </Row>
                  </div>
                  <hr /> */}

                      <div className="rincian-order-section">
                        <div className="form-group">
                          <h4>Rincian Order</h4>
                        </div>
                        <p>Jumlah Pembayaran</p>
                        <h5>{formatPrice(this.state.totalPay)} </h5>
                        <p>Nomor Pesanan</p>
                        <h5>{this.state.orderNo}</h5>
                        <p>Metode Pembayaran</p>
                        <h5>
                          Virtual Account Bank
                          {bankType}
                        </h5>
                      </div>
                      <hr />
                      <div className="rincian-order-section-2">
                        <p>
                          Dengan mengetuk tombol berikut, Kamu telah menyetujui
                          <a href="#"> Syarat &amp; Ketentuan</a> dan
                          <a href="#">Kebijakan Privasi</a> Fameo
                        </p>
                      </div>
                      {/* <SingleButton content={{ text: 'GENERATE VIRTUAL ACCOUNT', color: 'pink', url : '/order-rincian-how-payment' }} /> */}
                      <button className="btn-pink c-white" type="submit">
                        GENERATE VIRTUAL ACCOUNT
                      </button>
                    </Col>
                  </Row>
                </section>
              </div>
            </form>
          </div>
        </div>
      </>
    );
  }
}

export default OrderRincian;
