/* eslint-disable jsx-a11y/anchor-is-valid */

import React, { useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Header from '../components/Header';
import SingleButton from '../components/Button';
import Back from '../assets/images/icon-left-arrow-two.svg';
import Arrow from '../assets/images/icon-right-arrow.svg';
import { eraseCookie } from '../helpers';

const ProfileSettings = () => {
  const history = useHistory();

  const logout = () => {
    eraseCookie('UserId');
    eraseCookie('RoleId');

    history.push('/signin');
  };
  useEffect(() => {
    window.scroll(0, 0);
  }, []);
  return (
    <div className="profile-settings-wrapper">
      <Header content={{ title: 'Pengaturan', BtnLeftIcon: Back }} />
      <div className="after-heading-short-wrapper">
        <div className="container">
          <div className="border-bottom-wrapper">
            <h2 className="section-title">Personal</h2>
            <Link to="/update" className="setting-item-wrapper">
              <div className="setting-title-wrapper">
                <p className="m-0 setting-title">Edit Profil</p>
                <img src={Arrow} className="fameo-arrow" alt="fameo-arrow" />
              </div>
              <p className="setting-subheading">
                Ubah profilmu agar lebih menarik
              </p>
            </Link>
            <Link
              to="/change-password"
              className="setting-item-wrapper"
              style={{ marginTop: '20px', display: 'block' }}
            >
              <div className="setting-title-wrapper">
                <p className="m-0 setting-title">Ubah Password</p>
                <img src={Arrow} className="fameo-arrow" alt="fameo-arrow" />
              </div>
              <p className="setting-subheading">
                Ganti passwordmu dengan yang baru
              </p>
            </Link>
          </div>
          <div className="border-bottom-wrapper">
            <h2 className="section-title">Notifikasi</h2>
            <Link to="#" className="setting-item-wrapper">
              <div className="setting-title-wrapper">
                <p className="m-0 setting-title">
                  Aktifitas Terbaru (Coming soon)
                </p>
                <img src={Arrow} className="fameo-arrow" alt="fameo-arrow" />
              </div>
              <p className="setting-subheading">
                Izinkan pengiriman notifikasi aktifitas terbaru
              </p>
            </Link>
          </div>
          <div className="border-bottom-wrapper">
            <h2 className="section-title">About</h2>
            <div to="/" className="setting-item-wrapper">
              <div className="setting-title-wrapper">
                <p className="m-0 setting-title">App Version</p>
              </div>
              <p className="setting-subheading">Version 2.0</p>
            </div>
          </div>
          <div className="border-bottom-wrapper">
            <h2 className="section-title">Informasi Lainnya</h2>
            <Link to="/help" className="setting-item-wrapper">
              <div className="setting-title-wrapper">
                <p className="m-0 setting-title">Bantuan</p>
                <img src={Arrow} className="fameo-arrow" alt="fameo-arrow" />
              </div>
              <p className="setting-subheading">
                Ada masalah? Yuk kontak tim kami
              </p>
            </Link>
          </div>
          <div className="border-bottom-wrapper">
            <Link to="/terms-conditions" className="setting-item-wrapper">
              <div className="setting-title-wrapper">
                <p className="m-0 setting-title">Syarat & Ketentuan</p>
                <img src={Arrow} className="fameo-arrow" alt="fameo-arrow" />
              </div>
            </Link>
          </div>
          <div className="border-bottom-wrapper">
            <Link to="/terms-withdraw" className="setting-item-wrapper">
              <div className="setting-title-wrapper">
                <p className="m-0 setting-title">Kebijakan Pengembalian Dana</p>
                <img src={Arrow} className="fameo-arrow" alt="fameo-arrow" />
              </div>
            </Link>
          </div>
          <SingleButton
            onClick={() => {
              logout();
            }}
            content={{
              text: 'LOG OUT',
              color: 'transparent-pink',
              url: '/signup',
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default ProfileSettings;
