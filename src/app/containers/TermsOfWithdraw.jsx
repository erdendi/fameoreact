/* eslint-disable react/no-unescaped-entities */

import React from 'react';
// import { Form, FormGroup, Input } from 'reactstrap';

import { useHistory } from 'react-router-dom';
import Header from '../components/Header';
import SingleButton from '../components/Button';

import Back from '../assets/images/icon-back.svg';

const TermOfWithdraw = () => {
  const history = useHistory();

  const handleBack = () => {
    history.goBack();
  };
  return (
    <div>
      <Header
        content={{ title: 'Syarat & Ketentuan', BtnLeftIcon: Back, link: '/' }}
      />
      <div className="order terms-conditions">
        <div className="container">
          <div className="content-wrapper">
            <p className="subtitle c-white">Kebijakan Pengembalian Dana</p>
            <div className="box-terms">
              <p>
                <b>1. Tentang Pengembalian Uang</b>
                <br />
                &nbsp;&nbsp;&nbsp;Pengembalian uang berarti mengembalikan dana
                kepada pelanggan. Ini disebabkan oleh pemesanan ganda, produk /
                layanan tidak tersedia, penerbangan dibatalkan, dll. Untuk
                memproses pengembalian dana, atau mengembalikan dana ke
                pelanggan, kami perlu memastikan bahwa uang telah berhasil
                ditransfer dari pelanggan ke pedagang; atau secara operasional,
                status pembayaran telah diubah menjadi "Penyelesaian".Permintaan
                pengembalian dana sebagian besar berlaku dalam transaksi
                Pembayaran Kartu, dan berfungsi dengan mengembalikan batas
                Kartu. Pengembalian uang untuk metode pembayaran lainnya dapat
                diproses oleh pedagang itu sendiri, biasanya dengan mentransfer
                uang kembali. Proses pengembalian uang untuk Kartu Kredit
                biasanya memakan waktu 7-14 hari kerja. Pengembalian uang untuk
                Kartu Debit bisa lebih lama, hingga dua bulan; dan pelanggan
                perlu menghubungi Bank Penerbit mereka juga. Permintaan
                pengembalian dana memakan waktu beberapa hari karena perlu
                dikoordinasikan dengan berbagai pihak, seperti mengakuisisi dan
                menerbitkan bank. Jika pelanggan mengeluh bahwa batasan mereka
                belum dikembalikan, pedagang harus bertanya kepada Acquiring
                Banks dan pelanggan juga harus bertanya kepada Issuing Bank.
                Biasanya memerlukan data: orderID, detail pedagang, detail
                pelanggan, dan kode persetujuan. jika dalam 4 hari terhitung
                sejak jadwal pengunggahan video oleh talent tidak ada video yang
                dikirimkan, maka user berhak memilih untuk melakukan booking
                ulang atau mengajukan pengembalian dana. Proses pengembalian
                dana akan dilakukan dalam 14 hari kerja.
                <br />
                <br />
                <b>2. Kartu Kredit - Aggregator</b>
                <br />
                &nbsp;&nbsp;&nbsp;Jika Anda seorang pedagang yang menggunakan
                perjanjian agregator Fameo untuk pembayaran Kartu, permintaan
                pengembalian dana harus dilakukan melalui jingL. Anda dapat
                membuka Portal Administrasi Pedagang, mencari ID khusus yang
                harus Anda kembalikan, dan klik tombol Pengembalian Dana.
                Setelah menekan tombol, harap isi jumlah pengembalian uang dan
                alasan pengembalian uang kemudian tekan Kirim. Kami akan
                memproses permintaan Anda ke bank pada hari kerja berikutnya.
                Anda masih akan mendapat dana, tetapi kami akan memotong dana
                dari permintaan pembayaran Anda berikutnya.
                <br />
                <br />
                <b>2. Kartu Kredit - Fasilitator</b>
                <br />
                &nbsp;&nbsp;&nbsp;Jika Anda seorang pedagang yang menggunakan
                perjanjian fasilitator Fameo untuk pembayaran Kartu, permintaan
                pengembalian dana harus dilakukan langsung melalui Bank. Silakan
                hubungi Solusi Anda untuk memberi Anda templat permintaan
                pengembalian dana.
              </p>
            </div>
            <div className="button-holder">
              <SingleButton
                onClick={handleBack}
                content={{ text: 'OK', color: 'pink' }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TermOfWithdraw;
