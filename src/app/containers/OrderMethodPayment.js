import React, { useState, useEffect } from 'react';
import { Row, Col, Collapse, Button } from 'reactstrap';
import { useParams } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { getCookie } from '../helpers';
import { getConfirmation } from '../services/order';
import { postPaymentOvo, postPaymentGopay } from '../services/payment';

import Header from '../components/Header';
import LoadingFull from '../components/LoadingFull';

import Back from '../assets/images/icon-back.svg';
import Settings from '../assets/images/icon-more.svg';

const OrderMethodPayment = () => {
  const [loading, setLoading] = useState(true);
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  // const [isOpendua, setIsOpendua] = useState(false);
  // const toggledua = () => setIsOpendua(!isOpendua);
  // const [isOpentiga, setIsOpentiga] = useState(false);
  // const toggletiga = () => setIsOpentiga(!isOpentiga);

  const { bookId } = useParams();
  const [confirm, setConfirm] = useState(false);
  const [userID] = useState(Number(getCookie('UserId')));

  const PosVA = async () => {
    window.location = `/order-generate-virtual-account/${bookId}`;
  };

  const postPayment = val => {
    setLoading(true);
    const params = {
      PayMethod: val,
      BankSelected: val,
      UserId: confirm.BookedBy,
      Id: confirm.Id,
    };

    try {
      if (val === 'gopay') {
        postPaymentGopay(params).then(resp => {
          window.location = resp.redirect_url;
        });
      } else {
        postPaymentOvo(params).then(resp => {
          window.location = resp.redirect_url;
        });
      }
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    window.scroll(0, 0);

    const params = {
      Id: bookId,
      UserId: userID,
    };
    getConfirmation(params).then(resp => {
      setConfirm(resp);
      setLoading(false);
    });
  }, [bookId, userID]);

  return (
    <>
      {loading && <LoadingFull />}
      <Header
        content={{
          title: 'Pilih Metode Pembayaran',
          BtnLeftIcon: Back,
          BtnRightIcon: Settings,
          hasSubtitle: true,
        }}
      />
      <div className="order order-confirmation">
        <div className="container">
          <div className="content-wrapper">
            <section className="content-pembayaran">
              <Row>
                <Col xs="12">
                  {/* <div className="warning-selesaikan mb-4">
                    Selesaikan pembayaran dalam 09 mnt 11 dtk
                    <img src={Timeimg} className="profile pl-2" alt="" />
                  </div> */}
                  {/* <div className="">
                    <Row>
                      <Col xs="2">
                        <img src={Profileimg} className="profile pl-2 img-profile" alt="" />
                      </Col>
                      <Col xs="7">
                        <div className="form-group text-left">
                          <h4>Michelle Ziudith</h4>
                          <label className="mt-0">ORD-2019000184</label>
                          <h4>Rp 200.000</h4>
                        </div>
                      </Col>
                      <Col xs="3" className="text-right">
                        <a href="#" className="linkget">RINCIAN</a>
                      </Col>
                    </Row>
                  </div> */}
                  <hr />
                  <div className="rincian-order-section">
                    <div className="form-group">
                      <h4>Metode Pembayaran</h4>
                    </div>
                    <div className="accordion" id="accordion-pembayaran">
                      <div className="card">
                        <Button className="card-header" onClick={toggle}>
                          <h2 className="mb-0">
                            <button
                              className="btn btn-link"
                              type="button"
                              onClick={PosVA}
                            >
                              Virtual Account{' '}
                              <FontAwesomeIcon
                                icon={faArrowRight}
                                className="left-arrow---"
                              />
                            </button>
                          </h2>
                        </Button>
                        <Collapse className="collapse" isOpen={isOpen}>
                          <div className="card-body">
                            <div>
                              <img
                                className="profile"
                                alt="BCA"
                                src="../assets/images/pembayaran/bca.svg"
                                style={{
                                  borderRadius: 0,
                                  height: '15px',
                                  margin: '5px',
                                  width: 'max-content',
                                }}
                              />
                            </div>
                            <div>
                              <img
                                className="profile"
                                alt="BNI"
                                src="../assets/images/pembayaran/bni.svg"
                                style={{
                                  borderRadius: 0,
                                  height: '15px',
                                  margin: '5px',
                                  width: 'max-content',
                                }}
                              />
                            </div>
                            <div>
                              <img
                                className="profile"
                                alt="BRI"
                                src="../assets/images/pembayaran/bri.svg"
                                style={{
                                  borderRadius: 0,
                                  height: '15px',
                                  margin: '5px',
                                  width: 'max-content',
                                }}
                              />
                            </div>
                          </div>
                        </Collapse>
                      </div>
                      {/* <div className="card">
                        <div className="card-header" onClick={toggledua}>
                          <h2 className="mb-0">
                            <button className="btn btn-link" type="button" >
                              eWALLET <img className="left-arrow---" src={Leftimg} alt="" />
                            </button>
                          </h2>
                        </div>
                        <Collapse className="collapse" isOpen={isOpendua}>
                          <div className="card-body">
                            Metode Pembayaran
                          </div>
                        </Collapse>
                      </div> */}
                      <div className="card">
                        <Button
                          className="card-header"
                          onClick={() => postPayment('gopay')}
                        >
                          <h2 className="mb-0">
                            <button className="btn btn-link" type="button">
                              GOPAY{' '}
                              <FontAwesomeIcon
                                icon={faArrowRight}
                                className="left-arrow---"
                              />
                            </button>
                          </h2>
                        </Button>
                        {/* <Collapse className="collapse" isOpen={isOpendua} >
                          <div className="card-body">
                            Metode Pembayaran
                          </div>
                        </Collapse> */}
                      </div>
                      <div className="card">
                        <Button
                          className="card-header"
                          onClick={() => postPayment('ovo')}
                        >
                          <h2 className="mb-0">
                            <button className="btn btn-link" type="button">
                              OVO{' '}
                              <FontAwesomeIcon
                                icon={faArrowRight}
                                className="left-arrow---"
                              />
                            </button>
                          </h2>
                        </Button>
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
            </section>
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderMethodPayment;
