/**
 *
 * Asynchronously loads the component for DetailVideo
 *
 */

import { lazyLoad } from 'utils/loadable';

export const DetailVideo = lazyLoad(
  () => import('./index'),
  module => module.DetailVideo,
);
