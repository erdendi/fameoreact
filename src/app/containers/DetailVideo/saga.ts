import { getDetailVideo } from 'api/getTalentAPI';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { selectVideoId } from './selectors';
import { actions } from './slice';

export function* fetchDetailTalent() {
  const VideoId: string = yield select(selectVideoId);
  const params = {
    VideoId,
  };
  try {
    const repos = yield call(getDetailVideo, params);
    yield put(actions.getDetailVideoSuccess(repos));
  } catch (error) {
    yield put(actions.getDetailVideoFailure(error));
  }
}

export function* detailVideoSaga() {
  yield takeLatest(actions.getDetailVideoStart.type, fetchDetailTalent);
}
