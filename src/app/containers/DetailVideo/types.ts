/* --- STATE --- */
export interface DetailVideoState {
  isLoading: Boolean;
  error: String | null;
  videoResponse: object | any;
  videoData: object | any;
  userId: String;
  videoId: String | Number;
  player: object | any;
  video: object | any;
  playerOptions: any[];
}

export type ContainerState = DetailVideoState;
