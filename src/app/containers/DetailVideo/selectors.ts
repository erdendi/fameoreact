import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.detailVideo || initialState;

export const selectDetailVideo = createSelector(
  [selectDomain],
  detailVideoState => detailVideoState,
);

export const selectVideoId = createSelector(
  [selectDomain],
  detailState => detailState.videoId,
);
