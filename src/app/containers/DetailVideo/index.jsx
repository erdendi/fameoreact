/**
 *
 * DetailVideo
 *
 */

import React, { memo, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components/macro';

import { useParams } from 'react-router-dom';
import { SocialIcon } from 'react-social-icons';
import {
  useInjectReducer,
  useInjectSaga,
} from '../../../utils/redux-injectors';
import { actions, reducer, sliceKey } from './slice';
import { selectDetailVideo } from './selectors';
import { detailVideoSaga } from './saga';
import Header from '../../components/Header';
import Back from '../../assets/images/icon-back.svg';
import { Player } from '../../components/Player';

const playerOptions = {
  aspectRatio: '9:16',
};

// interface Props {}

export const DetailVideo = memo(props => {
  const [player, setPlayer] = useState();

  useInjectReducer({ key: sliceKey, reducer });
  useInjectSaga({ key: sliceKey, saga: detailVideoSaga });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const detailVideo = useSelector(selectDetailVideo);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const dispatch = useDispatch();

  const { videoId } = useParams();
  const { video, videoData } = detailVideo;

  useEffect(() => {
    dispatch(actions.setVideoId(videoId));
    dispatch(actions.getDetailVideoStart());
  }, [videoId]);

  // const log = (event, data) => console.log(event.type, data);

  useEffect(() => {
    if (player && video) {
      // player.unload({ loading: true });
      player.src(video.sources);
      // player.on('hls-quality', log);
      // player.on('hls-qualitychange', log);
    }
  }, [video, player]);

  return (
    <>
      <Helmet>
        <title>Detail Video</title>
        <meta name="description" content="Description of DetailVideo" />
      </Helmet>
      <Div>
        <Header
          content={{
            title: 'Video',
            BtnLeftIcon: Back,
            // BtnRightIcon: Settings,
          }}
        />
        <div className="order">
          <div className="container">
            <div className="content-wrapper">
              <div className="section-title section-title-violet">
                {videoData.BookedBy ? (
                  <>
                    Video dari <br />
                    <span className="headline c-white">
                      {videoData.TalentNm}
                    </span>{' '}
                    <br />
                    untuk <br />
                    <span className="headline c-white">
                      {videoData.BookedBy}
                    </span>
                  </>
                ) : (
                  <>
                    Video <br />
                    <span className="headline c-white">
                      {videoData.TalentNm}
                    </span>{' '}
                  </>
                )}
              </div>
              <Player playerOptions={playerOptions} onPlayerInit={setPlayer} />
              <div className="share-wrapper">
                <h5 className="c-white">Bagikan video ini:</h5>
                <div className="share-icon">
                  <SocialIcon
                    url={`https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.fameoapp.com%2Fvideo%2F${videoId}`}
                    target="_blank"
                  />
                </div>
                <div className="share-icon">
                  <SocialIcon
                    network="whatsapp"
                    url={`https://wa.me/?text=https%3A%2F%2Fwww.fameoapp.com%2Fvideo%2F${videoId}`}
                    target="_blank"
                  />
                </div>
              </div>
              ;
            </div>
          </div>
        </div>
      </Div>
    </>
  );
});

const Div = styled.div`
  .player {
    height: 100% !important;
  }
  .share-wrapper {
    display: block;
    margin-top: 20px;

    .share-icon {
      display: inline-block;
      margin: 0 10px;
    }
  }
  .section-title {
    font-family: 'Codec-Cold-Regular-trial';
    font-size: 18px;
    letter-spacing: 0.38px;
    font-weight: 700;
    text-align: left;
    margin-bottom: 20px;
  }
`;
