import { PayloadAction } from '@reduxjs/toolkit';
// import { Repo } from 'types/Repo';
import { createSlice } from '../../../utils/@reduxjs/toolkit';
import { ContainerState } from './types';

// The initial state of the DetailVideo container
export const initialState: ContainerState = {
  isLoading: false,
  error: null,
  videoResponse: {},
  videoData: {},
  userId: '',
  videoId: '',
  player: null,
  video: null,
  playerOptions: [],
};

const detailVideoSlice = createSlice({
  name: 'detailVideo',
  initialState,
  reducers: {
    setVideoId(state, action: PayloadAction<any>) {
      state.videoId = action.payload;
    },
    getDetailVideoStart(state) {
      state.isLoading = true;
      state.error = null;
    },
    getDetailVideoSuccess(state, action: PayloadAction<any[]>) {
      state.videoResponse = action.payload;
      state.videoData = state.videoResponse.Value;
      state.isLoading = false;
      state.error = null;

      console.log(action.payload);

      const videoObj = {
        sources: [
          {
            src: state.videoData.Link,
            // type: 'video/youtube',
            // type: 'application/x-mpegURL',
            type: 'video/mp4',
          },
        ],
      };

      state.video = videoObj;
    },
    getDetailVideoFailure(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
  },
});

export const { actions, reducer, name: sliceKey } = detailVideoSlice;
