/**
 *
 * Asynchronously loads the component for OrderList
 *
 */

import { lazyLoad } from 'utils/loadable';

export const OrderList = lazyLoad(
  () => import('./index'),
  module => module.OrderList,
);
