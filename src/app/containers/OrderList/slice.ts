import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';

// The initial state of the OrderList container
export const initialState: ContainerState = {
  isLoading: false,
  error: null,
  orderResponse: {},
  orderData: [],
  roleId: null,
  search: '',
  searchData: [],
};

const orderListSlice = createSlice({
  name: 'orderList',
  initialState,
  reducers: {
    getOrderListStart(state) {
      state.isLoading = true;
      state.error = null;
    },
    getOrderListSuccess(state, action: PayloadAction<any[]>) {
      state.isLoading = false;
      state.error = null;

      state.orderResponse = action.payload;
      const {
        AllBookListByTalent,
        UserModel,
        ListBooking,
      } = state.orderResponse;

      state.roleId = UserModel.RoleId;

      if (UserModel.RoleId === 3) {
        state.orderData = AllBookListByTalent;
      } else {
        state.orderData = ListBooking;
      }
    },
    getOrderListFailure(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
    getBackgroundOrderListStart(state) {
      state.error = null;
    },
    setSearchParam(state, action: PayloadAction<string>) {
      state.search = action.payload;

      const filteredOrder = state.orderData.filter(order => {
        return order.OrderNo.toLowerCase().includes(state.search.toLowerCase());
      });

      state.searchData = filteredOrder;
    },
  },
});

export const { actions, reducer, name: sliceKey } = orderListSlice;
