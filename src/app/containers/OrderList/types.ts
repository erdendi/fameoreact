/* --- STATE --- */
export interface OrderListState {
  isLoading: Boolean;
  error: null | String;
  orderResponse: Object | any;
  orderData: any[] | any;
  roleId: number | any;
  search: string;
  searchData: any[] | any;
}

export type ContainerState = OrderListState;
