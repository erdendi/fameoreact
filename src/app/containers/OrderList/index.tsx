/**
 *
 * OrderList
 *
 */

import React, { memo, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useSelector, useDispatch } from 'react-redux';

import styled from 'styled-components/macro';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey, actions } from './slice';
import { selectOrderList } from './selectors';
import { orderListSaga } from './saga';
import { FormGroup, Input } from 'reactstrap';
import TabsProfile from '../TabsProfile';
import Header from '../../components/Header';
import Back from '../../assets/images/icon-left-arrow-two.svg';
import Loading from '../../components/Loading';

interface Props {}

const tabContent = {
  data: [
    {
      id: 1,
      tabTitle: 'Berjalan',
      orderList: true,
      fanQuestions: false,
    },
    {
      id: 2,
      tabTitle: 'History',
      orderList: false,
      fanQuestions: false,
    },
  ],
};

export const OrderList = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: orderListSaga });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const orderList = useSelector(selectOrderList);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const dispatch = useDispatch();

  const { isLoading, orderData, search, searchData } = orderList;

  useEffect(() => {
    if (orderData.length === 0) {
      dispatch(actions.getOrderListStart());
    } else {
      dispatch(actions.getBackgroundOrderListStart());
    }
  }, []);

  const searchOrder = e => {
    e.preventDefault();
    dispatch(actions.setSearchParam(e.target.value));
  };

  return (
    <>
      <Helmet>
        <title>OrderList</title>
        <meta name="description" content="Description of OrderList" />
      </Helmet>
      <Div>
        <div className="order-list-wrapper">
          <Header content={{ title: 'List Order Saya', BtnLeftIcon: Back }} />
          <div className="after-heading-wrapper">
            <div className="container">
              <div className="search-bar">
                <FormGroup>
                  <Input
                    type="search"
                    name="search"
                    id="exampleSearch"
                    placeholder="Cari order Kamu"
                    onChange={e => searchOrder(e)}
                    disabled={Boolean(isLoading)}
                  />
                  <span className="search-icon" />
                </FormGroup>
              </div>
              {isLoading ? (
                <Loading />
              ) : (
                <>
                  {!search ? (
                    <TabsProfile items={tabContent} orderList={orderData} />
                  ) : (
                    <TabsProfile items={tabContent} orderList={searchData} />
                  )}
                </>
              )}
            </div>
          </div>
        </div>
      </Div>
    </>
  );
});

const Div = styled.div`
  .form-control:disabled,
  .form-control[readonly] {
    background: transparent;
  }
`;
