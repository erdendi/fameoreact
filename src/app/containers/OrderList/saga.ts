import { getCookie } from 'app/helpers';
import { getUser } from 'app/services/detail';
import { call, put, takeLatest } from 'redux-saga/effects';
import { actions } from './slice';

export function* fetchOrderList() {
  const userId = Number(getCookie('UserId'));
  const param = {
    userId,
  };

  try {
    const repos = yield call(getUser, param);
    yield put(actions.getOrderListSuccess(repos));
  } catch (error) {
    yield put(actions.getOrderListFailure(error));
  }
}

export function* orderListSaga() {
  yield takeLatest(actions.getOrderListStart.type, fetchOrderList);
}
