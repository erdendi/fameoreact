import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.orderList || initialState;

export const selectOrderList = createSelector(
  [selectDomain],
  orderListState => orderListState,
);
