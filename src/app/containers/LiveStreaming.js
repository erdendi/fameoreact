/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

import LiveBg from '../assets/images/live/bg.png';
import Back from '../assets/images/detail/left_arrow_video.png';
import Fameo from '../assets/images/live/fameo.png';
import NavOne from '../assets/images/live/idol-pose.png';
import ProfileRed from '../assets/images/live/profile-red.svg';
import Crown from '../assets/images/crown.png';
import Arrowr from '../assets/images/live/arrowr.svg';
// import Gift from '../assets/images/gift-box.svg'
import Check from '../assets/images/live/check.svg';
import Send from '../assets/images/live/send.svg';
import SA from '../assets/images/sticker/1.png';
import SB from '../assets/images/sticker/2.png';
import SC from '../assets/images/sticker/3.png';
import Custom from '../assets/images/icon-check-success.svg';
import Questionxx from '../assets/images/questionxx.svg';

import VideoD from '../assets/images/video/share.png';
import Livegift from '../assets/images/livegift.svg';
import VideoF from '../assets/images/video/chats.png';
import VideoG from '../assets/images/video/nice.png';

import Fb from '../assets/images/live/fb.svg';
import Tw from '../assets/images/live/tw.svg';
import Ig from '../assets/images/live/ig.svg';
import Google from '../assets/images/live/g.svg';

function showchat() {
  if (
    document.getElementById('chat-box').getAttribute('style') ===
    'display: none;'
  ) {
    document.getElementById('chat-box').style.display = 'block';
    document.getElementById('backdropchat').style.display = 'block';
  } else {
    document.getElementById('chat-box').style.display = 'none';
    document.getElementById('backdropchat').style.display = 'none';
  }
}
function showshare() {
  if (
    document.getElementById('share-box').getAttribute('style') ===
    'display: none;'
  ) {
    document.getElementById('share-box').style.display = 'block';
    document.getElementById('backdropshare').style.display = 'block';
  } else {
    document.getElementById('share-box').style.display = 'none';
    document.getElementById('backdropshare').style.display = 'none';
  }
}
function showsticker() {
  if (
    document.getElementById('sticker-box').getAttribute('style') ===
    'display: none;'
  ) {
    document.getElementById('sticker-box').style.display = 'block';
    document.getElementById('backdropsticker').style.display = 'block';
  } else {
    document.getElementById('sticker-box').style.display = 'none';
    document.getElementById('backdropsticker').style.display = 'none';
  }
}
function showboxchat() {
  if (
    document.getElementById('footer-menu-left').getAttribute('style') ===
    'display: none;'
  ) {
    document.getElementById('footer-menu-left').style.display = 'block';
  } else {
    document.getElementById('footer-menu-left').style.display = 'none';
  }
}

const LiveStreaming = () => {
  const [activeTab, setActiveTab] = useState('1');

  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  return (
    <>
      <div className="live-streaming-wrapper">
        <img src={LiveBg} className="live-video" alt="live" />

        <div className="container">
          <section className="navbar-top">
            <div className="row">
              <div className="col-2 pr-0 text-center">
                <img className="back-btn" src={Back} alt="back" />
              </div>
              <div className="col-10 pl-0">
                <div className="box-right">
                  <div className="row">
                    <div className="col-8">
                      <div className="row">
                        <div className="col-4">
                          <img
                            className="logo-fameo-2"
                            src={NavOne}
                            alt="nav"
                          />
                        </div>
                        <div className="col-8 p-0">
                          <h5>Michelle Ziudith</h5>
                          <h6>Kenalan yuk!</h6>
                        </div>
                      </div>
                    </div>
                    <div className="col-4">
                      <img className="logo-fameo" src={Fameo} alt="fam" />
                      <span className="logo-fameo-live">Live</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>

        <section id="footer-menu-left">
          <div className="box-chats">
            <div className="row">
              <div className="col-5 pr-0">
                <p className="text-1">
                  <img
                    className="logo-fameo-2"
                    style={{ marginTop: '-2px' }}
                    src={ProfileRed}
                    alt="fam"
                  />{' '}
                  Jhonny :
                </p>
              </div>
              <div className="col-7 pl-0">
                <p className="text-2">Hai, Salam Kenal</p>
              </div>
            </div>
          </div>
          <div className="box-chats">
            <div className="row">
              <div className="col-5 pr-0">
                <p className="text-1">
                  <img
                    className="logo-fameo-2"
                    style={{ marginTop: '-2px' }}
                    src={ProfileRed}
                    alt="fam2"
                  />{' '}
                  Buday :
                </p>
              </div>
              <div className="col-7 pl-0">
                <p className="text-2">Cantik banget kak!</p>
              </div>
            </div>
          </div>
        </section>

        <section id="footer-menu-right">
          <div className="row">
            <div className="col-12 text-center">
              <a className="btn-bottom-icon-crown" href="/">
                <img className="check" src={Check} alt="check" />
                <img src={Crown} alt="crown" />
              </a>
            </div>
            <div className="col-12 text-center">
              <a className="btn-bottom-icon" onClick={showshare}>
                <img src={VideoD} alt="video" />
              </a>
            </div>
            <div className="col-12 text-center">
              <a
                className="btn-bottom-icon"
                id="ask-btn-show"
                onClick={showchat}
              >
                <img src={Questionxx} style={{ padding: '3px' }} />
              </a>
            </div>
            <div className="col-12 text-center">
              <a className="btn-bottom-icon" onClick={showboxchat}>
                <img src={VideoF} />
              </a>
            </div>
            <div className="col-12 text-center">
              <a className="btn-bottom-icon-like" style={{ display: 'block' }}>
                <img className="m-0" src={VideoG} />
                <br />
                <span>10</span>
              </a>
            </div>
            <div className="col-12 text-center">
              <a className="btn-bottom-icon-gift" onClick={showsticker}>
                <img src={Livegift} style={{ marginTop: '-4px' }} />
              </a>
            </div>
          </div>
        </section>

        <section id="chat-box" style={{ display: 'none' }}>
          <div className="row">
            <div className="col-12">
              <input type="text" placeholder="Tuliskan pesan kamu" />
              <img src={Send} />
            </div>
          </div>
        </section>
        <section id="share-box" style={{ display: 'none' }}>
          <p>Share to Social Media :</p>
          <div className="row no-gutters">
            <div className="col-3">
              <div className="shareedbox">
                <img src={Fb} />
              </div>
            </div>
            <div className="col-3">
              <div className="shareedbox">
                <img src={Tw} />
              </div>
            </div>
            <div className="col-3">
              <div className="shareedbox">
                <img src={Ig} />
              </div>
            </div>
            <div className="col-3">
              <div className="shareedbox">
                <img src={Google} />
              </div>
            </div>
          </div>
        </section>
        <section id="sticker-box" style={{ display: 'none' }}>
          <div className="row no-gutters">
            <div className="col-12">
              <div className="filter-holder">
                <Nav tabs>
                  <NavItem>
                    <NavLink
                      className={classnames({ active: activeTab === '1' })}
                      onClick={() => {
                        toggle('1');
                      }}
                    >
                      Semua
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({ active: activeTab === '2' })}
                      onClick={() => {
                        toggle('2');
                      }}
                    >
                      Baru
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({ active: activeTab === '3' })}
                      onClick={() => {
                        toggle('3');
                      }}
                    >
                      Populer
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({ active: activeTab === '4' })}
                      onClick={() => {
                        toggle('4');
                      }}
                    >
                      VIP
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={classnames({ active: activeTab === '5' })}
                      onClick={() => {
                        toggle('5');
                      }}
                    >
                      Gift
                    </NavLink>
                  </NavItem>
                </Nav>
              </div>
              <TabContent className="results-holder" activeTab={activeTab}>
                <TabPane className="tab-content" tabId="1">
                  <div className="row">
                    <div className="col-3">
                      <div className="stickerbox">
                        <img src={SA} />
                        <p>Sticker 1</p>
                        <h3>
                          <img src={Custom} /> 100
                        </h3>
                      </div>
                    </div>
                    <div className="col-3">
                      <div className="stickerbox">
                        <img src={SB} />
                        <p>Sticker 2</p>
                        <h3>
                          <img src={Custom} /> 20
                        </h3>
                      </div>
                    </div>
                    <div className="col-3">
                      <div className="stickerbox">
                        <img src={SC} />
                        <p>Sticker 3</p>
                        <h3>
                          <img src={Custom} /> 4
                        </h3>
                      </div>
                    </div>
                    <div className="col-3">
                      <div className="stickerbox">
                        <img src={SA} />
                        <p>Sticker 4</p>
                        <h3>
                          <img src={Custom} /> 1000
                        </h3>
                      </div>
                    </div>
                    <div className="col-3">
                      <div className="stickerbox">
                        <img src={SA} />
                        <p>Sticker 1</p>
                        <h3>
                          <img src={Custom} /> 100
                        </h3>
                      </div>
                    </div>
                    <div className="col-3">
                      <div className="stickerbox">
                        <img src={SB} />
                        <p>Sticker 2</p>
                        <h3>
                          <img src={Custom} /> 20
                        </h3>
                      </div>
                    </div>
                    <div className="col-3">
                      <div className="stickerbox">
                        <img src={SC} />
                        <p>Sticker 3</p>
                        <h3>
                          <img src={Custom} /> 4
                        </h3>
                      </div>
                    </div>
                    <div className="col-3">
                      <div className="stickerbox">
                        <img src={SA} />
                        <p>Sticker 4</p>
                        <h3>
                          <img src={Custom} /> 1000
                        </h3>
                      </div>
                    </div>
                  </div>
                </TabPane>
                <TabPane className="tab-content" tabId="2" />
                <TabPane className="tab-content" tabId="3" />
                <TabPane className="tab-content" tabId="4" />
                <TabPane className="tab-content" tabId="5" />
              </TabContent>
              <div className="box-topup-footer">
                <div className="row no-gutters">
                  <div className="col-4 text-center">
                    <a className="btn btn-custom-1">Topup Koin</a>
                  </div>
                  <div className="col-4 text-center">
                    <a className="btn btn-custom-2">
                      <img style={{ height: '9px' }} src={Crown} /> Join VIP
                      Member <img src={Arrowr} />
                    </a>
                  </div>
                  <div className="col-4 text-center">
                    <a className="btn btn-custom-1">Send Sticker</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <div
          id="backdropchat"
          className="backdrop"
          style={{ display: 'none' }}
          onClick={showchat}
        />
        <div
          id="backdropshare"
          className="backdrop"
          style={{ display: 'none' }}
          onClick={showshare}
        />
        <div
          id="backdropsticker"
          className="backdrop"
          style={{ display: 'none' }}
          onClick={showsticker}
        />
      </div>
    </>
  );
};

export default LiveStreaming;
