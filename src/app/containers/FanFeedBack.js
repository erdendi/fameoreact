/* eslint-disable react/no-array-index-key */

/* eslint-disable no-useless-concat */

import React from 'react';
// import Swiper from 'react-id-swiper';
// import axios from 'axios';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import StarRatingComponent from 'react-star-rating-component';
import Heading from '../components/Heading';

const contentSwiperData = {
  headingData: {
    heading: 'FeedBack saya',
    headingColor: 'violet',
    // headingLetters: 'uppercase',
    linkName: 'Lihat Semua',
    url: '/search-qa',
  },
};
// const params = {
//   slidesPerView: 2.1,
//   slidesPerGroup: 2.1,
//   spaceBetween: 5,
//   rebuildOnUpdate: true,
// }

function FanFeedBack({ content }) {
  if (content.length === 0) {
    return (
      <div>
        <Heading content={contentSwiperData.headingData} />
        <div
          Style="display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          text-align: center;
      "
        >
          <div style={{ color: '#707070' }}>(Belum ada feedback) </div>
        </div>
      </div>
    );
  }

  return (
    <div className="fan-questions">
      <Heading content={contentSwiperData.headingData} />
      <div className="d-wrap">
        {content.map((item, i) => (
          <div className="slider-feedback" key={i}>
            <div className="slider-fan-feedback">
              <div className="fan-data">
                <img
                  src={item.UserImage}
                  className="fan-image"
                  alt="fameo-user-icon"
                />
                <h5 className="fan-feedback-name">
                  {item.CustomerName.slice(0, 20)}
                </h5>

                <div className="d-flex align-items-center">
                  <div className="star-rating-feedback ">
                    <StarRatingComponent
                      name="rate1"
                      starCount={item.Rate}
                      value={item.Rate}
                      starColor="#FFB4CC"
                      emptyStarColor="#fff"
                    />
                  </div>
                </div>
              </div>
              <div className="fan-data">
                {/* <h5 className="fan-name">To. <Link to={`/detail/${item.TalentId}`} className="read-more-text">{item.TalentNm.split(' ').slice(0, -1).join(' ')}</Link></h5> */}
              </div>
              <p className="fan-message-feedback">
                {`"${item.Review.length}` > 0
                  ? item.Review.slice(0, 35)
                  : '' + ' ... "'}
                <br />
                <Link
                  to={`/order-video-finish/${item.Id}`}
                  className="read-more-text"
                >
                  Read more
                </Link>
              </p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

FanFeedBack.propTypes = {
  content: PropTypes.array.isRequired,
};

export default React.memo(FanFeedBack);
