import { getDetailTalent } from 'api/getTalentAPI';
import { useParams } from 'react-router-dom';
import { take, call, put, select, takeLatest } from 'redux-saga/effects';
import { selectTalentId } from './selectors';
import { actions } from './slice';

export function* fetchDetailTalent() {
  const talentId: string = yield select(selectTalentId);
  const params = {
    talentId,
  };
  try {
    const repos = yield call(getDetailTalent, params);
    yield put(actions.getDetailTalentSuccess(repos));
  } catch (error) {
    yield put(actions.getDetailTalentFailure(error));
  }
}

export function* detailSaga() {
  yield takeLatest(actions.getDetailTalentStart.type, fetchDetailTalent);
}
