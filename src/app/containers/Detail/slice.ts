import { PayloadAction } from '@reduxjs/toolkit';
import { Repo } from 'types/Repo';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';

// The initial state of the Detail container
export const initialState: ContainerState = {
  isLoading: false,
  error: null,
  talent: [],
  userId: '',
  IsNotUserTalent: false,
  notif: false,
  modal: false,
  modalShare: false,
  modalShareVid: false,
  notifUnavailable: false,
  talentId: '',
  player: null,
  video: null,
  playerOptions: [],
};

const detailSlice = createSlice({
  name: 'detail',
  initialState,
  reducers: {
    setTalentId(state, action: PayloadAction<any>) {
      state.talentId = action.payload;
    },
    getDetailTalentStart(state) {
      state.isLoading = true;
      state.error = null;
    },
    getDetailTalentSuccess(state, action: PayloadAction<Repo[]>) {
      state.talent = action.payload;
      state.isLoading = false;
      state.error = null;

      const videoObj = {
        sources: [
          {
            src: state.talent.VideoIntro,
            type: 'video/mp4',
          },
        ],
      };

      state.video = videoObj;
      // state.playerOptions = videoObj
    },
    getDetailTalentFailure(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
    setModal(state, action: PayloadAction<any>) {
      state.modal = action.payload;
    },
    setVideo(state, action: PayloadAction<any>) {
      state.video = action.payload;
    },
    setPlayer(state, action: PayloadAction<any>) {
      state.player = action.payload;
    },
  },
});

export const { actions, reducer, name: sliceKey } = detailSlice;
