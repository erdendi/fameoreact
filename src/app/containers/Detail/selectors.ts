import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.detail || initialState;

export const selectDetail = createSelector(
  [selectDomain],
  detailState => detailState,
);

export const selectTalentId = createSelector(
  [selectDomain],
  detailState => detailState.talentId,
);
