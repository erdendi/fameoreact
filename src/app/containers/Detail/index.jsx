/**
 *
 * Detail
 *
 */

import React, { memo, useEffect, useMemo, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components/macro';
import { Link, useParams, useHistory } from 'react-router-dom';
import { SocialIcon } from 'react-social-icons';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import SweetAlert from 'react-bootstrap-sweetalert';
import Loading from 'app/components/Loading';
import TaskBar from 'app/components/TaskBar';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import { formatPrice } from 'utils/price';
import VideoCardSwiper from 'app/components/VideoCardSwiper';
import { getCookie } from 'app/helpers';
import { postLike } from 'api/postLikeAPI';
import { LikesVideo } from 'app/services/video';
import { Player } from 'app/components/Player';
import { reducer, sliceKey, actions } from './slice';
import { selectDetail } from './selectors';
import { detailSaga } from './saga';
import SwiperQuestions from '../FanQuestions';
// assets
import LeftArrow from '../../assets/images/icon-left-arrow.svg';
import Share from '../../assets/images/share.png';
import PlayButton from '../../assets/images/play.svg';
import Heart from '../../assets/images/heart.png';
import VideoA from '../../assets/images/detail/left_arrow_video.png';
import VideoC from '../../assets/images/detail/close.png';
import VideoD from '../../assets/images/video/share.png';
import VideoE from '../../assets/images/video/chat.png';
import VideoG from '../../assets/images/video/nice.png';
import VideoH from '../../assets/images/video/charz.svg';
import VideoI from '../../assets/images/video/origami.png';
import Question from '../../assets/images/icon-question-msg.svg';

import 'videojs-plus/dist/plugins/unload';

const playerOptions = {};

export const Detail = memo(props => {
  const history = useHistory();
  const [userId] = useState(Number(getCookie('UserId')));
  const [notif, setnotif] = useState(false);
  const [modal, setModal] = useState(false);
  const [modalShare, setModalShare] = useState(false);
  const [modalShareVid, setModalShareVid] = useState(false);
  const [notifUnavailable, setnotifUnavailable] = useState(false);

  const [player, setPlayer] = useState();
  // const [video, setVideo] = useState(null);

  useInjectReducer({ key: sliceKey, reducer });
  useInjectSaga({ key: sliceKey, saga: detailSaga });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const detail = useSelector(selectDetail);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const dispatch = useDispatch();

  const { talentId } = useParams();
  const { talent, IsNotUserTalent, video } = detail;

  useEffect(() => {
    dispatch(actions.setTalentId(talentId));
    dispatch(actions.getDetailTalentStart());
  }, [talentId]);

  useEffect(() => {
    if (player && video) {
      // player.unload({ loading: true });

      player.src(video.sources);
    }
  }, [video, player]);

  const toggle = () => {
    setModal(!modal);
  };
  const toggleShare = () => {
    setModalShare(!modalShare);
  };

  const toggleShareVid = () => {
    setModalShareVid(!modalShareVid);
  };
  const handlePopup = () => {
    setnotif(true);
  };

  const handleClick = () => {
    if (!userId) {
      setnotif(true);
    } else if (
      !isNaN(userId) &&
      talent.IsAvailable &&
      talent.IsTalentAcceptOrder === true
    ) {
      history.push(`/order-personal/${talentId}`);
    } else if (
      (!isNaN(userId) && !talent.IsAvailable) ||
      !talent.IsTalentAcceptOrder ||
      (!isNaN(userId) && !talent.IsAvailable && !talent.IsTalentAcceptOrder)
    ) {
      setnotifUnavailable(true);
    }
  };

  const askbtn = e => {
    history.push(`/asktalent/${e}`);

    // di Comment Sementara sampe fitur talent answer video udh jadi
    // if(show == false)
    // {
    //   setShow(true);
    // }
    // else
    // {
    //   setShow(false);
    // }
  };

  const LikesVideosModel = e => {
    if (isNaN(userId)) {
      setnotif(true);
    } else {
      const params = {
        UserId: userId,
        FileId: e,
      };
      LikesVideo(params).then(resp => {
        if (resp.Status === 'OK') {
          // document.getElementById('TotalLikes').innerHTML = resp.CountLikes;
        }
        // return resp
      });
    }
  };

  function myFunction() {
    const element = document.getElementById('taskbar');
    // if (element.getAttribute('class') === 'taskbar w-100') {
    // element.classList.add('hidden');
    // } else {
    // element.classList.remove('hidden');
    // stopvideo();
    // }
  }

  function askbtnshow() {
    // const element = document.getElementById('custom-ask-on-video');
    // if (element.getAttribute('class') === 'custom-ask-on-video-hidden') {
    // element.classList.add('show');
    // } else {
    // element.classList.remove('show');
    // }
  }

  const handleLike = () => {
    const data = {
      TalentId: talentId,
      UserId: userId,
    };
    postLike(data).then(resp => {
      if (resp.Status === 'OK') {
        // document.getElementById('FavouriteCounts').innerHTML =
        //   resp.FavouriteCount;
      }
    });
  };

  return (
    <>
      <Helmet>
        <title>Detail</title>
        <meta name="description" content="Description of Detail" />
      </Helmet>

      <div className="detail-wrapper">
        {video && (
          <Player playerOptions={playerOptions} onPlayerInit={setPlayer} />
        )}

        {talent.Status === 0 ? (
          <div className="height100vh">
            <div className="back-wrapper">
              <Button className="back-wrapper" onClick={() => history.go(-1)}>
                <img src={LeftArrow} className="go--1arrow" alt="fameo-back" />
              </Button>
            </div>
            <div className="message-wrapper">
              <div className="c-white">{talent.Message}</div>
            </div>
          </div>
        ) : (
          <>
            <div className="container px-0">
              <div
                onClick={() => history.go(-1)}
                className="idol-banner"
                aria-hidden="true"
              >
                {!talent.LinkImg && <Loading />}
                <img src={LeftArrow} className="back-arrow" alt="fameo-back" />
                {talent.LinkImg && (
                  <img src={talent.LinkImg} className="idol-image" alt="." />
                )}
              </div>
            </div>

            <div className="container">
              <div className="action-list">
                <div className="heart-wrapper">
                  {/* <input id="heartbox" type="checkbox" /> */}
                  <Button
                    style={{
                      border: 0,
                      backgroundColor: 'transparent',
                      padding: 0,
                    }}
                    htmlFor="heart"
                    onClick={!userId ? handlePopup : handleLike}
                  >
                    <img
                      src={Heart}
                      className="action-icon heart"
                      alt="fameo-heart"
                    />
                  </Button>
                </div>
                <button
                  type="button"
                  className="btn-round-white"
                  onClick={() => {
                    toggle();
                    myFunction();
                  }}
                >
                  <img src={PlayButton} alt="fameo-play" />
                </button>
                {/* <Link to="/"> */}
                <img
                  onClick={toggleShare}
                  src={Share}
                  className="action-icon share"
                  alt="fameo-share"
                  aria-hidden="true"
                />

                <Modal
                  isOpen={modalShare}
                  toggle={toggleShare}
                  contentClassName="modal-share"
                >
                  <ModalHeader toggle={toggleShare} charCode="X">
                    Share talent on
                  </ModalHeader>
                  <ModalBody>
                    <div className="modal-body-container">
                      <div className="modal-icon">
                        <SocialIcon
                          url={`https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.fameoapp.com%2Fdetail%2F${talentId}`}
                          target="_blank"
                        />
                      </div>
                      <div className="modal-icon">
                        <SocialIcon
                          network="whatsapp"
                          url={`https://wa.me/?text=https%3A%2F%2Fwww.fameoapp.com%2Fdetail%2F${talentId}`}
                          target="_blank"
                        />
                      </div>
                    </div>
                  </ModalBody>
                </Modal>
              </div>
              <div className="count-wrapper-likes">
                {/* <span Style={{marginLeft: 50}} className="like-counter c-petal-off">&emsp;{totalLike.FavoriteCount}</span> */}
                <label htmlFor="heart" id="FavouriteCounts">
                  {talent.FavoriteCount}
                </label>
              </div>
              <h2 className="idol-name text-center text-white">
                {talent.TalentNm}
              </h2>
              <div className="row idol-category">
                <div className="badge-list mx-auto text-white">
                  <span className="badge badge-one">{talent.CategoryName}</span>
                  <span className="badge badge-two">{talent.Profesion}</span>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="idol-message">
                <p className="detail-paragraph heading text-white">
                  {talent.Bio}
                </p>
                <div className="row pills-section">
                  <div className="pills-wrapper mx-auto">
                    {IsNotUserTalent && (
                      <div>
                        <Link
                          to={`/AskTalent/${talentId}`}
                          className="pill pill-purple"
                        >
                          <p className="pill-text">Ask</p>
                          <img
                            src={Question}
                            className="question-tick"
                            alt="question"
                          />
                          {/* <p className="pill-text font-weight-bold">{talent.FirstName}</p> */}
                        </Link>
                        <button
                          onClick={handleClick}
                          className="pill pill-pink 1"
                        >
                          {talent.SalePrice > 0 ? (
                            <>
                              {/* <p className="pill-text"><span class="diskon">{"Rp " + talent.PriceAmount}</span></p> */}
                              <p className="pill-text">
                                {formatPrice(talent.SalePrice)}
                                <span
                                  style={{
                                    textDecoration: 'line-through',
                                    color: '#707070',
                                    marginLeft: '8px',
                                  }}
                                >
                                  {formatPrice(talent.PriceAmount, '')}
                                </span>
                              </p>
                            </>
                          ) : (
                            <p className="pill-text">
                              Order {formatPrice(talent.PriceAmount)}
                            </p>
                          )}
                        </button>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>

            <div className="section-live mt-4">
              {!!talent.ListTalentVideo && talent.ListTalentVideo.length > 0 && (
                <>
                  {/* <Heading content={contentSwiperData.headingData} userId={talentId}/> */}
                  <div className="section-heading">
                    <div className="d-flex justify-content-between">
                      <p className="section-title section-title-white">
                        Video Terbaru
                      </p>
                      <Link
                        // to={{pathname: `/search`, talent: talent.TalentNm}}
                        to={`/search?talentId=${
                          talent.Id
                        }&talentname=${talent.TalentNm.replace(
                          ' ',
                          '-',
                        )}&Category=Ucapan`}
                        className="see-all-link"
                      >
                        Lihat Semua
                      </Link>
                    </div>
                  </div>
                  <VideoCardSwiper contents={talent.ListTalentVideo} />
                </>
              )}
              {!talent.ListTalentVideo && <Loading />}
            </div>

            <div className="section-live mt-5">
              {!!talent.ReactionVideoList &&
                talent.ReactionVideoList.length > 0 && (
                  <>
                    <div className="section-heading">
                      <div className="d-flex justify-content-between">
                        <p className="section-title section-title-white">
                          Video Reaksi
                        </p>
                        <Link
                          // to={{pathname: `/search`, talent: talent.TalentNm}}
                          to={`/search?talentId=${
                            talent.Id
                          }&talentname=${talent.TalentNm.replace(
                            ' ',
                            '-',
                          )}&Category=Reaksi`}
                          className="see-all-link"
                        >
                          Lihat Semua
                        </Link>
                      </div>
                    </div>

                    <VideoCardSwiper contents={talent.ReactionVideoList} />
                  </>
                )}
              {!talent.ListTalentVideo && <Loading />}
            </div>
            {/* <div className="section-live mt-5">
        <VideoCardSwiper contentSwiper={contentSwiperData2} />
      </div> */}
            {!!talent.QuestionList && !!talent.QuestionList.length && (
              <div className="section-questions mt-5">
                {/* <Heading content={questionHeadingData.headingData} /> */}
                <SwiperQuestions content={talent.QuestionList} />
              </div>
            )}

            <Modal isOpen={modal} toggle={toggle} id="ModalVideoPlayer">
              <ModalBody>
                <div className="box-video--">
                  <div className="row">
                    <div className="col">
                      <div
                        className="btn-close--"
                        aria-hidden="true"
                        onClick={() => {
                          toggle();
                          myFunction();
                        }}
                      >
                        <img src={VideoA} alt="" />
                      </div>
                    </div>
                    <div className="col-8">
                      <div className="media">
                        <img
                          style={{ marginLeft: '15px' }}
                          src={talent.LinkImg}
                          className="align-self-center mr-3"
                          alt=""
                        />
                        <div
                          className="media-body"
                          style={{ marginTop: '8px' }}
                        >
                          <h4>{talent.TalentNm}</h4>
                          {talent.ListTalentVideo &&
                            talent.ListTalentVideo.map(item => (
                              <p key={item.id}>{item.CustomerName}</p>
                            ))}
                        </div>
                      </div>
                    </div>
                    <div className="col">
                      <div
                        aria-hidden="true"
                        onClick={() => {
                          toggle();
                          myFunction();
                        }}
                      >
                        <img className="vid-logo-det" src={VideoC} alt="" />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="wrapper_video">
                  {/* <VideoPlayer source={talent.VideoIntro} /> */}
                  <Player
                    playerOptions={playerOptions}
                    onPlayerInit={setPlayer}
                  />
                </div>
                <div className="box-video--bottom">
                  <div className="row">
                    <div className="col-2 col-md-2">
                      <div className="btn-bottom-icon">
                        <img
                          src={VideoD}
                          alt="share"
                          onClick={toggleShareVid}
                          aria-hidden="true"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-2 col-md-2">
                      <div
                        onClick={() => askbtn(talent.Id)}
                        className="btn-bottom-icon"
                        id="ask-btn-show"
                        aria-hidden="true"
                      >
                        <img src={VideoE} style={{ padding: '6px' }} alt="" />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-2 col-md-2">
                      <div
                        onClick={() => LikesVideosModel(talent.VideoIntroId)}
                        className="btn-bottom-icon"
                        aria-hidden="true"
                      >
                        <img className="m-0" src={VideoG} alt="" />
                        <div style={{ color: 'white' }}>
                          <label id="TotalLikes">
                            {talent.VideoIntroTotalLikes}
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  id="custom-ask-on-video"
                  className="custom-ask-on-video-hidden"
                >
                  <div className="col-12 col-md-12">
                    <div
                      className="line----"
                      onClick={askbtnshow}
                      aria-hidden="true"
                    />
                    <p className="text-center">
                      <img className="btn-chat----" src={VideoH} alt="" />{' '}
                      Pertanyaan untuk
                      <b>Michelle Ziudith</b>
                    </p>
                    <input type="text" placeholder="Ajukan pertanyaan Kamu" />
                    <div ref="#">
                      <img src={VideoI} alt="" />
                    </div>
                  </div>
                </div>
              </ModalBody>
            </Modal>

            <TaskBar active="search-talent" />
          </>
        )}
      </div>

      <Modal
        isOpen={modalShareVid}
        toggle={toggleShareVid}
        contentClassName="modal-share"
      >
        <ModalHeader toggle={toggleShareVid} charCode="X">
          Share Video on
        </ModalHeader>
        <ModalBody>
          <div className="modal-body-container">
            <div className="modal-icon">
              <SocialIcon
                url={`https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.fameoapp.com%2Fvideo%2F${talent.FileId}`}
              />
            </div>
            <div className="modal-icon">
              <SocialIcon
                network="whatsapp"
                url={`https://wa.me/?text=https%3A%2F%2Fwww.fameoapp.com%2Fvideo%2F${talent.FileId}`}
              />
            </div>
          </div>
        </ModalBody>
      </Modal>

      <SweetAlert
        warning
        show={notif}
        confirmBtnText="Login"
        title=""
        onConfirm={() => {
          setnotif(false);
          window.location.href = '/signin';
        }}
        onCancel={() => {
          setnotif(false);
        }}
        // onEscapeKey={() => {
        //   setnotif(false);
        //   // window.location.href = `/detail/${talentId}`;
        // }}
        // onOutsideClick={() => {
        //   setnotif(false);
        //   // window.location.href = `/detail/${talentId}`;
        // }}
      >
        <p style={{ fontSize: '13pt' }}>Silahkan login terlebih dahulu</p>
      </SweetAlert>

      <SweetAlert
        warning
        show={notifUnavailable}
        confirmBtnText="OK"
        title=""
        onConfirm={() => {
          setnotifUnavailable(false);
        }}
        onCancel={() => {
          setnotifUnavailable(false);
        }}
        // onEscapeKey={() => {
        //   setnotifUnavailable(false);
        //   // window.location.href = `/detail/${talentId}`;
        // }}
        // onOutsideClick={() => {
        //   setnotifUnavailable(false);
        //   // window.location.href = `/detail/${talentId}`;
        // }}
      >
        <p style={{ fontSize: '13pt' }}>
          Mohon maaf {talent.TalentNm} saat ini tidak bisa menerima order
        </p>
      </SweetAlert>
    </>
  );
});
