import { BooleanLiteral, NullLiteral } from 'typescript';

/* --- STATE --- */
export interface DetailState {
  isLoading: Boolean;
  error: String | null;
  talent: any[] | any;
  userId: String;
  IsNotUserTalent: Boolean;
  notif: Boolean;
  modal: Boolean;
  modalShare: Boolean;
  modalShareVid: Boolean;
  notifUnavailable: Boolean;
  talentId: String | Number;
  player: object | any;
  video: object | any;
  playerOptions: any[];
}

export type ContainerState = DetailState;
