import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

const HowPayBCA = () => {
  const [activeTab, setActiveTab] = useState('1');
  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };
  return (
    <div className="row">
      <div className="col-12 col-md-12 how-paryment-m">
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '1' })}
              onClick={() => {
                toggle('1');
              }}
            >
              ATM BCA
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '2' })}
              onClick={() => {
                toggle('2');
              }}
            >
              Klik BCA Individual
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="1">
            <ul className="timeline">
              <li>
                <p>Pada menu Utama, pilih Transaksi Lainnya.</p>
              </li>
              <li>
                <p>Pilih Transfer.</p>
              </li>
              <li>
                <p>Pilih Rekening BCA Virtual Account.</p>
              </li>
              <li>
                <p>Masukan nomor Virtual Account.</p>
              </li>
              <li>
                <p>Masukan jumlah yang ingin dibayarkan dan pilih benar.</p>
              </li>
              <li>
                <p>
                  ketika muncul konfirmasi pembayaran Pastikan semua info sudah
                  benar dan silahkan pilih Ya.
                </p>
              </li>
            </ul>
          </TabPane>
          <TabPane tabId="2">
            <ul className="timeline">
              <li>
                <p>
                  Silahkan login ke aplikasi KlikBCA Individual,kemudian pilih
                  menu Transfer Dana
                </p>
              </li>
              <li>
                <p>Lalu pilih menu Transfer ke BCA Virtual Account.</p>
              </li>
              <li>
                <p>Masukkan nomor Virtual Account.</p>
              </li>
              <li>
                <p>
                  Masukan nominal yang ingin dibayarkan kemudian klik Lanjutkan.
                </p>
              </li>
              <li>
                <p>Validasi pembayaran kemudian klik kirim.</p>
              </li>
              <li>
                <p>Cetak nomor referensi sebagai bukti transfer Anda.</p>
              </li>
            </ul>
          </TabPane>
        </TabContent>
      </div>
    </div>
  );
};

export default HowPayBCA;
