/* eslint-disable react/no-unescaped-entities */

import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

const HowPayBRI = () => {
  const [activeTab, setActiveTab] = useState('1');
  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };
  return (
    <div className="row">
      <div className="col-12 col-md-12 how-paryment-m">
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '1' })}
              onClick={() => {
                toggle('1');
              }}
            >
              ATM BRI
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '2' })}
              onClick={() => {
                toggle('2');
              }}
            >
              Internet Banking
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="1">
            <ul className="timeline">
              <li>
                <p>Lakukan pembayaran melalui ATM Bank BRI</p>
              </li>
              <li>
                <p>Pilih menu Transaksi Lain</p>
              </li>
              <li>
                <p>Pilih menu Pembayaran dan Pilih menu Lainnya</p>
              </li>
              <li>
                <p>Pilih menu BRIVA</p>
              </li>
              <li>
                <p>
                  Masukkan "Nomor Virtual Account" dan "Nominal" yang akan
                  dibayarkan lalu tekan Benar.
                </p>
              </li>
              <li>
                <p>Pilih Ya untuk memproses pembayaran</p>
              </li>
            </ul>
          </TabPane>
          <TabPane tabId="2">
            <ul className="timeline">
              <li>
                <p>Login pada halaman Internet Banking BRI</p>
              </li>
              <li>
                <p>Pilih menu Pembayaran</p>
              </li>
              <li>
                <p>Pilih menu BRIVA</p>
              </li>
              <li>
                <p>
                  Masukkan "Nomor Virtual Account" dan "Nominal" yang akan
                  dibayarkan lalu tekan Benar.
                </p>
              </li>
              <li>
                <p>Masukkan password Internet Banking BRI</p>
              </li>
              <li>
                <p>Masukkan mToken Internet Banking BRI</p>
              </li>
              <li>
                <p>Anda akan mendapatkan notifikasi pembayaran</p>
              </li>
            </ul>
          </TabPane>
        </TabContent>
      </div>
    </div>
  );
};

export default HowPayBRI;
