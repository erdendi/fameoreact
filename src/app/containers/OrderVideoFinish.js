import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
// import Timer from 'react-compound-timer'
import { Collapse } from 'reactstrap';
import StarRatingComponent from 'react-star-rating-component';
import { formatPrice } from 'utils/price';
import Header from '../components/Header';
import Detail from '../components/Detail';
import Total from '../components/Total';
import Back from '../assets/images/icon-left-arrow-two.svg';
import Settings from '../assets/images/icon-more.svg';
import Play from '../assets/images/search/play.svg';
import ModalVideo from '../components/ModalVideo';
import { getOrderInfo } from '../services/orderInfo';
import { getCookie } from '../helpers';

const OrderVideoFinish = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen) & changeimgarrow();
  const [orderData, setOrderData] = useState([]);
  const [modalVideo, setModalVIdeo] = useState(false);
  const [dataModal, setDataModal] = useState(false);
  const [IsCompletedOrder, SetIsCompletedOrder] = useState(false);
  const { bookId } = useParams();
  const [userId] = useState(Number(getCookie('UserId')));
  const [totalIncome, setTotalIncome] = useState(0);

  const hideModalprop = () => {
    setDataModal(false);
    setModalVIdeo(false);
  };

  const showModalprop = content => {
    // setDataModal(content);
    const params = {
      Id: bookId,
      userId,
    };
    getOrderInfo(params).then(resp => {
      setDataModal(resp);
    });
    setModalVIdeo(true);
  };

  function changeimgarrow() {
    const element = document.getElementById('total-arrow-id');
    const elementtwo = document.getElementById('total-collapse-id');
    if (elementtwo.getAttribute('class') === 'collapse show') {
      element.style.transform = 'rotate(0)';
    } else {
      element.style.transform = 'rotate(180deg)';
    }
  }

  const GotoProfile = async () => {
    window.location = '/profile';
  };

  const getInfo = async () => {
    const params = {
      Id: bookId,
      userId,
    };
    getOrderInfo(params).then(resp => {
      const talentPercentage = resp.PaymentShare;
      const FameoPercentage = 100 - resp.PaymentShare;

      resp.FameoIncome = (resp.TotalPay * FameoPercentage) / 100;
      resp.TalentIncome = (resp.TotalPay * talentPercentage) / 100;

      const totalIncome = (resp.PriceAmount / 100) * (100 - resp.PaymentShare);

      setTotalIncome(totalIncome);

      setOrderData(resp);

      if (resp.Status > 6) {
        SetIsCompletedOrder(true);
      }
    });
  };

  useEffect(() => {
    window.scroll(0, 0);
    getInfo();
  }, []);

  return (
    <>
      <div className="order-information-wrapper ordervideofinish">
        <Header
          content={{
            title: 'Ucapan Untuk John Doe',
            BtnLeftIcon: Back,
            BtnRightIcon: Settings,
            hasSubtitle: true,
            subtitle: 'No. Pesanan Fameo ORD-2019000184',
          }}
        />
        <div className="after-heading-short-wrapper text-white">
          {/* <div className="timer-block">
          <div class="texttimer">Selesaikan order dalam <b>03</b> hr <b>12</b> jam <b>59</b> mnt <img src={Timeimg} className="profile pl-2" alt="prof"/></div>
        </div> */}
          <div className="container detail-block pt-4">
            <div>
              <div className="mb-4">
                <h2 className="section-title mb-3">Video Rekaman Terkirim</h2>
                <div className="box-btn-record-btn">
                  <img src={orderData.ThumbLink} alt="idol" />
                  <div className="play-icon">
                    <img
                      src={Play}
                      alt="play"
                      content={orderData}
                      onClick={showModalprop}
                    />
                  </div>
                </div>
              </div>
              <div>
                <Detail content={orderData} />
              </div>
              <hr />
              {IsCompletedOrder && (
                <div>
                  <div className="detail-item-wrapper">
                    <h2 className="section-title">Feedback untuk saya </h2>
                    <h5 className="detail-title">Rating </h5>
                    <div className="d-flex align-items-center">
                      <div className="star-rating">
                        <StarRatingComponent
                          name="rate1"
                          starCount={orderData.RateVideo}
                          value={orderData.RateVideo}
                          starColor="#FFB4CC"
                          emptyStarColor="#fff"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="detail-item-wrapper">
                    <h5 className="detail-title">Review dari kamu</h5>
                    <div className="d-flex align-items-center">
                      <h2 className="detail-data mb-0">{orderData.Review}</h2>
                    </div>
                  </div>
                </div>
              )}

              <div onClick={toggle}>
                <Total content={{ title: 'Detail Order' }} />
              </div>
              <Collapse isOpen={isOpen} id="total-collapse-id">
                <div className="mb-4">
                  <h2 className="section-title mt-4">Rincian Harga</h2>
                  <p className="rincian-harga">
                    Video Fee
                    <span className="float-right">
                      {formatPrice(orderData.PriceAmount)}
                    </span>
                  </p>
                  <p className="rincian-harga">
                    Fameo Fee
                    <span className="float-right">
                      {formatPrice(orderData.PaymentShare)}
                    </span>
                  </p>
                  <p className="rincian-harga-2">
                    Dana Yang Diterima
                    <span className="float-right">
                      {formatPrice(totalIncome)}
                    </span>
                  </p>
                </div>
              </Collapse>
              <div className="mb-4">
                {/* <SingleButton content={{ url: '/profile', text: 'SELESAI', color: 'pink' }} /> */}
                <button
                  type="button"
                  className="btn btn-router-proceed font-weight-bold"
                  onClick={GotoProfile}
                >
                  SELESAI
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {modalVideo && <ModalVideo contents={dataModal} isHide={hideModalprop} />}
    </>
  );
};

export default OrderVideoFinish;
