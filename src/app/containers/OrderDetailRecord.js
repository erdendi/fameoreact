import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
// import Timer from 'react-compound-timer'
import { Modal, ModalBody } from 'reactstrap';
import VideoRecorder from 'react-video-recorder';
import { getOrderInfo } from '../services/orderInfo';
import {
  UploadVideoFiles,
  UploadMaterialFileByBookId,
} from '../services/uploadVideo';
import { getCookie } from '../helpers';
import LoadingFull from '../components/LoadingFull';

import Header from '../components/Header';
import Detail from '../components/Detail';
import SingleButton from '../components/Button';

import Back from '../assets/images/icon-left-arrow-two.svg';
import Settings from '../assets/images/icon-more.svg';
// import Upload from '../assets/images/upload-two.svg';

import VideoA from '../assets/images/detail/left_arrow_video.png';
// import VideoB from '../assets/images/detail/content1.png';
import VideoC from '../assets/images/video/logo-video.png';
// import Bgvideo from '../assets/images/image_14.png';
import Recbtn from '../assets/images/menu-rec-btn.svg';
// import Pauserec from '../assets/images/pause-rec.svg';
import StopRecOne from '../assets/images/downloadp.svg';
import StopRecTwo from '../assets/images/stop-rec-2.svg';
import StopRecThree from '../assets/images/hapus.svg';
import Uploadrecord from '../assets/images/recordx.svg';
import TimerCountDown from './TimerCountDown';

const OrderDetailRecord = props => {
  const { bookId } = useParams();
  const [confirm, setConfirm] = useState(false);
  const [loading, setLoading] = useState(false);
  // const [filenya, setFilenya] = useState();
  const [userId] = useState(parseInt(getCookie('UserId')));
  // eslint-disable-next-line react/prop-types
  const { className } = props;
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  // Timer Function
  // =======================
  let x;
  let startstop = 0;

  // eslint-disable-next-line no-unused-vars
  function startStop() {
    startstop += 1;
    if (startstop === 1) {
      start();
      document.getElementById('start').innerHTML =
        '<div class="circle-action---"></div>';
      document.getElementById('box-pause-rec').style.display = 'block';
      document.getElementById('pause-right-show-hide').style.display = 'block';
      document.getElementById('pause-right-show-hide-stop').style.display =
        'none';
    } else if (startstop === 2) {
      document.getElementById('start').innerHTML = '';
      document.getElementById('pause-right-show-hide').style.display = 'none';
      document.getElementById('pause-right-show-hide-stop').style.display =
        'block';
      startstop = 0;
      stop();
    }
  }

  const GoToHelp = () => {
    window.location.href = '/help';
  };

  const uploadVideo = e => {
    let video = {};
    if (!e.target) {
      video = e;
    } else {
      e.preventDefault();
      video = e.target.files[0];
    }

    setLoading(true);
    const formData = new FormData();
    formData.append('Files', video);
    formData.append('UserId', userId);

    UploadVideoFiles(formData).then(resp => {
      const r = {
        UserID: userId,
        status: 5,
        FileId: resp,
        Id: bookId,
      };
      // eslint-disable-next-line no-shadow
      UploadMaterialFileByBookId(r).then(resp => {
        if (resp.Status === 'OK') {
          // history.push('/order-video-review/resp.BookId')
          window.location.href = `/order-video-review/${resp.BookId}`;
        }
      });
    });
  };

  // const uploadVideo = (e) => {

  //   let video = {};
  //   if(!e.target){
  //     video = e;
  //   }else{
  //     e.preventDefault() ;
  //     video = e.target.files[0]
  //   }

  //   setLoading(true);
  //   var formData = new FormData();
  //   formData.append("Files", video);
  //   formData.append("UserId", userId);

  //   UploadVideoFiles(formData)
  //     .then(resp => {
  //       let r = {
  //         UserID: userId,
  //         BookId:  parseInt(bookId),
  //         status: 5,
  //         FileId : resp,
  //       }
  //       UpdateMaterialSent(r)
  //       .then(resp => {
  //         if(resp.Status === 'OK'){
  //           history.push('/profile')
  //         }
  //     })
  //   })
  // };

  useEffect(() => {
    const params = {
      Id: bookId,
      userId,
    };
    getOrderInfo(params).then(resp => {
      setConfirm(resp);
      // setLoading(false)
    });
  }, [bookId]);

  function start() {
    x = setInterval(timer, 10);
  } /* Start */

  function stop() {
    clearInterval(x);
  } /* Stop */

  let milisec = 0;
  let sec = 0; /* holds incrementing value */
  let min = 0;
  let hour = 0;

  /* Contains and outputs returned value of  function checkTime */

  let secOut = 0;
  let minOut = 0;
  let hourOut = 0;

  /* Output variable End */

  function timer() {
    /* Main Timer */
    secOut = checkTime(sec);
    minOut = checkTime(min);
    hourOut = checkTime(hour);

    milisec = ++milisec;

    if (milisec === 100) {
      milisec = 0;
      sec = ++sec;
    }

    if (sec === 60) {
      min = ++min;
      sec = 0;
    }

    if (min === 60) {
      min = 0;
      hour = ++hour;
    }

    document.getElementById('sec').innerHTML = secOut;
    document.getElementById('min').innerHTML = minOut;
    document.getElementById('hour').innerHTML = hourOut;
  }

  /* Adds 0 when value is <10 */
  function checkTime(i) {
    if (i < 10) {
      i = `0${i}`;
    }
    return i;
  }

  // eslint-disable-next-line no-unused-vars
  function reset() {
    /* Reset */
    sec = 0;
    min = 0;
    hour = 0;
    document.getElementById('sec').innerHTML = '00';
    document.getElementById('min').innerHTML = '00';
    document.getElementById('hour').innerHTML = '00';
  }

  function showmenurecsubtitle() {
    const boxsubstitlerec = document
      .getElementById('box-substitle-rec')
      .getAttribute('style');
    if (boxsubstitlerec === 'display: none;') {
      document.getElementById('box-substitle-rec').style.display = 'block';
    } else {
      document.getElementById('box-substitle-rec').style.display = 'none';
    }
  }

  // eslint-disable-next-line no-unused-vars
  function stoprecordvideo() {
    document.getElementById('box-download-if-click-stop').style.display =
      'block';
  }

  return (
    <div className="order-information-wrapper">
      {loading && <LoadingFull />}
      <Header
        content={{
          title: confirm ? `Ucapan Untuk ${confirm.To}` : '',
          BtnLeftIcon: Back,
          BtnRightIcon: Settings,
          hasSubtitle: true,
          subtitle: confirm
            ? `No. Pesanan Fameo ${confirm.OrderNo}`
            : 'No. Pesanan Fameo ',
        }}
      />
      <div className="after-heading-short-wrapper text-white">
        <div className="timer-block">
          <div className="timer-block-2">
            <h1 className="section-title text-center">
              Selesaikan Order Sebelum Waktu Habis
            </h1>
            <center>
              <TimerCountDown Deadline={confirm.Deadline} />
            </center>
          </div>

          {/* <div class="texttimer">
          Selesaikan Order Sebelum Waktu Habis

            <TimerCountDown Deadline={confirm.Deadline} />
            <img src={Timeimg} className="profile pl-2" alt="" />
          </div> */}
        </div>
        <div className="container detail-block pt-4">
          <div>
            <div className="border-bottom-wrapper">
              <h2 className="section-title mb-3">Video Rekaman</h2>
              <div className="box-btn-record-btn-record">
                <div className="mb-3" onClick={toggle}>
                  <SingleButton
                    content={{
                      icon: Uploadrecord,
                      text: 'REKAM VIDEO',
                      color: 'pink',
                    }}
                    style={{ padding: '10px' }}
                  />
                </div>
                <div>
                  <input
                    onChange={e => uploadVideo(e)}
                    type="file"
                    encType="multipart/form-data"
                    accept="video/*"
                    className="btn btn-pink c-white"
                  />

                  {/* <SingleButton onClick={(e)=> uploadVideo(e)} content={{ icon: Upload , text: 'UPLOAD FILE', color: 'pink' }} /> */}
                </div>
              </div>
              {!!confirm && (
                <Detail
                  content={{
                    ...confirm,
                    title: 'Judul Project',
                    detail: 'Ulang Tahun Dimas',
                  }}
                />
              )}
            </div>

            <button className="btn btn-reject" onClick={GoToHelp}>
              Order ini bermasalah ? Klik disini.
            </button>
          </div>
        </div>
      </div>
      {loading && <LoadingFull />}
      <Modal
        isOpen={modal}
        toggle={toggle}
        className={className}
        id="ModalRecordPlayer"
      >
        <ModalBody>
          <div className="box-video--">
            <div className="row">
              <div className="col-2 col-md-2">
                <div
                  className="btn-close--"
                  onClick={function () {
                    toggle();
                    stop();
                  }}
                >
                  <img src={VideoA} alt="" />
                </div>
              </div>
              <div className="col-7 col-md-7">
                <div className="media">
                  <img
                    src={confirm.UserImage}
                    className="align-self-center mr-3"
                    alt=""
                  />
                  <div className="media-body">
                    <h4>
                      Ucapan Untuk
                      {confirm.To}
                    </h4>
                    <p>{confirm.OrderNo}</p>
                  </div>
                </div>
              </div>
              <div className="col-3 col-md-3 text-right">
                <div>
                  <img className="vid-logo-det-2" src={VideoC} alt="" />
                </div>
              </div>
            </div>
          </div>
          <VideoRecorder
            onRecordingComplete={videoBlob => uploadVideo(videoBlob)}
          />

          <div
            className="wrapper_btn_sub_box"
            id="box-substitle-rec"
            style={{ display: 'none' }}
          >
            <p>Dari :{confirm.From}</p>
            <p>Untuk :{confirm.To}</p>
            <p className="mb-1">Pesan : </p>
            <p>{confirm.BriefNeeds}</p>

            {/* <p>Nulla maximus pharetra interdum. Ut in justo libero. Sed feugiat, sem ut interdum pharetra, sem ante condimentum magna, vel convallis diam dolor venenatis dui. Ut finibus et dolor bibendum varius.</p>
            <p className="mb-1"><small>Pesan Spesifik</small></p>
            <p>Nulla maximus pharetra interdum. Ut in justo libero.</p> */}
          </div>
          <div
            className="wrapper_btn_sub_box_two"
            id="box-download-if-click-stop"
            style={{ display: 'none' }}
          >
            <p>
              Rekaman Kamu Sudah Selesai.
              <br />
              Silahkan Pilih Langkah Selanjutnya.
            </p>
            <div className="row justify-content-center">
              <div className="col-3">
                <div className="rec-dot-3--">
                  <img src={StopRecOne} style={{ margin: '-2px' }} alt="" />
                </div>
                <p>Simpan Video</p>
              </div>
              <div className="col-3">
                <div className="rec-dot-3--">
                  <img src={StopRecTwo} alt="" />
                </div>
                <p>Rekam Ulang</p>
              </div>
              <div className="col-3">
                <div className="rec-dot-3--">
                  <img
                    src={StopRecThree}
                    style={{ margin: '-4px -6px' }}
                    alt=""
                  />
                </div>
                <p>Hapus Video</p>
              </div>
            </div>
          </div>
          <div className="wrapper_btn_rec_box">
            {/* <div className="row">
              <div className="col-12">
                <div className="rec-dot">
                  <span id="hour">00</span>:
                  <span id="min">00</span>:
                  <span id="sec">00</span>
                </div>
              </div>
            </div> */}
            <div className="row">
              <div className="col-3 text-center p-menuz-rec">
                <div className="rec-dot-2" onClick={showmenurecsubtitle}>
                  <img src={Recbtn} alt="" />
                </div>
              </div>
              {/* <div className="col-6">
                <div className="rec-dot-3" onClick={startStop} id="start"></div>
              </div> */}
              {/* <div className="col-3 text-center p-menuz-rec-2" id="box-pause-rec" style={{display: "none"}}>
                <div className="rec-dot-2" id="pause-right-show-hide">
                  <img src={Pauserec}  alt=""/>
                </div>
                <div className="rec-dot-2" onClick={stoprecordvideo} id="pause-right-show-hide-stop" style={{display: "none"}}>
                  <div></div>
                </div>
              </div> */}
            </div>
          </div>
        </ModalBody>
      </Modal>

      {/* <Modal isOpen={modal} toggle={toggle} className={className} id="ModalRecordPlayer">
        <ModalBody>
          <div className="box-video--">
            <div className="row">
              <div className="col-2 col-md-2">
                <div className="btn-close--" onClick={function(){ toggle(); stop(); }}>
                  <img src={VideoA} alt=""/>
                </div>
              </div>
              <div className="col-7 col-md-7">
                <div className="media">
                  <img src={VideoB} className="align-self-center mr-3"  alt=""/>
                  <div className="media-body">
                    <h4>Ucapan Untuk Dimas</h4>
                    <p>ORD-2019000184</p>
                  </div>
                </div>
              </div>
              <div className="col-3 col-md-3 text-right">
                <div>
                  <img className="vid-logo-det-2" src={VideoC}  alt=""/>
                </div>
              </div>
            </div>
          </div>
          <div className="wrapper_video">
            <img src={Bgvideo} className="img-rec-bg"  alt=""/>
          </div>
          <div className="wrapper_btn_sub_box" id="box-substitle-rec" style={{display: "none"}}>
            <p className="mb-1"><small>Pesan</small></p>
            <p>Nulla maximus pharetra interdum. Ut in justo libero. Sed feugiat, sem ut interdum pharetra, sem ante condimentum magna, vel convallis diam dolor venenatis dui. Ut finibus et dolor bibendum varius.</p>
            <p className="mb-1"><small>Pesan Spesifik</small></p>
            <p>Nulla maximus pharetra interdum. Ut in justo libero.</p>
          </div>
          <div className="wrapper_btn_sub_box_two" id="box-download-if-click-stop" style={{display: "none"}}>
            <p>Rekaman Kamu Sudah Selesai.<br/>Silahkan Pilih Langkah Selanjutnya.</p>
            <div className="row justify-content-center">
              <div className="col-3">
                <div className="rec-dot-3--">
                  <img src={StopRecOne} style={{margin: "-2px"}}  alt=""/>
                </div>
                <p>Simpan Video</p>
              </div>
              <div className="col-3">
                <div className="rec-dot-3--">
                  <img src={StopRecTwo}  alt=""/>
                </div>
                <p>Rekam Ulang</p>
              </div>
              <div className="col-3">
                <div className="rec-dot-3--">
                  <img src={StopRecThree} style={{margin: "-4px -6px"}}  alt=""/>
                </div>
                <p>Hapus Video</p>
              </div>
            </div>
          </div>
          <div className="wrapper_btn_rec_box">
            <div className="row">
              <div className="col-12">
                <div className="rec-dot">
                  <span id="hour">00</span>:
                  <span id="min">00</span>:
                  <span id="sec">00</span>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-3 text-center p-menuz-rec">
                <div className="rec-dot-2" onClick={showmenurecsubtitle}>
                  <img src={Recbtn}  alt=""/>
                </div>
              </div>
              <div className="col-6">
                <div className="rec-dot-3" onClick={startStop} id="start"></div>
              </div>
              <div className="col-3 text-center p-menuz-rec-2" id="box-pause-rec" style={{display: "none"}}>
                <div className="rec-dot-2" id="pause-right-show-hide">
                  <img src={Pauserec}  alt=""/>
                </div>
                <div className="rec-dot-2" onClick={stoprecordvideo} id="pause-right-show-hide-stop" style={{display: "none"}}>
                  <div></div>
                </div>
              </div>
            </div>
          </div>

        </ModalBody>
      </Modal> */}
    </div>
  );
};

export default OrderDetailRecord;
