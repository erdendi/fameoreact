import React, { useState, useEffect } from 'react';
import { useParams, useHistory, Link } from 'react-router-dom';
import SweetAlert from 'react-bootstrap-sweetalert';
import Timer from 'react-compound-timer';
import StarRatingComponent from 'react-star-rating-component';
import Play from '../assets/images/search/play.svg';

import Header from '../components/Header';
import Detail from '../components/Detail';
import { getCookie } from '../helpers';
import Share from '../assets/images/share-two.svg';
import SingleButton from '../components/Button';

import { postUpdatePayment, DeclineOrderBook } from '../services/order';

import Back from '../assets/images/icon-left-arrow-two.svg';
import { getOrderInfo } from '../services/orderInfo';
import LoadingFull from '../components/LoadingFull';
import ModalVideo from '../components/ModalVideo';
import TimerCountDown from './TimerCountDown';

const OrderInformation = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(true);
  const [orderData, setOrderData] = useState([]);
  const [userId] = useState(Number(getCookie('UserId')));
  const [notif, setnotif] = useState(false);
  const [Reason, setReason] = useState();
  const [IsDeclineOrder, setIsDeclineOrder] = useState(false);
  const [IsTalent, setIsTalent] = useState(false);
  const [IsTalentNotCompleted, setIsTalentnotCompleted] = useState(false);
  const [IsBooked, setIsBooked] = useState(false);
  const [IsBookedCompleted, setIsBookedCompleted] = useState(false);
  const [dataModal, setDataModal] = useState(false);
  const [modalVideo, setModalVIdeo] = useState(false);
  const { bookId } = useParams();
  const hideModalprop = () => {
    setDataModal(false);
    setModalVIdeo(false);
  };

  const showModalprop = content => {
    // setDataModal(content);
    const params = {
      bookId,
      userId,
    };
    getOrderInfo(params).then(resp => {
      setDataModal(resp);
    });
    setModalVIdeo(true);
  };

  const GoToHelp = () => {
    window.location.href = '/help';
  };

  const SubmitReason = () => {
    const params = {
      Id: bookId,
      Reason,

      // TalentId: talentId,
      userId,
      // Question:QuestionData
    };
    DeclineOrderBook(params).then(resp => {
      if (resp.Status === 'OK') {
        document.getElementById('txtReason').value = '';
        rejectbox();
        setnotif(true);
      }
    });
  };

  function MetodePembayaran(metode, VA, BankName) {
    if (metode === 'bank_transfer') {
      return (
        <div>
          <h5 className="detail-title">Metode Pembayaran</h5>
          <div className="d-flex align-items-center">
            <h2 className="detail-data mb-0">Virtual Account Bank</h2>
          </div>
          <hr />
          <h5 className="detail-title">Nama Bank</h5>
          <div className="d-flex align-items-center">
            <h2 className="detail-data mb-0"> {BankName}</h2>
          </div>
          <hr />
          <h5 className="detail-title">Nomor Virtual Account</h5>
          <div className="d-flex align-items-center">
            <h2 className="detail-data mb-0">{VA}</h2>
          </div>
          <hr />
        </div>
      );
    }
    return (
      <div>
        <h5 className="detail-title">Metode Pembayaran</h5>
        <div className="d-flex align-items-center">
          <h2 className="detail-data mb-0">{metode}</h2>
        </div>
        <hr />
      </div>
    );
  }

  // function changeimgarrow() {
  //   const element = document.getElementById('total-arrow-id');
  //   const elementtwo = document.getElementById('total-collapse-id');
  //   if (elementtwo.getAttribute('class') === 'collapse show') {
  //     element.style.transform = 'rotate(0)';
  //   } else {
  //     element.style.transform = 'rotate(180deg)';
  //   }
  // }

  function rejectbox() {
    const element = document.getElementById('reject-box');
    if (element.getAttribute('style') === 'display: none;') {
      element.style.display = 'block';
    } else {
      element.style.display = 'none';
    }
  }

  const getInfo = async () => {
    const params = {
      Id: bookId,
      userId,
    };
    getOrderInfo(params).then(resp => {
      setOrderData(resp);

      if (resp.UserOfTalentId === userId) {
        setIsTalent(true);

        if (resp.Status >= 3) {
          setIsTalentnotCompleted(true);
        }

        if (resp.Status <= 2) {
          setIsDeclineOrder(true);
        }
      }

      if (resp.BookedBy === userId) {
        setIsBooked(true);
        if (resp.Status > 5) {
          setIsBookedCompleted(true);
        }

        if (resp.Status <= 2) {
          setIsDeclineOrder(true);
        }
      }

      setLoading(false);
    });
  };

  useEffect(() => {
    window.scroll(0, 0);
    getInfo();
  }, []);

  const PostMulaiMerekam = e => {
    setLoading(true);
    const bookparams = {
      Id: bookId,
      userId,
    };
    let BookIdData = bookId;
    const StatusData = 4;
    let TotalPayData = 0;
    let PotonganData = 0;
    let PriceAmountData = 0;
    let VoucherCodeData = '';

    getOrderInfo(bookparams).then(response => {
      BookIdData = response.Id;
      TotalPayData = response.TotalPay;
      PotonganData = response.Potongan;
      PriceAmountData = response.PriceAmount;
      VoucherCodeData = response.VoucherCode;

      const update = {
        Id: BookIdData,
        Status: StatusData,
        // "TotalPay": parseInt(parseInt(PriceOrderData) - parseInt(resp)),
        TotalPay: parseInt(TotalPayData),
        Potongan: PotonganData,
        PriceAmount: parseInt(PriceAmountData),
        VoucherCode: VoucherCodeData,
        UserId: userId,
      };

      postUpdatePayment(update).then(resp => {
        if (resp.Status === 'OK') {
          setLoading(false);
          history.push(`/order-detail-record/${resp.result.Id}`);
        }
      });
    });
  };

  return (
    <>
      <div className="order-information-wrapper">
        {loading && <LoadingFull />}
        <Header content={{ title: 'Michelle Ziudith', BtnLeftIcon: Back }} />
        <div className="after-heading-short-wrapper text-white">
          {IsBookedCompleted && (
            <div className="container detail-block pt-4">
              <div>
                <div className="mb-4">
                  <h2 className="section-title mb-3">Video Rekaman</h2>
                  <div className="box-btn-record-btn">
                    <img src={orderData.ThumbLink} alt="thumbnail" />
                    <div className="play-icon">
                      <img
                        src={Play}
                        alt="play"
                        content={orderData}
                        onClick={showModalprop}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="actions" style={{ marginTop: '10px' }}>
                <a href={orderData.Link} className="btn-share">
                  <img src={Share} alt="fameo-share" />
                </a>
                <a
                  href={orderData.Link}
                  className="btn btn-pink"
                  style={{ color: '#fff' }}
                  doenload
                >
                  DOWNLOAD VIDEO
                </a>
              </div>
            </div>
          )}

          {IsTalentNotCompleted && (
            <div className="timer-block-2">
              <h1 className="section-title text-center">
                Selesaikan Order Sebelum Waktu Habis
              </h1>
              <center>
                <TimerCountDown Deadline={orderData.Deadline} />
              </center>
            </div>
          )}

          <div className="container detail-block pt-4">
            <div>
              <div className="border-bottom-wrapper">
                <h2 className="section-title">Detail Order</h2>
                <Detail content={orderData} />
                <hr />
                <div className="detail-item-wrapper">
                  <h5 className="detail-title">Status Transaksi</h5>
                  <div className="d-flex align-items-center">
                    {/* {content.TalentPhotos ? <img src={content.TalentPhotos} className="mr-2" alt="fameo-detail" /> : ''} */}
                    <h2 className="detail-data mb-0">
                      {orderData.TransactionStatus}
                    </h2>
                  </div>
                </div>
                <hr />
                {IsBookedCompleted && (
                  <div>
                    <br />
                    <div className="detail-item-wrapper">
                      <h2 className="section-title">Feedback dari kamu </h2>
                      <h5 className="detail-title">Rating </h5>
                      <div className="d-flex align-items-center">
                        <div className="star-rating">
                          <StarRatingComponent
                            name="rate1"
                            starCount={orderData.RateVideo}
                            value={orderData.RateVideo}
                            starColor="#FFB4CC"
                            emptyStarColor="#fff"
                          />
                        </div>
                      </div>
                    </div>

                    <div className="detail-item-wrapper">
                      <h5 className="detail-title">Review dari kamu</h5>
                      <div className="d-flex align-items-center">
                        <h2 className="detail-data mb-0">{orderData.Review}</h2>
                      </div>
                    </div>
                    <hr />
                  </div>
                )}

                {IsDeclineOrder && (
                  <div>
                    <div className="detail-item-wrapper">
                      <h5 className="detail-title">Alasan Talent Menolak</h5>
                      <div className="d-flex align-items-center">
                        <h2 className="detail-data mb-0">{orderData.Reason}</h2>
                      </div>
                    </div>
                    <hr />
                  </div>
                )}

                {IsBooked && (
                  <div>
                    <br />
                    <h2 className="section-title">Informasi Pembayaran</h2>
                    <div className="detail-item-wrapper">
                      <h5 className="detail-title">Jumlah Pembayaran</h5>
                      <div className="d-flex align-items-center">
                        <h2 className="detail-data mb-0">
                          {' '}
                          IDR {orderData.TotalPay}
                        </h2>
                      </div>
                      <hr />
                      {MetodePembayaran(
                        orderData.PayMethod,
                        orderData.SnapToken,
                        orderData.BankName,
                      )}
                    </div>
                  </div>
                )}
              </div>
            </div>
            <div className="mt-5">
              <Timer
                initialTime={14400000}
                startImmediately
                lastUnit="h"
                onStop={() => console.log(<Timer.Hours />)}
                direction="backward"
              >
                {({ getTime }) => (
                  <div className="timer-wrapper">
                    <>
                      <div className="time-values-duplicate display-none">
                        <p className="hour-value">
                          <Timer.Hours />
                        </p>
                        <p className="minute-value">
                          <Timer.Minutes />
                        </p>
                        <p className="second-value">
                          <Timer.Seconds />
                        </p>
                      </div>
                      <div className="time-units-duplicate">
                        <span>HARI</span>
                        <span>JAM</span>
                        <span>MENIT</span>
                      </div>
                      {/* <Link
                      to={{
                        pathname: `/order-detail-record/`+bookId,
                        remainingTime: {
                          remaining: getTime(),
                        },
                      }}
                      className="btn btn-router-proceed font-weight-bold"
                    >
                      MULAI REKAM VIDEO
                    </Link> */}
                    </>
                  </div>
                )}
              </Timer>

              {IsTalentNotCompleted && (
                <button
                  type="button"
                  className="btn btn-router-proceed font-weight-bold"
                  onClick={PostMulaiMerekam}
                >
                  MULAI REKAM VIDEO
                </button>
              )}

              {!IsTalent && orderData.Status === -2 && (
                <Link
                  to={`/refund/${orderData.Id}/${orderData.OrderNo}`}
                  className="btn btn-pink c-white"
                >
                  Ajukan Refund
                </Link>
              )}

              <button
                className="btn btn-reject"
                onClick={IsTalent === true ? rejectbox : GoToHelp}
              >
                Order ini bermasalah ? Klik disini.
              </button>
              {IsTalent && (
                <div id="reject-box" style={{ display: 'none' }}>
                  <div className="reject-box-wrapper" onClick={rejectbox} />
                  <div className="reject-box-style">
                    <p>Alasan Menolak Pesanan</p>
                    <textarea
                      id="txtReason"
                      placeholder="Masukkan alasan di sini..."
                      onChange={e => {
                        setReason(e.target.value);
                      }}
                    />
                    <SingleButton
                      onClick={SubmitReason}
                      content={{
                        text: 'Submit Alasan',
                        color: 'pink',
                        url: '#',
                      }}
                    />
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
      {modalVideo && <ModalVideo contents={dataModal} isHide={hideModalprop} />}
      <SweetAlert
        success
        show={notif}
        title="Submit Successfully"
        onConfirm={() => {
          setnotif(false);
          // window.location.href = "/detail/"+talentId;
        }}
        onCancel={() => {
          setnotif(false);
        }}
        onEscapeKey={() => {
          setnotif(false);
          // window.location.href = "/detail/"+talentId;
        }}
        onOutsideClick={() => {
          setnotif(false);
          // window.location.href = "/detail/"+talentId;
        }}
      />
    </>
  );
};

export default OrderInformation;
