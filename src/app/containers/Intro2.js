   
import React from 'react';
import SingleButton from '../components/Button';

const Intro2 = () => (
  <div className="intro2">
    <div className="content-container">
      <h2>Mau Tanya Idola Kamu?</h2>
      <p>
        GRATISSS TIS TIS TIS!!! Pertanyaan dengan vote terbanyak akan dijawab
        langsung sama idola, pakai video. Jadi, vote vote vote!
      </p>
      <div className="btn-holder">
        <SingleButton
          content={{ text: 'MULAI KUY!', color: 'white', url: '/signup' }}
        />
      </div>
    </div>
  </div>
);

export default Intro2;
