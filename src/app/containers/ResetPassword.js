import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { FormGroup, Label, Input } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';
import { Helmet } from 'react-helmet-async';
import SingleButton from '../components/Button';
import { SendEmailForgetPassword } from '../services/reset-password';
// import { setCookie } from '../helpers';
import LoadingFull from '../components/LoadingFull';

const ResetPassword = () => {
  const [email, setEmail] = useState('');
  const [loading, setLoading] = useState(false);
  const [notif, setnotif] = useState(false);
  const [error, setError] = useState(false);

  const submit = e => {
    e.preventDefault();
    setLoading(true);
    const params = {
      Email: email,
    };
    SendEmailForgetPassword(params)
      .then(resp => {
        setLoading(false);
        if (resp.Status === 'OK') {
          setnotif(true);
        } else if (resp.Status === 'Error') {
          setError(true);
        }
      })
      .catch(err => {
        // eslint-disable-next-line no-console
        console.log(err);
      });
  };
  useEffect(() => {
    window.scroll(0, 0);
  }, []);
  return (
    <>
      <Helmet>
        <title>Reset Password - Fameo</title>
        <meta name="description" content="Reset Password" />
      </Helmet>
      <div className="login-form sign-in overlay-image">
        {loading && <LoadingFull />}
        <div className="container">
          <div className="content-wrapper">
            <p>Lupa Password?</p>
            <div>
              <FormGroup>
                <Label for="exampleSelect">Masukkan Email</Label>
                <Input
                  type="email"
                  name="email"
                  id="email"
                  placeholder="Email"
                  onChange={e => setEmail(e.target.value)}
                />
              </FormGroup>
              <div className="button-holder">
                <SingleButton
                  type="submit"
                  onClick={submit}
                  content={{ text: 'KIRIM LINK RESET PASSWORD', color: 'pink' }}
                />
              </div>
              <Label>
                Kembali ke
                <Link to="/signin">Sign in</Link>
              </Label>
            </div>
          </div>
        </div>
      </div>

      <SweetAlert
        warning
        show={notif}
        onConfirm={() => {
          setnotif(false);
          // window.location.href = "/profile"
        }}
        onCancel={() => {
          setnotif(false);
        }}
        onEscapeKey={() => {
          setnotif(false);
        }}
        onOutsideClick={() => {
          setnotif(false);
        }}
      >
        Link terkirim. Silahkan Cek email kamu
      </SweetAlert>
      <SweetAlert
        warning
        show={error}
        // Text =  "Username atau Password yang anda masukkan salah"
        onConfirm={() => {
          setError(false);
          // window.location.href = "/profile"
        }}
        onCancel={() => {
          setError(false);
        }}
        onEscapeKey={() => {
          setError(false);
        }}
        onOutsideClick={() => {
          setError(false);
        }}
      >
        Email belum terdaftar
      </SweetAlert>
    </>
  );
};

export default ResetPassword;
