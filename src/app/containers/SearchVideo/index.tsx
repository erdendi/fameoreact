import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey, actions } from './slice';
import { selectSearchVideo } from './selectors';
import { searchVideoSaga } from './saga';

import {
  FormGroup,
  Input,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import SkeletonCard from '../../components/SkeletonCard';

import classnames from 'classnames';
import ModalVideo from '../../components/ModalVideo';
import ClipCardWithIcon from '../../components/ClipCardWithIcon';

// Components
import Header from '../../components/Header';
import TaskBar from '../../components/TaskBar';
// import NormalCard from '../components/NormalCard';

// import Icon from '../assets/images/img-idol-1.png'
import Nice from '../../assets/images/icon-nice.svg';
import Chat from '../../assets/images/icon-chat.svg';
import { Helmet } from 'react-helmet-async';

interface Props {}
export function SearchVideo(props: Props) {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: searchVideoSaga });

  const history = useHistory();
  const [activeTab, setActiveTab] = useState('0');
  const [searchTerm, setSearchTerm] = useState('');
  const [offset, setOffset] = useState(0);
  const [fetch] = useState(0);
  const searchVideo = useSelector(selectSearchVideo);
  const { video, isLoading, totalData } = searchVideo;
  // const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();
  const [modalVideo, setModalVIdeo] = useState(false);
  const [dataModal, setDataModal] = useState(false);
  const urlParams = new URLSearchParams(window.location.search);
  const searchParamUrl = urlParams.get('search');
  const talentNameParamUrl = urlParams.get('talentname');

  useEffect(() => {
    if (talentNameParamUrl) {
      dispatch(actions.setSearchParam(talentNameParamUrl || ''));
      dispatch(actions.setTypeParam(activeTab));
      dispatch(actions.getSearchVideoByNameStart());
    } else if (searchParamUrl !== null) {
      dispatch(
        actions.setSearchParam(talentNameParamUrl || searchParamUrl || ''),
      );
      dispatch(actions.setTypeParam(activeTab));
    } else if (searchParamUrl !== '' && searchParamUrl != null) {
      setSearchTerm(searchParamUrl);
    } else {
      dispatch(actions.getSearchVideoStart());
    }
  }, []);

  const doSearch = () => {
    dispatch(actions.getSearchVideoByNameStart());
  };

  const toggle = id => {
    if (String(activeTab) !== String(id)) setActiveTab(id);
    dispatch(actions.setResetOffset('0'));
    dispatch(actions.setTypeParam(id));
    // dispatch(actions.setOffsetParam(offset));
    dispatch(actions.getSearchVideoStart());
  };
  const load = () => {
    const params: any = {};
    if (Number(activeTab) === 1) {
      params.Offset = Number(offset + fetch);
      params.Fetch = fetch;
      setOffset(Number(offset + fetch));
      dispatch(actions.setOffsetParam(String(offset)));
    } else {
      params.type = Number((activeTab as any) - 1);
      params.Offset = Number(offset + 10);
      params.Fetch = fetch;
      setOffset(Number(offset + 10));
      dispatch(actions.setOffsetParam(String(offset)));
    }
    if (searchTerm) {
      params.talentname = searchTerm;
    }

    dispatch(actions.getSearchVideoStart());
  };
  const handleChange = e => {
    setSearchTerm(e.target.value);
    dispatch(actions.setSearchParam(e.target.value));

    // if (e.key === 'Enter') {getSearchVideoStart
    //   // alert('enter')
    //   setSearchTerm(e.target.value);
    //   doSearch();
    // } else {
    //   setSearchTerm(e.target.value);
    //   // fetchData(params)
    // }
  };

  // const QsFilter = (T, Cat, TN) => {
  //   if (Cat === 'Ucapan') {
  //     setActiveTab('2');
  //     // alert(activeTab);
  //     param.type = 1;
  //     param.Offset = Number(offset + limit);
  //     param.Fetch = limit;
  //     setOffset(Number(offset + limit));
  //     toggle('2');
  //   } else if (Cat === 'Reaksi') {
  //     setActiveTab('3');
  //     param.type = 2;
  //     param.Offset = 0;
  //     param.Fetch = limit;

  //     setOffset(Number(offset + limit));
  //     toggle('3');
  //   } else if (Cat === 'TanyaIdola') {
  //     setActiveTab('4');
  //     param.type = 3;
  //     toggle('4');
  //   }

  //   param.TalentIdData = T;
  //   setSearchTerm(TN);

  //   fectData(param);
  // };
  const getsearchVideoToNol = () => {
    dispatch(actions.setOffsetParam('0'));
    dispatch(actions.setFetchParam('10'));
  };
  const ucapan = () => {
    dispatch(actions.setTypeParam('1'));
    dispatch(actions.setOffsetParam('0'));
    dispatch(actions.setFetchParam('10'));
    dispatch(actions.getSearchVideoByNameStart());
  };
  const reaksi = () => {
    dispatch(actions.setTypeParam('2'));
    dispatch(actions.setOffsetParam('0'));
    dispatch(actions.setFetchParam('10'));
    dispatch(actions.getSearchVideoByNameStart());
  };
  const tanyaJawab = () => {
    dispatch(actions.setTypeParam('3'));
    dispatch(actions.setOffsetParam('0'));
    dispatch(actions.setFetchParam('10'));
  };

  const hideModalprop = () => {
    setDataModal(false);
    setModalVIdeo(false);
  };

  const showModalprop = content => {
    setDataModal(content);
    setModalVIdeo(true);
  };
  const toDetailQa = e => {
    history.push(`/details-qa/${e}`);
  };
  const toDetail = e => {
    history.push(`/detail/${e}`);
  };
  return (
    <>
      <Helmet>
        <title>Search Video</title>
        <meta name="description" content="Reset Password" />
      </Helmet>
      <Header content={{ title: 'Cari Video' }} />
      <div className="search">
        <div className="container">
          {/* <Form className="search-bar"> */}
          <div className="search-bar">
            <FormGroup>
              <Input
                type="search"
                name="search"
                id="exampleSearch"
                value={talentNameParamUrl || searchTerm}
                onChange={e => handleChange(e)}
                onKeyDown={e => handleChange(e)}
                placeholder="Cari berdasarkan nama talent"
                // defaultValue='aku'
              />
              <button
                type="button"
                className="search-icon"
                onKeyPress={() => doSearch()}
                onClick={() => doSearch()}
                aria-label="search"
              />
            </FormGroup>
          </div>
          {/* </Form> */}
          <div className="filter-holder">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '0' })}
                  onClick={() => {
                    toggle('0');
                    getsearchVideoToNol();
                  }}
                >
                  Semua
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '1' })}
                  onClick={() => {
                    toggle('1');
                    ucapan();
                  }}
                >
                  Ucapan
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '2' })}
                  onClick={() => {
                    toggle('2');
                    reaksi();
                  }}
                >
                  Reaksi
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '3' })}
                  onClick={() => {
                    toggle('3');
                    tanyaJawab();
                  }}
                >
                  Tanya Jawab
                </NavLink>
              </NavItem>
            </Nav>
          </div>

          <TabContent className="results-holder" activeTab={activeTab}>
            {isLoading && <SkeletonCard totalData={totalData} />}
            <TabPane className="tab-content -m-1" tabId="0">
              {!!video.ListTalentVideo &&
                video.ListTalentVideo.filter(item => item.Link !== null).map(
                  item => (
                    <div className="w-50 d-inline-block p-1" key={item.id}>
                      <ClipCardWithIcon
                        content={item}
                        isShow={showModalprop}
                        propsToDetail={toDetail}
                        imagesIcon="kado"
                      />
                    </div>
                  ),
                )}
              {!!video.ListTalentVideo && (
                <>
                  <button
                    style={{ width: '100%', color: 'white' }}
                    className="btn-pink mb-4"
                    type="submit"
                    onClick={load}
                  >
                    Load More
                  </button>
                </>
              )}
            </TabPane>
            <TabPane className="tab-content" tabId="1">
              {isLoading && <SkeletonCard totalData={totalData} />}
              {!!video.ListTalentVideo &&
                video.ListTalentVideo.filter(item => item.Link !== null).map(
                  (item, i) => (
                    <div className="w-50 d-inline-block p-1" key={i}>
                      <ClipCardWithIcon
                        content={item}
                        isShow={showModalprop}
                        propsToDetail={toDetail}
                        imagesIcon="kado"
                      />
                    </div>
                  ),
                )}
              {!!video.ListTalentVideo && (
                <>
                  <button
                    style={{ width: '100%', color: 'white' }}
                    className="btn-pink mb-4"
                    type="submit"
                    onClick={load}
                  >
                    Load More
                  </button>
                </>
              )}
            </TabPane>

            <TabPane className="tab-content" tabId="2">
              {isLoading && <SkeletonCard totalData={totalData} />}
              {!!video.ReactionVideoList &&
                video.ReactionVideoList.length > 0 &&
                video.ReactionVideoList !== null &&
                video.ReactionVideoList.filter(item => item.Link !== null).map(
                  (item, i) => (
                    <div className="w-50 d-inline-block p-1" key={i}>
                      <ClipCardWithIcon
                        content={item}
                        isShow={showModalprop}
                        propsToDetail={toDetail}
                        imagesIcon="reaction"
                      />
                    </div>
                  ),
                )}
              {!!video.ReactionVideoList && (
                <>
                  <button
                    style={{ width: '100%', color: 'white' }}
                    className="btn-pink mb-4"
                    type="submit"
                    onClick={load}
                  >
                    Load More
                  </button>
                </>
              )}
            </TabPane>
            <TabPane className="tab-content" tabId="3">
              {isLoading && <SkeletonCard totalData={totalData} />}
              {!!video.QuestionVideoList &&
                video.QuestionVideoList.length > 0 &&
                video.QuestionVideoList.filter(item => item.Link !== null).map(
                  (item, i) =>
                    item.Answer ? (
                      <div className="w-50 d-inline-block p-1" key={i}>
                        <ClipCardWithIcon
                          content={item}
                          isShow={showModalprop}
                          propsToDetail={toDetail}
                          imagesIcon="tanya"
                        />
                      </div>
                    ) : (
                      <div
                        key={i}
                        className="w-50 d-inline-block p-1"
                        onClick={() => toDetailQa(item.QuestionVideoId)}
                      >
                        <div className="slider-fan-question">
                          <div className="fan-data">
                            <img
                              src={item.Thumbnails}
                              className="fan-image"
                              alt="fameo-user-icon"
                            />
                            <h5 className="fan-name text-white">
                              {item.TalentId}
                            </h5>
                          </div>
                          <p className="fan-message text-white">
                            {item.Question}
                            <Link
                              to={`/details-qa/${item.QuestionVideoId}`}
                              className="read-more-text"
                            >
                              Read more
                            </Link>
                          </p>
                          <div className="message-action-list">
                            <button className="btn-vote text-center">
                              <div className="vote-wrapper">
                                <img
                                  src={Nice}
                                  className="fan-nice"
                                  alt="fameo-nice"
                                />
                                <span className="fan-votes text-center">
                                  {item.votes} vote
                                </span>
                              </div>
                            </button>
                            <Link to={`/details-qa/${item.QuestionId}`}>
                              <img
                                src={Chat}
                                className="fan-chat"
                                alt="fameo-chat"
                              />
                            </Link>
                          </div>
                        </div>
                      </div>
                    ),
                )}
              {video.QuestionVideoList && (
                <button
                  style={{ width: '100%', color: 'white' }}
                  className="btn-pink mb-4"
                  type="submit"
                  onClick={load}
                >
                  Load More
                </button>
              )}
            </TabPane>
          </TabContent>
        </div>
        <TaskBar active="search" />
      </div>

      {modalVideo && (
        <ModalVideo contents={dataModal as any} isHide={hideModalprop} />
      )}
    </>
  );
}
