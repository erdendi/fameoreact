import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.searchVideo || initialState;

export const selectSearchVideo = createSelector(
  [selectDomain],
  searchVideoState => searchVideoState,
);

export const selectOffsetParam = createSelector(
  [selectDomain],
  searchVideoState => searchVideoState.offsetParam,
);
export const selectTypeParam = createSelector(
  [selectDomain],
  searchVideoState => searchVideoState.typeParam,
);
export const selectFetchParam = createSelector(
  [selectDomain],
  searchVideoState => searchVideoState.fetchParam,
);
export const selectResetOffset = createSelector(
  [selectDomain],
  searchVideoState => searchVideoState.resetOffset,
);

export const selectSearchParam = createSelector(
  [selectDomain],
  searchVideoState => searchVideoState.searchParam,
);
