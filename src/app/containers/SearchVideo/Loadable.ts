/**
 *
 * Asynchronously loads the component for SearchVideo
 *
 */

import { lazyLoad } from 'utils/loadable';

export const SearchTalent = lazyLoad(
  () => import('./index'),
  module => module.SearchVideo,
);
