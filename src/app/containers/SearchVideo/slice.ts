import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { SearchVideoState } from './types';
import { Repo } from 'types/Repo';

// The initial state of the SearchVideo
export const initialState: SearchVideoState = {
  video: [],
  isLoading: false,
  error: null,
  idParam: '0',
  resultsData: [],
  totalData: 0,
  searchParam: '',
  offsetParam: '0',
  fetchParam: 10,
  typeParam: 1,
  resetOffset: '0',
};

const searchVideoSlice = createSlice({
  name: 'searchVideo',
  initialState,
  reducers: {
    getSearchVideoStart(state) {
      state.isLoading = true;
      state.error = null;
    },
    getSearchVideoSuccess(state, action: PayloadAction<Repo[]>) {
      state.resultsData = action.payload;
      state.video = state.resultsData;
      // state.totalData = state.resultsData.ListTalentVideo.length;
      state.isLoading = false;
      state.error = null;
    },
    getSearchVideoFailure(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
    getSearchVideoByNameStart(state) {
      state.isLoading = true;
      state.error = null;
    },
    getSearchVideoByNameSuccess(state, action: PayloadAction<Repo[]>) {
      state.resultsData = action.payload;
      state.video = state.resultsData;
      state.totalData = state.resultsData.ListTalentVideo.length;
      state.isLoading = false;
      state.error = null;
    },
    getSearchVideoByNameFailure(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
    setSearchParam(state, action: PayloadAction<string>) {
      state.searchParam = action.payload;
    },
    setTypeParam(state, action: PayloadAction<string>) {
      state.typeParam = action.payload;
    },
    setOffsetParam(state, action: PayloadAction<string>) {
      state.offsetParam = action.payload;
    },
    setFetchParam(state, action: PayloadAction<string>) {
      state.fetchParam = action.payload;
    },
    setResetOffset(state, action: PayloadAction<string>) {
      state.resetOffset = action.payload;
    },
  },
});

export const { actions, reducer, name: sliceKey } = searchVideoSlice;
