import { Repo } from 'types/Repo';
/* --- STATE --- */
interface SearchVideoState {
  video: any[] | any;
  resultsData: any[] | any;
  isLoading: boolean;
  error: string | null;
  idParam: any;
  searchParam: string;
  totalData: number | null;
  offsetParam: any;
  fetchParam: any;
  typeParam: any;
  resetOffset: any;
}

interface SearchVideoLoaded {
  // ListTalentCategoryModel: [];
  talents: any[] | any;
  resultsData: any[] | any;
  // talentsById: Repo[];
  totalData: number | null;
}

export type { SearchVideoState, SearchVideoLoaded };
