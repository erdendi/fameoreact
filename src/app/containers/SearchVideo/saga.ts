import { call, put, select, takeLatest } from 'redux-saga/effects';
import { actions } from './slice';
import { searchVideo } from 'api/searchVideoAPI';
import {
  selectOffsetParam,
  selectTypeParam,
  selectFetchParam,
  selectSearchParam,
} from './selectors';

export function* fetchSearchVideo() {
  const offsetParam: string = yield select(selectOffsetParam);
  const typeParam: string = yield select(selectTypeParam);
  const fetchParam: string = yield select(selectFetchParam);
  let params;
  if (typeParam === '0') {
    params = { Offset: offsetParam || '', Fetch: fetchParam || '' };
  } else {
    params = {
      type: typeParam || '',
      Offset: offsetParam || '',
      Fetch: fetchParam || '',
    };
  }
  try {
    const response = yield call(searchVideo, params);
    yield put(actions.getSearchVideoSuccess(response));
  } catch (error) {
    yield put(actions.getSearchVideoFailure(error));
  }
}
export function* fetchSearchVideoByName() {
  const offsetParam: string = yield select(selectOffsetParam);
  const typeParam: string = yield select(selectTypeParam);
  const fetchParam: string = yield select(selectFetchParam);
  const searchParam: string = yield select(selectSearchParam);
  let params;
  if (typeParam === '0') {
    params = {
      Offset: offsetParam || '',
      Fetch: fetchParam || '',
      talentname: searchParam || '',
    };
  } else {
    params = {
      type: typeParam || '',
      Offset: offsetParam || '',
      Fetch: fetchParam || '',
      talentname: searchParam || '',
    };
  }

  try {
    const response = yield call(searchVideo, params);
    yield put(actions.getSearchVideoByNameSuccess(response));
  } catch (error) {
    yield put(actions.getSearchVideoByNameFailure(error));
  }
}

export function* searchVideoSaga() {
  // Watches for loadRepos actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(actions.getSearchVideoStart.type, fetchSearchVideo);
  yield takeLatest(
    actions.getSearchVideoByNameStart.type,
    fetchSearchVideoByName,
  );
}
