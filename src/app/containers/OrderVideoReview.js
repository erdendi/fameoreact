import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { getCookie } from '../helpers';
// import { Link } from 'react-router-dom'
// import Timer from 'react-compound-timer'
// import { Collapse, Button, CardBody, Card } from 'reactstrap';
import Header from '../components/Header';
import Detail from '../components/Detail';
import SingleButton from '../components/Button';
import Back from '../assets/images/icon-left-arrow-two.svg';
import Settings from '../assets/images/icon-more.svg';
import Timeimg from '../assets/images/pembayaran/time.svg';
import Play from '../assets/images/search/play.svg';
import { getOrderInfo } from '../services/orderInfo';
import ModalVideo from '../components/ModalVideo';
import { UpdateMaterialSent } from '../services/uploadVideo';
import LoadingFull from '../components/LoadingFull';

const OrderVideoReview = () => {
  const { bookId } = useParams();
  const [confirm, setConfirm] = useState(false);
  const [modalVideo, setModalVIdeo] = useState(false);
  const [dataModal, setDataModal] = useState(false);
  const [userId] = useState(Number(getCookie('UserId')));
  const [loading, setLoading] = useState(false);

  const hideModalprop = () => {
    setDataModal(false);
    setModalVIdeo(false);
  };

  const showModalprop = content => {
    // setDataModal(content);
    const params = {
      Id: bookId,
      userId,
    };
    getOrderInfo(params).then(resp => {
      setDataModal(resp);
    });
    setModalVIdeo(true);
  };

  const SendVideo = () => {
    setLoading(true);
    const params = {
      Id: bookId,
      userId,
    };
    getOrderInfo(params).then(resp => {
      const r = {
        UserID: userId,
        status: 5,
        FileId: resp.FileId,
        Id: bookId,
      };
      UpdateMaterialSent(r).then(resp => {
        setLoading(false);
        if (resp.Status === 'OK') {
          // history.push('/order-video-review/resp.BookId')
          // window.location.href = "/order-video-review/" + resp.BookId;
          window.location.href = `/order-video-finish/${bookId}`;
        }
      });
    });
  };

  const BackToRecord = () => {
    window.location.href = `/order-detail-record/${bookId}`;
  };

  useEffect(() => {
    const params = {
      Id: bookId,
      userId,
    };
    getOrderInfo(params).then(resp => {
      setConfirm(resp);
      // setLoading(false)
    });
  }, [bookId]);

  return (
    <>
      {loading && <LoadingFull />}
      <div className="order-information-wrapper">
        <Header
          content={{
            title: 'Ucapan Untuk John Doe',
            BtnLeftIcon: Back,
            BtnRightIcon: Settings,
            hasSubtitle: true,
            subtitle: 'No. Pesanan Fameo ORD-2019000184',
          }}
        />
        <div className="after-heading-short-wrapper text-white">
          <div className="timer-block">
            <div className="texttimer">
              Selesaikan order dalam
              <b>03</b> hr
              <b>12</b> jam
              <b>59</b> mnt
              <img src={Timeimg} className="profile pl-2" alt="time" />
            </div>
          </div>
          <div className="container detail-block pt-4">
            <div>
              <div className="mb-4">
                <h2 className="section-title mb-3">Video Rekaman</h2>
                <div className="box-btn-record-btn">
                  <img src={confirm.TalentPhotos} alt="video" />
                  <div className="play-icon">
                    <img src={Play} alt="play" onClick={showModalprop} />
                  </div>
                </div>
              </div>
              <div className="border-bottom-wrapper">
                <Detail content={confirm} />
              </div>

              <div className="mb-2">
                <SingleButton
                  onClick={SendVideo}
                  content={{ text: 'KIRIM VIDEO', color: 'pink' }}
                />
              </div>
              <div className="mb-2">
                <button className="btn btn-reject" onClick={BackToRecord}>
                  GANTI VIDEO KEMBALI
                </button>
              </div>

              {/* <div className="border-bottom-wrapper">
              <Detail content={{ title: 'Ucapan Untuk', detail: 'Dimas' }} />
            </div>
            <div className="border-bottom-wrapper">
              <Detail content={{ title: 'Ucapan Dari', detail: 'Jane Doe' }} />
            </div>
            <div className="border-bottom-wrapper">
              <Detail content={{ title: 'Pesan', detail: 'Nulla maximus pharetra interdum. Ut in justo libero. Sed feugiat, sem ut interdum pharetra, sem ante condimentum magna, vel convallis diam dolor venenatis dui. Ut finibus et dolor bibendum varius.' }} />
            </div>
            <div className="border-bottom-wrapper mb-4">
              <Detail content={{ title: 'Pesan Spesifik', detail: 'Nulla maximus pharetra interdum. Ut in justo libero.' }} />
            </div>
            <div className="mb-2">
              <SingleButton content={{ url: '/order-confirmation-total', text: 'KIRIM VIDEO', color: 'pink' }} />
            </div>
            <div className="mb-2">
              <button className="btn btn-reject">GANTI VIDEO KEMBALI</button>
            </div> */}
            </div>
          </div>
        </div>
      </div>
      {modalVideo && <ModalVideo contents={dataModal} isHide={hideModalprop} />}
    </>
  );
};

export default OrderVideoReview;
