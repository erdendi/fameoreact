/* eslint-disable react/react-in-jsx-scope */

/* eslint-disable jsx-a11y/anchor-is-valid */

import Header from '../components/Header';
// import SingleButton from '../components/Button'

import Back from '../assets/images/icon-back.svg';
// import History from '../assets/images/history-coin.png'
import Coinx from '../assets/images/coinxx.svg';

const HistoryCoin = () => (
  <div className="history-coin-wrapper">
    <Header content={{ title: 'History Koin', BtnLeftIcon: Back }} />
    <div className="box-coin">
      <div className="section-1">Total Koin</div>
      <div className="section-2">
        <img src={Coinx} style={{ verticalAlign: 'sub' }} alt="coin" /> 2000
      </div>
    </div>
    <div className="after-heading-wrapper">
      <div className="container">
        <a href="#" className="link-aturan">
          <div className="box-tanda-tanya">?</div>
          Aturan tentang Koin Fameo
        </a>
        <div className="box-text">
          <ul className="timeline">
            <li>
              <p>
                10 Maret 2020 | 19:30
                <br />
                <span>Beli 500 koin emas</span>
              </p>
            </li>
            <li>
              <p>
                8 Maret 2020 | 10:02
                <br />
                <span>Mendapatkan 250 koin emas</span>
              </p>
            </li>
            <li>
              <p>
                3 Maret 2020 | 11:40
                <br />
                <span>Mendapatkan 250 koin emas</span>
              </p>
            </li>
            <li>
              <p>
                3 Maret 2020 | 11:40
                <br />
                <span>Mendapatkan 250 koin emas</span>
              </p>
            </li>
            <li>
              <p>
                3 Maret 2020 | 11:40
                <br />
                <span>Mendapatkan 250 koin emas</span>
              </p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
);

export default HistoryCoin;
