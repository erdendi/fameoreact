import React, { useState, useEffect } from 'react';
// import { Link } from 'react-router-dom'
import { useParams } from 'react-router-dom';
import { Row, Col } from 'reactstrap';
import { formatPrice } from 'utils/price';
import { getCookie } from '../helpers';
import Header from '../components/Header';
import { getOrderInfo } from '../services/orderInfo';
import HowPayBCA from './HowPayBCA';
import HowPayMandiri from './HowPayMandiri';
import HowPayBNI from './HowPayBNI';
import HowPayBRI from './HowPayBRI';
import TimerCountDown from './TimerCountDown';
import LoadingFull from '../components/LoadingFull';

const OrderRincianHowPay = props => {
  const [loading, setLoading] = useState(true);
  const { bookId } = useParams();
  const [confirm, setConfirm] = useState(false);
  const [IsMandiri, setIsMandiri] = useState(false);
  const [IsBCA, setIsBCA] = useState(false);
  const [IsBRI, setIsBRI] = useState(false);
  const [IsBNI, setIsBNI] = useState(false);

  const [userId] = useState(Number(getCookie('UserId')));

  useEffect(() => {
    const params = {
      Id: bookId,
      userId,
    };
    getOrderInfo(params).then(resp => {
      if (resp.PaymentChannel === '702') {
        setIsBCA(true);
      } else if (resp.PaymentChannel === '801') {
        setIsBNI(true);
      } else if (resp.PaymentChannel === '800') {
        setIsBRI(true);
      } else if (resp.PaymentChannel === '802') {
        setIsMandiri(true);
      }

      setConfirm(resp);
      setLoading(false);
    });
  }, [bookId]);

  return (
    <>
      {loading && <LoadingFull />}
      <Header
        content={{
          title: 'Pilih Metode Pembayaran',
          // BtnLeftIcon: Back,
          // BtnRightIcon: Settings,
          hasSubtitle: true,
          subtitle: `No. Pesanan Fameo${confirm.OrderNo}`,
        }}
      />
      <div className="order order-confirmation">
        <div className="container">
          <div className="content-wrapper">
            <section className="content-pembayaran">
              <Row>
                <Col xs="12">
                  <div className="warning-selesaikan mb-4">
                    <div className="timer-block-2">
                      <h1 className="section-title text-center">
                        Selesaikan Pembayaran Sebelum Waktu Habis
                      </h1>
                      <TimerCountDown Deadline={confirm.ExpiredDate} />
                    </div>
                  </div>

                  <div className="rincian-order-section">
                    <div className="form-group">
                      <h4>Rincian Order</h4>
                    </div>
                    <p>Jumlah Pembayaran</p>
                    <h5 className="copy-text---border">
                      {formatPrice(confirm.TotalPay)}
                      <div className="copy-text---">SALIN</div>
                    </h5>
                    <p>Nomor Pesanan</p>
                    <h5 className="copy-text---border">
                      {confirm.OrderNo}
                      <div className="copy-text---">SALIN</div>
                    </h5>
                    <p>Metode Pembayaran</p>
                    <h5 className="copy-text---border">
                      Virtual Account Bank
                      {confirm.BankName}
                    </h5>
                    <p>Nomor Virtual Account</p>
                    <h5>
                      {confirm.SnapToken}
                      <div className="copy-text---">SALIN</div>
                    </h5>
                    <p>Nama Talent</p>
                    <h5>{confirm.TalentNm}</h5>
                    <p>Judul Project</p>
                    <h5>{confirm.ProjectNm}</h5>
                    <p>Ucapan Untuk</p>
                    <h5>{confirm.To}</h5>
                    <p>Ucapan Dari</p>
                    <h5>{confirm.From}</h5>
                    <p>Pesan</p>
                    <h5>{confirm.BriefNeeds}</h5>
                  </div>
                  <hr />
                  <div className="rincian-order-section">
                    <div className="form-group mb-0">
                      <h4>Cara Pembayaran</h4>
                    </div>
                    {IsBCA && <HowPayBCA />}
                    {IsMandiri && <HowPayMandiri />}
                    {IsBNI && <HowPayBNI />}
                    {IsBRI && <HowPayBRI />}
                  </div>
                  <hr />
                  <div className="rincian-order-section-3">
                    <div className="form-group mb-0">
                      Kirim cara pembayaran ke <a href="">email saya</a>
                    </div>
                  </div>
                  <a
                    href="/order-payment-success"
                    className="btn btn-success c-white"
                  >
                    Selesai
                  </a>
                  <hr />
                  <div className="rincian-order-section">
                    <div className="form-group">
                      <h4>Kamu Sudah Membayar?</h4>
                    </div>
                    <h5 className="mb-4">
                      Setelah pembayaran Kamu dikonfirmasi, kami akan mengirim
                      bukti pemesanan ke alamat email Kamu.{' '}
                    </h5>
                  </div>
                </Col>
              </Row>
            </section>
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderRincianHowPay;
