/* eslint-disable linebreak-style */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-shadow */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable func-names */
/* eslint-disable no-param-reassign */
/* eslint-disable radix */
/* eslint-disable linebreak-style */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
// import Timer from 'react-compound-timer'
import { Modal, ModalBody } from 'reactstrap';
import VideoRecorder from 'react-video-recorder';
import { getConfirmation } from '../services/order';
import { UploadVideoFiles, UpdateMaterialSent } from '../services/uploadVideo';
import { getCookie } from '../helpers';

import Header from '../components/Header';
import Detail from '../components/Detail';
import SingleButton from '../components/Button';

import Back from '../assets/images/icon-left-arrow-two.svg';
import Settings from '../assets/images/icon-more.svg';
import Timeimg from '../assets/images/pembayaran/time.svg';
import Upload from '../assets/images/upload-two.svg';

import VideoA from '../assets/images/detail/left_arrow_video.png';
import VideoB from '../assets/images/detail/content1.png';
import VideoC from '../assets/images/video/logo-video.png';
import Bgvideo from '../assets/images/image_14.png';
import Recbtn from '../assets/images/menu-rec-btn.svg';
import Pauserec from '../assets/images/pause-rec.svg';
import StopRecOne from '../assets/images/downloadp.svg';
import StopRecTwo from '../assets/images/stop-rec-2.svg';
import StopRecThree from '../assets/images/hapus.svg';
import Uploadrecord from '../assets/images/recordx.svg';
import PlayBtn from '../assets/images/search/play-2.svg';
import UploadBtn from '../assets/images/search/upload.svg';

const TalentAnswerFinish = props => {
  const { bookId } = useParams();
  const history = useHistory();
  const [confirm, setConfirm] = useState(false);
  const [loading, setLoading] = useState(false);
  // const [filenya, setFilenya] = useState();
  const [userId] = useState(Number(getCookie('UserId')));
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  // Timer Function
  // =======================
  let x;
  let startstop = 0;

  function startStop() {
    startstop += 1;
    if (startstop === 1) {
      start();
      document.getElementById('start').innerHTML =
        '<div class="circle-action---"></div>';
      document.getElementById('box-pause-rec').style.display = 'block';
      document.getElementById('pause-right-show-hide').style.display = 'block';
      document.getElementById('pause-right-show-hide-stop').style.display =
        'none';
    } else if (startstop === 2) {
      document.getElementById('start').innerHTML = '';
      document.getElementById('pause-right-show-hide').style.display = 'none';
      document.getElementById('pause-right-show-hide-stop').style.display =
        'block';
      startstop = 0;
      stop();
    }
  }

  // const changeUploadVideo = (e) => {
  //     setFilenya(e.target.files[0]);
  //     uploadVideo(e);
  //   }

  const uploadVideo = e => {
    let video = {};
    if (!e.target) {
      video = e;
    } else {
      e.preventDefault();
      video = e.target.files[0];
    }

    setLoading(true);
    const formData = new FormData();
    formData.append('Files', video);
    formData.append('UserId', userId);

    UploadVideoFiles(formData).then(resp => {
      const r = {
        UserID: userId,
        BookId: Number(bookId),
        status: 5,
        FileId: resp,
      };
      UpdateMaterialSent(r).then(resp => {
        if (resp.Status === 'OK') {
          history.push('/profile');
        }
      });
    });
  };

  useEffect(() => {
    const params = {
      Id: bookId,
      userId,
    };
    getConfirmation(params).then(resp => {
      setConfirm(resp);
      // setLoading(false)
    });
  }, [bookId]);

  function start() {
    x = setInterval(timer, 10);
  } /* Start */

  function stop() {
    clearInterval(x);
  } /* Stop */

  let milisec = 0;
  let sec = 0; /* holds incrementing value */
  let min = 0;
  let hour = 0;

  /* Contains and outputs returned value of  function checkTime */

  let secOut = 0;
  let minOut = 0;
  let hourOut = 0;

  /* Output variable End */

  function timer() {
    /* Main Timer */
    secOut = checkTime(sec);
    minOut = checkTime(min);
    hourOut = checkTime(hour);

    milisec = ++milisec;

    if (milisec === 100) {
      milisec = 0;
      sec = ++sec;
    }

    if (sec === 60) {
      min = ++min;
      sec = 0;
    }

    if (min === 60) {
      min = 0;
      hour = ++hour;
    }

    document.getElementById('sec').innerHTML = secOut;
    document.getElementById('min').innerHTML = minOut;
    document.getElementById('hour').innerHTML = hourOut;
  }

  /* Adds 0 when value is <10 */
  function checkTime(i) {
    if (i < 10) {
      i = `0${i}`;
    }
    return i;
  }

  function reset() {
    /* Reset */
    sec = 0;
    min = 0;
    hour = 0;
    document.getElementById('sec').innerHTML = '00';
    document.getElementById('min').innerHTML = '00';
    document.getElementById('hour').innerHTML = '00';
  }

  function showmenurecsubtitle() {
    const boxsubstitlerec = document
      .getElementById('box-substitle-rec')
      .getAttribute('style');
    if (boxsubstitlerec === 'display: none;') {
      document.getElementById('box-substitle-rec').style.display = 'block';
    } else {
      document.getElementById('box-substitle-rec').style.display = 'none';
    }
  }

  function stoprecordvideo() {
    document.getElementById('box-download-if-click-stop').style.display =
      'block';
  }

  function closewrapper() {
    document.getElementById('box-download-if-click-stop').style.display =
      'none';
    document.getElementById('box-video').style.display = 'none';
  }

  return (
    <div className="order-information-finish-wrapper">
      <div id="ModalRecordPlayer">
        <ModalBody>
          <div className="box-video--" id="box-video">
            <div className="row no-gutters">
              <div className="col-1 col-md-2">
                <div
                  className="btn-close--"
                  onClick={function () {
                    closewrapper();
                  }}
                >
                  <img src={VideoA} alt="" className="pr-2" />
                </div>
              </div>
              <div className="col-9 col-md-8 px-2">
                <div className="media">
                  <img src={VideoB} className="align-self-center mr-2" alt="" />
                  <div className="media-body">
                    <h4>Jawab Pertanyaan Fans</h4>
                    <p>ORD-2019000184</p>
                  </div>
                </div>
              </div>
              <div className="col-2 col-md-2 text-right">
                <div>
                  <img className="vid-logo-det-2" src={VideoC} alt="" />
                </div>
              </div>
            </div>
          </div>
          <VideoRecorder
            onRecordingComplete={videoBlob => uploadVideo(videoBlob)}
          />

          <div
            className="wrapper_btn_sub_box"
            id="box-substitle-rec"
            style={{ display: 'none' }}
          >
            <p className="mb-1">
              <small>Pesan</small>
            </p>
            <p>
              Nulla maximus pharetra interdum. Ut in justo libero. Sed feugiat,
              sem ut interdum pharetra, sem ante condimentum magna, vel
              convallis diam dolor venenatis dui. Ut finibus et dolor bibendum
              varius.
            </p>
            <p className="mb-1">
              <small>Pesan Spesifik</small>
            </p>
            <p>Nulla maximus pharetra interdum. Ut in justo libero.</p>
          </div>
          <div
            className="wrapper_btn_sub_box_two"
            id="box-download-if-click-stop"
            style={{ display: 'block' }}
          >
            <p>
              Rekaman Kamu Sudah Selesai.
              <br />
              Silahkan Pilih Langkah Selanjutnya.
            </p>
            <div className="row justify-content-center">
              <div className="col-4">
                <div className="rec-dot-3--">
                  <img src={PlayBtn} style={{ margin: '-4px 2px' }} alt="" />
                </div>
                <p>Review Video</p>
              </div>
              <div className="col-4">
                <div className="rec-dot-3--">
                  <img src={StopRecTwo} alt="" />
                </div>
                <p>Rekam Ulang</p>
              </div>
              <div className="col-4">
                <div className="rec-dot-3--">
                  <img src={UploadBtn} style={{ margin: '0 -6px' }} alt="" />
                </div>
                <p>Upload Video</p>
              </div>
            </div>
          </div>
          <div className="wrapper_btn_rec_box">
            {/* <div className="row">
              <div className="col-12">
                <div className="rec-dot">
                  <span id="hour">00</span>:
                  <span id="min">00</span>:
                  <span id="sec">00</span>
                </div>
              </div>
            </div> */}
            <div className="row">
              <div className="col-3 text-center p-menuz-rec">
                <div className="rec-dot-2" onClick={showmenurecsubtitle}>
                  <img src={Recbtn} alt="" />
                </div>
              </div>
              {/* <div className="col-6">
                <div className="rec-dot-3" onClick={startStop} id="start"></div>
              </div> */}
              {/* <div className="col-3 text-center p-menuz-rec-2" id="box-pause-rec" style={{display: "none"}}>
                <div className="rec-dot-2" id="pause-right-show-hide">
                  <img src={Pauserec}  alt=""/>
                </div>
                <div className="rec-dot-2" onClick={stoprecordvideo} id="pause-right-show-hide-stop" style={{display: "none"}}>
                  <div></div>
                </div>
              </div> */}
            </div>
          </div>
        </ModalBody>
      </div>
    </div>
  );
};

export default TalentAnswerFinish;
