/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/alt-text */

import React from 'react';
import Header from '../components/Header';
import SingleButton from '../components/Button';
import Back from '../assets/images/icon-back.svg';
import Sticker from '../assets/images/sticker/1.png';
import Coinx from '../assets/images/coinxx.svg';

function buysticker() {
  const element = document.getElementById('buy-box');
  if (element.getAttribute('style') === 'display: none;') {
    element.style.display = 'block';
  } else {
    element.style.display = 'none';
  }
}
function topup() {
  const element = document.getElementById('topup-box');
  if (element.getAttribute('style') === 'display: none;') {
    element.style.display = 'block';
  } else {
    element.style.display = 'none';
    document.getElementById('buy-box').style.display = 'none';
  }
}

const DetailSticker = () => (
  <>
    <Header content={{ title: 'Sticker Info', BtnLeftIcon: Back, link: '/' }} />
    <div className="box-coin-sticker">
      <div className="section-1">Total Koin</div>
      <div className="section-2">
        <img src={Coinx} style={{ verticalAlign: 'sub' }} /> 2000
      </div>
    </div>
    <div className="order detail-sticker">
      <div className="container">
        <div className="content-wrapper">
          <div className="box-store">
            <div className="sub-box-store">
              <div className="row">
                <div className="col-6">
                  <img src={Sticker} />
                </div>
                <div className="col-6 pl-0">
                  <h3>Sticker OK</h3>
                  <h6>
                    <img src={Coinx} />
                    10
                  </h6>
                </div>
              </div>
            </div>

            <div className="update-form-wrapper" onClick={buysticker}>
              <SingleButton
                content={{ url: '#', text: 'BELI STICKER', color: 'pink' }}
              />
            </div>
          </div>
        </div>
      </div>
      <hr />
      <div className="desc-coin">
        <h4>Deskripsi Produk</h4>
        <p>
          Nulla maximus pharetra interdum. Ut in justo libero. Sed feugiat, sem
          ut interdum pharetra, sem ante condimentum magna, vel convallis diam
          dolor venenatis dui. Ut finibus et dolor bibendum varius. Nulla
          maximus pharetra interdum. Ut in justo libero.
        </p>
        <p>
          Nulla maximus pharetra interdum. Ut in justo libero. Sed feugiat, sem
          ut interdum pharetra, sem ante condimentum magna, vel convallis diam
          dolor venenatis dui. Ut finibus et dolor bibendum varius. Nulla
          maximus pharetra interdum. Ut in justo libero.
        </p>
      </div>
      <div id="buy-box" style={{ display: 'none' }}>
        <div className="reject-box-wrapper" onClick={buysticker} />
        <div className="reject-box-style">
          <div className="line----" onClick={buysticker} />
          <p>Ingin membeli Sticker Fameo ?</p>
          <h1>Sticker OK</h1>
          <div className="row">
            <div className="col-6" onClick={topup}>
              <SingleButton
                content={{ url: '#', text: 'BELI', color: 'white' }}
              />
            </div>
            <div className="col-6" onClick={buysticker}>
              <SingleButton
                content={{ url: '#', text: 'BATAL', color: 'white' }}
              />
            </div>
          </div>
        </div>
      </div>
      <div id="topup-box" style={{ display: 'none' }}>
        <div className="reject-box-wrapper" onClick={topup} />
        <div className="reject-box-style">
          <div className="line----" onClick={topup} />
          <p style={{ color: '#f44336' }}>Koin Kamu tidak cukup!</p>
          <h1>Topup Koin Sekarang?</h1>
          <div className="row">
            <div className="col-6">
              <SingleButton
                content={{ url: '#', text: 'TOPUP', color: 'white' }}
              />
            </div>
            <div className="col-6" onClick={topup}>
              <SingleButton
                content={{ url: '#', text: 'BATAL', color: 'white' }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
);

export default DetailSticker;
