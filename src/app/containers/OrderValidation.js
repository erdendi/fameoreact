   
import React from 'react';
import { Form, FormGroup, Input } from 'reactstrap';
import Header from '../components/Header';
import SingleButton from '../components/Button';
import Back from '../assets/images/icon-back.svg';

const OrderValidation = () => (
  <>
    <Header
      content={{ title: 'Validasi Akun', BtnLeftIcon: Back, link: '/' }}
    />
    <div className="order order-validation">
      <div className="container">
        <div className="content-wrapper">
          <h2 className="title c-white">Validasi Akunmu</h2>
          <p className="subtitle c-white">
            Validasi akun dengan mengisi nomor handphone
          </p>
          <Form>
            <FormGroup>
              <Input
                type="number"
                name="number"
                id="exampleNumber"
                className="textbox"
                placeholder="No. Handphone"
              />
            </FormGroup>
            <div className="button-holder">
              <SingleButton
                content={{ text: 'VALIDASI AKUN SAYA', color: 'white' }}
              />
            </div>
          </Form>
        </div>
      </div>
    </div>
  </>
);

export default OrderValidation;
