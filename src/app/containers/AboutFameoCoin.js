/* eslint-disable jsx-a11y/alt-text */

import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';
import Header from '../components/Header';
import Back from '../assets/images/icon-back.svg';
import History from '../assets/images/history-coin.png';
import BgCoin from '../assets/images/bg-about-coin.png';
import Coinx from '../assets/images/coinxx.svg';

const AboutFameoCoin = () => {
  const [activeTab, setActiveTab] = useState('1');
  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  return (
    <div className="fameo-coin-wrapper">
      <Header
        content={{
          title: 'Tentang Koin Fameo',
          BtnLeftIcon: Back,
          BtnRightIcon: History,
        }}
      />
      <div className="box-coin">
        <div className="section-1">Total Koin</div>
        <div className="section-2">
          <img src={Coinx} style={{ verticalAlign: 'sub' }} /> 2000
        </div>
      </div>
      <div className="bg-about-coin">
        <img src={BgCoin} />
      </div>
      <div className="after-heading-wrapper">
        <div className="container">
          <div className="how-paryment-m">
            <Nav tabs>
              <NavItem style={{ width: '40%' }}>
                <NavLink
                  className={classnames({ active: activeTab === '1' })}
                  onClick={() => {
                    toggle('1');
                  }}
                >
                  Tentang Koin Fameo
                </NavLink>
              </NavItem>
              <NavItem style={{ width: '60%' }}>
                <NavLink
                  className={classnames({ active: activeTab === '2' })}
                  onClick={() => {
                    toggle('2');
                  }}
                >
                  Cara Mendapatkan Koin Fameo
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={activeTab}>
              <TabPane tabId="1">
                <h2>Tentang Koin Fameo</h2>
                <p>
                  Nulla maximus pharetra interdum. Ut in justo libero. Sed
                  feugiat, sem ut interdum pharetra, sem ante condimentum magna,
                  vel convallis diam dolor venenatis dui.
                </p>
                <p>
                  Nulla maximus pharetra interdum. Ut in justo libero. Sed
                  feugiat, sem ut interdum pharetra, sem ante condimentum magna,
                  vel convallis diam dolor venenatis dui.
                </p>
                <p>
                  Nulla maximus pharetra interdum. Ut in justo libero. Sed
                  feugiat, sem ut interdum pharetra, sem ante condimentum magna,
                  vel convallis diam dolor venenatis dui.
                </p>
              </TabPane>
              <TabPane tabId="2">
                <h2>Cara Mendapatkan Koin Fameo</h2>
                <p>
                  Nulla maximus pharetra interdum. Ut in justo libero. Sed
                  feugiat, sem ut interdum pharetra, sem ante condimentum magna,
                  vel convallis diam dolor venenatis dui.
                </p>
              </TabPane>
            </TabContent>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutFameoCoin;
