import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import VideoSwiper from '../components/VideoSwiper';
import CardSwiper from '../components/CardSwiper';
import EntertainmentTabs from '../components/EntertainmentTabs';
import SwiperQuestions from './FanQuestions';
import Header from '../components/Header';
import TaskBar from '../components/TaskBar';
import { GetBannerData } from '../services/Banner';
import Loading from '../components/Loading';
import {
  //  getDataHome,
  getCategoryByID,
  getAllQuestion,
} from '../services/home';

const contentSwiperData2 = {
  headingData: {
    heading: 'Featured',
    headingColor: 'violet',
  },
  isNormalCard: true,
  urlAPI: 'https://apifameo.azurewebsites.net/api/home/index?categoryid=25',
};

const entertainmentContent = {
  data: [
    {
      id: 61,
      tabTitle: 'Popular',
      icon: require('../assets/images/icon-popular.svg'),
      iconName: 'icon popular',
    },
    {
      id: 11,
      tabTitle: 'Film & TV',
      icon: require('../assets/images/icon-film.svg'),
      iconName: 'icon film',
    },
    {
      id: 51,
      tabTitle: 'Athlete',
      icon: require('../assets/images/basketball.svg'),
      iconName: 'icon basketball',
    },
    {
      id: 22,
      tabTitle: 'Musician',
      icon: require('../assets/images/icon-sing.svg'),
      iconName: 'icon musician',
    },
    {
      id: 59,
      tabTitle: 'International',
      icon: require('../assets/images/Icon -international.svg'),
      iconName: 'icon musician',
    },
    {
      id: 2,
      tabTitle: 'Influencer',
      icon: require('../assets/images/influencer-color.svg'),
      iconName: 'icon influencer',
    },
    {
      id: 52,
      tabTitle: 'New',
      icon: require('../assets/images/icon-new.svg'),
      iconName: 'icon joystick',
    },
  ],
  card: false,
};

const Home = () => {
  const [talentsFeature, setTalentsFeature] = useState(false);
  const [entertainmentContent_, setEntertainmentContent_] = useState(
    entertainmentContent,
  );
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [childLoading, setChildLoading] = useState(true);
  const [quot, setQuot] = useState(false);
  const [BannerData, setBannerData] = useState();

  const childLoading_ = () => {
    setChildLoading(true);
  };
  const Idnya = async e => {
    const params = {
      categoryid: e,
      limit: 20,
    };
    await getCategoryByID(params)
      .then(resp => {
        setEntertainmentContent_({
          ...entertainmentContent_,
          card: resp.ListTalentCategoryModel,
        });
        setChildLoading(false);
      })
      .catch(err => {
        setError(true);
        console.log(err);
      });
  };

  useEffect(() => {
    // fetchUserInfo(['25', '20']).then(a => console.log(JSON.stringify(a)));
    const getData = async () => {
      const param = {
        BannerCategory: 'HomeScr',
      };
      await GetBannerData(param)
        .then(resp => {
          setBannerData(resp.Result);
        })
        .catch(err => {
          setError(true);
          console.log(err);
        });

      const par1 = {
        categoryid: 61,
        limit: 20,
      };
      const par2 = {
        categoryid: 25,
        limit: 20,
      };

      await Promise.all([getCategoryByID(par1), getCategoryByID(par2)])
        .then(resp => {
          const listEntertaintment = resp[0].ListTalentCategoryModel;
          const listTalentFeature = resp[1].ListTalentCategoryModel;

          setEntertainmentContent_({
            ...entertainmentContent_,
            card: listEntertaintment,
          });

          setTalentsFeature(listTalentFeature);
          setChildLoading(false);
        })
        .catch(err => {
          console.log(err);
        });

      await getAllQuestion()
        .then(resp => {
          setQuot(resp.getCurrentData);
        })
        .catch(err => {
          // setError(true);
          console.log(err);
        });

      setLoading(false);
    };

    getData();
  }, []);
  return (
    <>
      <Helmet>
        <title>Home</title>
        <meta name="description" content="Home" />
      </Helmet>
      {error && <p>Please check the internet connection...</p>}
      {loading && <Loading />}
      {!error && !loading && (
        <div className="home">
          <Header
            content={{
              notificationTitle: ' Welcome to Fameo!',
              Notification: true,
            }}
          />

          {!BannerData && <Loading />}
          {BannerData && (
            <div className="container">
              <VideoSwiper content={BannerData} />
            </div>
          )}

          {!talentsFeature && <Loading />}
          {talentsFeature && (
            <div className="section-featured">
              <CardSwiper
                contentSwiper={contentSwiperData2}
                dataTalent={talentsFeature}
              />
            </div>
          )}
          {!talentsFeature && <Loading />}
          {entertainmentContent_.card && (
            <EntertainmentTabs
              items={entertainmentContent_}
              propsID={Idnya}
              childLoad={childLoading_}
              isLoad={childLoading}
            />
          )}

          {loading && <Loading />}
          {!!quot && <SwiperQuestions content={quot} />}

          <TaskBar active="home" />
        </div>
      )}
    </>
  );
};

export default Home;
