/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-bitwise */

import React, { useState } from 'react';
import { Collapse } from 'reactstrap';

import Header from '../components/Header';
// import SingleButton from '../components/Button'

import Back from '../assets/images/icon-back.svg';

const Faq = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggleA = () => setIsOpen(!isOpen) & arrowA();
  const toggleB = () => setIsOpen(!isOpen) & arrowB();
  const toggleC = () => setIsOpen(!isOpen) & arrowC();
  const toggleD = () => setIsOpen(!isOpen) & arrowD();
  const toggleE = () => setIsOpen(!isOpen) & arrowE();

  function arrowA() {
    const element = document.getElementById('plus-one');
    const elementtwo = document.getElementById('desc-one');
    if (elementtwo.getAttribute('class') === 'collapse show') {
      element.innerHTML = '+';
    } else {
      element.innerHTML = '-';
    }
  }
  function arrowB() {
    const element = document.getElementById('plus-two');
    const elementtwo = document.getElementById('desc-two');
    if (elementtwo.getAttribute('class') === 'collapse show') {
      element.innerHTML = '+';
    } else {
      element.innerHTML = '-';
    }
  }
  function arrowC() {
    const element = document.getElementById('plus-three');
    const elementtwo = document.getElementById('desc-three');
    if (elementtwo.getAttribute('class') === 'collapse show') {
      element.innerHTML = '+';
    } else {
      element.innerHTML = '-';
    }
  }
  function arrowD() {
    const element = document.getElementById('plus-four');
    const elementtwo = document.getElementById('desc-four');
    if (elementtwo.getAttribute('class') === 'collapse show') {
      element.innerHTML = '+';
    } else {
      element.innerHTML = '-';
    }
  }
  function arrowE() {
    const element = document.getElementById('plus-five');
    const elementtwo = document.getElementById('desc-five');
    if (elementtwo.getAttribute('class') === 'collapse show') {
      element.innerHTML = '+';
    } else {
      element.innerHTML = '-';
    }
  }

  return (
    <div className="faq-coin-wrapper">
      <Header
        content={{ title: 'Aturan Tentang Koin Fameo', BtnLeftIcon: Back }}
      />
      <div className="after-heading-wrapper">
        <div className="container">
          <div className="box-text">
            <div className="box-faq" onClick={toggleA}>
              <div className="main-box">
                Q : Apa itu Koin Fameo
                <div id="plus-one" className="plus-one-css">
                  +
                </div>
              </div>
              <Collapse isOpen={isOpen} id="desc-one">
                <div>
                  <p>
                    Nulla maximus pharetra interdum. Utin justo libero. Sed
                    feugiat, sem utinterdum pharetra, sem ante condimentum
                    magna, vel convallis diam dolor venenatis dui. Ut finibuset
                    dolor bibendum varius.
                  </p>
                </div>
              </Collapse>
            </div>
            <div className="box-faq" onClick={toggleB}>
              <div className="main-box">
                Q : Bagaimana cara mendapatkannya?
                <div id="plus-two" className="plus-one-css">
                  +
                </div>
              </div>
              <Collapse isOpen={isOpen} id="desc-two">
                <div>
                  <p>
                    Nulla maximus pharetra interdum. Utin justo libero. Sed
                    feugiat, sem utinterdum pharetra, sem ante condimentum
                    magna, vel convallis diam dolor venenatis dui. Ut finibuset
                    dolor bibendum varius.
                  </p>
                </div>
              </Collapse>
            </div>
            <div className="box-faq" onClick={toggleC}>
              <div className="main-box">
                Q : Apa itu Koin Fameo
                <div id="plus-three" className="plus-one-css">
                  +
                </div>
              </div>
              <Collapse isOpen={isOpen} id="desc-three">
                <div>
                  <p>
                    Nulla maximus pharetra interdum. Utin justo libero. Sed
                    feugiat, sem utinterdum pharetra, sem ante condimentum
                    magna, vel convallis diam dolor venenatis dui. Ut finibuset
                    dolor bibendum varius.
                  </p>
                </div>
              </Collapse>
            </div>
            <div className="box-faq" onClick={toggleD}>
              <div className="main-box">
                Q : Apa itu Koin Fameo
                <div id="plus-four" className="plus-one-css">
                  +
                </div>
              </div>
              <Collapse isOpen={isOpen} id="desc-four">
                <div>
                  <p>
                    Nulla maximus pharetra interdum. Utin justo libero. Sed
                    feugiat, sem utinterdum pharetra, sem ante condimentum
                    magna, vel convallis diam dolor venenatis dui. Ut finibuset
                    dolor bibendum varius.
                  </p>
                </div>
              </Collapse>
            </div>
            <div className="box-faq" onClick={toggleE}>
              <div className="main-box">
                Q : Apa itu Koin Fameo
                <div id="plus-five" className="plus-one-css">
                  +
                </div>
              </div>
              <Collapse isOpen={isOpen} id="desc-five">
                <div>
                  <p>
                    Nulla maximus pharetra interdum. Utin justo libero. Sed
                    feugiat, sem utinterdum pharetra, sem ante condimentum
                    magna, vel convallis diam dolor venenatis dui. Ut finibuset
                    dolor bibendum varius.
                  </p>
                </div>
              </Collapse>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Faq;
