/* eslint-disable react/button-has-type */
/* eslint-disable react/no-array-index-key */

import React from 'react';
import PropTypes from 'prop-types';
// import Swiper from 'react-id-swiper';
// import axios from 'axios';
import { Link } from 'react-router-dom';
// import 'swiper/css/swiper.css';
// import { getAllQuestion } from '../services/home';
// import Loading from '../components/Loading';

// import Icon from '../assets/images/img-idol-1.png'
import Nice from '../assets/images/icon-nice.svg';
import Chat from '../assets/images/icon-chat.svg';

// const swiperData = [
//   {
//     image: Icon,
//     name: "John Doe",
//     message: "When you were young what was your...",
//     votes: "12"
//   },
//   {
//     image: Icon,
//     name: "Jane Doe",
//     message: "When you were young what was your...",
//     votes: "12"
//   },
//   {
//     image: Icon,
//     name: "Jenny Doe",
//     message: "When you were young what was your...",
//     votes: "12"
//   }
// ]

// const params = {
//   slidesPerView: 2.1,
//   slidesPerGroup: 2.1,
//   spaceBetween: 5,
//   rebuildOnUpdate: true,
// }

function SwiperQuestions({ content }) {
  return (
    <>
      <div className="d-wrap">
        {/* {loading && <Loading/> } */}
        {content.slice(1).map((item, i) => (
          <div key={i}>
            <div className="slider-fan-question">
              <div className="fan-data">
                <img
                  src={item.UserProfPicLink}
                  className="fan-image"
                  alt="fameo-user-icon"
                />
                <h5 className="fan-name text-white">{item.UserName}</h5>
              </div>
              <p className="fan-message text-white">
                {item.Question}
                <Link to="/" className="read-more-text">
                  Read more
                </Link>
              </p>
              <div className="message-action-list">
                <button className="btn-vote text-center">
                  <div className="vote-wrapper">
                    <img src={Nice} className="fan-nice" alt="fameo-nice" />
                    <span className="fan-votes text-center">
                      {item.TotalLike}
                    </span>
                  </div>
                </button>
                <Link to="/">
                  <img src={Chat} className="fan-chat" alt="fameo-chat" />
                </Link>
              </div>
            </div>
          </div>
        ))}
      </div>
    </>
  );
}

const FanQuestionsTalentProfile = ({ comment }) => (
  <div className="fan-questions">
    <SwiperQuestions content={comment} />
  </div>
);

FanQuestionsTalentProfile.propTypes = {
  comment: PropTypes.array.isRequired,
};
SwiperQuestions.propTypes = {
  content: PropTypes.array.isRequired,
};

export default FanQuestionsTalentProfile;
