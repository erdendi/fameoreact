/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import {
  Form,
  FormGroup,
  Input,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col,
} from 'reactstrap';
import classnames from 'classnames';

// Components
import Header from '../components/Header';
// import TaskBar from '../components/TaskBar'

import Back from '../assets/images/icon-back.svg';
import Custom from '../assets/images/icon-check-success.svg';
import SA from '../assets/images/sticker/1.png';
import SB from '../assets/images/sticker/2.png';
import SC from '../assets/images/sticker/3.png';

const Sticker = () => {
  const people = [
    {
      name: 'Michelle Ziudith',
      category: 'Film & TV',
    },
    {
      name: 'Joe SNow',
      category: 'Film & TV',
    },
  ];

  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const handleChange = event => {
    setSearchTerm(event.target.value);
  };

  useEffect(() => {
    const results = people.filter(person =>
      person.name.toLowerCase().includes(searchTerm.toLowerCase()),
    );

    setSearchResults(results);
  }, [searchTerm, people]);

  const [activeTab, setActiveTab] = useState('1');

  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  return (
    <>
      <Header content={{ title: 'Fameo Sticker', BtnLeftIcon: Back }} />
      <div className="sticker-wrapper">
        <div className="container">
          <Form className="search-bar">
            <FormGroup>
              <Input
                type="search"
                name="search"
                id="exampleSearch"
                value={searchTerm}
                onChange={handleChange}
                placeholder="Cari sticker"
              />
              <span className="search-icon" />
            </FormGroup>
          </Form>
          <div className="filter-holder">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '1' })}
                  onClick={() => {
                    toggle('1');
                  }}
                >
                  Semua
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '2' })}
                  onClick={() => {
                    toggle('2');
                  }}
                >
                  Baru
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '3' })}
                  onClick={() => {
                    toggle('3');
                  }}
                >
                  Populer
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '4' })}
                  onClick={() => {
                    toggle('4');
                  }}
                >
                  VIP
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '5' })}
                  onClick={() => {
                    toggle('5');
                  }}
                >
                  Gift
                </NavLink>
              </NavItem>
            </Nav>
          </div>
          <TabContent className="results-holder" activeTab={activeTab}>
            <TabPane className="tab-content" tabId="1">
              <div className="card-listing">
                <div className="card-holder">
                  <div className="custom-card clip-card">
                    <img src={SA} />
                  </div>
                  <p>Sticker OK</p>
                  <div className="coin-num">
                    <img src={Custom} />
                    300
                  </div>
                </div>
                <div className="card-holder">
                  <div className="custom-card clip-card">
                    <img src={SB} />
                  </div>
                  <p>Sticker Top</p>
                  <div className="coin-num">
                    <img src={Custom} />
                    10
                  </div>
                </div>
                <div className="card-holder">
                  <div className="custom-card clip-card">
                    <img src={SC} />
                  </div>
                  <p>Sticker Good</p>
                  <div className="coin-num">
                    <img src={Custom} />
                    700
                  </div>
                </div>
                <div className="card-holder">
                  <div className="custom-card clip-card">
                    <img src={SA} />
                  </div>
                  <p>Sticker Wow</p>
                  <div className="coin-num">
                    <img src={Custom} />
                    100
                  </div>
                </div>
                <div className="card-holder">
                  <div className="custom-card clip-card">
                    <img src={SB} />
                  </div>
                  <p>Sticker Top</p>
                  <div className="coin-num">
                    <img src={Custom} />
                    10
                  </div>
                </div>
                <div className="card-holder">
                  <div className="custom-card clip-card">
                    <img src={SC} />
                  </div>
                  <p>Sticker Good</p>
                  <div className="coin-num">
                    <img src={Custom} />
                    700
                  </div>
                </div>
              </div>
            </TabPane>
            <TabPane className="tab-content" tabId="2">
              2
            </TabPane>
            <TabPane className="tab-content" tabId="3">
              3
            </TabPane>
            <TabPane className="tab-content" tabId="4">
              4
            </TabPane>
            <TabPane className="tab-content" tabId="5">
              4
            </TabPane>
          </TabContent>
        </div>
      </div>
    </>
  );
};

export default Sticker;
