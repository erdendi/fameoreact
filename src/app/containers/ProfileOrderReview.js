import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import StarRatingComponent from 'react-star-rating-component';
import { Form, FormGroup, Label, Input } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { faStar } from '@fortawesome/free-solid-svg-icons';
import Header from '../components/Header';

import Back from '../assets/images/icon-left-arrow-two.svg';
import Settings from '../assets/images/icon-settings.svg';
import LoadingFull from '../components/LoadingFull';
import { postUpdateCompletedBooking } from '../services/order';
import SingleButton from '../components/Button';
import { getCookie } from '../helpers';

const ProfileOrderReview = () => {
  const [rating, setRating] = useState('1');
  const [loading, setLoading] = useState(false);
  const [Review, setReview] = useState();
  const { bookId } = useParams();
  const [userId] = useState(Number(getCookie('UserId')));

  function onStarClick(nextValue) {
    setRating(nextValue);
  }

  const PostData = e => {
    e.preventDefault();
    setLoading(true);
    const data = {
      // "BookCategory": parseInt(bookCat) || "",
      // "ProjectNm": projekNM || "",
      // "TalentId": parseInt(talentId),
      // "BriefNeeds": briefNeed || "",
      // "From": from || "",
      // "To": to || "",
      // "UserId": userId,
      // "IsPublic": 1,
      Rate: rating,
      Review,
      Id: bookId,
      Status: 7,
      UserId: userId,
    };

    postUpdateCompletedBooking(data).then(resp => {
      setLoading(false);
      if (resp.Status === 'OK') {
        // history.push('/order-confirmation/' + resp.BookId);
        window.location.href = '/profile';
      }
    });
  };

  return (
    <>
      {loading && <LoadingFull />}
      <div className="order-review-wrapper">
        <Header
          content={{
            title: 'Michelle Ziudith',
            BtnLeftIcon: Back,
            BtnRightIcon: Settings,
          }}
        />
        <div className="after-heading-short-wrapper">
          <div className="container order-container">
            <div className="rating-wrapper">
              <h2 className="section-title text-center">Pilih Rating</h2>
              {/* <h2 className="hello">Rating from state: {rating}</h2> */}
              <div className="star-rating">
                <StarRatingComponent
                  name="rate1"
                  starCount={5}
                  value={rating}
                  starColor="#FFB4CC"
                  emptyStarColor="#fff"
                  onStarClick={onStarClick}
                  renderStarIcon={() => (
                    <FontAwesomeIcon icon={faStar} className="star-icon" />
                  )}
                />
              </div>
            </div>
            <div className="review-wrapper">
              <Form className="form-review">
                <FormGroup>
                  <Label for="exampleNumber" className="lbl-review">
                    Review
                  </Label>
                  <Input
                    type="text"
                    name="number"
                    className="input-review"
                    onChange={e => {
                      setReview(e.target.value);
                    }}
                    id="exampleNumber"
                    placeholder="Tulis review pesanan Kamu"
                  />
                </FormGroup>
                <SingleButton
                  onClick={e => PostData(e)}
                  content={{ text: 'KIRIM REVIEW', color: 'pink' }}
                />
              </Form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProfileOrderReview;
