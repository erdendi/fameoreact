   

import React from 'react';
import { useParams } from 'react-router-dom';
import { formatPrice } from 'utils/price';
import Header from '../components/Header';
import SingleButton from '../components/Button';
import Check from '../assets/images/icon-check-success.svg';

const OrderWithdrawSuccess = () => {
  const { Nominal } = useParams();

  const GotoProfile = () => {
    window.location.href = '/profile';
  };

  return (
    <>
      <Header content={{ title: 'Order Sukses' }} />
      <div className="order withdraw-success">
        <div className="container">
          <div className="content-wrapper">
            <img src={Check} className="icon-check" alt="" />
            <h2 className="title c-white">Sukses!</h2>
            <p className="subtitle c-white">
              Tarik Dana Rp {formatPrice(Nominal)}
              <br />
              <br />
              <span>Proses pesanan Kamu sedang diproses saat ini.</span>
            </p>
          </div>
          <div className="payment-success-btn">
            <SingleButton
              onClick={GotoProfile}
              content={{
                url: '',
                text: 'KEMBALI KE PROFILE',
                color: 'white',
                url: '/talent',
              }}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderWithdrawSuccess;
