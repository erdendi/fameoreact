import React, { useState, useEffect } from 'react';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import Header from '../components/Header';
import PromoCard from '../components/SinglePromoCard';
import Back from '../assets/images/icon-back.svg';
// import SingleButton from '../components/Button'
import {
  getConfirmation,
  postVoucher,
  getVoucerlist,
  postUpdateBookingAmount,
} from '../services/order';
import { getCookie } from '../helpers';
import LoadingFull from '../components/LoadingFull';

const OrderPromoCode = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(true);
  const [cookieBookID] = useState(parseInt(getCookie('BookId')));
  const [userId] = useState(parseInt(getCookie('UserId')));
  const [voucer, setVoucer] = useState();
  const [searchTerm, setSearchTerm] = useState('');
  const handleChange = event => {
    setSearchTerm(event.target.value);
  };

  const getVoucer = () => {
    const bookparams = {
      Id: cookieBookID,
      userId,
    };
    getConfirmation(bookparams).then(resp => {
      const params = {
        talentid: resp.TalentId,
        code: null,
      };
      getVoucerlist(params).then(resp => {
        setVoucer(resp);
        setLoading(false);
      });

      setLoading(false);
    });
  };

  const VerifyVoucher = () => {
    setLoading(true);
    const body = {
      Id: cookieBookID,
      VoucherCd: searchTerm,
      // "PriceAmount" : 10000,
      // "UserId" : userId
    };
    postVoucher(body).then(resp => {
      if (resp.Status === 'OK') {
        const bookparams = {
          Id: cookieBookID,
          userId,
        };
        getConfirmation(bookparams).then(response => {
          const PriceOrderData =
            Number(response.SalePrice) > 0
              ? Number(response.SalePrice)
              : Number(response.PriceAmount);

          const update = {
            Id: cookieBookID,
            Status: 0,
            TotalPay: Number(Number(PriceOrderData) - Number(resp.Result)),
            // "TotalPay": Number(PriceOrderData),
            Potongan: resp.Result,
            PriceAmount: Number(PriceOrderData),
            VoucherCode: searchTerm,
            UserId: userId,
          };
          postUpdateBookingAmount(update).then(resp => {
            if (resp.Status === 'OK') {
              history.push(`/order-confirmation/${resp.result.Id}`);
            }
          });
        });
      } else {
        const Label = document.getElementById('LblMessage');
        Label.innerHTML = resp.Message;
        setLoading(false);
      }
    });
  };

  const postVoucer = e => {
    setLoading(true);
    const body = {
      Id: cookieBookID,
      VoucherCd: e,
      // "PriceAmount" : 10000,
      // "UserId" : userId
    };
    postVoucher(body).then(resp => {
      if (resp.Status === 'OK') {
        const bookparams = {
          Id: cookieBookID,
          userId,
        };
        getConfirmation(bookparams).then(response => {
          const PriceOrderData =
            Number(response.SalePrice) > 0
              ? Number(response.SalePrice)
              : Number(response.PriceAmount);

          const update = {
            Id: cookieBookID,
            Status: 0,
            TotalPay: Number(PriceOrderData) - Number(resp.Result),
            // "TotalPay": Number(PriceOrderData),
            Potongan: resp.Result,
            PriceAmount: Number(PriceOrderData),
            VoucherCode: e,
            UserId: userId,
          };
          postUpdateBookingAmount(update).then(resp => {
            if (resp.Status === 'OK') {
              history.push(`/order-confirmation/${resp.result.Id}`);
            }
          });
        });
      } else {
        const Label = document.getElementById('LblMessage');
        Label.innerHTML = resp.Message;
      }

      setLoading(false);
      // if (resp !== false) {
      //   let bookparams = {
      //     Id: cookieBookID,
      //     userId,
      //   };
      //   getConfirmation(bookparams).then(response => {
      //     var PriceOrderData =
      //       Number(response.SalePrice) > 0
      //         ? Number(response.SalePrice)
      //         : Number(response.PriceAmount);

      //     let update = {
      //       Id: cookieBookID,
      //       Status: 0,
      //       TotalPay: Number(PriceOrderData) - Number(resp),
      //       // "TotalPay": Number(PriceOrderData),
      //       Potongan: resp.result,
      //       PriceAmount: Number(PriceOrderData),
      //       VoucherCode: e,
      //       UserId: userId,
      //     };
      //     postUpdatePayment(update).then(resp => {
      //       console.log(resp);
      //       // if (resp.Status === 'OK') {
      //       //   history.push('/order-confirmation/' + resp.result.Id);
      //       // }
      //     });
      //   });
      // }
    });
  };
  useEffect(() => {
    // const results = people.filter(person =>
    //   person.name.toLowerCase().includes(searchTerm.toLowerCase())
    // );

    getVoucer();
    // setSearchResults(results);
  }, []);

  return (
    <>
      {loading && <LoadingFull />}
      <Header
        content={{
          title: 'Kode Promo',
          BtnLeftIcon: Back,
          BtnRightText: 'Selesai',
        }}
      />
      <div className="order order-promo">
        <div className="container">
          <div className="content-wrapper">
            <Form>
              <FormGroup>
                <Label className="c-white title-textbox" for="example">
                  Punya Kode Voucher ?
                </Label>
                <div className="d-flex base-line">
                  <Input
                    type="search"
                    name="search"
                    id="exampleSearch"
                    className="textbox"
                    value={searchTerm}
                    onChange={handleChange}
                    placeholder="Masukkan kode voucher"
                    style={{ color: 'black' }}
                  />

                  <div className="w-35" style={{ marginLeft: '10px' }}>
                    {/* <SingleButton onClick={postVoucer} content={{ text: 'Pakai', color: 'pink' }} /> */}
                    <button
                      type="button"
                      className="btn btn-success"
                      onClick={VerifyVoucher}
                    >
                      Gunakan
                    </button>
                  </div>
                </div>
                <div>
                  <Label className="c-white title-textbox" id="LblMessage" />
                </div>
              </FormGroup>
            </Form>
            <div className="results-holder">
              {!!voucer &&
                voucer.map(item => (
                  // <div className="card-holder">{item.name}</div>
                  <PromoCard content={item} actionProps={e => postVoucer(e)} />
                ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderPromoCode;
