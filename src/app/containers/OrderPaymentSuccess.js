   
import React from 'react';
import { Link } from 'react-router-dom';
import Header from '../components/Header';
import SingleButton from '../components/Button';
import Check from '../assets/images/icon-check-success.svg';

const OrderPaymentSuccess = () => (
  <>
    <Header content={{ title: 'Sukses' }} />
    <div className="order order-success">
      <div className="container">
        <div className="content-wrapper">
          <img src={Check} className="icon-check" alt="" />
          <h2 className="title c-white">Terimakasih!</h2>
          <p className="subtitle c-white">Pesanan Kamu sedang diproses.</p>
        </div>
        <Link to="/profile">
          <div className="payment-success-btn">
            <SingleButton
              content={{
                text: 'KEMBALI KE PROFILE',
                color: 'white',
                url: 'orderList',
              }}
            />
          </div>
        </Link>
      </div>
    </div>
  </>
);

export default OrderPaymentSuccess;
