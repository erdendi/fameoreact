import React, { useState } from 'react';
import {
  Form,
  FormGroup,
  Input,
  Label,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import classnames from 'classnames';
import Header from '../components/Header';
import SingleButton from '../components/Button';

import Back from '../assets/images/icon-back.svg';
import date from '../assets/images/live/date-live.svg';
import clock from '../assets/images/live/jam-live.svg';
import live from '../assets/images/live/record-live.svg';

const StartLiveStreaming = () => {
  const [activeTab, setActiveTab] = useState('1');
  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  return (
    <div className="fameo-live-start-wrapper">
      <Header content={{ title: 'Start Live Streaming', BtnLeftIcon: Back }} />

      <div className="after-heading-wrapper">
        <div className="container">
          <div className="how-paryment-m">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '1' })}
                  onClick={() => {
                    toggle('1');
                  }}
                >
                  Start Sekarang
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '2' })}
                  onClick={() => {
                    toggle('2');
                  }}
                >
                  Schedule
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={activeTab}>
              <TabPane tabId="1">
                <Form>
                  <FormGroup>
                    <Label className="c-white title-textbox">Tanggal</Label>
                    <Input
                      type="text"
                      name="date"
                      className="textbox"
                      placeholder="Pilih Tanggal"
                      value=""
                    />
                    <img className="date-icon" src={date} alt="date" />
                  </FormGroup>
                  <FormGroup>
                    <Input type="select" name="tema" className="select-bar">
                      <option>Pilih Tema</option>
                      <option>option</option>
                    </Input>
                    <img className="date-icon" src={live} alt="live" />
                  </FormGroup>

                  <SingleButton
                    content={{
                      text: 'START LIVE STREAMING',
                      color: 'pink',
                      url: '/order-confirmation',
                    }}
                  />
                </Form>
              </TabPane>
              <TabPane tabId="2">
                <Form>
                  <FormGroup>
                    <Label className="c-white title-textbox">Tanggal</Label>
                    <Input
                      type="text"
                      name="date"
                      className="textbox"
                      placeholder="Pilih Tanggal"
                      value=""
                    />
                    <img className="date-icon" src={date} alt="date" />
                  </FormGroup>
                  <FormGroup>
                    <Input type="select" name="jam" className="select-bar">
                      <option>Pilih Jam</option>
                      <option>00.00</option>
                      <option>01.00</option>
                      <option>02.00</option>
                      <option>03.00</option>
                      <option>04.00</option>
                      <option>05.00</option>
                    </Input>
                    <img className="date-icon" src={clock} alt="clock" />
                  </FormGroup>
                  <FormGroup>
                    <Input type="select" name="tema" className="select-bar">
                      <option>Pilih Tema</option>
                      <option>option</option>
                    </Input>
                    <img className="date-icon" src={live} alt="live" />
                  </FormGroup>
                  <SingleButton
                    content={{
                      text: 'BUAT JADWAL LIVE STREAMING',
                      color: 'pink',
                      url: '/order-confirmation',
                    }}
                  />
                </Form>
              </TabPane>
            </TabContent>
          </div>
        </div>
      </div>
    </div>
  );
};

export default StartLiveStreaming;
