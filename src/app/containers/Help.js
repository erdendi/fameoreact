import React, { useState } from 'react';
// import { useDropzone } from 'react-dropzone';/
import { Form, FormGroup, Input, Label } from 'reactstrap';

import SweetAlert from 'react-bootstrap-sweetalert';
import { Helmet } from 'react-helmet-async';
import Header from '../components/Header';
import SingleButton from '../components/Button';
import Back from '../assets/images/icon-left-arrow-two.svg';
import { postHelp } from '../services/help';
import { getCookie } from '../helpers';

const Help = () => {
  const [subject, setSubject] = useState('');
  const [details, setDetails] = useState('');
  const [emailAddress, setEmailAddress] = useState('');
  const [notif, setnotif] = useState(false);
  const [userID] = useState(Number(getCookie('UserId')));

  const submit = e => {
    e.preventDefault();
    // setLoading(true)
    const data = {
      Subject: subject || '',
      Details: details || '',
      CreatedBy: userID || '',
      EmailAddress: emailAddress || '',
    };
    postHelp(data).then(resp => {
      if (resp === 'OK') {
        setnotif(true);
      }
    });
  };

  return (
    <>
      <Helmet>
        <title>Help</title>
        <meta name="description" content="Help" />
      </Helmet>
      <div className="help-wrapper">
        <Header content={{ title: 'Bantuan', BtnLeftIcon: Back }} />
        <div className="after-heading-wrapper">
          <div className="container">
            <div className="update-form-wrapper">
              <Form onSubmit={event => submit(event)}>
                <FormGroup>
                  <Label for="first-name" className="title-textbox">
                    Nama
                  </Label>
                  <Input
                    onChange={e => {
                      setSubject(e.target.value);
                    }}
                    type="text"
                    name="first-name"
                    id="first-name"
                    className="textbox"
                    placeholder=""
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="last-name" className="title-textbox" type="email">
                    Email
                  </Label>
                  <Input
                    onChange={e => {
                      setEmailAddress(e.target.value);
                    }}
                    type="text"
                    name="last-name"
                    id="last-name"
                    className="textbox"
                    placeholder=""
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="username" className="title-textbox">
                    Pesan
                  </Label>
                  <Input
                    onChange={e => {
                      setDetails(e.target.value);
                    }}
                    type="text"
                    name="username"
                    id="username"
                    className="textbox"
                    placeholder=""
                  />
                </FormGroup>
                {/* <div className="gcaptcha">Google Recaptcha</div> */}
                <br />
                <SingleButton
                  content={{ text: 'KIRIM PESAN', color: 'pink' }}
                  onClick={submit}
                />
                <p className="p-atau">atau kontak langsung dengan</p>
                <a href="https://wa.me/6281388396191?text=Hello">
                  <SingleButton
                    content={{ text: 'WhatsApp Tim Kami', color: 'green' }}
                  />
                </a>
              </Form>
            </div>
          </div>
        </div>
      </div>
      <SweetAlert
        success
        show={notif}
        title="Submit Successfully"
        onConfirm={() => {
          setnotif(false);
          window.location.href = '/help';
        }}
        onCancel={() => {
          setnotif(false);
        }}
        onEscapeKey={() => {
          setnotif(false);
          window.location.href = '/help';
        }}
        onOutsideClick={() => {
          setnotif(false);
          window.location.href = '/help';
        }}
      />
    </>
  );
};

export default Help;
