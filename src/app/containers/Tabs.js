import React, { useState } from 'react';
import { TabContent, TabPane } from 'reactstrap';
import SwiperQuestions from '../components/QuestionsList';
import 'swiper/css/swiper.css';
import { TAB_DATA } from '../../constants/tabDataSearchQa';

const Tabs = ({ question }) => {
  const [activeTab] = useState('1');

  function ReturnTabs() {
    return (
      <div className="tab-wrapper">
        <TabContent activeTab={activeTab}>
          <TabPane tabId={activeTab}>
            <SwiperQuestions question={question} />
          </TabPane>
        </TabContent>
      </div>
    );
  }

  return (
    <div>
      <ReturnTabs items={TAB_DATA} />
    </div>
  );
};
export default Tabs;
