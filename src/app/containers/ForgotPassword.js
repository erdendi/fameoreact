   
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { FormGroup, Input } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';
import { Helmet } from 'react-helmet-async';
import SingleButton from '../components/Button';
import { SendForgotPassword } from '../services/reset-password';
import LoadingFull from '../components/LoadingFull';
// import LeftArrow from '../assets/images/icon-left-arrow.svg';
// import Header from '../components/Header';
// import Back from '../assets/images/icon-back.svg';
// import Settings from '../assets/images/icon-more.svg';

const ForgotPassword = () => {
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [passwordShown, setPasswordShown] = useState(false);
  const [notif, setnotif] = useState(false);
  const [alertError, setAlertError] = useState(false);
  const { token } = useParams();

  const submit = e => {
    e.preventDefault();
    if (password !== confirmPassword) {
      setAlertError(true);
    } else {
      setLoading(true);
      const params = {
        RequestCode: token,
        Password: password,
      };
      SendForgotPassword(params)
        .then(resp => {
          setLoading(false);
          if (resp.Status === 'OK') {
            setnotif(true);
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
  };

  const togglePasswordVisiblity = () => {
    setPasswordShown(!passwordShown);
  };

  useEffect(() => {
    window.scroll(0, 0);
  }, []);

  return (
    <>
      <Helmet>
        <title>Forgot Password</title>
        <meta name="description" content="Forgot Password" />
      </Helmet>
      <div className="login-form sign-in overlay-image">
        {loading && <LoadingFull />}
        <div className="container">
          <div className="content-wrapper">
            <div className="rubah-password-holder">
              <p className="p-text">Rubah password Fameo kamu</p>
            </div>
            <div>
              <FormGroup>
                <div className="password-holder">
                  <Input
                    type={passwordShown ? 'text' : 'password'}
                    name="password"
                    id="examplePassword"
                    placeholder="Masukkan password baru"
                    onChange={e => setPassword(e.target.value)}
                  />
                  <span
                    className="eye-icon"
                    onClick={() => togglePasswordVisiblity()}
                    aria-hidden="true"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="18.467"
                      height="10"
                      viewBox="0 0 18.467 10"
                    >
                      <g id="eye" transform="translate(0 -104.721)">
                        <g
                          id="Group_822"
                          data-name="Group 822"
                          transform="translate(0 104.721)"
                        >
                          <path
                            id="Path_457"
                            data-name="Path 457"
                            d="M18.15,109.32a13.791,13.791,0,0,0-2.466-2.3,10.973,10.973,0,0,0-6.45-2.3,10.973,10.973,0,0,0-6.45,2.3,13.793,13.793,0,0,0-2.466,2.3l-.317.4.317.4a13.794,13.794,0,0,0,2.466,2.3,10.973,10.973,0,0,0,6.45,2.3,10.973,10.973,0,0,0,6.45-2.3,13.791,13.791,0,0,0,2.466-2.3l.317-.4Zm-8.916,3.9a3.5,3.5,0,0,1-3.392-4.379l-.8-.135a4.308,4.308,0,0,0,1.263,4.172A11.351,11.351,0,0,1,3.579,111.4a13.59,13.59,0,0,1-1.893-1.677,13.593,13.593,0,0,1,1.893-1.677,11.353,11.353,0,0,1,2.726-1.484l.536.6a3.5,3.5,0,1,1,2.392,6.059Zm5.655-1.826a11.353,11.353,0,0,1-2.726,1.484,4.3,4.3,0,0,0,0-6.322,11.354,11.354,0,0,1,2.726,1.484,13.592,13.592,0,0,1,1.893,1.677A13.585,13.585,0,0,1,14.889,111.4Z"
                            transform="translate(0 -104.721)"
                            fill="#a8abbb"
                          />
                          <path
                            id="Path_458"
                            data-name="Path 458"
                            d="M170.855,172.405a2.334,2.334,0,1,0,.615-1l1.149,1.3Z"
                            transform="translate(-163.851 -168.086)"
                            fill="#a8abbb"
                          />
                        </g>
                      </g>
                    </svg>
                  </span>
                </div>
              </FormGroup>
              <FormGroup>
                <div className="password-holder">
                  <Input
                    type={passwordShown ? 'text' : 'password'}
                    name="confirm-password"
                    id="confirmPassword"
                    placeholder="Masukkan lagi password baru"
                    onChange={e => setConfirmPassword(e.target.value)}
                  />
                  <span
                    className="eye-icon"
                    onClick={() => togglePasswordVisiblity()}
                    aria-hidden="true"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="18.467"
                      height="10"
                      viewBox="0 0 18.467 10"
                    >
                      <g id="eye" transform="translate(0 -104.721)">
                        <g
                          id="Group_822"
                          data-name="Group 822"
                          transform="translate(0 104.721)"
                        >
                          <path
                            id="Path_457"
                            data-name="Path 457"
                            d="M18.15,109.32a13.791,13.791,0,0,0-2.466-2.3,10.973,10.973,0,0,0-6.45-2.3,10.973,10.973,0,0,0-6.45,2.3,13.793,13.793,0,0,0-2.466,2.3l-.317.4.317.4a13.794,13.794,0,0,0,2.466,2.3,10.973,10.973,0,0,0,6.45,2.3,10.973,10.973,0,0,0,6.45-2.3,13.791,13.791,0,0,0,2.466-2.3l.317-.4Zm-8.916,3.9a3.5,3.5,0,0,1-3.392-4.379l-.8-.135a4.308,4.308,0,0,0,1.263,4.172A11.351,11.351,0,0,1,3.579,111.4a13.59,13.59,0,0,1-1.893-1.677,13.593,13.593,0,0,1,1.893-1.677,11.353,11.353,0,0,1,2.726-1.484l.536.6a3.5,3.5,0,1,1,2.392,6.059Zm5.655-1.826a11.353,11.353,0,0,1-2.726,1.484,4.3,4.3,0,0,0,0-6.322,11.354,11.354,0,0,1,2.726,1.484,13.592,13.592,0,0,1,1.893,1.677A13.585,13.585,0,0,1,14.889,111.4Z"
                            transform="translate(0 -104.721)"
                            fill="#a8abbb"
                          />
                          <path
                            id="Path_458"
                            data-name="Path 458"
                            d="M170.855,172.405a2.334,2.334,0,1,0,.615-1l1.149,1.3Z"
                            transform="translate(-163.851 -168.086)"
                            fill="#a8abbb"
                          />
                        </g>
                      </g>
                    </svg>
                  </span>
                </div>
              </FormGroup>
              <FormGroup check />
              <div className="button-holder">
                <SingleButton
                  type="submit"
                  onClick={submit}
                  content={{ text: 'UBAH PASSWORD', color: 'pink' }}
                />
              </div>
              {/* <Label>
                Kembali ke
                <Link to="/signin">Sign in</Link>
              </Label> */}
            </div>
          </div>
        </div>
      </div>

      <SweetAlert
        warning
        show={alertError}
        // Text =  "Username atau Password yang anda masukkan salah"
        onConfirm={() => {
          setAlertError(false);
          // window.location.href = "/profile"
        }}
        onCancel={() => {
          setAlertError(false);
        }}
        onEscapeKey={() => {
          setAlertError(false);
          // window.location.href = "/profile"
        }}
        onOutsideClick={() => {
          setAlertError(false);
          // window.location.href = "/profile"
        }}
      >
        Password tidak cocok, masukkan password lagi
      </SweetAlert>
      <SweetAlert
        warning
        show={notif}
        // Text =  "Username atau Password yang anda masukkan salah"
        button="Sign in"
        onConfirm={() => {
          setnotif(false);
          window.location.href = '/signin';
        }}
        onCancel={() => {
          setnotif(false);
        }}
        onEscapeKey={() => {
          setnotif(false);
        }}
        onOutsideClick={() => {
          setnotif(false);
        }}
      >
        Password berhasil dirubah, silahkan sign in
      </SweetAlert>
    </>
  );
};

export default ForgotPassword;
