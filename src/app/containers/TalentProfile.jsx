/* eslint-disable global-require */

/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import {
  getDetailTalent,
  getOrderList,
  getTopComment,
} from '../services/talentProfile';
// eslint-disable-next-line linebreak-style

import Header from '../components/Header';
import Taskbar from '../components/TaskBar';
import Heading from '../components/Heading';
import ProfileData from '../components/ProfileData';
import BestVotedQuestions from '../components/BestVotedQuestion';
import FanQuestionsTalentProfile from './FanQuestionsTalentProfile';
import TalentFeedback from '../components/TalentFeedback';
import CardSwiperProfile from '../components/CardSwiperProfile';
import VideoCardSwiper from '../components/VideoCardSwiper';
import Burger from '../assets/images/icon-burger-menu.svg';
import Settings from '../assets/images/icon-settings.svg';

const UserProfileData = {
  type: 'talent',
  icon: require('../assets/images/img-idol-1.png'),
  name: 'Michelle Ziudith',
  inbox: 2,
  notif: true,
};

const BestVotedHeading = {
  headingData: {
    heading: 'Pertanyaan Saya',
    headingColor: 'white',
    linkName: 'Lihat Semua',
    url: '/',
  },
};

const MyFeedBackHeading = {
  headingData: {
    heading: 'Feedback Saya',
    headingColor: 'white',
    linkName: 'Lihat Semua',
    url: '/',
  },
};

const contentSwiperData = {
  headingData: {
    heading: 'Order Saya',
    headingColor: 'white',
    linkName: 'Lihat Semua',
    url: '/',
  },
  isNormalCard: true,
};

const contentSwiperData2 = {
  headingData: {
    heading: 'Video Reaksi Saya',
    headingColor: 'violet',
    linkName: 'Lihat Semua',
    url: '/',
  },
  isNormalCard: true,
};

const TalentProfile = () => {
  const [talents, setTalents] = useState([]);
  const [Video, setVideo] = useState([]);
  const [orders, setOrders] = useState([]);
  const [topComment, setTopComment] = useState([]);
  const [talentId, setTalentId] = useState([]);
  const [error, setError] = useState(false);

  const userID = 300;
  const getData = async () => {
    setError(false);
    let params = {
      userid: userID,
    };
    getDetailTalent(params)
      .then(resp => {
        setTalents(resp.TalentModel);
        setVideo(resp.AllVideoList);
        setTalentId(resp.TalentModel.Id);
      })
      .catch(err => {
        setError(true);
      });

    params = {
      UserId: userID,
    };
    getOrderList(params)
      .then(resp => {
        setOrders(resp);
      })
      .catch(err => {
        setError(true);
      });

    params = {
      talentid: talentId,
    };
    getTopComment(params)
      .then(resp => {
        setTopComment(resp.getCurrentData);
      })
      .catch(err => {
        setError(true);
      });
  };

  useEffect(() => {
    window.scroll(0, 0);
    getData();
  }, []);

  return (
    <>
      <div className="talent-profile-wrapper">
        <Header
          content={{
            title: 'Profile Saya',
            BtnLeftIcon: Burger,
            BtnRightIcon: Settings,
          }}
        />
        <div className="after-heading-wrapper">
          <ProfileData content={UserProfileData} profile={talents} />
          <div className="section-gift mt-4">
            <CardSwiperProfile
              contentSwiper={contentSwiperData}
              contents={orders}
              userID={userID}
            />
          </div>
          <div className="mt-5">
            <Heading content={BestVotedHeading.headingData} />
            <BestVotedQuestions comment={topComment} />
          </div>
          <FanQuestionsTalentProfile comment={topComment} />
          <div className="mt-5 mb-4">
            <Heading content={MyFeedBackHeading.headingData} />
            <TalentFeedback />
          </div>
          <div className="section-reaction">
            {!!Video && Video.length > 0 && (
              <VideoCardSwiper
                contentSwiper={contentSwiperData2}
                contents={Video}
                content2={Video}
              />
            )}
          </div>
        </div>
        <Taskbar />
      </div>
    </>
  );
};

export default TalentProfile;
