/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable padded-blocks */
/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/anchor-is-valid */

import React, { useState } from 'react';
import SingleButton from '../components/Button';
import VideoC from '../assets/images/video/logo-video.png';
import Bgvideo from '../assets/images/live/bg-live.png';
import icon3 from '../assets/images/live/hourglass-live.svg';
import icon4 from '../assets/images/live/eye-live.svg';
import icon5 from '../assets/images/live/url-live.svg';
import icon6 from '../assets/images/live/muang-live.svg';
import user from '../assets/images/live/user-live.svg';
import pop1 from '../assets/images/live/pop-1.svg';
import pop2 from '../assets/images/live/pop-2.svg';
import pop3 from '../assets/images/live/pop-3.svg';
import pop4 from '../assets/images/live/pop-4.svg';

const TalentLiveStreaming = () => {
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);

  function stoprecordvideo() {
    document.getElementById('box-download-if-click-stop').style.display =
      'block';
  }

  return (
    <div className="talent-live-wrapper">
      <div className="box-video--">
        <div className="row no-gutters toolbar align-items-center">
          <div className="col mr-4">
            <img src={icon4} alt="icon4" />{' '}
            <span className="icon-dsc">837 Viewers</span>
          </div>
          <div className="col mr-4">
            <img src={icon5} alt="icon5" />{' '}
            <span className="icon-dsc">530 Stickers</span>
          </div>
          <div className="col">
            <img src={icon3} alt="icon4" />{' '}
            <span className="icon-dsc">95 Menit</span>
          </div>
          <div className="col">
            <img src={icon6} alt="icon6" />{' '}
            <span className="icon-dsc">250.000</span>
          </div>
        </div>
        <div className="row no-gutters">
          <div className="col-12 text-right">
            <a>
              <img className="vid-logo-det-2" src={VideoC} alt="videoC" />
            </a>
          </div>
        </div>
      </div>

      <div className="wrapper_video">
        <img src={Bgvideo} className="img-rec-bg" />
      </div>

      <div
        className="wrapper_btn_sub_box_two"
        id="box-download-if-click-stop"
        style={{ display: 'none' }}
      >
        <p className="live-berakhir">Live Streaming Berakhir!</p>
        <div className="row justify-content-center">
          <div className="col-3 p-0">
            <div className="rec-dot-3--">
              <img src={pop1} style={{ margin: '0 -6px' }} />
            </div>
            <p>837 Viewers</p>
          </div>
          <div className="col-3 p-0">
            <div className="rec-dot-3--">
              <img src={pop2} style={{ margin: '-4px -6px' }} />
            </div>
            <p>530 Stickers</p>
          </div>
          <div className="col-3 p-0">
            <div className="rec-dot-3--">
              <img src={pop3} />
            </div>
            <p>95 Menit</p>
          </div>
          <div className="col-3 p-0">
            <div className="rec-dot-3--">
              <img src={pop4} />
            </div>
            <p>Rp. 250.000</p>
          </div>
        </div>
        <div className="row">
          <div className="col-12 text-center" onClick={stoprecordvideo}>
            <SingleButton
              content={{
                text: 'KEMBALI KE PROFILE',
                color: 'pink',
                url: 'talent',
              }}
            />
          </div>
        </div>
      </div>

      <div className="wrapper_talent-btn">
        <div className="box-chat-bbb">
          <img src={user} /> <span className="username">Jhonny :</span> Hai,
          Salam Kenal
        </div>
        <div className="box-chat-bbb">
          <img src={user} /> <span className="username">Buday :</span> Cantik
          banget kak!
        </div>
        <div className="row no-gutters">
          <div className="col-12 text-center" onClick={stoprecordvideo}>
            <SingleButton
              content={{
                text: 'SELESAI LIVE STREAMING',
                color: 'pink',
                url: '#',
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default TalentLiveStreaming;
