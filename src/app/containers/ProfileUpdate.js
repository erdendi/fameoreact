import React, { useState, useEffect } from 'react';
import Dropzone, { useDropzone } from 'react-dropzone';
import { Form, FormGroup, Input, Label, Button } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import SweetAlert from 'react-bootstrap-sweetalert';
import { useForm, Controller } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';
import Back from '../assets/images/icon-left-arrow-two.svg';
import Camera from '../assets/images/icon-camera.svg';
import {
  getEditProfile,
  postEditProfile,
  getBank,
} from '../services/profileEdit';
import { getCookie } from '../helpers';
import LoadingFullProgress from 'app/components/LoadingFullProgress';

const ProfileUpdate = () => {
  const history = useHistory();
  const [files, setFiles] = useState([]);
  const [user, setUser] = useState(false);
  const [loading, setLoading] = useState(true);
  const [userID] = useState(Number(getCookie('UserId')));
  const [PhoneNumber, setPhoneNumber] = useState('');
  const [bio, setBio] = useState('');
  const [Bank, setBank] = useState('');
  const [AccountNumber, setAccountNumber] = useState('');
  const [BeneficiaryName, setBeneficiaryName] = useState('');
  const [notif, setnotif] = useState(false);
  const [filesKTP, setFilesKTP] = useState([]);
  // const [filesNPWP, setFilesNPWP] = useState([]);
  const [filesProfileVideo, setFilesProfileVideo] = useState([]);
  const [filesAccountNumber, setFilesAccountNumber] = useState([]);
  const [BankValue, setBankValue] = useState();
  const [errorImageProfileSize, setErrorImageProfileSize] = useState(false);
  const [progressValue, setProgressValue] = useState(0);

  const [RoleId] = useState(Number(getCookie('RoleId')));
  const { control, handleSubmit, errors } = useForm({
    mode: 'onChange',
  });

  const maxImageFileSize = 102400; // 102400 b = 100kb

  const { getRootProps, getInputProps, acceptedFiles } = useDropzone({
    accept: 'image/*',
    maxSize: maxImageFileSize,
    onDropAccepted: acceptedFiles => {
      setFiles(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      );
      setErrorImageProfileSize(false);
    },
    onDropRejected: fileRejections => {
      setErrorImageProfileSize(true);
    },
  });

  const handleOption = e => {
    setBankValue(e.target.value);
  };
  const getBankData = () => {
    getBank().then(resp => {
      setBank(resp);
    });
  };

  const getData = async () => {
    getEditProfile(userID).then(resp => {
      setUser(resp);
      setLoading(false);
    });
  };
  const onPostUpdate = data => {
    setLoading(true);
    const formData = new FormData();
    formData.append('Id', userID);
    formData.append('ProfImg', acceptedFiles[0] || user.ProfPicLink);
    formData.append('FirstName', data.firstName || user.FirstName);
    formData.append('LastName', data.lastName || user.LastName);
    formData.append('UserName', user.UserName);
    formData.append('Password', data.password || user.Password);
    formData.append('PhoneNumber', PhoneNumber || user.PhoneNumber);
    formData.append('Bio', bio || user.Bio);
    formData.append('Email', data.email || user.Email);
    formData.append('Bank', BankValue || user.Bank);
    formData.append('AccountNumber', AccountNumber || user.AccountNumber);
    formData.append('BeneficiaryName', BeneficiaryName || user.BeneficiaryName);
    formData.append('IdCardImg', filesKTP[0]);
    formData.append('AccountNumberImg', filesAccountNumber[0]);
    // formData.append('NpwpImg', filesNPWP);
    formData.append('ProfileVideo', filesProfileVideo[0]);

    postEditProfile(formData)
      .progress(value => setProgressValue(value))
      .then(resp => {
        if (resp === 'OK') {
          setnotif(true);
        }
      });
  };

  useEffect(() => {
    getData();
    getBankData();
  }, []);

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [files],
  );

  const thumbs = files.map(file => (
    <img src={file.preview} className="round-image" alt="fameo-user" />
  ));

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      filesKTP.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [filesKTP],
  );

  const thumbsKtp = filesKTP.map(file => (
    <div key={file.name} style={{ position: 'relative' }}>
      <Button
        close
        onClick={e => removeImageKtp(e)}
        style={{
          position: 'absolute',
          top: '5px',
          right: '10px',
          color: '#fff',
          fontSize: '40px',
        }}
      />
      <img className="mediafoto" src={file.preview} />
    </div>
  ));

  const removeImageKtp = e => {
    e.preventDefault();
    setFilesKTP([]);
  };

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      filesAccountNumber.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [filesAccountNumber],
  );

  const thumbsAccountNumber = filesAccountNumber.map(file => (
    <div key={file.name} style={{ position: 'relative' }}>
      <Button
        close
        onClick={e => removeImageAccountNumber(e)}
        style={{
          position: 'absolute',
          top: '5px',
          right: '10px',
          color: '#fff',
          fontSize: '40px',
        }}
      />
      <img className="mediafoto" src={file.preview} />
    </div>
  ));

  const removeImageAccountNumber = e => {
    e.preventDefault();
    setFilesAccountNumber([]);
  };

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      filesProfileVideo.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [filesProfileVideo],
  );

  const thumbsProfileVideo = filesProfileVideo.map(file => (
    <div key={file.name} style={{ position: 'relative' }}>
      <Button
        close
        onClick={e => removeProfileVideo(e)}
        style={{
          position: 'absolute',
          top: '5px',
          right: '10px',
          color: '#fff',
          fontSize: '40px',
        }}
      />
      {/* <img className="mediafoto" src={file.preview} /> */}
      <video id="mediaprofileVideo" width="100%" height="auto" controls>
        <source src={file.preview} />
      </video>
    </div>
  ));

  const removeProfileVideo = e => {
    e.preventDefault();
    setFilesProfileVideo([]);
  };

  return (
    <>
      <div className="profile-update-wrapper">
        <header>
          <div className="container">
            <div className="content-holder">
              <div
                onClick={() => history.goBack()}
                className="position-absolute"
              >
                <img src={Back} className="icon-header" alt="" />
              </div>
              <span className="title">Ubah Profil</span>
            </div>
          </div>
        </header>

        <div className="after-heading-wrapper">
          <div className="container">
            {!!user && (
              <>
                <div className="profile-image-wrapper">
                  {user.ProfPicLink !== '' && (
                    <img
                      src={user.ProfPicLink}
                      className="round-image"
                      alt="fameo-profile"
                    />
                  )}
                  {acceptedFiles.length > 0 ? (
                    <img
                      src={acceptedFiles[0].preview}
                      className="round-image"
                      alt="fameo-profile"
                    />
                  ) : (
                    thumbs
                  )}
                  <div {...getRootProps({ className: 'dropzone' })}>
                    <input {...getInputProps()} />
                    <div className="dropzone-data">
                      <img
                        src={Camera}
                        className="fameo-upload"
                        alt="fameo-upload"
                      />
                    </div>
                  </div>
                </div>
                {errorImageProfileSize && (
                  <div
                    className="invalid-feedback"
                    style={{ display: 'block' }}
                  >
                    Besaran file tidak boleh lebih dari 100KB
                  </div>
                )}

                <div className="update-form-wrapper">
                  <Form onSubmit={handleSubmit(onPostUpdate)}>
                    <FormGroup>
                      <Label for="first-name" className="title-textbox">
                        Nama Depan
                      </Label>
                      <Controller
                        as={Input}
                        name="firstName"
                        control={control}
                        defaultValue={user.FirstName}
                        rules={{
                          required: 'Nama depan diperlukan!',
                        }}
                        invalid={errors.firstName}
                        placeholder="nama depan"
                      />
                      <ErrorMessage
                        errors={errors}
                        name="firstName"
                        as={<div className="invalid-feedback" />}
                      >
                        {({ message }) => <>{message}</>}
                      </ErrorMessage>
                    </FormGroup>
                    <FormGroup>
                      <Label for="last-name" className="title-textbox">
                        Nama Belakang
                      </Label>
                      <Controller
                        as={Input}
                        name="lastName"
                        control={control}
                        defaultValue={user.LastName}
                        rules={{
                          required: 'Nama belakang diperlukan!',
                        }}
                        invalid={errors.lastName}
                        placeholder="nama belakang"
                      />
                      <ErrorMessage
                        errors={errors}
                        name="lastName"
                        as={<div className="invalid-feedback" />}
                      >
                        {({ message }) => <>{message}</>}
                      </ErrorMessage>
                    </FormGroup>
                    <FormGroup>
                      <Label for="exampleSelect" className="title-textbox">
                        No. Handphone
                      </Label>
                      <div className="phone-holder">
                        <div className="select-holder">
                          <Input
                            type="select"
                            name="select"
                            id="exampleSelect"
                            className="textbox"
                          >
                            <option defaultValue>+62</option>
                          </Input>
                        </div>
                        <Input
                          type="tel"
                          name="number"
                          onChange={e => {
                            setPhoneNumber(e.target.value);
                          }}
                          defaultValue={user.PhoneNumber}
                          className="textbox phone-input"
                          placeholder="812 3456 7890"
                        />
                      </div>
                    </FormGroup>
                    <FormGroup>
                      <Label for="email" className="title-textbox">
                        Email
                      </Label>
                      <Controller
                        as={Input}
                        name="email"
                        control={control}
                        defaultValue={user.Email}
                        rules={{
                          required: 'email kamu diperlukan!',
                          pattern: /^[a-zA-Z0-9_]+(?:\.[a-zA-Z0-9]+)*@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/,
                        }}
                        invalid={Boolean(errors.email)}
                        placeholder="emailkamu@alamat.com"
                      />
                      <ErrorMessage
                        errors={errors}
                        name="email"
                        render={({ message }) => (
                          <div className="invalid-feedback">
                            <ul>
                              {message && <li>{message}</li>}
                              <li>
                                email tidak boleh ada spasi atau format email
                                salah
                              </li>
                            </ul>
                          </div>
                        )}
                      />
                    </FormGroup>
                    <FormGroup>
                      <Label for="bio" className="title-textbox">
                        Bio
                      </Label>
                      <textarea
                        className="form-control"
                        defaultValue={user.Bio || ''}
                        onChange={e => {
                          setBio(e.target.value);
                        }}
                      />
                    </FormGroup>
                    <FormGroup>
                      <Label for="email" className="title-textbox">
                        Bank
                      </Label>
                      <Input
                        type="select"
                        name="select"
                        id="exampleSelect"
                        className="form-control"
                        onChange={e => handleOption(e)}
                        defaultValue={user.Bank}
                      >
                        <option>Pilih Bank</option>
                        {!!Bank &&
                          Bank.map((v, i) => (
                            <option key={i} value={v.BankCode}>
                              {v.BankDescription}
                            </option>
                          ))}
                      </Input>
                    </FormGroup>
                    <FormGroup>
                      <Label for="norek" className="title-textbox">
                        Nomor Rekening
                      </Label>
                      <Input
                        type="text"
                        name="norek"
                        defaultValue={user.AccountNumber}
                        onChange={e => {
                          setAccountNumber(e.target.value);
                        }}
                        className="textbox"
                        placeholder=""
                      />
                    </FormGroup>
                    <FormGroup>
                      <Label for="beneficiaryName" className="title-textbox">
                        Nama Pemilik Rekening
                      </Label>
                      <Input
                        type="text"
                        name="beneficiaryName"
                        defaultValue={user.BeneficiaryName}
                        onChange={e => {
                          setBeneficiaryName(e.target.value);
                        }}
                        className="textbox"
                        placeholder=""
                      />
                    </FormGroup>
                    {RoleId === 3 ? (
                      <>
                        <FormGroup>
                          <Label for="email" className="title-textbox">
                            Foto KTP
                          </Label>
                          <div style={{ marginBottom: '10px' }}>
                            <Dropzone
                              accept="image/*"
                              maxSize={maxImageFileSize}
                              onDrop={acceptedFiles => {
                                setFilesKTP(
                                  acceptedFiles.map(file =>
                                    Object.assign(file, {
                                      preview: URL.createObjectURL(file),
                                    }),
                                  ),
                                );
                              }}
                              onDropRejected={() => {
                                alert(`Maksimal gambar 100KB`);
                              }}
                            >
                              {({ getRootProps, getInputProps }) => (
                                <section>
                                  <div
                                    {...getRootProps({
                                      className: 'basic-image-upload-drop',
                                    })}
                                  >
                                    <input {...getInputProps()} />
                                    <p>Klik untuk pilih gambar KTP</p>
                                  </div>
                                </section>
                              )}
                            </Dropzone>
                          </div>
                          <div>
                            {filesKTP.length ? (
                              thumbsKtp
                            ) : (
                              <img
                                id="mediaKTP"
                                className="mediafoto"
                                src={user.IdCardImg}
                                alt="KTP"
                              />
                            )}
                          </div>
                        </FormGroup>

                        <FormGroup>
                          <Label for="accountnumber" className="title-textbox">
                            Foto Buku Rekening
                          </Label>
                          <div style={{ marginBottom: '10px' }}>
                            <Dropzone
                              accept="image/*"
                              maxSize={maxImageFileSize}
                              onDrop={acceptedFiles => {
                                setFilesAccountNumber(
                                  acceptedFiles.map(file =>
                                    Object.assign(file, {
                                      preview: URL.createObjectURL(file),
                                    }),
                                  ),
                                );
                              }}
                              onDropRejected={() => {
                                alert(`Maksimal gambar 100KB`);
                              }}
                            >
                              {({ getRootProps, getInputProps }) => (
                                <section>
                                  <div
                                    {...getRootProps({
                                      className: 'basic-image-upload-drop',
                                    })}
                                  >
                                    <input {...getInputProps()} />
                                    <p>Klik untuk pilih gambar buku rekening</p>
                                  </div>
                                </section>
                              )}
                            </Dropzone>
                          </div>
                          <div>
                            {filesAccountNumber.length ? (
                              thumbsAccountNumber
                            ) : (
                              <img
                                id="mediaaccountnumber"
                                className="mediafoto"
                                src={user.AcNumImg}
                                alt="Nomor rekening"
                              />
                            )}
                          </div>
                        </FormGroup>

                        <FormGroup>
                          <Label for="profilevideo" className="title-textbox">
                            Profile Video
                          </Label>
                          <div style={{ marginBottom: '10px' }}>
                            <Dropzone
                              accept="video/mp4"
                              onDrop={acceptedFiles => {
                                setFilesProfileVideo(
                                  acceptedFiles.map(file =>
                                    Object.assign(file, {
                                      preview: URL.createObjectURL(file),
                                    }),
                                  ),
                                );
                              }}
                              onDropRejected={() => {
                                alert(`Maksimal gambar 100KB`);
                              }}
                            >
                              {({ getRootProps, getInputProps }) => (
                                <section>
                                  <div
                                    {...getRootProps({
                                      className: 'basic-image-upload-drop',
                                    })}
                                  >
                                    <input {...getInputProps()} />
                                    <p>Klik untuk pilih video profil</p>
                                  </div>
                                </section>
                              )}
                            </Dropzone>
                          </div>
                          <div>
                            {filesProfileVideo.length ? (
                              thumbsProfileVideo
                            ) : (
                              <div id="MediaVideo">
                                <video
                                  id="mediaprofileVideo"
                                  width="100%"
                                  height="auto"
                                  controls
                                >
                                  <source
                                    src={
                                      user.VideoProfile != null
                                        ? user.VideoProfile.Link
                                        : '#'
                                    }
                                  />
                                </video>
                              </div>
                            )}
                          </div>
                        </FormGroup>
                      </>
                    ) : (
                      ''
                    )}
                    <div className="button-holder">
                      <button type="submit" className="btn btn-pink c-white">
                        SIMPAN
                      </button>
                    </div>
                  </Form>
                </div>
              </>
            )}
            {loading && <LoadingFullProgress progressValue={progressValue} />}
          </div>
        </div>
      </div>

      <SweetAlert
        success
        show={notif}
        title="Update Succesfully"
        onConfirm={() => {
          setnotif(false);
          window.location.reload();
        }}
        onCancel={() => {
          setnotif(false);
          window.location.reload();
        }}
        onEscapeKey={() => {
          setnotif(false);
          window.location.reload();
        }}
        onOutsideClick={() => {
          setnotif(false);
          window.location.reload();
        }}
      />
    </>
  );
};

export default ProfileUpdate;
