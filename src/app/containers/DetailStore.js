/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React from 'react';
import Header from '../components/Header';
import SingleButton from '../components/Button';
import Back from '../assets/images/icon-back.svg';
import Money from '../assets/images/cmoney.png';
import Boxtop from '../assets/images/box-top-coin.svg';

function rejectbox() {
  const element = document.getElementById('reject-box');
  if (element.getAttribute('style') === 'display: none;') {
    element.style.display = 'block';
  } else {
    element.style.display = 'none';
  }
}

const DetailStore = () => (
  <>
    <Header content={{ title: '25000 Koin', BtnLeftIcon: Back, link: '/' }} />
    <div className="order detail-store">
      <div className="container">
        <div className="content-wrapper">
          <div className="box-store">
            <div className="sub-box-store">
              <div
                className="box-top"
                style={{ background: `url(${Boxtop})no-repeat` }}
              >
                <span>Rp.100.000</span>
              </div>
              <div className="row">
                <div className="col-5 offset-1">
                  <img src={Money} alt="money" />
                </div>
                <div className="col-5 p-0">
                  <h3>Koin Emas</h3>
                  <h6>3000 + 100 Koin</h6>
                </div>
              </div>
            </div>

            <div className="update-form-wrapper" onClick={rejectbox}>
              <SingleButton
                content={{ url: '#', text: 'BELI KOIN FAMEO', color: 'pink' }}
              />
            </div>
          </div>
        </div>
      </div>
      <hr />
      <div className="desc-coin">
        <h4>Deskripsi Produk</h4>
        <p>
          Nulla maximus pharetra interdum. Ut in justo libero. Sed feugiat, sem
          ut interdum pharetra, sem ante condimentum magna, vel convallis diam
          dolor venenatis dui. Ut finibus et dolor bibendum varius. Nulla
          maximus pharetra interdum. Ut in justo libero.
        </p>
        <p>
          Nulla maximus pharetra interdum. Ut in justo libero. Sed feugiat, sem
          ut interdum pharetra, sem ante condimentum magna, vel convallis diam
          dolor venenatis dui. Ut finibus et dolor bibendum varius. Nulla
          maximus pharetra interdum. Ut in justo libero.
        </p>
      </div>
      <div id="reject-box" style={{ display: 'none' }}>
        <div className="reject-box-wrapper" onClick={rejectbox} />
        <div className="reject-box-style">
          <div className="line----" onClick={rejectbox} />
          <p>Ingin membeli Fameo Koin ?</p>
          <h1>KOIN EMAS 3000 + 100</h1>
          <div className="row">
            <div className="col-6">
              <SingleButton
                content={{ url: '#', text: 'BELI', color: 'white' }}
              />
            </div>
            <div className="col-6" onClick={rejectbox}>
              <SingleButton
                content={{ url: '#', text: 'BATAL', color: 'white' }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
);

export default DetailStore;
