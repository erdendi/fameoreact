/* eslint-disable linebreak-style */
/* eslint-disable radix */
/* eslint-disable linebreak-style */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Arrow from '../assets/images/icon-right-arrow.svg';
import { getCookie } from '../helpers';

function OrderList({ content }) {
  const [userId] = useState(Number(getCookie('UserId')));

  if (content.BookedBy === userId) {
    if (content.Status === 0) {
      return (
        // /order-rincian-how-payment/:bookId/:banktype
        //  <Link to={`orderInfo/${content.Id}`}>
        <Link to={`/order-rincian-how-payment/${content.Id}`}>
          <div className="order-wrapper">
            <div className="order-data">
              <img
                src={content.TalentPhotos}
                className="round-image"
                alt="fameo-idol-icon"
              />
              <div className="order-cred">
                <h5 className="idol-name">{content.TalentNm}</h5>
                <h5 className="order-id">{content.OrderNo}</h5>
              </div>
            </div>
            <div className="order-status-wrapper">
              <div className="status-texts">
                <h5 className="status-label">Status</h5>
                <h4 className="order-status">{content.TransactionStatus}</h4>
              </div>
              <img src={Arrow} className="order-arrow" alt="fameo-arrow" />
            </div>
          </div>
        </Link>
      );
    }
    if (content.Status === 1) {
      return (
        // /order-rincian-how-payment/:bookId/:banktype
        //  <Link to={`orderInfo/${content.Id}`}>
        <Link to={`/order-confirmation/${content.Id}`}>
          <div className="order-wrapper">
            <div className="order-data">
              <img
                src={content.TalentPhotos}
                className="round-image"
                alt="fameo-idol-icon"
              />
              <div className="order-cred">
                <h5 className="idol-name">{content.TalentNm}</h5>
                <h5 className="order-id">{content.OrderNo}</h5>
              </div>
            </div>
            <div className="order-status-wrapper">
              <div className="status-texts">
                <h5 className="status-label">Status</h5>
                <h4 className="order-status">{content.TransactionStatus}</h4>
              </div>
              <img src={Arrow} className="order-arrow" alt="fameo-arrow" />
            </div>
          </div>
        </Link>
      );
    }
    if (content.Status === 5) {
      return (
        <Link to={`/orderReady/${content.Id}`}>
          <div className="order-wrapper">
            <div className="order-data">
              <img
                src={content.TalentPhotos}
                className="round-image"
                alt="fameo-idol-icon"
              />
              <div className="order-cred">
                <h5 className="idol-name">{content.TalentNm}</h5>
                <h5 className="order-id">{content.OrderNo}</h5>
              </div>
            </div>
            <div className="order-status-wrapper">
              <div className="status-texts">
                <h5 className="status-label">Status</h5>
                <h4 className="order-status">{content.TransactionStatus}</h4>
              </div>
              <img src={Arrow} className="order-arrow" alt="fameo-arrow" />
            </div>
          </div>
        </Link>
      );
    }
    if (content.Status > 7) {
      return (
        <Link to={`/orderinfo/${content.Id}`}>
          <div className="order-wrapper">
            <div className="order-data">
              <img
                src={content.TalentPhotos}
                className="round-image"
                alt="fameo-idol-icon"
              />
              <div className="order-cred">
                <h5 className="idol-name">{content.TalentNm}</h5>
                <h5 className="order-id">{content.OrderNo}</h5>
              </div>
            </div>
            <div className="order-status-wrapper">
              <div className="status-texts">
                <h5 className="status-label">Status</h5>
                <h4 className="order-status">{content.TransactionStatus}</h4>
              </div>
              <img src={Arrow} className="order-arrow" alt="fameo-arrow" />
            </div>
          </div>
        </Link>
      );
    }

    return (
      <Link to={`/orderinfo/${content.Id}`}>
        <div className="order-wrapper">
          <div className="order-data">
            <img
              src={content.TalentPhotos}
              className="round-image"
              alt="fameo-idol-icon"
            />
            <div className="order-cred">
              <h5 className="idol-name">{content.TalentNm}</h5>
              <h5 className="order-id">{content.OrderNo}</h5>
            </div>
          </div>
          <div className="order-status-wrapper">
            <div className="status-texts">
              <h5 className="status-label">Status</h5>
              <h4 className="order-status">{content.TransactionStatus}</h4>
            </div>
            <img src={Arrow} className="order-arrow" alt="fameo-arrow" />
          </div>
        </div>
      </Link>
    );
  }

  // used for talent page
  if (content.Status === 3) {
    return (
      // /order-rincian-how-payment/:bookId/:banktype
      <Link to={`/orderInfo/${content.Id}`}>
        {/* // <Link to={`order-rincian-how-payment/${content.Id}/`}> */}
        <div className="order-wrapper">
          <div className="order-data">
            <img
              src={content.TalentPhotos}
              className="round-image"
              alt="fameo-idol-icon"
            />
            <div className="order-cred">
              <h5 className="idol-name">{content.TalentNm}</h5>
              <h5 className="order-id">{content.OrderNo}</h5>
            </div>
          </div>
          <div className="order-status-wrapper">
            <div className="status-texts">
              <h5 className="status-label">Status</h5>
              <h4 className="order-status">{content.TransactionStatus}</h4>
            </div>
            <img src={Arrow} className="order-arrow" alt="fameo-arrow" />
          </div>
        </div>
      </Link>
    );
  }
  if (content.Status === 4) {
    return (
      <Link to={`/order-detail-record/${content.Id}`}>
        {/* // <Link to={`order-rincian-how-payment/${content.Id}/`}> */}
        <div className="order-wrapper">
          <div className="order-data">
            <img
              src={content.TalentPhotos}
              className="round-image"
              alt="fameo-idol-icon"
            />
            <div className="order-cred">
              <h5 className="idol-name">{content.TalentNm}</h5>
              <h5 className="order-id">{content.OrderNo}</h5>
            </div>
          </div>
          <div className="order-status-wrapper">
            <div className="status-texts">
              <h5 className="status-label">Status</h5>
              <h4 className="order-status">{content.TransactionStatus}</h4>
            </div>
            <img src={Arrow} className="order-arrow" alt="fameo-arrow" />
          </div>
        </div>
      </Link>
    );
  }
  if (content.Status > 4) {
    return (
      <Link to={`/order-video-finish/${content.Id}`}>
        {/* // <Link to={`order-rincian-how-payment/${content.Id}/`}> */}
        <div className="order-wrapper">
          <div className="order-data">
            <img
              src={content.TalentPhotos}
              className="round-image"
              alt="fameo-idol-icon"
            />
            <div className="order-cred">
              <h5 className="idol-name">{content.TalentNm}</h5>
              <h5 className="order-id">{content.OrderNo}</h5>
            </div>
          </div>
          <div className="order-status-wrapper">
            <div className="status-texts">
              <h5 className="status-label">Status</h5>
              <h4 className="order-status">{content.TransactionStatus}</h4>
            </div>
            <img src={Arrow} className="order-arrow" alt="fameo-arrow" />
          </div>
        </div>
      </Link>
    );
  }

  return (
    <Link to={`/orderInfo/${content.Id}`}>
      {/* // <Link to={`order-rincian-how-payment/${content.Id}/`}> */}
      <div className="order-wrapper">
        <div className="order-data">
          <img
            src={content.TalentPhotos}
            className="round-image"
            alt="fameo-idol-icon"
          />
          <div className="order-cred">
            <h5 className="idol-name">{content.TalentNm}</h5>
            <h5 className="order-id">{content.OrderNo}</h5>
          </div>
        </div>
        <div className="order-status-wrapper">
          <div className="status-texts">
            <h5 className="status-label">Status</h5>
            <h4 className="order-status">{content.TransactionStatus}</h4>
          </div>
          <img src={Arrow} className="order-arrow" alt="fameo-arrow" />
        </div>
      </div>
    </Link>
  );

  // return (
  //   <>
  //       <Link to={`/orderReady/${content.Id}`}>
  //         <div className="order-wrapper">
  //           <div className="order-data">
  //             <img src={content.TalentPhotos} className="round-image" alt="fameo-idol-icon" />
  //             <div className="order-cred">
  //               <h5 className="idol-name">{content.TalentNm}</h5>
  //               <h5 className="order-id">{content.OrderNo}</h5>
  //             </div>
  //           </div>
  //           <div className="order-status-wrapper">
  //             <div className="status-texts">
  //               <h5 className="status-label">Status</h5>
  //               <h4 className="order-status">{content.TransactionStatus}</h4>
  //             </div>
  //             <img src={Arrow} className="order-arrow" alt="fameo-arrow" />
  //           </div>
  //         </div>
  //       </Link>
  //   </>
  // )
}

const ProfileOrderList = ({ orderList }) => <OrderList content={orderList} />;

ProfileOrderList.propTypes = {
  orderList: PropTypes.object.isRequired,
};
OrderList.propTypes = {
  content: PropTypes.object.isRequired,
};

export default ProfileOrderList;
