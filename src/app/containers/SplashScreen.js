import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Logo from '../assets/images/FameoLogo2020-03.svg';

function RenderSplashScreen() {
  const history = useHistory();
  useEffect(() => {
    const timer = setTimeout(() => history.push('/home'), 3000);
    return () => clearTimeout(timer);
  }, [history]);
  return (
    <>
      <div className="container splash-screen-wrapper splash-body d-flex justify-content-center">
        <img src={Logo} className="align-self-center" alt="logo" />
      </div>
    </>
  );
}

const SplashScreen = () => (
  // const [splashReady, setSplash] = useState('1');

  <RenderSplashScreen />
);
export default SplashScreen;
