import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Header from '../components/Header';
import FanComment from '../components/FanComment';
import Back from '../assets/images/icon-back.svg';
import {
  getDetailQa,
  postComment,
  getComment,
  postLike,
  postUnlike,
} from '../services/detailQA';
import TaskBar from '../components/TaskBar';
import { getCookie } from '../helpers';
import SweetAlertNotification from '../components/SweetAlertNotification';

const questionData = {
  name: 'John Doe',
  userImage: require('../assets/images/user-icon-web.png'),
  artistName: 'Marion Jola',
  question:
    'Sed nec finibus felis. Mauris vulputate nisl sed massa dapibus, in pretium?',
  hasVoting: true,
  buttonDetails: {
    color: 'pink',
    icon: require('../assets/images/icon-nice-bigger.svg'),
    text: ' Vote',
  },
  data: [
    {
      commentCard: {
        name: 'John Doe',
        userImage: require('../assets/images/user-icon-web.png'),
        date: 'March 01, 2020',
        time: '8:15 pm',
        comment:
          'Sed nec finibus felis. Mauris vulpu tate nisl sed massa dapibus, in pretium nisl sed massa dapibus, in pretium.',
      },
    },
    {
      commentCard: {
        name: 'John Doe2',
        userImage: require('../assets/images/user-icon-web.png'),
        date: 'March 01, 2020',
        time: '8:15 pm',
        comment:
          'Sed nec finibus felis. Mauris vulpu tate nisl sed massa dapibus, in pretium nisl sed massa dapibus, in pretium.',
      },
    },
    {
      commentCard: {
        name: 'John Doe3',
        userImage: require('../assets/images/user-icon-web.png'),
        date: 'March 01, 2020',
        time: '8:15 pm',
        comment:
          'Sed nec finibus felis. Mauris vulpu tate nisl sed massa dapibus, in pretium nisl sed massa dapibus, in pretium.',
      },
    },
  ],
};

const DetailsQA = () => {
  const [message, setMessage] = useState('');
  const [detailQa, setDetailQa] = useState([]);
  const [comment, setComment] = useState([]);
  const [userID] = useState(Number(getCookie('UserId')));
  // const [error, setError] = useState(false);
  const [like, setLike] = useState(false);
  // const [dislike, setDislike] = useState(false)
  // const [likeActive, setLikeActive] = useState(false)
  // const [dislikeActive, setDislikeActive] = useState(false)
  const [warningnotif, setwarningnotif] = useState(false);

  const AlertNotificationType = {
    notiftype: 'warning',
    isshow: warningnotif,
    message: 'Silahkan login terlebih dahulu',
    OnConfirmUrl: '/signin/',
    url: '/signin/',
  };

  const { QuestionId } = useParams();

  const getData = async () => {
    const params = {
      QuestionId,
    };
    getDetailQa(params).then(resp => {
      setDetailQa(resp.getCurrentData);
    });
    getComment(params).then(resp => {
      setComment(resp.getCurrentData);
    });
  };

  const submit = e => {
    if (isNaN(userID)) {
      setwarningnotif(true);
      // history.push('/signin')
    } else {
      e.preventDefault();

      const data = {
        CommentMsg: message || '',
        QuestionId: QuestionId || '',
        UserId: userID,
      };

      postComment(data).then(resp => {
        if (resp.Status === 'OK') {
          const params = {
            QuestionId,
          };
          getComment(params).then(resp => {
            setComment(resp.getCurrentData);
            setMessage('');
          });
        }
      });
    }
  };

  const handleLike = () => {
    if (isNaN(userID)) {
      setwarningnotif(true);
      // history.push('/signin')
    } else {
      const data = {
        QuestionId,
        UserId: userID,
      };
      setLike(!like);

      if (like) {
        postLike(data).then(resp => {
          if (resp.Status === 'OK') {
            const params = {
              QuestionId,
            };
            getDetailQa(params).then(resp => {
              setDetailQa(resp.getCurrentData);
            });
          }
        });
      } else {
        postUnlike(data).then(resp => {
          if (resp.Status === 'OK') {
            const params = {
              QuestionId,
            };
            getDetailQa(params).then(resp => {
              setDetailQa(resp.getCurrentData);
            });
          }
        });
      }
    }
  };
  useEffect(() => {
    window.scroll(0, 0);
    getData();
  }, []);

  return (
    <>
      {detailQa.map((content, i) => (
        <div className="details-qa" key={i}>
          <Header
            content={{
              title: 'Detail Pertanyaan',
              BtnLeftIcon: Back,
              hasSubtitle: true,
              subtitle: `Untuk ${content.TalentName} dari ${content.UserName}`,
            }}
          />
          <div className="container">
            {/* <div className="content-container"> */}
            <FanComment
              content={content}
              comment={comment}
              data={questionData}
              submit={submit}
              setMessage={setMessage}
              handleClick={handleLike}
              totalLike={detailQa}
            />

            {/* </div> */}
          </div>
          <TaskBar active="tanyajawab" />
        </div>
      ))}

      <SweetAlertNotification content={AlertNotificationType} />
    </>
  );
};

export default DetailsQA;
