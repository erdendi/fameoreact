   
import React from 'react';
import { Link } from 'react-router-dom';
import Header from '../components/Header';
import NotificationDetail from '../components/NotificationDetail';
import Back from '../assets/images/icon-back.svg';

const details = {
  status: 'Sedang dalam proses',
  date: '5 Maret 2020',
  time: '08:56 AM',
  name: 'Michelle Ziudith',
  card: 'Bank BCA - 8901234567',
  amount: 'IDR 100.000,00',
};

const OrderPaymentNotificationDetail = () => (
  <>
    <div className="order-payment-notification">
      <Header
        content={{
          title: 'Tarik Dana',
          BtnLeftIcon: Back,
          hasSubtitle: 'true',
          subtitle: 'No. Pesanan CLM2020000006',
        }}
      />
      <div className="container">
        <NotificationDetail content={details} />

        <div className="question">
          <p>
            Ada pertanyaan?
            <Link to="/">Hubungi kami disini</Link>
          </p>
        </div>
      </div>
    </div>
  </>
);

export default OrderPaymentNotificationDetail;
