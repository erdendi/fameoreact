import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Form, Label } from 'reactstrap';
import Header from '../components/Header';
import SingleButton from '../components/Button';
import Back from '../assets/images/icon-back.svg';
import Settings from '../assets/images/icon-more.svg';
import { getDetailTalent } from '../services/detail';
import { sendTextQuestion } from '../services/question';
import { getCookie } from '../helpers';
import SweetAlertNotification from '../components/SweetAlertNotification';

const AskTalent = () => {
  const [talents, setTalents] = useState([]);
  const [userID] = useState(Number(getCookie('UserId')));
  const { talentId } = useParams();
  const [QuestionData, setQuestionData] = useState();
  const [notif, setnotif] = useState(false);
  const [warningnotif, setwarningnotif] = useState(false);

  const SweetAlertType = {
    notiftype: 'Success',
    isshow: notif,
    message: 'Submit Successfully',
    OnConfirmUrl: `/detail/${talentId}`,
    url: `/detail/${talentId}`,
  };

  const AlertNotificationType = {
    notiftype: 'warning',
    isshow: warningnotif,
    message: 'Silahkan login terlebih dahulu',
    OnConfirmUrl: '/signin/',
    url: '/signin/',
  };

  const getTalents = async () => {
    // setError(false);
    const params = {
      TalentId: talentId,
    };
    getDetailTalent(params).then(resp => {
      setTalents(resp);
    });
  };

  const SubmitQuestion = () => {
    if (typeof QuestionData !== 'undefined') {
      if (QuestionData.length > 0) {
        document.getElementById('QuestionValidation').style.display = 'none';
        const params = {
          TalentId: talentId,
          UserId: userID,
          Question: QuestionData,
        };

        sendTextQuestion(params).then(resp => {
          if (resp.Status === 'OK') {
            setnotif(true);
          }
        });
      } else {
        document.getElementById('QuestionValidation').style.display = 'block';
      }
    } else {
      document.getElementById('QuestionValidation').style.display = 'block';
    }
  };
  useEffect(() => {
    window.scroll(0, 0);
    document.getElementById('QuestionValidation').style.display = 'none';

    if (isNaN(userID)) {
      setwarningnotif(true);
      // history.push('/signin')
    } else {
      getTalents();
    }
  }, [userID]);

  return (
    <>
      <Header
        content={{
          title: 'Tanya Talent',
          BtnLeftIcon: Back,
          BtnRightIcon: Settings,
        }}
      />
      <div className="order order-personal ask-talent">
        <div className="container">
          <div className="content-wrapper">
            <h2 className="title c-white">Punya Pertanyaan?</h2>
            <div className="profile-image">
              <img src={talents.LinkImg} className="image" alt="" />
            </div>
            <p className="name c-white">{talents.TalentNm}</p>
            <Form>
              <Label className="c-white title-textbox" for="example">
                Tuliskan Pertanyaan
              </Label>
              <textarea
                name="code"
                id="textarea-ask"
                placeholder="Tuliskan pertanyaan"
                onChange={e => {
                  setQuestionData(e.target.value);
                }}
              />
              <label className="validation" id="QuestionValidation">
                Mohon diisi field diatas !!
              </label>
              <SingleButton
                type="submit"
                onClick={SubmitQuestion}
                content={{ text: 'KIRIM PERTANYAAN', color: 'pink', url: '#' }}
              />
            </Form>
          </div>
        </div>
      </div>

      <SweetAlertNotification content={SweetAlertType} />

      <SweetAlertNotification content={AlertNotificationType} />
    </>
  );
};

export default AskTalent;
