/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React from 'react';
import { UncontrolledCollapse } from 'reactstrap';
import Header from '../components/Header';
import Back from '../assets/images/icon-left-arrow-two.svg';
import DownArrow from '../assets/images/icon-down-arrow.svg';
import icon1 from '../assets/images/live/date-live.svg';
import icon2 from '../assets/images/live/jam-live.svg';
import icon3 from '../assets/images/live/hourglass-live.svg';
import icon4 from '../assets/images/live/eye-live.svg';
import icon5 from '../assets/images/live/url-live.svg';
import icon6 from '../assets/images/live/muang-live.svg';

const Reports = () => {
  function changeimg(e) {
    document.getElementById(e).classList.toggle('mystyle');
  }

  return (
    <div className="order-information-wrapper reports-wrapper">
      <Header content={{ title: 'Reports', BtnLeftIcon: Back }} />
      <div className="after-heading-short-wrapper text-white">
        <div className="container detail-block pt-4">
          <div
            id="toggler1A"
            onClick={() => {
              changeimg('toggler1B');
            }}
            className="main-box-wrapper"
          >
            <div className="total-wrapper">
              <p className="total-label">Kumpul bersama teman baru</p>
              <div className="amount-wrapper">
                <img src={DownArrow} alt="fameo-down-arrow" id="toggler1B" />
              </div>
            </div>
            <UncontrolledCollapse toggler="#toggler1A">
              <div>
                <h2 className="section-title">Tema : Komedi</h2>
                <div className="row no-gutters">
                  <div className="col-4 col-md-4">
                    <img src={icon1} />{' '}
                    <span className="desc--">20 Maret 2020</span>
                  </div>
                  <div className="col-4 col-md-4">
                    <img src={icon2} />{' '}
                    <span className="desc--">17:00 - 18.35</span>
                  </div>
                  <div className="col-4 col-md-4">
                    <img src={icon3} /> <span className="desc--">95 Menit</span>
                  </div>
                  <div className="col-4 col-md-4">
                    <img src={icon4} />{' '}
                    <span className="desc--">837 Viewers</span>
                  </div>
                  <div className="col-4 col-md-4">
                    <img src={icon5} />{' '}
                    <span className="desc--">837 Viewers</span>
                  </div>
                  <div className="col-4 col-md-4">
                    <img src={icon6} /> <span className="desc--">250.000</span>
                  </div>
                </div>
              </div>
            </UncontrolledCollapse>
          </div>

          <div
            id="toggler2A"
            onClick={() => {
              changeimg('toggler2B');
            }}
            className="main-box-wrapper"
          >
            <div className="total-wrapper">
              <p className="total-label">Bicara tentang percintaan</p>
              <div className="amount-wrapper">
                <img src={DownArrow} alt="fameo-down-arrow" id="toggler2B" />
              </div>
            </div>
            <UncontrolledCollapse toggler="#toggler2A" />
          </div>

          <div
            id="toggler3A"
            onClick={() => {
              changeimg('toggler3B');
            }}
            className="main-box-wrapper"
          >
            <div className="total-wrapper">
              <p className="total-label">Lagi Bete</p>
              <div className="amount-wrapper">
                <img src={DownArrow} alt="fameo-down-arrow" id="toggler3B" />
              </div>
            </div>
            <UncontrolledCollapse toggler="#toggler3A" />
          </div>

          <div
            id="toggler4A"
            onClick={() => {
              changeimg('toggler4B');
            }}
            className="main-box-wrapper"
          >
            <div className="total-wrapper">
              <p className="total-label">Baru putus</p>
              <div className="amount-wrapper">
                <img src={DownArrow} alt="fameo-down-arrow" id="toggler4B" />
              </div>
            </div>
            <UncontrolledCollapse toggler="#toggler4A" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Reports;
