/* eslint-disable react/no-unescaped-entities */

import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

const HowPayBNI = () => {
  const [activeTab, setActiveTab] = useState('1');
  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };
  return (
    <div className="row">
      <div className="col-12 col-md-12 how-paryment-m">
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '1' })}
              onClick={() => {
                toggle('1');
              }}
            >
              ATM BNI
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '2' })}
              onClick={() => {
                toggle('2');
              }}
            >
              Internet Banking
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="1">
            <ul className="timeline">
              <li>
                <p>Pada menu Utama, pilih Menu Lainnya.</p>
              </li>
              <li>
                <p>Pilih Transfer.</p>
              </li>
              <li>
                <p>Pilih Rekening Tabungan.</p>
              </li>
              <li>
                <p>Pilih ke Rekening BNI.</p>
              </li>
              <li>
                <p>
                  Masukkan "Nomor Virtual Account" dan pilih tekan jika benar
                  dan lalu tekan Benar.{' '}
                </p>
              </li>
              <li>
                <p>
                  Masukkan jumlah tagihan yang akan anda bayar secara lengkap,
                  pembayaran dengan jumlah tidak sesuai akan otomatis ditolak.
                </p>
              </li>
            </ul>
          </TabPane>
          <TabPane tabId="2">
            <ul className="timeline">
              <li>
                <p>
                  Ketik alamat (https://ibank.bni.co.id/) kemudian klik Masuk.{' '}
                </p>
              </li>
              <li>
                <p>Silahkan masukan UserID dan Password.</p>
              </li>
              <li>
                <p>
                  Klik Menu Transfer kemudian pilih Tambah Rekening Favorit.
                </p>
              </li>
              <li>
                <p>Masukan nama, nomor rekening dan email, lalu klik Lanjut.</p>
              </li>
              <li>
                <p>Masukkan Kode Otentikasi dari token Anda dan klik Lanjut.</p>
              </li>
              <li>
                <p>
                  Kembali ke menu utama dan pilih Transfer lalu Transfer Antar
                  Rekening BNI
                </p>
              </li>
              <li>
                <p>
                  Pilih rekening yang telah anda favoritkan sebelumnya di
                  rekening tujuan lalu lanjutkan pengisian dan tekan Lanjut.
                </p>
              </li>
              <li>
                <p>
                  Pastikan detail Transaksi anda benar, lalu masukan Otentikasi
                  dan tekan Lanjut.
                </p>
              </li>
            </ul>
          </TabPane>
        </TabContent>
      </div>
    </div>
  );
};

export default HowPayBNI;
