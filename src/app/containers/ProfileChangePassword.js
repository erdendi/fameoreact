/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Form, FormGroup, Input, Label, Alert } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import SweetAlert from 'react-bootstrap-sweetalert';
import Back from '../assets/images/icon-left-arrow-two.svg';
import Eye from '../assets/images/eye-show.svg';
import SingleButton from '../components/Button';
import { getEditProfile, postChangePassword } from '../services/profileEdit';
import { getCookie } from '../helpers';
import LoadingFull from '../components/LoadingFull';

const ProfileUpdate = () => {
  const history = useHistory();
  const [user, setUser] = useState(false);
  const [loading, setLoading] = useState(false);
  const [togleVal, setTogleVal] = useState(true);
  const [togleVal2, setTogleVal2] = useState(true);
  const [userID] = useState(Number(getCookie('UserId')));

  const [oldpassword, oldPassword] = useState('');
  const [newpassword, newPassword] = useState('');

  const [notif, setnotif] = useState(false);
  const [alert, setAlert] = useState(false);

  const postUPdate = async () => {
    setLoading(true);

    const validPassword = newpassword.match(
      /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])([a-zA-Z0-9]{8,})$/,
    );
    /*
      /^
      (?=.*\d)          // should contain at least one digit
      (?=.*[a-z])       // should contain at least one lower case
      (?=.*[A-Z])       // should contain at least one upper case
      [a-zA-Z0-9]{8,}   // should contain at least 8 from the mentioned characters
      $/
    */

    if (oldpassword === newpassword) {
      if (validPassword) {
        const payload = {
          Id: userID,
          // OldPassword: oldpassword,
          NewPassword: newpassword,
        };

        postChangePassword(payload)
          .then(resp => {
            if (resp.Status === '1') {
              setnotif(true);
              setAlert(false);
              setLoading(false);
            } else {
              setAlert(true);
              setLoading(false);
            }
          })
          .catch(() => {
            setAlert(true);
            setLoading(false);
          });
      } else {
        setAlert(true);
        setLoading(false);
      }
    } else {
      setAlert(true);
      setLoading(false);
    }
  };

  function showhidekatasandi() {
    setTogleVal(!togleVal);
  }
  function showhidekatasandi2() {
    setTogleVal2(!togleVal2);
  }

  return (
    <>
      <div className="profile-update-wrapper">
        {/* <Header content={{ title: 'Ubah Profil', BtnLeftIcon: Back, BtnRightText: 'Simpan'}} /> */}

        <header>
          <div className="container">
            <div className="content-holder">
              <div
                onClick={() => history.goBack()}
                className="position-absolute"
              >
                <img src={Back} className="icon-header" alt="" />
              </div>
              <span className="title">Ubah Password</span>
              {/* <div className="a header-right" onClick={() => postUPdate()}>
                Simpan
              </div> */}
            </div>
          </div>
        </header>

        <div className="after-heading-wrapper">
          <div className="container">
            <div className="update-form-wrapper">
              <Form>
                <FormGroup>
                  <div className="password-holder">
                    <Label for="password" className="title-textbox">
                      Kata Sandi Baru
                    </Label>
                    <Input
                      type={togleVal ? 'password' : 'text'}
                      name="password"
                      className="textbox"
                      placeholder="Ubah Kata Sandi"
                      onChange={e => {
                        oldPassword(e.target.value);
                      }}
                    />
                    <img
                      src={Eye}
                      id="show-hide-kata-sandi"
                      onClick={showhidekatasandi}
                      alt="eye"
                      style={{ position: 'absolute', top: '30px' }}
                    />
                  </div>
                </FormGroup>
                <FormGroup>
                  <div className="password-holder">
                    <Label for="password" className="title-textbox">
                      Konfirmasi Kata Sandi Baru
                    </Label>
                    <Input
                      type={togleVal2 ? 'password' : 'text'}
                      name="password"
                      className="textbox"
                      placeholder="Ubah Kata Sandi"
                      onChange={e => {
                        newPassword(e.target.value);
                      }}
                    />
                    <img
                      src={Eye}
                      id="show-hide-kata-sandi"
                      onClick={showhidekatasandi2}
                      alt="eye"
                      style={{ position: 'absolute', top: '30px' }}
                    />
                  </div>
                </FormGroup>
                <Alert
                  color="warning"
                  isOpen={alert}
                  style={{ fontSize: '12px' }}
                >
                  Kata sandi baru tidak sama <br />
                  Kata sandi minimal 8 digit, campuran huruf kecil, huruf besar
                  dan angka
                </Alert>
                <SingleButton
                  type="submit"
                  onClick={() => postUPdate()}
                  content={{ text: 'SIMPAN', color: 'pink' }}
                />
              </Form>
            </div>
            {loading && <LoadingFull />}
          </div>
        </div>
      </div>

      <SweetAlert
        success
        show={notif}
        title="Update Succesfully"
        // Text =  "Username atau Password yang anda masukkan salah"
        onConfirm={() => {
          setnotif(false);
          window.location.reload();
        }}
        onCancel={() => {
          setnotif(false);
          window.location.reload();
        }}
        onEscapeKey={() => {
          setnotif(false);
          window.location.reload();
          // window.location.href = "/profile"
        }}
        onOutsideClick={() => {
          setnotif(false);
          window.location.reload();
          // window.location.href = "/profile"
        }}
      />
    </>
  );
};

export default ProfileUpdate;
