/* eslint-disable object-curly-newline */

/* eslint-disable react/no-array-index-key */
/* eslint-disable react/jsx-props-no-spreading */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';
import Swiper from 'react-id-swiper';
import ProfileOrderList from './ProfileOrderList';
import 'swiper/css/swiper.css';

const params = {
  slidesPerView: 4.1,
  spaceBetween: 5,
};
const Tabs = ({ items, orderList }) => {
  const [activeTab, setActiveTab] = useState(1);
  const OrderProgress = orderList.filter(v => v.Status > 1 && v.Status < 7);
  const OrderDone = orderList.filter(v => v.Status > 5 || v.Status < -1);
  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  function ReturnTabs({ content }) {
    return (
      <div className="tab-wrapper">
        {/* <h5 className="text-white font-weight-bold">Order Saya</h5> */}
        <Nav tabs>
          <Swiper {...params}>
            {content.data.map((itemSwiper, i) => (
              <NavItem className="tab-item" key={i}>
                <NavLink
                  className={classnames('tab-link', {
                    active: activeTab === itemSwiper.id,
                  })}
                  onClick={() => {
                    toggle(itemSwiper.id);
                  }}
                >
                  <span>{itemSwiper.tabTitle}</span>
                </NavLink>
              </NavItem>
            ))}
          </Swiper>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId={1}>
            {OrderProgress.map(item => (
              <>
                {/* {item.fanQuestions === true && item.id === activeTab ? (
                  <FanQuestions />
                ) : (
                  ''
                )} */}
                {activeTab ? <ProfileOrderList orderList={item} /> : ''}
              </>
            ))}
          </TabPane>

          <TabPane tabId={2}>
            {OrderDone.map(item => (
              // if (item.TransactionStatus === "Selesai dan telah memberikan rating")
              <>{activeTab ? <ProfileOrderList orderList={item} /> : ''}</>
            ))}
          </TabPane>
        </TabContent>
      </div>
    );
  }

  ReturnTabs.propTypes = {
    content: PropTypes.array.isRequired,
  };

  return (
    <div>
      <ReturnTabs content={items} />
    </div>
  );
};

Tabs.propTypes = {
  items: PropTypes.object.isRequired,
  orderList: PropTypes.array.isRequired,
};
export default Tabs;
