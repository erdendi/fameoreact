/* eslint-disable react/no-unescaped-entities */

import React, { useState } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

const HowPayMandiri = () => {
  const [activeTab, setActiveTab] = useState('1');
  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };
  return (
    <div className="row">
      <div className="col-12 col-md-12 how-paryment-m">
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '1' })}
              onClick={() => {
                toggle('1');
              }}
            >
              ATM Mandiri
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '2' })}
              onClick={() => {
                toggle('2');
              }}
            >
              Internet Banking
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="1">
            <ul className="timeline">
              <li>
                <p>Pada menu Utama, pilih Bayar/Beli.</p>
              </li>
              <li>
                <p>Pilih menu Lainnya.</p>
              </li>
              <li>
                <p>Pilih menu Multi Payment.</p>
              </li>
              <li>
                <p>Pilih penyedia "Fast Pay" dan pilih Lanjut.</p>
              </li>
              <li>
                <p>
                  Masukkan "Nomor Virtual Account" dan "Nominal" yang akan
                  dibayarkan lalu tekan Benar.
                </p>
              </li>
              <li>
                <p>
                  Pada halaman konfirmasi akan muncul detail pembayaran Anda.
                  Jika informasi telah sesuai tekan Ya.
                </p>
              </li>
            </ul>
          </TabPane>
          <TabPane tabId="2">
            <ul className="timeline">
              <li>
                <p>
                  Login ke Internet Banking Mandiri
                  (https://ib.bankmandiri.co.id)
                </p>
              </li>
              <li>
                <p>
                  Pada menu utama, pilih
                  <b>Bayar</b>, lalu pilih
                  <b>Multi Payment</b>{' '}
                </p>
              </li>
              <li>
                <p>
                  Pilih akun anda dari rekening, kemudian di penyedia jasa pilih
                  <b>Faspay</b>
                </p>
              </li>
              <li>
                <p>
                  Masukkan
                  <b>"Nomor Virtual Account" </b> dan
                  <b>"Nominal" </b> yang akan dibayarkakn dan klik
                  <b>Lanjut</b>{' '}
                </p>
              </li>
              <li>
                <p>
                  Masukan PIN dan Transaksi selesai, simpan bukti pembayaran
                  Anda
                </p>
              </li>
            </ul>
          </TabPane>
        </TabContent>
      </div>
    </div>
  );
};

export default HowPayMandiri;
