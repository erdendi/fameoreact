import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
// import { Label, Input } from 'reactstrap'
import { formatPrice } from 'utils/price';
import Header from '../components/Header';
import Detail from '../components/Detail';
import SingleButton from '../components/Button';
import Back from '../assets/images/icon-back.svg';
import Settings from '../assets/images/icon-more.svg';
import { getCookie } from '../helpers';
import {
  getConfirmation,
  CancelledBooking,
  postUpdatePayment,
} from '../services/order';
// import Loading from '../components/Loading';
import LoadingFull from '../components/LoadingFull';

const OrderConfirmation = () => {
  const history = useHistory();
  const { bookId } = useParams();
  const [loading, setLoading] = useState(true);
  // const [cookieBookID] = useState(parseInt(getCookie("BookId")));
  const [confirm, setConfirm] = useState(false);
  const [userId] = useState(Number(getCookie('UserId')));

  const CancelledBookingOrder = () => {
    setLoading(true);
    const params = {
      Id: bookId,
      UserId: userId,
    };
    CancelledBooking(params).then(resp => {
      if (resp.Status === 'OK') {
        history.push('/profile');
        setLoading(false);
      }
    });
  };

  const toPayment = () => {
    if (confirm.TotalPay === 0 && confirm.VoucherCode != null) {
      setLoading(true);
      const params = {
        Id: bookId,
        UserId: userId,
      };
      getConfirmation(params).then(resp => {
        const update = {
          Id: resp.Id,
          Status: 3,
          // "TotalPay": parseInt(parseInt(PriceOrderData) - parseInt(resp)),
          TotalPay: Number(resp.TotalPay),
          Potongan: resp.Potongan,
          PriceAmount: Number(resp.PriceAmount),
          VoucherCode: resp.VoucherCode,
          userId,
        };

        postUpdatePayment(update)
          // eslint-disable-next-line no-shadow
          .then(resp => {
            if (resp.Status === 'OK') {
              setLoading(false);
              history.push('/order-payment-success');
            }
          });
      });
    } else if (
      confirm.TransactionStatus === 'Order telah diterima oleh talent'
    ) {
      history.push('/profile');
    } else {
      history.push(`/order-method-payment/${bookId}`);
    }
  };
  useEffect(() => {
    window.scroll(0, 0);
    const params = {
      Id: bookId,
      UserId: userId,
    };
    getConfirmation(params).then(resp => {
      setConfirm(resp);
      setLoading(false);
    });
  }, [bookId, userId]);

  return (
    <>
      {loading && <LoadingFull />}
      <Header
        content={{
          title: 'Konfirmasi Pemesanan',
          BtnLeftIcon: Back,
          BtnRightIcon: Settings,
          hasSubtitle: true,
          subtitle: confirm
            ? `No. Pesanan Fameo ${confirm.OrderNo}`
            : 'No. Pesanan Fameo ',
        }}
      />
      <div className="order order-confirmation">
        <div className="container">
          <div className="content-wrapper">
            <h2 className="title c-white">Konfirmasi Pesanan</h2>
            <Detail content={confirm} />
            <div className="promo-holder">
              {!!confirm && confirm.Potongan !== null ? (
                <>
                  <div className="content-holder">
                    <span>Ingin ganti Voucher ? </span>
                    <p className="pConfirmation">
                      <a
                        href={`/order-promo-code/${Number(
                          getCookie('BookId'),
                        )}`}
                        className="btn btn-info"
                        style={{ color: 'white' }}
                      >
                        Ganti Voucher
                      </a>
                    </p>
                  </div>
                  <div className="content-holder">
                    <span>Kode Voucher</span>
                    <p>{confirm.VoucherCode}</p>
                  </div>
                  <div className="content-holder">
                    <span>Total</span>
                    <p>
                      {confirm.SalePrice > 0
                        ? formatPrice(confirm.SalePrice)
                        : formatPrice(confirm.PriceAmount)}
                    </p>
                  </div>
                  <div className="content-holder">
                    <span>Promo Discount</span>
                    <p>-{formatPrice(confirm.Potongan)}</p>
                  </div>
                </>
              ) : (
                <div className="content-holder">
                  <span>Punya Kode Promo?</span>
                  {/* <Link to={"/order-promo-code/"+ confirm.SalePrice > 0 ? confirm.SalePrice : confirm.PriceAmount}>TAMBAH</Link> */}
                  <a href={`/order-promo-code/${Number(getCookie('BookId'))}`}>
                    TAMBAH
                  </a>
                </div>
              )}
            </div>
            <div className="subtotal-holder">
              <span className="label">Subtotal</span>
              <p className="subtotal-amount">
                {formatPrice(confirm.TotalPay)}
                {/* {confirm.TotalPay || confirm.SalePrice > 0 ? confirm.SalePrice : confirm.PriceAmount}
                 */}
                {/* {confirm.VoucherCode == null ? confirm.TotalPay || confirm.SalePrice > 0 ? confirm.SalePrice : confirm.PriceAmount :confirm.TotalPay}
                 */}
              </p>
            </div>
            <SingleButton
              onClick={toPayment}
              content={{ text: 'LANJUTKAN', color: 'pink' }}
            />

            <button
              type="button"
              onClick={CancelledBookingOrder}
              className="btn btn-warning"
              style={{
                marginBottom: '35px',
                marginTop: '0px',
                borderRadius: '100px',
              }}
            >
              Batalkan Order
            </button>
            {/* <SingleButton onClick={toPayment} content={{ text: 'Batalkan Order', color: 'yellow' }} />
             */}
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderConfirmation;
