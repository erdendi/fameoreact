/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable radix */

/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import {
  useParams,
  // useHistory
} from 'react-router-dom';
import { Row, Col } from 'reactstrap';
import Header from '../components/Header';
import Back from '../assets/images/icon-back.svg';
import Settings from '../assets/images/icon-more.svg';

// import Timeimg from '../assets/images/pembayaran/time.svg';
import Infoimg from '../assets/images/pembayaran/info.svg';
import Bcaimg from '../assets/images/pembayaran/bca.svg';
import Bniimg from '../assets/images/pembayaran/bni.svg';
// import Briimg from '../assets/images/pembayaran/bri.svg';
import MandiriImg from '../assets/images/pembayaran/mandiri.png';
import Leftimg from '../assets/images/pembayaran/left_arrow.png';
// import { getCookie } from '../helpers';

const OrderGenerateVirtual = () => {
  // const history = useHistory();
  // const [cookieBookID] = useState(Number(getCookie('BookId')));
  const { bookId } = useParams();

  const PosVABCA = async () => {
    window.location = `/order-rincian/${bookId}/BCA`;
  };
  const PosVABNI = async () => {
    window.location = `/order-rincian/${bookId}/BNI`;
  };
  // const PosVABRI = async () => {
  //   window.location = `/order-rincian/${bookId}/BRI`;
  // };
  const PosVAMandiri = async () => {
    window.location = `/order-rincian/${bookId}/Mandiri`;
  };

  return (
    <>
      <Header
        content={{
          title: 'Pilih Metode Pembayaran',
          BtnLeftIcon: Back,
          BtnRightIcon: Settings,
          hasSubtitle: true,
          subtitle: 'No. Pesanan Fameo ORD-2019000184',
        }}
      />
      <div className="order order-confirmation">
        <div className="container">
          <div className="content-wrapper">
            {/* <div className="warning-selesaikan" style={{ marginBottom : '15px' }}>
              Selesaikan pembayaran dalam 09 mnt 11 dtk
               <img src={Timeimg} className="profile pl-2" alt="" />
            </div> */}

            <section className="content-pembayaran">
              <Row>
                <Col xs="1">
                  <img
                    src={Infoimg}
                    className="profile"
                    alt=""
                    style={{ height: '15px' }}
                  />
                </Col>
                <Col xs="11">
                  <p className="info-detail-3-d">
                    Kamu bisa transfer dari layanan perbankan ATM dan Internet
                    Banking.
                  </p>
                </Col>
              </Row>
              <hr />
              <Row>
                <Col md="12">
                  <div className="form-group">
                    <h4>Pilih Bank</h4>
                  </div>
                  <div className="accordion" id="accordion-pembayaran">
                    <a onClick={PosVABCA}>
                      <div className="card">
                        <div className="card-header detail-3-l">
                          <h2 className="mb-0">
                            <button className="btn btn-link" type="button">
                              <Row>
                                <Col xs="3">
                                  <img
                                    src={Bcaimg}
                                    className="profile"
                                    alt=""
                                  />
                                </Col>
                                <Col xs="6">
                                  <span>Transfer BCA</span>
                                </Col>
                                <Col xs="3">
                                  <img
                                    src={Leftimg}
                                    className="left-arrow---"
                                    alt=""
                                    style={{ top: '8px' }}
                                  />
                                </Col>
                              </Row>
                            </button>
                          </h2>
                        </div>
                      </div>
                    </a>
                    <a onClick={PosVABNI}>
                      <div className="card">
                        <div className="card-header detail-3-l">
                          <h2 className="mb-0">
                            <button className="btn btn-link" type="button">
                              <Row>
                                <Col xs="3">
                                  <img
                                    src={Bniimg}
                                    className="profile"
                                    alt=""
                                  />
                                </Col>
                                <Col xs="6">
                                  <span>Transfer BNI</span>
                                </Col>
                                <Col xs="3">
                                  <img
                                    src={Leftimg}
                                    className="left-arrow---"
                                    alt=""
                                    style={{ top: '8px' }}
                                  />
                                </Col>
                              </Row>
                            </button>
                          </h2>
                        </div>
                      </div>
                    </a>
                    {/* <a onClick={PosVABRI}>
                      <div className="card">
                        <div className="card-header detail-3-l">
                          <h2 className="mb-0">
                            <button className="btn btn-link" type="button">
                              <Row>
                                <Col xs="3">
                                  <img src={Briimg} className="profile" alt="" />
                                </Col>
                                <Col xs="6">
                                  <span>Transfer BRI</span>
                                </Col>
                                <Col xs="3">
                                  <img src={Leftimg} className="left-arrow---" alt="" style={{ top: '8px' }} />
                                </Col>
                              </Row>
                            </button>
                          </h2>
                        </div>
                      </div>
          </a> */}

                    <a onClick={PosVAMandiri}>
                      <div className="card">
                        <div className="card-header detail-3-l">
                          <h2 className="mb-0">
                            <button className="btn btn-link" type="button">
                              <Row>
                                <Col xs="3">
                                  <img
                                    src={MandiriImg}
                                    width={60}
                                    height={40}
                                    className="profile"
                                    alt=""
                                  />
                                </Col>
                                <Col xs="6">
                                  <span>Transfer Mandiri</span>
                                </Col>
                                <Col xs="3">
                                  <img
                                    src={Leftimg}
                                    className="left-arrow---"
                                    alt=""
                                    style={{ top: '8px' }}
                                  />
                                </Col>
                              </Row>
                            </button>
                          </h2>
                        </div>
                      </div>
                    </a>
                  </div>
                </Col>
              </Row>
            </section>
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderGenerateVirtual;
