import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { SocialIcon } from 'react-social-icons';
import Header from '../components/Header';
import Back from '../assets/images/icon-left-arrow-two.svg';
import Settings from '../assets/images/icon-settings.svg';
import Share from '../assets/images/share-two.svg';
import SingleButton from '../components/Button';

import { getOrderInfo } from '../services/orderInfo';
import Play from '../assets/images/search/play.svg';
import ModalVideo from '../components/ModalVideo';
import { getCookie } from '../helpers';

// import Idol from '../assets/images/idol-pose.png'
// import IconPlay from '../assets/images/icon-play-white.svg'

const ProfileOrderReady = () => {
  const [orderData, setOrderData] = useState([]);
  const [modalVideo, setModalVIdeo] = useState(false);
  const [dataModal, setDataModal] = useState(false);
  const [modalShare, setModalShare] = useState(false);
  const toggleShare = () => {
    setModalShare(!modalShare);
  };

  const { bookId } = useParams();
  const [userId] = useState(Number(getCookie('UserId')));

  const hideModalprop = () => {
    setDataModal(false);
    setModalVIdeo(false);
  };

  const GotoUploadReaction = () => {
    window.location.href = `/upload/${bookId}`;
  };

  const showModalprop = content => {
    // setDataModal(content);
    const params = {
      Id: bookId,
    };
    getOrderInfo(params).then(resp => {
      setDataModal(resp);
    });
    setModalVIdeo(true);
  };

  const getInfo = async () => {
    const params = {
      Id: bookId,
      userId,
    };
    getOrderInfo(params).then(resp => {
      setOrderData(resp);
    });
  };

  // const getData = async () => {
  //   setError(false);
  //   let params = {
  //     bookId: bookId
  //   }
  //   getOrderVideo(params)
  //     .then(resp => {
  //       setVideo(resp);
  //     })
  //     .catch(err => {
  //       setError(true);
  //       console.log(err);
  //     });

  // };
  useEffect(() => {
    window.scroll(0, 0);
    getInfo();
  }, []);

  return (
    <>
      <div className="profile-ready-wrapper">
        <Header content={{ BtnLeftIcon: Back, BtnRightIcon: Settings }} />
        <div className="after-heading-short-wrapper">
          <div className="container ready-container">
            <h2 className="section-title mb-3" style={{ marginTop: '25px' }}>
              Video Pesanan
            </h2>
            <div className="content-wrapper">
              <div className="box-btn-record-btn">
                <img src={orderData.ThumbLink} alt="idol" />
                <div className="play-icon">
                  <img
                    src={Play}
                    alt="play"
                    content={orderData}
                    onClick={showModalprop}
                  />
                </div>
              </div>

              <div className="actions" style={{ marginTop: '10px' }}>
                <div href="#" className="btn-share">
                  <img src={Share} alt="fameo-share" onClick={toggleShare} />
                </div>
                <a
                  href={orderData.Link}
                  className="btn btn-pink"
                  style={{ color: '#fff' }}
                  doenload
                >
                  DOWNLOAD VIDEO
                </a>
              </div>

              <div className="mb-4" style={{ marginTop: '10px' }}>
                <h3 className="section-title mb-3">Upload Your Reaction</h3>
                {/* <button type="button" className="btn btn-pink" > Upload Reaction</button> */}
                <SingleButton
                  onClick={GotoUploadReaction}
                  content={{ text: 'UPLOAD VIDEO REAKSI', color: 'pink' }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      {modalVideo && <ModalVideo contents={dataModal} isHide={hideModalprop} />}
      <Modal
        isOpen={modalShare}
        toggle={toggleShare}
        contentClassName="modal-share"
      >
        <ModalHeader toggle={toggleShare} charCode="X">
          Share Video on
        </ModalHeader>
        <ModalBody>
          <div className="modal-body-container">
            <div className="modal-icon">
              <SocialIcon
                url={`https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.fameoapp.com%2Fvideo%2F${orderData.FileId}`}
              />
            </div>
            <div className="modal-icon">
              <SocialIcon
                network="whatsapp"
                url={`https://wa.me/?text=https%3A%2F%2Fwww.fameoapp.com%2Fvideo%2F${orderData.FileId}`}
              />
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
};

export default ProfileOrderReady;
