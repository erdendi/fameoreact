/* eslint-disable global-require */

import React from 'react';
import Header from '../components/Header';
import FanComment from '../components/FanComment';
import Back from '../assets/images/icon-back.svg';
// import { Form, FormGroup, Input, Label } from 'reactstrap';

const questionData = {
  name: 'John Doe',
  userImage: require('../assets/images/user-icon-web.png'),
  artistName: 'Marion Jola',
  question:
    'Sed nec finibus felis. Mauris vulputate nisl sed massa dapibus, in pretium?',
  buttonDetails: {
    color: 'pink',
    icon: require('../assets/images/icon-nice-bigger.svg'),
    text: '174 Vote',
  },
  data: [
    {
      commentCard: {
        name: 'John Doe',
        userImage: require('../assets/images/user-icon-web.png'),
        date: 'March 01, 2020',
        time: '8:15 pm',
        comment:
          'Sed nec finibus felis. Mauris vulpu tate nisl sed massa dapibus, in pretium nisl sed massa dapibus, in pretium.',
      },
    },
    {
      commentCard: {
        name: 'John Doe2',
        userImage: require('../assets/images/user-icon-web.png'),
        date: 'March 01, 2020',
        time: '8:15 pm',
        comment:
          'Sed nec finibus felis. Mauris vulpu tate nisl sed massa dapibus, in pretium nisl sed massa dapibus, in pretium.',
      },
    },
    {
      commentCard: {
        name: 'John Doe3',
        userImage: require('../assets/images/user-icon-web.png'),
        date: 'March 01, 2020',
        time: '8:15 pm',
        comment:
          'Sed nec finibus felis. Mauris vulpu tate nisl sed massa dapibus, in pretium nisl sed massa dapibus, in pretium.',
      },
    },
  ],
};
const DetailsQAWithoutButton = () => (
  <div className="details-qa">
    <Header
      content={{
        title: 'Detail Pertanyaan',
        BtnLeftIcon: Back,
        hasSubtitle: true,
        subtitle: 'Untuk Marion Jola dari John Doe',
      }}
    />
    <div className="container">
      {/* <div className="content-container"> */}
      <FanComment content={questionData} />
      {/* </div> */}
    </div>
  </div>
);

export default DetailsQAWithoutButton;
