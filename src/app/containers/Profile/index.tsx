/**
 *
 * Profile
 *
 */

import React, { memo, useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components/macro';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey, actions } from './slice';
import { selectProfile } from './selectors';
import { profileSaga } from './saga';

import { useHistory } from 'react-router-dom';
import TaskBar from '../../components/TaskBar';
import ProfileData from '../../components/ProfileData';
import CardSwiperProfile from '../../components/CardSwiperProfile';
import CardSwiper from '../../components/CardSwiper';
import Loading from '../../components/Loading';
import Settings from '../../assets/images/icon-settings.svg';
import VideoCardSwiper from '../../components/VideoCardSwiper';
import FanQuestions from '../FanQuestions';
import FanFeedBack from '../FanFeedBack';
import Heading from 'app/components/Heading';

interface Props {}

const UserProfileData = {
  type: 'user',
  icon: require('../../assets/images/img-idol-1.png'),
  name: 'John Doe',
  inbox: 2,
  notif: true,
};

const contentSwiperData = {
  headingData: {
    heading: 'Order Saya',
    headingColor: 'white',
    linkName: 'Lihat Semua',
  },

  isNormalCard: true,
};

const contentSwiperData2 = {
  headingData: {
    heading: 'Favorit Saya',
    headingColor: 'white',
    linkName: 'Lihat Semua',
    url: '/',
  },
  isNormalCard: true,
};

export const Profile = memo((props: Props) => {
  const history = useHistory();

  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: profileSaga });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const profile = useSelector(selectProfile);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const dispatch = useDispatch();

  const {
    isLoading,
    user,
    talent,
    order,
    question,
    feedback,
    reaction,
    favorite,
    roleId,
  } = profile;

  useEffect(() => {
    if (order.length === 0) {
      dispatch(actions.getProfileStart());
    } else {
      dispatch(actions.getBackgroundProfileStart());
    }
  }, []);

  const toSettings = () => {
    history.push('/settings');
  };

  return (
    <>
      <Helmet>
        <title>Profile</title>
        <meta name="description" content={`profil ${user.Name}`} />
      </Helmet>
      <Div>
        <div className="section-profile talent-profile-wrapper">
          <header>
            <div className="container">
              <div className="content-holder">
                <span className="title">Profile Saya</span>
                <div className="a header-right" onClick={() => toSettings()}>
                  <img src={Settings} className="icon-header" alt="" />
                </div>
              </div>
            </div>
          </header>
          <div className="after-heading-wrapper">
            <div className="container px-0">
              <ProfileData
                content={UserProfileData}
                talent={user}
                saldo={talent}
              />

              {isLoading ? (
                <Loading />
              ) : (
                <>
                  {order && order.length > 0 && (
                    <div className="section-gift mt-4">
                      <CardSwiperProfile
                        contentSwiper={contentSwiperData}
                        contents={order}
                        userID={String(user.Id)}
                      />
                    </div>
                  )}
                  {favorite.length !== 0 ? (
                    <div className="section-favorite mt-4 pb-3">
                      <CardSwiper
                        contentSwiper={contentSwiperData2}
                        dataTalent={favorite}
                      />
                    </div>
                  ) : (
                    <div className="section-favorite mt-4">
                      <Heading content={contentSwiperData2.headingData} />
                      <div className="not-yet-message">
                        <div style={{ color: '#707070' }}>
                          (Belum ada favorite){' '}
                        </div>
                      </div>
                    </div>
                  )}
                  {!!question && roleId === 3 && (
                    <div className="section-questions mt-5">
                      <FanQuestions content={question} />
                    </div>
                  )}

                  {!!feedback && roleId === 3 && (
                    <div className="section-questions mt-5">
                      <FanFeedBack content={feedback} />
                    </div>
                  )}

                  {!!reaction && (
                    <div className="section-gift mt-4">
                      <VideoCardSwiper contents={reaction} />
                    </div>
                  )}
                </>
              )}
            </div>
          </div>
          <TaskBar active="profile" />
        </div>
      </Div>
    </>
  );
});

const Div = styled.div`
  .not-yet-message {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    text-align: center;
  }
`;
