import { call, put, takeLatest } from 'redux-saga/effects';
import { actions } from './slice';
import { getUser } from 'api/getUserAPI';
import { getCookie } from 'app/helpers';

export function* fetchProfile() {
  const userId = Number(getCookie('UserId'));
  const param = {
    userId,
  };

  try {
    const repos = yield call(getUser, param);
    yield put(actions.getProfileSuccess(repos));
  } catch (error) {
    yield put(actions.getProfileFailure(error));
  }
}

export function* profileSaga() {
  yield takeLatest(actions.getProfileStart.type, fetchProfile);
  yield takeLatest(actions.getBackgroundProfileStart.type, fetchProfile);
}
