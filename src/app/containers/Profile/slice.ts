import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';

// The initial state of the Profile container
export const initialState: ContainerState = {
  isLoading: false,
  error: null,
  profileResponse: {},
  roleId: null,
  order: [],
  question: [],
  feedback: [],
  reaction: [],
  user: {},
  talent: {},
  favorite: [],
};

const profileSlice = createSlice({
  name: 'profile',
  initialState,
  reducers: {
    getProfileStart(state) {
      state.isLoading = true;
      state.error = null;
    },
    getProfileSuccess(state, action: PayloadAction<any[]>) {
      state.isLoading = false;
      state.error = null;

      state.profileResponse = action.payload;
      const {
        UserModel,
        TalentBookList,
        QuestionList,
        TalentReactionVideoList,
        ListBooking,
        TalentModel,
        ListTalentWishlist,
      } = state.profileResponse;

      state.roleId = UserModel.RoleId;
      state.user = UserModel;
      state.talent = TalentModel || {};
      state.reaction = TalentReactionVideoList || [];
      state.question = QuestionList || [];
      state.feedback = TalentReactionVideoList || [];
      state.favorite = ListTalentWishlist || [];

      if (state.roleId === 3) {
        state.order = TalentBookList;
      } else {
        state.order = ListBooking;
      }
    },
    getProfileFailure(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
    getBackgroundProfileStart(state) {
      state.error = null;
    },
  },
});

export const { actions, reducer, name: sliceKey } = profileSlice;
