/* --- STATE --- */
export interface ProfileState {
  isLoading: Boolean;
  error: null | String;
  profileResponse: Object | any;
  roleId: Number | null;
  order: any[];
  question: any[];
  feedback: any[];
  reaction: any[];
  favorite: any[];
  user: Object | any;
  talent: Object | any;
}

export type ContainerState = ProfileState;
