/**
 *
 * SearchTalent
 *
 */

import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components/macro';

import { Form, FormGroup, Input } from 'reactstrap';
import { NavLink } from 'react-router-dom';
// import classNames from 'classnames';

import {
  useInjectReducer,
  useInjectSaga,
} from '../../../utils/redux-injectors';
import { reducer, sliceKey, actions } from './slice';
import { selectSearchTalent } from './selectors';
import { searchTalentSaga } from './saga';

import Header from '../../components/Header';
import TaskBar from '../../components/TaskBar';
// import Loading from '../../components/Loading';
import NormalCard from '../../components/NormalCard';
import SkeletonCard from '../../components/SkeletonCard';

// import { TAB_DATA } from 'constants/tabData';
import TAB_DATA from '../../../constants/tabData';
var classNames = require('classnames');

interface Props {}

export function SearchTalent(props: Props) {
  useInjectReducer({ key: sliceKey, reducer });
  useInjectSaga({ key: sliceKey, saga: searchTalentSaga });

  const [searchTerm, setSearchTerm] = useState('');
  const [activeTab, setActiveTab] = useState('0');

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const searchTalent = useSelector(selectSearchTalent);
  // const idParam = useSelector(selectIdParam);
  const { talents, isLoading, totalData } = searchTalent;
  const dispatch = useDispatch();

  const urlParams = new URLSearchParams(window.location.search);
  const searchParamUrl = urlParams.get('search');
  const idParam = urlParams.get('id');

  useEffect(() => {
    if (searchParamUrl !== null)
      dispatch(actions.setSearchParam(searchParamUrl || ''));
    dispatch(actions.setIdParam(idParam));

    if (searchParamUrl !== '' && searchParamUrl != null) {
      setSearchTerm(searchParamUrl);
      dispatch(actions.getSearchTalentCategoryByIdStart());
    } else if (idParam !== '' || idParam != null || idParam !== '0') {
      toggle(idParam);
    } else {
      dispatch(actions.getSearchTalentStart());
    }
  }, []);

  const handleChange = event => {
    setSearchTerm(event.target.value);
    dispatch(actions.setSearchParam(event.target.value));
  };

  const doSearch = () => {
    dispatch(actions.getSearchTalentCategoryByIdStart());
  };

  const toggle = id => {
    if (String(activeTab) !== String(id)) setActiveTab(id);
    dispatch(actions.setIdParam(id));
    dispatch(actions.getSearchTalentCategoryByIdStart());
  };

  return (
    <Wrapper>
      <Helmet>
        <title>Search Talent</title>
        <meta name="search talent" content="Description of SearchTalent" />
      </Helmet>
      <Header content={{ title: 'TEMUKAN IDOLAMU' }} />
      <div className="search">
        <div className="container">
          <Form className="search-bar">
            <FormGroup>
              <Input
                type="search"
                name="search"
                id="exampleSearch"
                value={searchTerm}
                onChange={event => handleChange(event)}
                onKeyDown={event => handleChange(event)}
                placeholder="Cari berdasarkan nama talent"
              />
              <button
                type="button"
                className="search-icon"
                onKeyPress={() => doSearch()}
                onClick={() => doSearch()}
                aria-label="search"
              />
            </FormGroup>
          </Form>
          <div className="filter-holder">
            <ul className="nav nav-tabs">
              {TAB_DATA.map(item => (
                <li className="nav-item" key={item.id}>
                  <NavLink
                    className={classNames('nav-link', {
                      active: Number(activeTab) === Number(item.id),
                    })}
                    onClick={() => {
                      toggle(item.id);
                    }}
                    to={{
                      pathname: '/search-talent',
                      search: `?id=${item.id}`,
                    }}
                    activeClassName="selectedLink"
                  >
                    {item.name}
                  </NavLink>
                </li>
              ))}
            </ul>
          </div>

          <div className="height-100">
            {isLoading && <SkeletonCard totalData={totalData} />}
            {!isLoading &&
              talents.map(item => (
                <div className="w-50 d-inline-block p-1" key={item.UserId}>
                  <NormalCard content={item} />
                </div>
              ))}
          </div>
        </div>
        <TaskBar active="search-talent" />
      </div>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  .search > .container {
    min-height: 100vh;
  }
  .taskbar {
    height: 70px;

    .taskbar-wrapper {
      width: 100%;
      height: 70px;
      position: sticky;
      text-align: center;
      bottom: 0;
      left: 0;

      .taskbar-item {
        height: 50px;
      }
    }
  }
`;
