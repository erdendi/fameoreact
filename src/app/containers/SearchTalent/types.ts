import { Repo } from 'types/Repo';
/* --- STATE --- */
interface SearchTalentState {
  talents: any[] | any;
  resultsData: any[] | any;
  isLoading: boolean;
  error: string | null;
  talentsById: Repo[] | any;
  idParam: any;
  searchParam: string;
  totalData: number | null;
}

interface SearchTalentLoaded {
  ListTalentCategoryModel: [];
  talents: any[] | any;
  resultsData: any[] | any;
  talentsById: Repo[];
  totalData: number | null;
}

export type { SearchTalentState, SearchTalentLoaded };
