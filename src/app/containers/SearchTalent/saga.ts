import { call, put, select, takeLatest } from 'redux-saga/effects';
import { searchTalent, searchTalentCategoryById } from 'api/searchTalentAPI';
import { actions } from './slice';
import { selectIdParam, selectSearchParam } from './selectors';

export function* fetchSearchTalent() {
  try {
    const response = yield call(searchTalent);
    yield put(actions.getSearchTalentSuccess(response));
  } catch (error) {
    yield put(actions.getSearchTalentFailure(error));
  }
}

export function* fetchSearchTalentCategoryById() {
  const idParam: string = yield select(selectIdParam);
  const searchParam: string = yield select(selectSearchParam);

  const param = { categoryid: idParam || '', talentname: searchParam || '' };

  try {
    const repos = yield call(searchTalentCategoryById, param);
    yield put(actions.getSearchTalentCategoryByIdSuccess(repos));
  } catch (error) {
    yield put(actions.getSearchTalentCategoryByIdFailure(error));
  }
}

export function* searchTalentSaga() {
  // Watches for loadRepos actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(actions.getSearchTalentStart.type, fetchSearchTalent);
  yield takeLatest(
    actions.getSearchTalentCategoryByIdStart.type,
    fetchSearchTalentCategoryById,
  );
}
