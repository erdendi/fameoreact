import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { Repo } from 'types/Repo';
import { SearchTalentState } from './types';

// The initial state of the SearchTalent container
export const initialState: SearchTalentState = {
  talents: [],
  talentsById: [],
  isLoading: false,
  error: null,
  idParam: '0',
  resultsData: [],
  totalData: 12,
  searchParam: '',
};

const searchTalentSlice = createSlice({
  name: 'searchTalent',
  initialState,
  reducers: {
    getSearchTalentStart(state) {
      state.isLoading = true;
      state.error = null;
    },
    getSearchTalentSuccess(state, action: PayloadAction<Repo[]>) {
      state.resultsData = action.payload;
      state.talents = state.resultsData.ListTalentCategoryModel;
      state.totalData = state.resultsData.ListTalentCategoryModel.length;
      state.isLoading = false;
      state.error = null;
    },
    getSearchTalentFailure(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
    getSearchTalentCategoryByIdStart(state) {
      state.isLoading = true;
      state.error = null;
    },
    getSearchTalentCategoryByIdSuccess(state, action: PayloadAction<Repo[]>) {
      state.resultsData = action.payload;
      state.talents = state.resultsData.ListTalentCategoryModel;
      state.totalData = state.resultsData.ListTalentCategoryModel.length;
      state.isLoading = false;
      state.error = null;
    },
    getSearchTalentCategoryByIdFailure(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
    setIdParam(state, action: PayloadAction<any>) {
      state.idParam = action.payload;
    },
    setSearchParam(state, action: PayloadAction<string>) {
      state.searchParam = action.payload;
    },
  },
});

export const { actions, reducer, name: sliceKey } = searchTalentSlice;
