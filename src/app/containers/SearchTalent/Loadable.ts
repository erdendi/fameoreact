/**
 *
 * Asynchronously loads the component for SearchTalent
 *
 */

import { lazyLoad } from 'utils/loadable';

export const SearchTalent = lazyLoad(
  () => import('./index'),
  module => module.SearchTalent,
);
