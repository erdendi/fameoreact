import { createSelector } from '@reduxjs/toolkit';
import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.searchTalent || initialState;

export const selectSearchTalent = createSelector(
  [selectDomain],
  searchTalentState => searchTalentState,
);

export const selectIdParam = createSelector(
  [selectDomain],
  searchTalentState => searchTalentState.idParam,
);

export const selectSearchParam = createSelector(
  [selectDomain],
  searchTalentState => searchTalentState.searchParam,
);
