/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */

/* eslint-disable radix */
import React, { useState, useEffect } from 'react';
import { FormGroup, Input } from 'reactstrap';

import { useHistory } from 'react-router-dom';
import Header from '../components/Header';
import SingleButton from '../components/Button';
import { signUpVerification } from '../services/signup';
import { getCookie } from '../helpers';
// import md5 from "md5";
// import aesjs from "aes-js";

const OrderVerification = () => {
  const history = useHistory();
  const [kodeVerf, setKodeVerf] = useState();

  const submitVerif = e => {
    e.preventDefault();
    const params = {
      Id: parseInt(getCookie('UserId')),
      VerificationCode: kodeVerf,
    };
    signUpVerification(params)
      .then(resp => {
        resp && history.push('/update');
      })
      .catch(err => {
        console.log(err);
      });
  };

  useEffect(() => {
    window.scroll(0, 0);
  }, []);

  return (
    <>
      <Header content={{ title: 'Kodo Verifikasi' }} />
      <div className="order order-verification">
        <div className="container">
          <div className="content-wrapper">
            <h2 className="title c-white">Kode Verifikasi</h2>
            <p className="subtitle c-white">
              Kami telah mengirim kode verifikasi ke nomor handphone Kamu
            </p>
            <div>
              <FormGroup>
                {/* <Input
                  type="number"
                  name="number"
                  id="exampleNumber"
                  className="textbox"
                />
                <Input
                  type="number"
                  name="number"
                  id="exampleNumber"
                  className="textbox"
                />
                <Input
                  type="number"
                  name="number"
                  id="exampleNumber"
                  className="textbox"
                /> */}
                <Input
                  type="number"
                  name="number"
                  id="exampleNumber"
                  className="textbox"
                  onChange={e => setKodeVerf(e.target.value)}
                />
              </FormGroup>
              <div className="button-holder">
                <SingleButton
                  type="submit"
                  onClick={submitVerif}
                  content={{ text: 'Verifikasi', color: 'pink' }}
                />
              </div>
            </div>
            <div className="trivia-holder c-white">
              <p>Fun Fact:</p>
              <p>Chris John is spelt C-h-r-I-s J-o-h-n. Crazy neh?</p>
            </div>
            <p className="note c-white">
              Belum menerima kode verifikasi? Tunggu
              <span>00:15</span> untuk kirim ulang.
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderVerification;
