import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Header from '../components/Header';
import SingleButton from '../components/Button';

import Back from '../assets/images/icon-back.svg';
import { getCookie } from '../helpers';

const TermsConditions = () => {
  const [RoleId] = useState(Number(getCookie('RoleId')));
  return (
    <>
      <Header
        content={{ title: 'Syarat & Ketentuan', BtnLeftIcon: Back, link: '/' }}
      />
      <div className="order terms-conditions">
        <div className="container">
          {RoleId === 2 ? (
            <div className="content-wrapper">
              <p className="subtitle c-white">
                Dengan menggunakan situs atau layanan, Anda menyetujui
                persyaratan ini. Harap baca dengan seksama.
              </p>
              <div className="box-terms">
                <p>
                  <b>1. Fameo Video</b>
                  <br />
                  Melalui Situs dan Layanan, Anda memiliki kesempatan untuk
                  membeli video yang dipersonalisasi ("Video Fameo") dari artis
                  dan influencer ("Pengguna Bakat"). Anda akan mengirimkan
                  permintaan untuk Video Fameo ke Influencer. Setelah permintaan
                  Anda diterima, akun pembayaran Anda akan ditagih sesuai talent
                  yang anda pilih. Setelah menerima pembayaran penuh untuk Video
                  Fameo Anda, Anda diberikan lisensi non-eksklusif di seluruh
                  dunia untuk menggunakan Video Fameo untuk keperluan pribadi,
                  non-komersial, dan non-promosi Anda sendiri. <br />
                  <br />
                  Talent berhak untuk tidak menyebutkan kata/kalimat yang
                  mengandung promosi/komersil jika isi pesan yang disampaikan
                  berbentuk promosi/komersil dalam bentuk apapun, baik dengan
                  mencantumkan nama suatu merek, produk, nama badan usaha, atau
                  komunitas tertentu. <br />
                  <br />
                  Pesanan akan diproses/direkam oleh talent dalam estimasi 3 - 7
                  hari sejak pesanan diterima oleh talent. Jika dalam waktu yang
                  telah ditentukan Anda belum menerima video yang telah dipesan
                  di Fameo, Anda berhak untuk mengajukan pengembalian dana
                  sebesar yang telah Anda bayarkan melalui aplikasi Fameo. Anda
                  juga berhak untuk memperpanjang durasi pengerjaan video dengan
                  persetujuan talent yang bersangkutan dengan menghubungi tim
                  Fameo melalui fitur yang telah disediakan. <br /> <br />
                  Video Fameo dilisensikan dan tidak dijual. Anda tidak boleh
                  menjual kembali hak Anda di Video Fameo. Anda hanya dapat
                  mensublisensikan hak Anda sebagaimana diperlukan agar Anda
                  dapat menggunakan Video Fameo sebagaimana diizinkan dalam
                  Ketentuan ini. Anda hanya dapat menggunakan Video Fameo sesuai
                  dengan Ketentuan ini, termasuk Pembatasan Penggunaan yang
                  Dapat Diterima di Bagian 5 di bawah ini. Jika Anda melanggar
                  salah satu ketentuan Ketentuan, lisensi Anda untuk Video Fameo
                  dihentikan.
                </p>
                <p>
                  <b>2. Ketentuan Tambahan</b>
                  <br />
                  Beberapa Layanan kami memiliki syarat dan ketentuan tambahan.
                  Dengan menggunakan Layanan itu, Anda menyetujui Ketentuan
                  Tambahan.
                </p>
                <p>
                  <b>3. Kelayakan</b>
                  <br />
                  Anda harus berusia minimal 18 tahun untuk menggunakan Situs
                  atau Layanan. Jika Anda berada di bawah usia dewasa di negara
                  tempat tinggal Anda, anak di bawah umur, orang tua atau wali
                  Anda harus menyetujui Ketentuan ini atas nama Anda dan Anda
                  hanya dapat mengakses dan menggunakan Situs dan Layanan dengan
                  izin dari orang tua atau wali Anda.
                </p>
                <p>
                  <b>4. Kepemilikan</b>
                  <br />
                  Kami memiliki atau melisensikan semua hak, kepemilikan, dan
                  kepentingan dalam dan untuk (a) Situs dan Layanan, termasuk
                  semua perangkat lunak, teks, media, dan konten lainnya yang
                  tersedia di Situs dan Layanan ("Konten Kami"); dan (b) merek
                  dagang, logo, dan elemen merek kami ("Marks"). Situs dan
                  Layanan, Konten Kami, dan Merek semuanya dilindungi oleh
                  undang-undang. Tampilan dan nuansa Situs dan Layanan adalah
                  hak cipta &copy; PT Fameo Kreasi Indonesia. Semua hak
                  dilindungi undang-undang. Anda tidak boleh menggandakan,
                  menyalin, atau menggunakan kembali bagian dari HTML / CSS,
                  Javascript, atau elemen atau konsep desain visual tanpa izin
                  tertulis dari kami.
                </p>
                <p>
                  <b>5. Penggunaan Situs dan Layanan yang dapat diterima</b>{' '}
                  <br />
                  Anda bertanggung jawab atas penggunaan Situs dan Layanan, dan
                  untuk setiap penggunaan Situs atau Layanan yang dibuat
                  menggunakan akun Anda. Tujuan kami adalah untuk menciptakan
                  pengalaman pengguna yang positif, bermanfaat, dan aman. Untuk
                  mempromosikan tujuan ini, kami melarang jenis perilaku
                  tertentu yang mungkin berbahaya bagi pengguna lain atau bagi
                  kami. Saat Anda menggunakan Situs atau Layanan, Anda tidak
                  boleh:
                  <br />
                  <br />
                  Melanggar hukum atau peraturan apa pun;
                  <br />
                  <br />
                  Melanggar, atau menyalahgunakan kekayaan intelektual orang
                  lain, privasi, publisitas, atau hak hukum lainnya;
                  <br />
                  <br />
                  Memposting, berbagi, atau meminta sesuatu yang ilegal, kasar,
                  melecehkan, merusak reputasi, pornografi, tidak senonoh, tidak
                  senonoh, cabul, benci, rasis, atau tidak menyenangkan;
                  <br />
                  <br />
                  Mengirim iklan atau komunikasi komersial yang tidak diminta
                  atau tidak sah, seperti spam;
                  <br />
                  <br />
                  Menguntit, melecehkan, atau menyakiti orang lain;
                  <br />
                  <br />
                  Berkedok sebagai orang atau entitas atau melakukan aktivitas
                  penipuan serupa lainnya, seperti phishing;
                  <br />
                  <br />
                  Mengadvokasi, mendorong, atau membantu pihak ketiga mana pun
                  dalam melakukan hal-hal di atas. Anda mengakui bahwa kami
                  tidak berkewajiban untuk memantau akses Anda (atau orang lain)
                  ke atau penggunaan Situs atau Layanan, tetapi kami memiliki
                  hak untuk melakukannya untuk mengoperasikan Situs atau
                  Layanan, untuk memastikan kepatuhan Anda dengan Ketentuan ini
                  , atau untuk mematuhi hukum yang berlaku atau perintah atau
                  persyaratan pengadilan, badan administratif atau badan
                  pemerintah lainnya.
                </p>
              </div>
              <div className="button-holder">
                <Link to="/settings">
                  <SingleButton
                    content={{
                      text:
                        'Saya telah membaca dan menyetujui syarat & ketentuan ',
                      color: 'pink',
                    }}
                  />
                </Link>
              </div>
            </div>
          ) : (
            <div className="content-wrapper">
              <p className="subtitle c-white">
                Dengan menggunakan situs atau layanan, Anda menyetujui
                persyaratan ini. Harap baca dengan seksama.
              </p>
              <div className="box-terms">
                <p>
                  <b>Untuk Talent</b>
                  <br />
                  <b>1. Proses Shooting dan Editing</b>
                  <br />
                  Talent wajib berpakaian rapi dan sopan agar terlihat baik di
                  depan kamera. Talent akan tunduk pada format dan
                  arahan/petunjuk yang ditetapkan oleh Fameo untuk tercapai
                  standar dan kualitas yang ditetapkan oleh Fameo. Jika talent
                  karena satu dan lain hal berhalangan melaksanakan pembuatan
                  Konten Video sesuai ketentuan Pasal 1 dan Pasal 2 Perjanjian,
                  maka talent wajib memberitahukan Fameo secara tertulis dalam
                  waktu 2 (dua) kali 24 (dua puluh empat) jam sebelumnya.
                  <br />
                  <br />
                  Dalam hal talent, tanpa alasan yang sah, lalai (tidak)
                  melaksanakan pembuatan Konten Video sesuai dengan ketentuan
                  Pasal 1 dan Pasal 2 Perjanjian yang mengakibatkan kerugian
                  bagi Fameo, maka talent wajib untuk membayar denda/ganti rugi
                  kepada Fameo sebesar 25% (dua puluh lima perseratus) dari
                  Imbalan yang diperjanjikan akan diterima, dan apabila perlu
                  Fameo berhak untuk membatalkan Perjanjian. Denda/ganti rugi
                  wajib dibayarkan kepada Fameo dalam waktu 7 (tujuh) hari
                  kalender setelah pelaksanaan pembuatan Konten Video secara
                  seketika dan sekaligus lunas. Fameo berhak memotong pembayaran
                  Imbalan yang akan diterima talent sejumlah denda/ganti rugi
                  yang harus dibayarkan talent kepada Fameo.
                  <br />
                  <br />
                  Talent wajib untuk aktif menghubungi Fameo untuk
                  mengantisipasi, mengkonfirmasi ulang semua kegiatan dalam
                  rangka pembuatan Konten Video; Talent wajib untuk
                  memberitahukan Fameo apabila mempunyai rencana-rencana
                  pribadi, termasuk akan tetapi tidak terbatas pada rencana
                  bepergian ke luar negeri atau menunaikan ibadah agama, yang
                  diperkirakan dapat mengganggu jadwal unggahan Konten Video
                  (upload video), paling lambat 14 (empat belas) hari kerja
                  sebelum rencana pribadi tersebut dilaksanakan. Dalam hal
                  terjadi kepentingan pribadi talent yang darurat atau tidak
                  diperkirakan terjadi, termasuk tetapi tidak terbatas pada
                  kehamilan atau sakit yang memerlukan perawatan khusus, maka
                  talent wajib untuk memberitahukan kepada Fameo paling lambat 1
                  (satu) hari kerja setelah terjadinya keadaan tersebut.
                  <br />
                  <br />
                  Talent wajib untuk menghadiri setiap acara kegiatan promosi
                  dan/atau undangan yang berkaitan dengan promosi Konten Video.
                  Talent wajib untuk melakukan promosi atas Konten Video melalui
                  akun pribadi yang dimiliki dan/atau dikelola oleh talent
                  dan/atau manajemennya di media sosial yang sekarang dikenal
                  maupun yang akan ada di kemudian hari(seperti Twitter,
                  Instagram, atau Facebook)sekurang-kurangnya sebanyak 3 (tiga)
                  kali unggahan (upload) diberikan setelah pelaksanaan upload
                  Konten Video Fameo, tanpa diberikan biaya tambahan dari Fameo.
                  Apabila pada hasil produksi Konten Video terdapat bagian atau
                  adegan yang kurang baik atau tidak sesuai dengan penilaian
                  Fameo, maka talent wajib melakukan pengambilan gambar ulang.
                  Apabila setelah proses shooting Konten Video dinyatakan
                  selesai olej talent namun dalam proses editing terdapat
                  kekurangan durasi Konten Video, maka talent wajib
                  menyelesaikan sisa shooting untuk melengkapi durasi Konten
                  Video dimaksud.
                  <br />
                  <br />
                  Fameo sewaktu-waktu berhak untuk mengambil cuplikan-cuplikan
                  adegan shooting talent dalam Konten Video dalam bentuk
                  referensi maupun flashback dan photo property pada pembuatan
                  episode atau Konten Video baru atau untuk keperluan lainnya.
                </p>
                <p>
                  <b>2. Hak Atas Konten</b>
                  <br />
                  Video Para Pihak setuju bahwa dengan ditandatanganinya
                  Perjanjian, maka Fameo memiliki hak atas seluruh kekayaan
                  intelektual serta hak-hak lainnya yang terkait dengan Konten
                  Video (Hak Terkait), termasuk akan tetapi tidak terbatas pada
                  hak atas ide cerita, script/naskah, dialog, lelucon, judul,
                  hak siar, lagu tema (theme song) dan hak-hak yang melekat pada
                  Konten Video serta hak atas master Konten Video, hak
                  menggandakan, mengalihkan, menjual, melisensikan, menayangkan
                  dan menayangkan ulang (re-run), hak mengubah format Konten
                  Video ke dalam bentuk acara atau format media lainnya serta
                  mendistribusikan seluruh atau sebagian Konten Video, baik di
                  dalam maupun di luar wilayah Negara Republik Indonesia untuk
                  jangka waktu yang tidak terbatas lamanya, dan karenanya talent
                  tidak berhak menuntut sesuatu hak dalam bentuk dan berupa
                  apapun yang menyangkut hak atas Konten Video. Fameo sebagai
                  pihak yang memiliki hak atas kekayaan intelektual sebagaimana
                  dimaksud dalam ayat 3.1 pasal ini sepenuhnya berhak untuk:
                  <br />
                  <br />
                  Menyiarkan, menyiarkan ulang (re-run), menjual atau dengan
                  cara lain mengalihkan, melisensikan baik seluruh maupun
                  sebagian hak-hak yang dimilikinya atas Konten Video dalam
                  bentuk apapun kepada pihak lain, dan pihak yang menerima
                  pengalihan hak Konten Video tersebut akan menggantikan posisi
                  Fameo serta berhak atas hak yang dialihkan tersebut;
                  Menggandakan/memperbanyak dan/ atau mengubah format seluruh
                  atau sebagian Konten Video atau cuplikan Konten Video atau
                  theme song Konten Video dalam bentuk media video casette,
                  laser disc, VCD, DVD atau dalam bentuk media lainnya, termasuk
                  menjadi data komunikasi melalui teknologi multimedia baik di
                  bidang media telekomunikasi cellular phone maupun internet,
                  yang sekarang dikenal maupun yang akan dikenal/ada di kemudian
                  hari; dan mengedit/mengubah format Konten Video, baik seluruh
                  maupun sebagian, ke dalam bentuk media lain, baik yang
                  sekarang dikenal maupun yang akan dikenal/ada di kemudian
                  hari, dari Konten Video yang dibuat/dirancang di kemudian
                  hari, dengan atau tanpa menggunakan gambar, foto, adegan,
                  suara talent yang terdapat pada Konten Video; tanpa Fameo
                  wajib membayar kepada talent maupun kepada pihak-pihak lain
                  dalam bentuk dan berupa apapun yang menyangkut hak atas Konten
                  Video.
                  <br />
                  <br />
                  Talent dengan ini memberikan persetujuannya dan mengizinkan
                  Fameo atau pihak yang menerima pengalihan seluruh atau
                  sebagian hak kekayaan intelektual atas Konten Video untuk
                  dalam waktu yang tidak terbatas menggunakan cuplikan foto,
                  gambar, suara atau adegan-adegan yang terdapat pada Konten
                  Video yang memuat foto, gambar, suara atau adegan talent untuk
                  durasi yang tidak terbatas bagi kepentingan apapun yang
                  dianggap perlu oleh Fameo, termasuk akan tetapi tidak terbatas
                  untuk program promosi maupun Konten Video lainnya yang dibuat
                  di kemudian hari oleh Fameo ataupun memberikan hak kepada
                  pihak ketiga atau sponsor untuk menggunakan cuplikan foto,
                  gambar, suara atau adegan tersebut untuk kepentingan pihak
                  ketiga atau sponsor, baik yang memberikan keuntungan maupun
                  yang tidak memberikan keuntungan langsung kepada Fameo, tanpa
                  ada kewajiban dari Fameo untuk memberikan kompensasi berupa
                  dan dalam bentuk apapun kepada talent.
                </p>
                <p>
                  <b>3. Jaminan</b>
                  <br />
                  Talent dengan ini menyatakan dan menjamin kepada Fameo, bahwa:
                  <br />
                  Talent memiliki hak dan kewenangan penuh untuk membuat dan
                  menandatangani serta melaksanakan Perjanjian ini; Talent tidak
                  sedang terikat dalam kontrak eksklusif atau perjanjian lainnya
                  yang melarang talent untuk mengadakan perjanjian atau terikat
                  kontrak dengan pihak lain, termasuk Fameo, atau membutuhkan
                  persetujuan dari pihak ketiga untuk menandatangani Perjanjian;
                  <br />
                  Talent tidak akan mengadakan kerjasama dengan perusahan
                  penyedia/pembuat Konten Video lain selain yang dimiliki oleh
                  Fameo untuk jenis program yang sama atau mempunyai kemiripan
                  dengan Konten Video milik Fameo;
                  <br />
                  Seluruh data-data yang diberikan dalam Perjanjian ini termasuk
                  akan tetapi tidak terbatas pada nomor rekening dan nama
                  pemegang rekening penerima Imbalan adalah benar. Apabila di
                  kemudian hari, Fameo menemukan bahwa data-data talent tidak
                  benar, maka Fameo berhak untuk mengenakan denda kepada talent
                  sebesar jumlah kerugian yang terjadi akibat pemberian
                  data-data yang tidak benar oleh talent kepada Fameo;
                  <br />
                  Segala syarat dan ketentuan Perjanjian tidak bertentangan
                  dengan dan melanggar setiap kewajiban yang mengikat talent
                  dengan pihak lain berdasarkan suatu perjanjian yang dibuat
                  sebelum Perjanjian ini; Talent tidak sedang menghadapi
                  tuntutan atau gugatan dari pihak ketiga yang akan
                  mengakibatkan tertundanya atau bahkan tidak dapat
                  terlaksananya Konten Video;
                  <br />
                  Talent akan senantiasa menjaga nama baiknya sendiri dan nama
                  baik Fameo di hadapan publik selama Jangka Waktu Perjanjian;
                  Talent akan dapat menjalankan perannya secara baik dan
                  profesional; dan
                  <br />
                  Dalam hal tidak terpenuhinya jaminan-jaminan dan
                  kewaiiban-kewajiban oleh talent seperti yang dimaksud pada
                  Perjanjian dan Ketentuan Umum Perjanjian ini, maka talent
                  dengan ini menyatakan akan melepaskan Fameo dari segala
                  tuntutan pihak ketiga dan resiko hukum, apabila ada, dan Fameo
                  berhak untuk mengakhiri Perjanjian secara sepihak tanpa wajib
                  memberikan ganti rugi/kompensasi berupa dan dalam bentuk
                  apapun kepada talent.
                  <br />
                  <br />
                </p>
                <p>
                  <b>4. Larangan</b>
                  <br />
                  Tanpa persetujuan tertulis terlebih dahulu dari Fameo, selama
                  Jangka Waktu Perjanjian, talent tidak diperkenankan memberikan
                  pelayanan, terikat kontrak dengan pihak lain untuk suatu
                  Konten Video dengan tugas dan pekerjaan yang sama atau sejenis
                  dengan Konten Video yang secara langsung/tidak langsung
                  bersaing dengan Fameo. Talent dilarang melakukan
                  tindakan-tindakan dan kegiatan-kegiatan yang menurunkan
                  derajat, harkat, martabat Fameo atau prinsipalnya dan/atau
                  menimbulkan penilaian negatif masyarakat terhadap Konten Video
                  dan/atau kegiatan yang bertentangan dengan hukum yang akan
                  berakibat buruk terhadap citra Fameo.
                  <br />
                  Selama berlakunya Perjanjian, talent dilarang mempromosikan
                  suatu produk tertentu untuk kepentingan pihak lain dengan
                  bentuk/menyerupai atau hampir sama seperti, dekorasi, gaya,
                  penampilan, musik atau ucapan-ucapan talent yang merupakan
                  ciri khas Konten Video tanpa izin dari Fameo.
                  <br />
                  Talent dilarang untuk mengalihkan hak-hak dan
                  kewajiban-kewajibannya sebagaimana diatur dalam Perjanjian
                  ini, baik sebagian maupun seluruhnya, kepada pihak ketiga
                  tanpa persetujuan tertulis terlebih dahulu dari Fameo.
                  <br />
                  Fameo berhak mengakhiri Perjanjian secara sepihak tanpa wajib
                  memberikan ganti rugi/kompensasi berupa dan dalam bentuk
                  apapun kepada talent apabila talent melanggar larangan
                  sebagaimana diatur dalam pasal ini.
                </p>
                <p>
                  <b>5. Force Majeure</b>
                  <br />
                  Apabila terjadi force majeure yang mengakibatkan kelambatan
                  atau kegagalan pemenuhan kewajiban dari salah satu pihak
                  terhadap pihak lain dalam Perjanjian, maka
                  kelambatan/kegagalan tersebut dilindungi/tidak akan mengalami
                  tuntutan atas kerugian dari pihak lain.
                  <br />
                  Pihak yang mengalami force majeure harus segera memberitahukan
                  secara tertulis kepada pihak lainnya tentang penyebab dan
                  akibat force majeure tersebut dalam waktu selambat-lambatnya 2
                  (dua) hari kalender sejak saat terjadinya.
                  <br />
                  Jika terjadinya force majeure sebagaimana dimaksud dalam pasal
                  ini mengakibatkan tidak dapat dilaksanakannya seluruh atau
                  sebagian kewajiban salah satu pihak berdasarkan Perjanjian ini
                  untuk waktu lebih dari 7 (tujuh) hari kalender, maka di antara
                  Para Pihak akan diadakan pemberesan dan perhitungan terhadap
                  kewajiban dimaksud.
                  <br />
                  <b>6. Kerahasiaan</b>
                  <br />
                  Para Pihak wajib untuk merahasiakan seluruh informasi tentang
                  Perjanjian dan informasi lainnya yang diperoleh dalam
                  hubungannya dengan Perjanjian. Kecuali dinyatakan diizinkan
                  berdasarkan Perjanjian, Para Pihak tidak diizinkan untuk
                  memberitahukan dan mendiskusikan setiap informasi dalam mana
                  salah satu pihak telah menyetujui untuk merahasiakannya tanpa
                  adanya izin tertulis dari pihak pemberi informasi. Menjaga
                  kerahasiaan sebagaimana tercantum dalam pasal ini merupakan
                  kualifikasi dari kerahasiaan yang tercantum dalam Pasal 322
                  ayat (1) Kitab Undang-undang Hukum Pidana yang berlaku di
                  Indonesia.
                  <br />
                  <br />
                  <b>7. Evaluasi</b>
                  <br />
                  Fameo setiap waktu berhak melakukan review/evaluasi atas
                  performance talent, dan apabila hasil review/evaluasi tersebut
                  dinyatakan tidak sesuai dengan standar persyaratan yang
                  ditetapkan oleh Fameo, maka Fameo berhak untuk mengakhiri
                  Perjanjian tanpa wajib memberikan ganti rugi/kompensasi berupa
                  dan dalam bentuk apapun kepada talent. Dalam hal demikian,
                  maka Fameo hanya wajib membayar Imbalan kepada talent untuk
                  dan sebanyak episode yang sesuai dengan standar saja.
                  <br />
                  <br />
                  <b>8. Pengakhiran Perjanjian</b>
                  <br />
                  Dengan tidak mengesampingkan ketentuan mengenai pengakhiran
                  Perjanjian sebagaimana diatur dalam pasal-pasal sebelumnya,
                  Fameo berhak melakukan pengakhiran Perjanjian secara sepihak
                  tanpa wajib memberikan ganti rugi/kompensasi berupa dan dalam
                  bentuk apapun kepada talent, dalam hal: Talent melakukan
                  pelanggaran atau tidak memenuhi kewajibannya sebagaimana yang
                  telah ditentukan dalam Perjanjian; atau Talent melakukan
                  tindakan/kegiatan yang dapat dikategorikan merusak nama baik
                  Fameo atau Konten Video, berlawanan dengan Fameo atau bahkan
                  melakukan tindakan/kegiatan yang bertentangan dengan hukum
                  yang berlaku di Indonesia; atau Konten Video tidak jadi dibuat
                  dan belum dimulainya proses produksi Konten Video; atau
                  Berdasarkan penilaian Fameo, penampilan talent dalam Konten
                  Video tidak lolos evaluasi atau tidak sesuai dengan standar
                  yang ditetapkan oleh Fameo sebagaimana dimaksud dalam Pasal 8
                  Ketentuan Umum Perjanjian; atau Dimana dalam hal timbulnya
                  pengakhiran Perjanjian berdasarkan alasan-alasan yang
                  disebutkan dalam ayat 9.1 pasal ini, maka Fameo hanya wajib
                  untuk membayar Imbalan atas sejumlah Konten Video yang telah
                  diproduksi dan diunggah (upload). Dalam hal terjadi
                  pengakhiran Perjanjian sebagaimana tersebut dalam ayat 9.1 (a)
                  dan (b) Pasal ini, maka talent bertanggung jawab atas seluruh
                  kerugian yang diderita oleh Fameo atau pihak lain. Para Pihak
                  atas persetujuan tertulis bersama dapat mengakhiri Perjanjian
                  sebelum berakhirnya Jangka Waktu Perjanjian. Para Pihak
                  sepakat untuk mengesampingkan ketentuan Pasal 1266 dan 1267
                  Kitab Undang-undang Hukum Perdata yang berlaku di Republik
                  Indonesia sepanjang mengenai diisyarakatkannya keputusan badan
                  peradilan untuk pengakhiran suatu perjanjian, sehingga
                  mengenai pengakhiran Perjanjian ini tidak diperlukan lagi
                  keputusan atau penetapan Pengadilan. Apabila pada saat
                  Perjanjian ini berakhir terdapat kewajiban yang belum
                  diselesaikan oleh Para Pihak, maka ketentuan-ketentuan dalam
                  Perjanjian dan Ketentuan Umum tetap berlaku sampai dengan
                  diselesaikannya kewajiban tersebut.
                  <br />
                  <b>9. Ketentuan Lain</b>
                  <br />
                  Talent setuju bahwa Fameo berhak untuk setiap saat mengalihkan
                  hak-hak dan kewajiban-kewajiban yang dimilikinya sebagaimana
                  diatur di dalam Perjanjian kepada pihak ketiga lainnya yang
                  ditunjuk oleh Fameo. Hal-hal yang tidak diatur atau belum
                  cukup diatur dalam Perjanjian akan diputuskan oleh Para Pihak
                  dengan musyawarah untuk mufakat. Para Pihak setuju untuk
                  melaksanakan Perjanjian dan Ketentuan Umum dengan itikad baik
                  dan penuh tanggung jawab. Para Pihak setuju untuk melaksanakan
                  Perjanjian dan Ketentuan Umum dengan itikad baik dan penuh
                  tanggung jawab. Perjanjian, Ketentuan Umum, hak dan kewajiban
                  Para Pihak yang timbul berdasarkan Perjanjian harus tunduk dan
                  ditafsirkan sesuai dengan undang-undang yang berlaku di Negara
                  Republik Indonesia.
                </p>
              </div>
              <div className="button-holder">
                <a href="https://forms.gle/EdVaqDh3FtuYS2BB7">
                  <SingleButton
                    content={{
                      text: 'Lihat kontrakmu ',
                      color: 'pink',
                    }}
                  />
                </a>
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default TermsConditions;
