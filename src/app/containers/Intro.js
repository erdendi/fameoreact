/* eslint-disable react/no-array-index-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-unescaped-entities */

import React from 'react';
import { Link } from 'react-router-dom';
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css';

const swiperData = [
  {
    title: 'Gokil!',
    description:
      'Mau sampaikan sesuatu? Minta Idola kamu yang sampaikan langsung',
  },

  {
    title: 'Mau Idola Kamu Ngomong Apa?',
    description: (
      <span>
        Michelle Ziu ucapin ulang tahun untuk "adik" kamu! ;)
        <br />
        <br />
        Minta Chris John nagih utang yang katanya mau dibayar besok, besok,
        besok dari tahun '97.
        <br />
        <br />
        Limbad nanya pak RT bebeknya kemana? (bebek?).
        <br />
        <br />
        Chef Arnold bantuin kamu minta maap ke Mama udah ngilangin Tupperware,
        sama sendok 3 (Kamu dalam masalah… selamat dech)
      </span>
    ),
  },

  {
    title: 'Abadikan Momentnya',
    description:
      'Rekam dan upload video reaksinya siapa tau viral. Nanti dapat sesuatu deh dari Fameo.',
  },
  {
    title: '',
    description: '',
  },
];

const params = {
  navigation: {
    nextEl: '.swiper-button-next',
  },
  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable: 'true',
  },
  slidesPerView: 1,
  slidesPerGroup: 1,
  on: {
    reachEnd() {
      window.location.assign('#/intro2');
    },
  },
};

function SwiperIntro({ content }) {
  return (
    <div className="swiper">
      <div className="content-container">
        <Swiper {...params}>
          {content.map((item, i) => (
            <div data-url={item.url}>
              <div key={i} className="slider-content">
                <h2>{item.title}</h2>
                <p>{item.description}</p>
              </div>
            </div>
          ))}
        </Swiper>
        <Link to="/signup" className="skip">
          SKIP
        </Link>
      </div>
    </div>
  );
}

const Intro = () => (
  <div className="intro">
    <SwiperIntro content={swiperData} />
  </div>
);

export default Intro;
