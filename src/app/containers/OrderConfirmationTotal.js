/* eslint-disable linebreak-style */
import React from 'react';

// import { Link } from 'react-router-dom'

// import { Label, Input } from 'reactstrap'

import Header from '../components/Header';

import Detail from '../components/Detail';

import SingleButton from '../components/Button';

import Back from '../assets/images/icon-back.svg';

import Settings from '../assets/images/icon-more.svg';

const Details = {
  allList: [
    {
      title: 'Nama Talent',

      detail: 'Michelle Ziudith',
    },

    {
      title: 'Nomor Pesanan',

      detail: 'ORD-2019000184',
    },

    {
      title: 'Judul Project',

      detail: 'Ulang Tahun John Doe',
    },

    {
      title: 'Ucapan Untuk',

      detail: 'John Doe',
    },

    {
      title: 'Ucapan Dari',

      detail: 'Jane Doe',
    },

    {
      title: 'Pesan',

      detail:
        'Nulla maximus pharetra interdum. Ut in justo libero. Sed feugiat, sem ut interdum pharetra, sem ante condimentum magna, vel convallis diam dolor venenatis dui. Ut finibus et dolor bibendum varius.',
    },
  ],
};

const OrderConfirmationTotal = () => (
  <>
    <Header
      content={{
        title: 'Konfirmasi Pemesanan',

        BtnLeftIcon: Back,

        BtnRightIcon: Settings,

        hasSubtitle: true,

        subtitle: 'No. Pesanan Fameo ORD-2019000184',
      }}
    />

    <div className="order order-confirmation">
      <div className="container">
        <div className="content-wrapper">
          <h2 className="title c-white">Konfirmasi Pesanan</h2>

          {Details.allList.map((item, i) => (
            // eslint-disable-next-line react/no-array-index-key

            <Detail key={i} content={item} />
          ))}

          <div className="promo-holder">
            <div className="content-holder">
              <span>Total</span>

              <p>IDR 250.000</p>
            </div>

            <div className="content-holder">
              <span>Promo</span>

              <p className="discount">-IDR 50.000</p>
            </div>
          </div>

          <div className="subtotal-holder">
            <span className="label">Subtotal</span>

            <p className="total-amount">IDR 200.000</p>
          </div>

          <SingleButton
            content={{
              text: 'BAYAR',

              color: 'pink',

              url: '/order-method-payment',
            }}
          />
        </div>
      </div>
    </div>
  </>
);

export default OrderConfirmationTotal;
