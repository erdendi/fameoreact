import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.searchQuestion || initialState;

export const selectSearchQuestion = createSelector(
  [selectDomain],
  searchQuestionState => searchQuestionState,
);

export const selectTypeParam = createSelector(
  [selectDomain],
  searchTalentState => searchTalentState.typeParam,
);
export const selectLimitParam = createSelector(
  [selectDomain],
  searchTalentState => searchTalentState.limitParam,
);

export const selectSearchParam = createSelector(
  [selectDomain],
  searchQuestionState => searchQuestionState.searchParam,
);
