import { call, put, select, debounce } from 'redux-saga/effects';
import { actions } from './slice';
import { getQuestion } from 'api/getQaAPI';
import {
  selectTypeParam,
  selectSearchParam,
  selectLimitParam,
} from './selectors';

export function* fetchSearchQuestion() {
  const limitParam: string = yield select(selectLimitParam);
  const searchParam: string = yield select(selectSearchParam);
  const typeParam: string = yield select(selectTypeParam);
  let params;
  if (searchParam === '') {
    params = { limitall: limitParam || '' };
  } else {
    params = {
      type: typeParam || '',
      talentname: searchParam || '',
      limitvideo: limitParam,
    };
  }
  try {
    const response = yield call(getQuestion, params);
    yield put(actions.getSearchQuestionSuccess(response));
  } catch (error) {
    yield put(actions.getSearchQuestionFailure(error));
  }
}

export function* searchQuestionSaga() {
  // Watches for loadRepos actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield debounce(
    1500,
    actions.getSearchQuestionStart.type,
    fetchSearchQuestion,
  );
}
