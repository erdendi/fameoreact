/**
 *
 * Asynchronously loads the component for SearchTalent
 *
 */

import { lazyLoad } from 'utils/loadable';

export const SearchQuestion = lazyLoad(
  () => import('./index'),
  module => module.SearchQuestion,
);
