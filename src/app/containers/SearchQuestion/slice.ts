import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { SearchQuestionState } from './types';
import { Repo } from 'types/Repo';

// The initial state of the SearchTalent container
export const initialState: SearchQuestionState = {
  questions: [],
  isLoading: false,
  error: null,
  resultsData: [],
  totalData: 12,
  typeParam: '0',
  limitParam: '0',
  searchParam: '',
};
const searchQuestionSlice = createSlice({
  name: 'searchQuestion',
  initialState,
  reducers: {
    getSearchQuestionStart(state) {
      state.isLoading = true;
      state.error = null;
    },
    getSearchQuestionSuccess(state, action: PayloadAction<Repo[]>) {
      state.resultsData = action.payload;
      state.questions = state.resultsData;
      state.totalData = state.resultsData.length;
      state.isLoading = false;
      state.error = null;
    },
    getSearchQuestionFailure(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
    setTypeParam(state, action: PayloadAction<any>) {
      state.typeParam = action.payload;
    },
    setLimitParam(state, action: PayloadAction<any>) {
      state.limitParam = action.payload;
    },
    setSearchParam(state, action: PayloadAction<string>) {
      state.searchParam = action.payload;
    },
  },
});

export const { actions, reducer, name: sliceKey } = searchQuestionSlice;
