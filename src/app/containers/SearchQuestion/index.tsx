import React, { useState, useEffect } from 'react';
import { Form, FormGroup, Input } from 'reactstrap';
import Tabs from '../Tabs';
import Header from '../../components/Header';
import Heading from '../../components/Heading';
import TaskBar from '../../components/TaskBar';
import { Helmet } from 'react-helmet-async';
import { useSelector, useDispatch } from 'react-redux';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey, actions } from './slice';
import { selectSearchQuestion } from './selectors';
import { searchQuestionSaga } from './saga';
import SkeletonCardWide from '../../components/SkeletonCardWide';

const FansQeustions = {
  headingData: {
    heading: 'Pertanyaan Fans',
    headingColor: 'white',
  },
};

interface Props {}

export function SearchQuestion(props: Props) {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: searchQuestionSaga });
  const dispatch = useDispatch();
  const searchQuestion = useSelector(selectSearchQuestion);
  const { questions, isLoading } = searchQuestion;
  const [searchTerm, setSearchTerm] = useState('');

  const handleChange = event => {
    setSearchTerm(event.target.value);
    dispatch(actions.setTypeParam(1));
    dispatch(actions.setSearchParam(event.target.value));
  };
  const doSearch = () => {
    dispatch(actions.getSearchQuestionStart());
  };

  useEffect(() => {
    dispatch(actions.setLimitParam(20));
    dispatch(actions.getSearchQuestionStart());
  }, []);

  return (
    <>
      <Helmet>
        <title>Search Question</title>
        <meta name="description" content="Search question" />
      </Helmet>
      <div className="search-qa-wrapper">
        <Header content={{ title: 'TANYA IDOLA' }} />
        <div className="after-heading-wrapper">
          <div className="section-gift mt-4"></div>
          <div className="mt-4">
            <Heading content={FansQeustions.headingData} />
            <div className="container">
              <div className="search-wrapper mb-4">
                <Form className="search-bar">
                  <FormGroup>
                    <Input
                      type="search"
                      name="search"
                      value={searchTerm}
                      onChange={handleChange}
                      onKeyDown={e => handleChange(e)}
                      onKeyPress={() => doSearch()}
                      id="exampleSearch"
                      placeholder="Cari berdasarkan nama talent"
                    />
                    <button
                      type="button"
                      className="search-icon"
                      onClick={() => doSearch()}
                      aria-label="search"
                    />
                  </FormGroup>
                </Form>
                {/* <img src={Sort} alt="fameo-sort" /> */}
              </div>
              {isLoading && <SkeletonCardWide totalData={12} />}
              <Tabs question={questions.getCurrentData} />
            </div>
          </div>
        </div>
        <TaskBar active="tanyajawab" />
      </div>
    </>
  );
}

export default SearchQuestion;
