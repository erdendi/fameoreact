import { Repo } from 'types/Repo';
/* --- STATE --- */
interface SearchQuestionState {
  questions: any[] | any;
  resultsData: any[] | any;
  isLoading: boolean;
  error: string | null;
  typeParam: any;
  limitParam: any;
  searchParam: string;
  totalData: number | null;
}
export type { SearchQuestionState };
