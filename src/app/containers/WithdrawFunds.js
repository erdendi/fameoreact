import React, { useState, useEffect } from 'react';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import SweetAlert from 'react-bootstrap-sweetalert';
import { formatPrice } from 'utils/price';
import CurrencyInput from 'react-currency-input';
import styled from 'styled-components/macro';
import Header from '../components/Header';
import Back from '../assets/images/icon-back.svg';
import { postWithdraw, getDetail } from '../services/withdrawFunds';
import { getCookie } from '../helpers';
import LoadingFull from '../components/LoadingFull';

const WithdrawFunds = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [nominalTransfer, setNominalTransfer] = useState('0');
  const [claimMessage, setClaimMessage] = useState();
  const [NotifMessage, setNotifMessage] = useState();
  const [saldo, setSaldo] = useState(0);
  const [notif, setnotif] = useState(false);
  const [userID] = useState(Number(getCookie('UserId')));

  const getData = async () => {
    const params = {
      userid: userID,
    };
    getDetail(params)
      .then(resp => {
        if (resp.TalentModel.Income < 100000) {
          document.getElementById('BtnSubmit').disabled = true;
        } else {
          document.getElementById('BtnSubmit').disabled = false;
        }

        setLoading(false);
        setSaldo(resp.TalentModel || []);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const submit = e => {
    e.preventDefault();
    setLoading(true);
    const data = {
      UserId: userID,
      NominalTransfer: nominalTransfer || '',
      ClaimMessage: claimMessage || '',
    };

    postWithdraw(data)
      .then(resp => {
        setLoading(false);
        if (resp.Status === '2') {
          history.push(`/order-withdraw-success/${nominalTransfer}`);
          setNotifMessage('Success tarik dana');
          setnotif(true);
        }
        if (resp.Status === '0') {
          setNotifMessage('Minimal penarikan dana Rp. 100.000');
          setnotif(true);
        }
        if (resp.Status === '1') {
          setNotifMessage('Jumlah yang anda masukkan terlalu besar');
          setnotif(true);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  useEffect(() => {
    window.scroll(0, 0);
    if (!userID) {
      history.push('/signin');
    } else {
      getData();
    }
  }, []);

  function handleNominalChange(event, maskedvalue, floatvalue) {
    setNominalTransfer(floatvalue);
  }

  return (
    <>
      {loading && <LoadingFull />}
      <div className="withdraw-funds">
        <Header content={{ title: 'Tarik Dana', BtnLeftIcon: Back }} />
        <div className="total-balance">
          <div className="container">
            <h3>Total Saldo</h3>
            <h3>{formatPrice(saldo.Income)}</h3>
          </div>
        </div>

        <div className="container">
          {/* <div className="details"> */}
          {/* <DetailWithdraw content={detail} /> */}
          {/* <DetailWithdraw content={{ title: 'Nama Bank', detail: `${detail.Bank}`  }} /> */}
          {/* <DetailWithdraw content={{ title: 'Nomor Virtual Account', detail: '8830832432000273' }} /> */}
          {/* </div> */}

          <div className="form">
            <Form onSubmit={event => submit(event)}>
              <FormGroup>
                <Label for="first-name" className="title-textbox">
                  Nominal Tarik Dana
                </Label>
                <Wrapper>
                  <CurrencyInput
                    value={nominalTransfer}
                    onChangeEvent={handleNominalChange}
                    className="textbox"
                    prefix="Rp "
                    precision="0"
                    thousandSeparator="."
                  />
                </Wrapper>
                <Label className="title-textbox">
                  * Minimum Tarik dana {formatPrice(100000)}
                </Label>
              </FormGroup>

              <FormGroup>
                <Label for="first-name" className="title-textbox">
                  Pesan (Opsional)
                </Label>
                <Input
                  onChange={e => {
                    setClaimMessage(e.target.value);
                  }}
                  type="text"
                  name="first-name"
                  id="first-name"
                  className="textbox"
                  placeholder="Tulisa pesan Kamu"
                />
              </FormGroup>

              <div className="button-holder">
                <button
                  id="BtnSubmit"
                  className="btn btn-pink c-white"
                  type="button"
                  onClick={submit}
                >
                  TARIK DANA SEKARANG
                </button>
              </div>
            </Form>
          </div>
        </div>
      </div>
      <SweetAlert
        info
        show={notif}
        onConfirm={() => {
          setnotif(false);
        }}
        onCancel={() => {
          setnotif(false);
        }}
        onEscapeKey={() => {
          setnotif(false);
        }}
        onOutsideClick={() => {
          setnotif(false);
        }}
      >
        {NotifMessage}
      </SweetAlert>
    </>
  );
};

const Wrapper = styled.div`
  .textbox {
    width: 100%;
  }
`;

export default WithdrawFunds;
