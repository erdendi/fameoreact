/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-unused-vars */

import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import MetaTags from 'react-meta-tags';
import Header from '../components/Header';
import { GetFilesVideo } from '../services/video';
import Back from '../assets/images/icon-left-arrow-two.svg';
import Settings from '../assets/images/icon-settings.svg';
import Play from '../assets/images/search/play.svg';
import ModalVideo from '../components/ModalVideo';
import Timeimg from '../assets/images/pembayaran/time.svg';
import Heading from '../components/Heading';
import '../stylesheets/pages/_share-video.scss';
// import Idol from '../assets/images/idol-pose.png'
// import IconPlay from '../assets/images/icon-play-white.svg'

const ShareVideo = () => {
  const history = useHistory();
  const [video, setVideo] = useState([]);
  const [orderData, setOrderData] = useState([]);
  const [modalVideo, setModalVIdeo] = useState(false);
  const [dataModal, setDataModal] = useState(false);
  const [loading, setLoading] = useState(true);

  const { bookId } = useParams();

  const hideModalprop = () => {
    setDataModal(false);
    setModalVIdeo(false);
  };

  const toTalentDetail = TalentId => {
    history.push(`/detail/${TalentId}`);
  };

  const showModalprop = (TalentId, FileId) => {
    // setDataModal(content);

    const params = {
      TalentId,
      FileId,
    };
    GetFilesVideo(params).then(resp => {
      setDataModal(resp.Result);
    });
    setModalVIdeo(true);
  };

  //    const getInfo = async () => {
  //     setError(false);
  //     let params = {
  //       bookid: bookId
  //     }
  //     getOrderInfo(params)
  //       .then(resp => {

  //         setOrderData(resp);

  //         setLoading(false)
  //       })
  //   };

  const QsFilter = (T, FId) => {
    const params = {
      TalentId: T,
      FileId: FId,
    };

    GetFilesVideo(params).then(resp => {
      setOrderData(resp.Result);
      // document.getElementsByTagName("META")[8].content=resp.Result.TalentNm;
      // // document.getElementsByTagName("META")[9].content=resp.Result.TalentNm;
      //  document.getElementsByTagName("META")[10].content=resp.Result.ThumbLink;
      //  document.getElementsByTagName("META")[7].content= window.location.href;

      // document.querySelector('meta[name="og:description"]').setAttribute("content", resp.TalentNm);

      setLoading(false);
    });
  };

  useEffect(() => {
    window.scroll(0, 0);
    const param = {};

    const urlParams = new URLSearchParams(window.location.search);
    const TalentidParam = urlParams.get('talentid');
    const FileIdParam = urlParams.get('fileid');

    if (
      TalentidParam !== '' &&
      TalentidParam !== null &&
      FileIdParam !== '' &&
      FileIdParam !== ''
    ) {
      QsFilter(TalentidParam, FileIdParam);
    }

    // getInfo();
  }, []);

  return (
    <>
      <MetaTags>
        <meta property="og:title" content={orderData.TalentNm} />
        <meta property="og:image" content={orderData.ThumbLink} />
      </MetaTags>
      <div className="profile-ready-wrapper">
        <Header content={{ BtnLeftIcon: Back, BtnRightIcon: Settings }} />
        <div className="after-heading-short-wrapper">
          <div className="container ready-container">
            {/* <h2 className="section-title mb-3" style={{marginTop: "25px"}}>Video</h2>
             */}
            <div className="col-8 col-md-8">
              <div
                className="media"
                onClick={() => toTalentDetail(orderData.TalentId)}
              >
                <img
                  src={
                    orderData.ProfImg ||
                    orderData.LinkImg ||
                    orderData.ThumbLink
                  }
                  className="TalentImg"
                  alt=""
                />
                <div className="media-body">
                  <div className="TalentName">{orderData.TalentNm}</div>
                  {/* {content2.ListTalentVideo && content2.ListTalentVideo.map((item, i) => {
                      return (
                        <p key={i}>{item.CustomerName}</p>
                      )
                    })} */}
                </div>
              </div>
            </div>
            <div className="content-wrapper">
              <div className="box-btn-record-btn">
                <center>
                  <img
                    style={{ width: '80%', height: 'auto' }}
                    src={orderData.Thumbnails}
                    alt="idol"
                  />
                  <div className="play-icon">
                    <img
                      src={Play}
                      alt="play"
                      content={orderData}
                      onClick={() =>
                        showModalprop(orderData.TalentId, orderData.FileId)
                      }
                    />
                  </div>
                </center>
              </div>

              <div style={{ margin: '30px' }}>
                <a href="/" className="btn btn-success">
                  Kembali ke Home
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      {modalVideo && <ModalVideo contents={dataModal} isHide={hideModalprop} />}
    </>
  );
};

export default ShareVideo;
