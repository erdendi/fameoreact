/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/button-has-type */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable import/no-unresolved */
/* eslint-disable no-empty */

import React, { useState, useEffect } from 'react';
import Swiper from 'react-id-swiper';
import axios from 'axios';
import { Link } from 'react-router-dom';
import 'swiper/css/swiper.css';

// import Icon from '../assets/images/img-idol-1.png'
import Nice from '../assets/images/icon-nice.svg';
import Chat from '../assets/images/icon-chat.svg';

const params = {
  slidesPerView: 2.1,
  slidesPerGroup: 2.1,
  spaceBetween: 5,
  rebuildOnUpdate: true,
};

function SwiperQuestions() {
  const [talents, setTalents] = useState([]);
  // const [error, setError] = useState(false);

  useEffect(() => {
    const getArticles = async () => {
      try {
        const resp = await axios(
          'https://apifameo.azurewebsites.net/api/QA/GetAllQuestion?answered=false',
        );
        setTalents(resp.data.getCurrentData);
      } catch (error) {}
    };
    getArticles();
  }, []);

  return (
    <div className="fan-questions">
      <div className="swiper">
        <Swiper {...params}>
          {talents.map((item, i) => (
            <div key={i} className="slider-fan-question">
              <div className="fan-data">
                <img
                  src={item.UserProfPicLink}
                  className="fan-image"
                  alt="fameo-user-icon"
                />
                <h5 className="fan-name text-white">{item.TalentId}</h5>
              </div>
              <p className="fan-message text-white">
                {item.Question}
                <Link to="/" className="read-more-text">
                  Read more
                </Link>
              </p>
              <div className="message-action-list">
                <button className="btn-vote text-center">
                  <div className="vote-wrapper">
                    <img src={Nice} className="fan-nice" alt="fameo-nice" />
                    <span className="fan-votes text-center">
                      `{item.votes} vote
                    </span>
                  </div>
                </button>
                <Link to="/">
                  <img src={Chat} className="fan-chat" alt="fameo-chat" />
                </Link>
              </div>
            </div>
          ))}
        </Swiper>
      </div>
    </div>
  );
}

// const FanQuestions = () => {
//   return (
//     <div className="fan-questions">
//       <SwiperQuestions content={swiperData} />
//     </div>
//   );
// };

export default SwiperQuestions;
