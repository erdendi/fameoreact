/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/no-array-index-key */

import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
// Images
import Logo from '../assets/images/fameo-glow.svg';
import HourGlass from '../assets/images/hourglass.svg';
import Bell from '../assets/images/bell.svg';
import Calendar from '../assets/images/date-p.svg';
import Promotions from '../assets/images/offer.svg';

// Components
// import TaskBar from '../components/TaskBar'

const permissionData = [
  {
    image: HourGlass,
    imageAlt: 'hourglass',
    permission: 'Your Order Process',
  },
  {
    image: Bell,
    imageAlt: 'bell',
    permission: 'Order Notifications',
  },
  {
    image: Calendar,
    imageAlt: 'calendar',
    permission: 'Special Events',
  },
  {
    image: Promotions,
    imageAlt: 'promotions',
    permission: 'Promotions',
  },
];

function DisplayPermissions({ content }) {
  return (
    <ul className="permission-wrapper">
      {content.map((permission, i) => (
        <li key={i} className="permission-item">
          <div className="permission-icon">
            <img src={permission.image} alt={permission.imageAlt} />
          </div>
          <span className="permission-title">{permission.permission}</span>
        </li>
      ))}
    </ul>
  );
}

const Permissions = () => (
  // <div className="home">
  <div className="permission-list-wrapper">
    <div className="container permission-container">
      <img src={Logo} className="d-block mx-auto" alt="test" />
      <p className="page-title text-center font-weight-bold">
        Enable Notifications and we'll update you about:
      </p>
      <DisplayPermissions content={permissionData} />
      <div className="d-flex justify-content-center align-items-center">
        <Link to="/splashscreen" className="link-text">
          No, Thank you
        </Link>
        <Link to="/" className="btn btn-pink w-50 c-white">
          ALLOW
        </Link>
      </div>
    </div>
  </div>
  // </div>
);

DisplayPermissions.propTypes = {
  content: PropTypes.array.isRequired,
};

export default Permissions;
