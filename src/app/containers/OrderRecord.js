   
import React from 'react';
import Timer from 'react-compound-timer';

const OrderRecord = props => (
  <div className="order-record-wrapper">
    <Timer
      initialTime={props.location.remainingTime.remaining}
      startImmediately
      lastUnit="h"
      // onStop={() => console.log(<Timer.Hours />)}
      direction="backward"
    >
      {({ start, resume, pause, stop, reset, getTimerState, getTime }) => (
        <>
          <Timer.Hours /> hours
          <Timer.Minutes /> minutes
          <Timer.Seconds /> seconds
          <div>
            Remaining time:
            {getTime()}
          </div>
        </>
      )}
    </Timer>
  </div>
);

export default OrderRecord;
