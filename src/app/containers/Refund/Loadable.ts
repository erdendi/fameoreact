/**
 *
 * Asynchronously loads the component for Refund
 *
 */

import { lazyLoad } from 'utils/loadable';

export const Refund = lazyLoad(
  () => import('./index'),
  module => module.Refund,
);
