/**
 *
 * Refund
 *
 */

import React, { memo, useEffect, useMemo } from 'react';
import { Helmet } from 'react-helmet-async';
import { useSelector, useDispatch } from 'react-redux';
import { useParams, Link } from 'react-router-dom';
import styled from 'styled-components/macro';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey, actions } from './slice';
import { selectRefund } from './selectors';
import { refundSaga } from './saga';
import LoadingFull from '../../components/LoadingFull';
import Header from '../../components/Header';
import Back from '../../assets/images/icon-back.svg';
import { getCookie } from '../../helpers/index';
import { useForm, Controller } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';
import { formatPrice } from 'utils/price';
import {
  faExclamationTriangle,
  faCheckCircle,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface Props {}

export const Refund = memo((props: Props) => {
  const { id, orderNo } = useParams();
  const { control, handleSubmit, errors, setValue } = useForm();
  const userId = Number(getCookie('UserId'));

  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: refundSaga });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const refund = useSelector(selectRefund);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const dispatch = useDispatch();

  const {
    isLoading,
    refundData,
    refundResponse,
    refundSuccess,
    bankData,
  } = refund;

  const param = {
    id: Number(id),
    orderNo,
    userId,
  };

  useMemo(() => {
    dispatch(actions.setParam(param));
    dispatch(actions.getRefundStart());
    dispatch(actions.getBankStart());
  }, [id, orderNo]);

  useEffect(() => {
    setValue('ordernumber', orderNo);
    setValue('bankname', refundData.BankName || '');
    setValue('bankcode', refundData.BankCode || '');
    setValue('accountnumber', refundData.AccountNumber || '');
    setValue('accountname', refundData.BeneficiaryName || '');
  }, [refundData]);

  function onSubmit(data) {
    const param = {
      id,
      orderNo,
      userId: refundData.UserId,
      bankCode: data.bankname,
      accountNumber: data.accountnumber,
      beneficiaryName: data.accountname,
    };
    dispatch(actions.postRefundStart(param));
  }

  return (
    <>
      <Helmet>
        <title>Refund</title>
        <meta name="description" content="fameo, refund" />
      </Helmet>
      <Header content={{ title: 'Refund', BtnLeftIcon: Back }} />
      <Div>
        {isLoading && <LoadingFull />}
        <div className="container --wrapper">
          {refundResponse.Status === 'Error' ? (
            <div className="message-error">
              <FontAwesomeIcon
                icon={faExclamationTriangle}
                className="icon-big"
              />
              <h5>{refundResponse.Message}</h5>
              <Link
                to={'/orderList/' + userId}
                className="btn btn-pink c-white"
              >
                Kembali ke halaman daftar order
              </Link>
            </div>
          ) : (
            <>
              {refundSuccess ? (
                <div className="message-error">
                  <FontAwesomeIcon icon={faCheckCircle} className="icon-big" />
                  <h5>Refund berhasil di submit</h5>
                  <Link
                    to={'/orderList/' + userId}
                    className="btn btn-pink c-white"
                  >
                    Kembali ke halaman daftar order
                  </Link>
                </div>
              ) : (
                <div className="form">
                  <Form onSubmit={handleSubmit(onSubmit)}>
                    <FormGroup disabled>
                      <Label for="ordernumber">Nomor Order</Label>
                      <Controller
                        as={Input}
                        name="ordernumber"
                        control={control}
                        disabled
                      />
                      <ErrorMessage
                        errors={errors}
                        name="ordernumber"
                        as={<div className="invalid-feedback" />}
                      >
                        {({ message }) => <>{message}</>}
                      </ErrorMessage>
                    </FormGroup>
                    <FormGroup disabled>
                      <Label for="bankname">Bank</Label>
                      <Controller
                        control={control}
                        name="bankname"
                        rules={{ required: 'pilih bank' }}
                        invalid={errors.bankname}
                        render={({ onChange }) => (
                          <Input
                            type="select"
                            id="bankname"
                            className="form-control"
                            onChange={e => onChange(e.target.value)}
                            defaultValue={refundData.BankName}
                            rules={{ required: 'pilih bank' }}
                            invalid={errors.bankname}
                          >
                            <option>Pilih Bank</option>
                            {!!bankData &&
                              bankData.map((v, i) => (
                                <option key={i} value={v.BankCode}>
                                  {v.BankDescription}
                                </option>
                              ))}
                          </Input>
                        )}
                      />
                    </FormGroup>
                    <FormGroup>
                      <Label for="accountnumber">Nomor Rekening</Label>
                      <Controller
                        as={Input}
                        name="accountnumber"
                        control={control}
                        rules={{
                          required: 'minimal 10 digit',
                          minLength: 10,
                        }}
                        invalid={errors.accountnumber}
                      />
                      <ErrorMessage
                        errors={errors}
                        name="accountnumber"
                        as={<div className="invalid-feedback" />}
                      >
                        {({ message }) => <>{message}</>}
                      </ErrorMessage>
                    </FormGroup>
                    <FormGroup disabled>
                      <Label for="accountname">Nama Pemilik Rekening</Label>
                      <Controller
                        as={Input}
                        name="accountname"
                        control={control}
                        rules={{
                          required: 'nama pemilik rekening diperlukan',
                          minLength: 3,
                        }}
                        invalid={errors.accountname}
                      />
                      <ErrorMessage
                        errors={errors}
                        name="accountname"
                        as={<div className="invalid-feedback" />}
                      >
                        {({ message }) => <>{message}</>}
                      </ErrorMessage>
                    </FormGroup>
                    <FormGroup disabled>
                      <Label for="amount">Jumlah</Label>
                      <Input
                        type="text"
                        value={formatPrice(refundData.Amount)}
                        disabled
                      />
                    </FormGroup>

                    <div className="button-holder">
                      <button type="submit" className="btn btn-pink c-white">
                        REFUND
                      </button>
                    </div>
                  </Form>
                </div>
              )}
            </>
          )}
        </div>
      </Div>
    </>
  );
});

const Div = styled.div`
  .message-error {
    text-align: center;
  }
  .icon-big {
    font-size: 2em;
    margin-bottom: 18px;
  }
  .invalid-feedback {
    display: block;
  }
`;
