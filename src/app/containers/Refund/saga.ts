import { call, put, select, takeLatest } from 'redux-saga/effects';
import { actions } from './slice';
import { getRefund, submitRefund } from 'api/refundAPI';
import { selectParam } from './selectors';
import { getBank } from '../../services/profileEdit';

export function* fetchRefund() {
  const params = yield select(selectParam);

  try {
    const repos = yield call(getRefund, params);
    yield put(actions.getRefundSuccess(repos));
  } catch (error) {
    yield put(actions.getRefundFailure(error));
  }
}

export function* fetchBank() {
  try {
    const repos = yield call(getBank);
    yield put(actions.getBankSuccess(repos));
  } catch (error) {
    yield put(actions.getBankFailure(error));
  }
}
export function* postRefund() {
  const params = yield select(selectParam);

  try {
    const repos = yield call(submitRefund, params);
    if (repos.Status === 'OK') {
      yield put(actions.postRefundSuccess());
    }
  } catch (error) {
    yield put(actions.postRefundFailure(error));
  }
}

export function* refundSaga() {
  yield takeLatest(actions.getRefundStart.type, fetchRefund);
  yield takeLatest(actions.postRefundStart.type, postRefund);
  yield takeLatest(actions.getBankStart.type, fetchBank);
}
