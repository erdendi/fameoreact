import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';

// The initial state of the Refund container
export const initialState: ContainerState = {
  isLoading: false,
  error: null,
  refundResponse: {},
  refundData: {},
  param: {
    id: null,
    orderNo: '',
    userId: null,
  },
  refundSuccess: false,
  postParam: {},
  bankData: [],
};

const refundSlice = createSlice({
  name: 'refund',
  initialState,
  reducers: {
    setParam(state, action: PayloadAction<any>) {
      state.param.id = action.payload.id;
      state.param.orderNo = action.payload.orderNo;
      state.param.userId = action.payload.userId;
    },
    getRefundStart(state) {
      state.isLoading = true;
      state.error = null;
    },
    getRefundSuccess(state, action: PayloadAction<any[]>) {
      state.refundResponse = action.payload;
      state.refundData = state.refundResponse.result;
      state.isLoading = false;
      state.error = null;
    },
    getRefundFailure(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
    getBankStart(state) {
      state.error = null;
    },
    getBankSuccess(state, action: PayloadAction<any[]>) {
      state.bankData = action.payload;
    },
    getBankFailure(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
    },
    postRefundStart(state, action: PayloadAction<any>) {
      state.isLoading = true;
      state.postParam = action.payload;
      state.error = null;
    },
    postRefundSuccess(state) {
      state.isLoading = false;
      state.refundSuccess = true;
    },
    postRefundFailure(state, action: PayloadAction<string>) {
      state.isLoading = false;
      state.error = action.payload;
      state.refundSuccess = false;
    },
  },
});

export const { actions, reducer, name: sliceKey } = refundSlice;
