/* --- STATE --- */
export interface RefundState {
  isLoading: Boolean;
  error: String | null;
  refundResponse: Object | any;
  refundData: Object | any;
  param: Object | any;
  postParam: Object | any;
  refundSuccess: Boolean;
  bankData: any[];
}

export type ContainerState = RefundState;
