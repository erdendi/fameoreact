import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.refund || initialState;

export const selectRefund = createSelector(
  [selectDomain],
  refundState => refundState,
);

export const selectParam = createSelector(
  [selectDomain],
  refundState => refundState.param,
);
