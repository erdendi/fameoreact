/* eslint-disable no-useless-concat */
/* eslint-disable no-restricted-globals */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

const TimerCountDown = ({ Deadline }) => {
  useEffect(() => {
    const countDownDate = new Date(Deadline).getTime();

    const x = setInterval(() => {
      // Get today's date and time
      const now = new Date().getTime();

      // Find the distance between now and the count down date
      const distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      const days = Math.floor(distance / (1000 * 60 * 60 * 24));
      const hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
      );
      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Output the result in an element with id="demo"

      if (isNaN(days) || isNaN(hours) || isNaN(minutes) || isNaN(seconds)) {
        // document.getElementById("demo").innerHTML = "<h1> </h1>";
      } else {
        document.getElementById('demo').innerHTML =
          `<h4>${days} Hari ${hours} Jam ${minutes} Menit ` + '</h4>';

        // document.getElementById("demo").innerHTML = "<div className='time-values'><p className='hour-value'> " + days + "</p><p className='minute-value'>"  +  hours +" </p>  <p className='second-value'>"+     minutes+ "  </p>  </div> <div class='time-units'> <span>HARI</span> <span>JAM</span> <span>MENIT</span> </div>";
      }

      // If the count down is over, write some text
      if (distance < 0) {
        clearInterval(x);
        document.getElementById('demo').innerHTML = 'EXPIRED';
      }
    }, 1000);
  });

  return (
    <div style={{ color: 'white' }} id="demo">
      {/* { minutes === 0 && seconds === 0
            ? null
            : <h1> {minutes}:{seconds < 10 ?  `0${seconds}` : seconds}</h1>
        } */}
    </div>
  );
};
TimerCountDown.propTypes = {
  Deadline: PropTypes.object.isRequired,
};

export default TimerCountDown;
