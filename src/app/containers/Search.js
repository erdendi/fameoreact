   
import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import {
  FormGroup,
  Input,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import classnames from 'classnames';
import { Helmet } from 'react-helmet-async';
import ModalVideo from '../components/ModalVideo';
import ClipCardWithIcon from '../components/ClipCardWithIcon';

// Components
import Header from '../components/Header';
import TaskBar from '../components/TaskBar';
import { searchVideo } from '../services/search';
// import NormalCard from '../components/NormalCard';
import Loading from '../components/Loading';
import SingleButton from '../components/Button';

// import Icon from '../assets/images/img-idol-1.png'
import Nice from '../assets/images/icon-nice.svg';
import Chat from '../assets/images/icon-chat.svg';

const Search = () => {
  const history = useHistory();
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const [offset, setOffset] = useState(0);
  const [param, setParam] = useState();
  const [modalVideo, setModalVIdeo] = useState(false);
  const [dataModal, setDataModal] = useState(false);
  const [activeTab, setActiveTab] = useState('1');
  const limit = 10;

  const handleChange = e => {
    setSearchTerm(e.target.value);
    const params = {
      type: parseInt(activeTab),
      talentname: e.target.value,
      limitvideo: limit,
    };
    setParam(params);

    if (e.key === 'Enter') {
      // alert('enter')
      setSearchTerm(e.target.value);
      doSearch();
    } else {
      setSearchTerm(e.target.value);
      // fetchData(params)
    }
  };

  const QsFilter = (T, Cat, TN) => {
    if (Cat === 'Ucapan') {
      setActiveTab('2');
      // alert(activeTab);
      param.type = 1;
      param.Offset = parseInt(offset + limit);
      param.Fetch = limit;
      setOffset(parseInt(offset + limit));
      toggle('2');
    } else if (Cat === 'Reaksi') {
      setActiveTab('3');
      param.type = 2;
      param.Offset = 0;
      param.Fetch = limit;

      setOffset(parseInt(offset + limit));
      toggle('3');
    } else if (Cat === 'TanyaIdola') {
      setActiveTab('4');
      param.type = 3;
      toggle('4');
    }

    param.TalentIdData = T;
    setSearchTerm(TN);

    fectData(param);
  };

  const doSearch = () => {
    setIsLoading(true);
    const param_ = {
      Offset: 0,
      Fetch: limit,
    };
    if (searchTerm !== '') {
      param_.talentname = searchTerm;
    }
    if (parseInt(activeTab) > 1) {
      param_.type = parseInt(activeTab - 1);
    }
    setOffset(0);
    fectData(param_);
  };
  const fectData = params => {
    searchVideo(params)
      .then(resp => {
        setSearchResults(resp);
        setIsLoading(false);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const getsearchVideoToNol = e => {
    e.preventDefault();
    setOffset(0);
    getsearchVideo(0);
  };
  const getsearchVideo = v => {
    setIsLoading(true);
    window.scroll(0, 0);
    const params_ = {
      Offset: v || parseInt(offset),
      Fetch: limit,
    };
    if (searchTerm) {
      params_.talentname = searchTerm;
    }
    fectData(params_);
  };
  const ucapan = () => {
    setIsLoading(true);
    setOffset(0);
    const params_ = {
      type: 1,
      Offset: parseInt(offset),
      Fetch: limit,
    };
    if (searchTerm) {
      params_.talentname = searchTerm;
    }
    fectData(params_);
  };
  const reaksi = () => {
    setIsLoading(true);
    setOffset(0);
    const params_ = {
      type: 2,
      Offset: 0,
      Fetch: limit,
    };
    if (searchTerm) {
      params_.talentname = searchTerm;
    }
    fectData(params_);
    // setLimitGet(10);
  };
  const tanyaJawab = () => {
    setIsLoading(true);
    setOffset(0);
    const params_ = {
      type: 3,
      Offset: 0,
      Fetch: limit,
      // limitvideo: 10,
    };
    if (searchTerm) {
      params_.talentname = searchTerm;
    }
    fectData(params_);
  };
  const load = () => {
    window.scroll(0, 0);
    setIsLoading(true);
    const params_ = {};
    if (parseInt(activeTab) === 1) {
      params_.Offset = parseInt(offset + limit);
      params_.Fetch = limit;
      setOffset(parseInt(offset + limit));
    } else {
      params_.type = parseInt(activeTab - 1);
      params_.Offset = parseInt(offset + 10);
      params_.Fetch = limit;
      setOffset(parseInt(offset + 10));
    }
    if (searchTerm) {
      params_.talentname = searchTerm;
    }

    fectData(params_);
  };
  useEffect(() => {
    window.scroll(0, 0);

    const urlParams = new URLSearchParams(window.location.search);
    const TalentParam = urlParams.get('talentId');
    const CategoryParam = urlParams.get('Category');
    const TalentNameParam = urlParams.get('talentname');

    if (
      TalentParam !== '' &&
      TalentParam != null &&
      CategoryParam !== '' &&
      CategoryParam !== ''
    ) {
      QsFilter(TalentParam, CategoryParam, TalentNameParam.replace('-', ' '));
    } else {
      getsearchVideo(0);
    }
  }, []);

  const hideModalprop = () => {
    setDataModal(false);
    setModalVIdeo(false);
  };

  const showModalprop = content => {
    setDataModal(content);
    setModalVIdeo(true);
  };
  const toDetailQa = e => {
    history.push(`/details-qa/${e}`);
  };
  const toDetail = e => {
    history.push(`/detail/${e}`);
  };
  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };
  return (
    <>
      <Helmet>
        <title>Search Video - Fameo</title>
        <meta name="description" content="Reset Password" />
      </Helmet>
      <Header content={{ title: 'Cari Video' }} />
      <div className="search">
        <div className="container">
          {/* <Form className="search-bar"> */}
          <div className="search-bar">
            <FormGroup>
              <Input
                type="search"
                name="search"
                id="exampleSearch"
                value={searchTerm}
                // onChange={handleChange}
                onChange={e => handleChange(e)}
                onKeyDown={e => handleChange(e)}
                placeholder="Cari berdasarkan nama talent"
              />
              <span className="search-icon" onClick={() => doSearch()} />
            </FormGroup>
          </div>
          {/* </Form> */}
          <div className="filter-holder">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '1' })}
                  onClick={e => {
                    toggle('1');
                    getsearchVideoToNol(e);
                  }}
                >
                  Semua
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '2' })}
                  onClick={() => {
                    toggle('2');
                    ucapan();
                  }}
                >
                  Ucapan
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '3' })}
                  onClick={() => {
                    toggle('3');
                    reaksi();
                  }}
                >
                  Reaksi
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === '4' })}
                  onClick={() => {
                    toggle('4');
                    tanyaJawab();
                  }}
                >
                  Tanya Jawab
                </NavLink>
              </NavItem>
            </Nav>
          </div>

          <TabContent className="results-holder" activeTab={activeTab}>
            {isLoading && (
              <div className="height-100">
                <Loading />
              </div>
            )}
            <TabPane className="tab-content -m-1" tabId="1">
              {!!searchResults.ListTalentVideo &&
                searchResults.ListTalentVideo.filter(
                  item => item.Link !== null,
                ).map((item, i) => (
                  <div className="w-50 d-inline-block p-1" key={i}>
                    {/* <NormalCard content={item} /> */}
                    <ClipCardWithIcon
                      content={item}
                      isShow={showModalprop}
                      propsToDetail={toDetail}
                      imagesIcon="kado"
                    />
                  </div>
                ))}
              {!!searchResults.ListTalentVideo && (
                <>
                  <SingleButton
                    type="submit"
                    onClick={load}
                    content={{ text: 'Watch More Videos', color: 'pink' }}
                  />
                </>
              )}
            </TabPane>
            <TabPane className="tab-content" tabId="2">
              {!!searchResults.ListTalentVideo &&
                searchResults.ListTalentVideo.filter(
                  item => item.Link !== null,
                ).map((item, i) => (
                  <div className="w-50 d-inline-block p-1" key={i}>
                    {/* <NormalCard content={item} /> */}
                    <ClipCardWithIcon
                      content={item}
                      isShow={showModalprop}
                      propsToDetail={toDetail}
                      imagesIcon="kado"
                    />
                  </div>
                ))}
              {!!searchResults.ListTalentVideo && (
                <SingleButton
                  type="submit"
                  onClick={load}
                  content={{ text: 'Watch More Videos', color: 'pink' }}
                />
              )}
            </TabPane>

            <TabPane className="tab-content" tabId="3">
              {!!searchResults.ReactionVideoList &&
                searchResults.ReactionVideoList.length > 0 &&
                searchResults.ReactionVideoList.filter(
                  item => item.Link !== null,
                ).map((item, i) => (
                  <div className="w-50 d-inline-block p-1" key={i}>
                    <ClipCardWithIcon
                      content={item}
                      isShow={showModalprop}
                      propsToDetail={toDetail}
                      imagesIcon="reaction"
                    />
                    {/* <NormalCard content={item} isReaksi={true} /> */}
                  </div>
                ))}
              {!!searchResults.ReactionVideoList && (
                <SingleButton
                  type="submit"
                  onClick={load}
                  content={{ text: 'Watch More Videos', color: 'pink' }}
                />
              )}
            </TabPane>
            <TabPane className="tab-content" tabId="4">
              {!!searchResults.QuestionVideoList &&
                searchResults.QuestionVideoList.length > 0 &&
                searchResults.QuestionVideoList.filter(
                  item => item.Link !== null,
                ).map((item, i) =>
                  item.Answer ? (
                    <div className="w-50 d-inline-block p-1" key={i}>
                      <ClipCardWithIcon
                        content={item}
                        isShow={showModalprop}
                        propsToDetail={toDetail}
                        imagesIcon="tanya"
                      />
                      {/* <NormalCard content={item} isReaksi={true} /> */}
                    </div>
                  ) : (
                    <div
                      key={i}
                      className="w-50 d-inline-block p-1"
                      onClick={() => toDetailQa(item.QuestionVideoId)}
                    >
                      <div className="slider-fan-question">
                        <div className="fan-data">
                          <img
                            src={item.Thumbnails}
                            className="fan-image"
                            alt="fameo-user-icon"
                          />
                          <h5 className="fan-name text-white">
                            {item.TalentId}
                          </h5>
                        </div>
                        <p className="fan-message text-white">
                          {item.Question}
                          <Link
                            to={`/details-qa/${item.QuestionVideoId}`}
                            className="read-more-text"
                          >
                            Read more
                          </Link>
                        </p>
                        <div className="message-action-list">
                          <button className="btn-vote text-center">
                            <div className="vote-wrapper">
                              <img
                                src={Nice}
                                className="fan-nice"
                                alt="fameo-nice"
                              />
                              <span className="fan-votes text-center">
                                {item.votes} vote
                              </span>
                            </div>
                          </button>
                          <Link to={`/details-qa/${item.QuestionId}`}>
                            <img
                              src={Chat}
                              className="fan-chat"
                              alt="fameo-chat"
                            />
                          </Link>
                        </div>
                      </div>
                    </div>
                  ),
                )}
              {!!searchResults.QuestionVideoList && (
                <SingleButton
                  type="submit"
                  onClick={load}
                  content={{ text: 'Watch More Videos', color: 'pink' }}
                />
              )}
            </TabPane>
          </TabContent>
        </div>
        <TaskBar active="search" />
      </div>

      {modalVideo && <ModalVideo contents={dataModal} isHide={hideModalprop} />}
    </>
  );
};

export default Search;
