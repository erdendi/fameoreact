/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable func-names */

/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import Header from '../components/Header';
import Back from '../assets/images/icon-back.svg';
import Money from '../assets/images/cmoney.png';
import Boxtop from '../assets/images/box-top-coin.svg';

const Store = () => (
  <>
    <Header content={{ title: 'Fameo Store', BtnLeftIcon: Back, link: '/' }} />
    <div className="order store-conditions">
      <div className="container">
        <div className="content-wrapper">
          <p className="subtitle c-white">Silakan pilih koin Fameo</p>
          <div className="box-store">
            <div
              className="sub-box-store"
              onClick={function () {
                window.location.href = '/fameoapp/#/detail-store';
              }}
            >
              <div
                className="box-top"
                style={{ background: `url(${Boxtop})no-repeat` }}
              >
                <span>Rp.100.000</span>
              </div>
              <img src={Money} />
              <div className="box-bottom">
                <span>Rp.540.000</span>
              </div>
            </div>
            <div
              className="sub-box-store"
              onClick={function () {
                window.location.href = '/fameoapp/#/detail-store';
              }}
            >
              <div
                className="box-top"
                style={{ background: `url(${Boxtop})no-repeat` }}
              >
                <span>Rp.540.000</span>
              </div>
              <img src={Money} />
              <div className="box-bottom">
                <span>Rp.50.000</span>
              </div>
            </div>
            <div
              className="sub-box-store"
              onClick={function () {
                window.location.href = '/fameoapp/#/detail-store';
              }}
            >
              <div
                className="box-top"
                style={{ background: `url(${Boxtop})no-repeat` }}
              >
                <span>Rp.540.000</span>
              </div>
              <img src={Money} />
              <div className="box-bottom">
                <span>Rp.1.000</span>
              </div>
            </div>
            <div
              className="sub-box-store"
              onClick={function () {
                window.location.href = '/fameoapp/#/detail-store';
              }}
            >
              <div
                className="box-top"
                style={{ background: `url(${Boxtop})no-repeat` }}
              >
                <span>Rp.50.000</span>
              </div>
              <img src={Money} />
              <div className="box-bottom">
                <span>Rp.600.000</span>
              </div>
            </div>
            <div
              className="sub-box-store"
              onClick={function () {
                window.location.href = '/fameoapp/#/detail-store';
              }}
            >
              <div
                className="box-top"
                style={{ background: `url(${Boxtop})no-repeat` }}
              >
                <span>Rp.5.000</span>
              </div>
              <img src={Money} />
              <div className="box-bottom">
                <span>Rp.500</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
);

export default Store;
