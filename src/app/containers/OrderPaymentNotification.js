   
import React from 'react';
import Header from '../components/Header';
import NotificationCardListing from '../components/NotificationCardListing';
import Back from '../assets/images/icon-back.svg';

const cardDetails = {
  data: [
    {
      notifCard: {
        title: 'Tarik Dana - CLM2020000006',
        description: 'Sedang dalam proses',
        descriptionColor: 'light-pink',
        time: '6 minutes',
      },
    },
    {
      notifCard: {
        title: 'Tarik Dana - CLM2020000005',
        description: 'Penarikan dana berhasil',
        descriptionColor: 'purple',
        time: '6 minutes',
      },
    },
    {
      notifCard: {
        title: 'Tarik Dana - CLM2020000004',
        description: 'Sedang dalam proses',
        descriptionColor: 'light-pink',
        time: '6 minutes',
      },
    },
    {
      notifCard: {
        title: 'Tarik Dana - CLM2020000003',
        description: 'Penarikan dana gagal',
        descriptionColor: 'pink',
        time: '6 minutes',
      },
    },
    {
      notifCard: {
        title: 'Tarik Dana - CLM2020000002',
        description: 'Penarikan dana berhasil',
        descriptionColor: 'purple',
        time: '6 minutes',
      },
    },
    {
      notifCard: {
        title: 'Tarik Dana - CLM2020000001',
        description: 'Penarikan dana berhasil',
        descriptionColor: 'purple',
        time: '6 minutes',
      },
    },
  ],
};

const OrderPaymentNotification = () => (
  <div className="order-payment-notification">
    <Header content={{ title: 'Notifikasi', BtnLeftIcon: Back }} />
    <div className="container">
      <NotificationCardListing content={cardDetails} />
    </div>
  </div>
);

export default OrderPaymentNotification;
