/* eslint-disable global-require */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React from 'react';
import Header from '../components/Header';
import Detail from '../components/Detail';
import Total from '../components/Total';
import FooterLink from '../components/FooterLink';
import Back from '../assets/images/icon-left-arrow-two.svg';
import Settings from '../assets/images/icon-settings.svg';

function rincianharga() {
  if (
    document.getElementById('boxrincian').getAttribute('style') ===
    'display: none;'
  ) {
    document.getElementById('boxrincian').style.display = 'block';
    document.getElementById('total-arrow-id').style.transform =
      'rotate(180deg)';
  } else {
    document.getElementById('boxrincian').style.display = 'none';
    document.getElementById('total-arrow-id').style.transform = 'rotate(0)';
  }
}

const ProfileOrderDetail = () => (
  <div className="order-detail-wrapper">
    <Header
      content={{
        title: 'Michelle Ziudith',
        BtnLeftIcon: Back,
        BtnRightIcon: Settings,
      }}
    />
    <div className="after-heading-wrapper">
      <div className="container">
        <div className="border-bottom-wrapper">
          <h2 className="section-title mt-4">Detail Order</h2>
          <Detail
            content={{
              title: 'Tanggal Pesnan',
              detail: 'Senin, 10 Februari 2020',
            }}
          />
        </div>
        <div className="border-bottom-wrapper">
          <Detail
            content={{ title: 'Judul Project', detail: 'Ulang Tahun John Doe' }}
          />
          <Detail content={{ title: 'Ucapan Untuk', detail: 'John Doe' }} />
          <Detail content={{ title: 'Ucapan Dari', detail: 'Jane Doe' }} />
          <Detail
            content={{
              title: 'Pesan',
              detail:
                'Nulla maximus pharetra interdum. Utin justo libero. Sed feugiat, sem utinterdum pharetra, sem antecondimentum magna, vel convallisdiam dolor venenatis dui. Ut finibus et dolor bibendum varius.',
            }}
          />
        </div>
        <div className="border-bottom-wrapper" onClick={rincianharga}>
          <Total content={{ title: 'Rincian Harga', amount: 'RP 200.000' }} />
        </div>
        <div
          className="border-bottom-wrapper"
          id="boxrincian"
          style={{ display: 'none' }}
        >
          <h2 className="section-title mt-3">Informasi Pembayaran</h2>
          <Detail
            content={{ title: 'Metode Pembayaran', detail: 'Virtual Account' }}
          />
          <Detail
            content={{
              title: 'Nama Bank',
              detail: 'Bank Mandiri',
              image: require('../assets/images/icon-mandiri.svg'),
            }}
          />
          <Detail
            content={{
              title: 'Nomor Virtual Account',
              detail: '8830832432000273',
            }}
          />
        </div>
        <FooterLink
          content={{
            text: 'Ada pertanyaan? Hubungi Kami ',
            linkText: 'disini',
            url: '/help',
          }}
        />
      </div>
    </div>
  </div>
);

export default ProfileOrderDetail;
