   
import React from 'react';
import Tabs from './Tabs';

const tabContent = [
  {
    id: 1,
    tabTitle: 'test PAGE 1',
    orderList: true,
    fanQuestions: false,
  },
  {
    id: 2,
    tabTitle: 'ehehe',
    orderList: true,
    fanQuestions: false,
  },
  {
    id: 3,
    tabTitle: 'hay nako???',
    orderList: true,
    fanQuestions: true,
  },
];
const Test = () => (
  <div className="wrapper">
    <Tabs items={tabContent} />
  </div>
);
export default Test;
