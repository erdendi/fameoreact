import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { FormGroup, Label, Input, Button } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';
import { Helmet } from 'react-helmet-async';
import SingleButton from '../components/Button';
import Social from '../components/SocialButton';
import { login, signupWithGoogle } from '../services/signup';
import { setCookie } from '../helpers';
import LoadingFull from '../components/LoadingFull';
// import FacebookLogin from 'react-facebook-login';

const SignIn = () => {
  const history = useHistory();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [passwordShown, setPasswordShown] = useState(false);
  const [notif, setnotif] = useState(false);

  const submit = e => {
    e.preventDefault();
    setLoading(true);
    const params = {
      UserName: email,
      Password: password,
    };
    login(params)
      .then(resp => {
        setLoading(false);
        if (resp.User === 'Valid') {
          setCookie('UserId', resp.UserId, 1);
          setCookie('RoleId', resp.RoleId, 1);
          history.push('/home');
        } else {
          setnotif(true);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  const togglePasswordVisiblity = () => {
    setPasswordShown(!passwordShown);
  };

  const responseFacebook = response => {
    setLoading(true);
    const params = {
      Email: response.email,
      FirstName: response.name.split(' ')[0],
      LastName: response.name.split(' ')[0],
      SignUpType: 'facebook',
    };
    signupWithGoogle(params).then(resp => {
      setLoading(false);
      if (resp.Status === '4') {
        // SignUp User Baru

        setCookie('UserId', resp.getdata.Id, 1);
        setCookie('RoleId', resp.getdata.RoleId, 1);
        history.push('/home');
      } else if (resp.User === 'Valid') {
        // Login Berhasil
        setCookie('UserId', resp.UserId, 1);
        setCookie('RoleId', resp.RoleId, 1);
        history.push('/home');
      } else {
        setLoading(false);
        // alert('user login failed')
        setnotif(true);
      }
    });
  };

  const withGogle = data => {
    const params = {
      Email: data.email,
      FirstName: data.givenName,
      LastName: data.familyName,
      SignUpType: 'Google',
    };
    signupWithGoogle(params).then(resp => {
      setLoading(false);
      if (resp.Status === '4') {
        // SignUp User Baru
        setCookie('UserId', resp.getdata.Id, 1);
        setCookie('RoleId', resp.getdata.RoleId, 1);
        history.push('/home');
      } else if (resp.User === 'Valid') {
        // Login Berhasil
        setCookie('UserId', resp.UserId, 1);
        setCookie('RoleId', resp.RoleId, 1);
        history.push('/home');
      } else {
        setLoading(false);
        // alert('user login failed')
        setnotif(true);
      }
    });
  };

  const responseGoogle = response => {
    // setLoading(true);
    withGogle(response.profileObj);
  };

  const handleBack = () => {
    history.push('/home');
  };

  useEffect(() => {
    window.scroll(0, 0);
  }, []);
  return (
    <>
      <Helmet>
        <title>Sign In - Fameo</title>
        <meta name="description" content="Sign in" />
      </Helmet>
      <div className="login-form sign-in overlay-image">
        {loading && <LoadingFull />}
        <div className="container">
          <div className="content-wrapper">
            <div className="close-holder">
              <Button onClick={handleBack} className="btn-close">
                X
              </Button>
            </div>
            <h2 className="title">Selamat Datang!</h2>
            <p>Masuk ke dalam akunmu.</p>
            <div className="icon-holder">
              <GoogleLogin
                // clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
                clientId="772188898688-042l6cmrcue6fmq6nuns9ns76mmlj5hc.apps.googleusercontent.com"
                render={renderProps => (
                  <button
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                    className="btn-custome"
                  >
                    <Social>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="20"
                        height="20"
                        viewBox="0 0 19.595 20"
                      >
                        <g
                          id="google-plus"
                          transform="translate(-19.517 -35.446)"
                        >
                          <g
                            id="Group_817"
                            data-name="Group 817"
                            transform="translate(19.517 35.446)"
                          >
                            <path
                              id="Path_454"
                              data-name="Path 454"
                              d="M19.523,45.458a9.993,9.993,0,0,0,6.741,9.452c3.724,1.251,8.3.315,10.78-2.874,1.809-2.234,2.22-5.213,2.024-8-3.189-.029-6.378-.019-9.562-.014-.005,1.136,0,2.268,0,3.4,1.909.053,3.819.029,5.728.067a5.524,5.524,0,0,1-3.246,3.785A6.315,6.315,0,1,1,27.739,39.4c2.048-.721,4.158.091,5.91,1.155.888-.831,1.719-1.719,2.535-2.616a10.051,10.051,0,0,0-16.661,7.519Z"
                              transform="translate(-19.517 -35.446)"
                              fill="#fff"
                            />
                          </g>
                        </g>
                      </svg>
                    </Social>
                  </button>
                )}
                buttonText="Login"
                onSuccess={e => responseGoogle(e)}
                onFailure={e => responseGoogle(e)}
                cookiePolicy="single_host_origin"
              />
              <FacebookLogin
                appId="1608911229239324"
                autoLoad={false}
                fields="name,email,picture"
                callback={e => responseFacebook(e)}
                isMobile={false}
                render={renderProps => (
                  <button
                    className="btn-custome"
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                  >
                    <Social>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="10.813"
                        height="20"
                        viewBox="0 0 10.813 20"
                      >
                        <g id="facebook" transform="translate(-22.077)">
                          <path
                            id="Path_455"
                            data-name="Path 455"
                            d="M32.483,0,29.889,0a4.555,4.555,0,0,0-4.8,4.922V7.191H22.485a.408.408,0,0,0-.408.408v3.288a.408.408,0,0,0,.408.408h2.608v8.3A.408.408,0,0,0,25.5,20h3.4a.408.408,0,0,0,.408-.408V11.3h3.049a.408.408,0,0,0,.408-.408V7.6a.408.408,0,0,0-.408-.408H29.31V5.268c0-.925.22-1.394,1.425-1.394h1.747a.408.408,0,0,0,.408-.408V.412A.408.408,0,0,0,32.483,0Z"
                            fill="#fff"
                          />
                        </g>
                      </svg>
                    </Social>
                  </button>
                )}
              />
            </div>
            {/* <div className="icon-holder">
            <Social onClick={withGogle}>
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 19.595 20">
                <g id="google-plus" transform="translate(-19.517 -35.446)">
                  <g id="Group_817" data-name="Group 817" transform="translate(19.517 35.446)">
                    <path id="Path_454" data-name="Path 454" d="M19.523,45.458a9.993,9.993,0,0,0,6.741,9.452c3.724,1.251,8.3.315,10.78-2.874,1.809-2.234,2.22-5.213,2.024-8-3.189-.029-6.378-.019-9.562-.014-.005,1.136,0,2.268,0,3.4,1.909.053,3.819.029,5.728.067a5.524,5.524,0,0,1-3.246,3.785A6.315,6.315,0,1,1,27.739,39.4c2.048-.721,4.158.091,5.91,1.155.888-.831,1.719-1.719,2.535-2.616a10.051,10.051,0,0,0-16.661,7.519Z" transform="translate(-19.517 -35.446)" fill="#fff" />
                  </g>
                </g>
              </svg>
            </Social>
            <Social>
              <svg xmlns="http://www.w3.org/2000/svg" width="10.813" height="20" viewBox="0 0 10.813 20">
                <g id="facebook" transform="translate(-22.077)">
                  <path id="Path_455" data-name="Path 455" d="M32.483,0,29.889,0a4.555,4.555,0,0,0-4.8,4.922V7.191H22.485a.408.408,0,0,0-.408.408v3.288a.408.408,0,0,0,.408.408h2.608v8.3A.408.408,0,0,0,25.5,20h3.4a.408.408,0,0,0,.408-.408V11.3h3.049a.408.408,0,0,0,.408-.408V7.6a.408.408,0,0,0-.408-.408H29.31V5.268c0-.925.22-1.394,1.425-1.394h1.747a.408.408,0,0,0,.408-.408V.412A.408.408,0,0,0,32.483,0Z" fill="#fff" />
                </g>
              </svg>
            </Social>
          </div> */}
            <p>atau gunakan form dibawah ini</p>
            <div>
              <FormGroup>
                <Label for="exampleSelect">Username atau Email</Label>
                {/* <div className="phone-holder">
                <div className="select-holder">
                  <Input type="select" name="select" id="exampleSelect">
                    <option defaultValue>+62</option>
                  </Input>
                </div>
              </div> */}
                <Input
                  type="text"
                  name="number"
                  id="email"
                  placeholder="Username"
                  onChange={e => setEmail(e.target.value)}
                />
              </FormGroup>
              <FormGroup>
                <div className="password-holder">
                  <Input
                    type={passwordShown ? 'text' : 'password'}
                    name="password"
                    id="password"
                    placeholder="Kata Sandi"
                    onChange={e => setPassword(e.target.value)}
                  />
                  <span
                    className="eye-icon"
                    onClick={() => togglePasswordVisiblity()}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="18.467"
                      height="10"
                      viewBox="0 0 18.467 10"
                    >
                      <g id="eye" transform="translate(0 -104.721)">
                        <g
                          id="Group_822"
                          data-name="Group 822"
                          transform="translate(0 104.721)"
                        >
                          <path
                            id="Path_457"
                            data-name="Path 457"
                            d="M18.15,109.32a13.791,13.791,0,0,0-2.466-2.3,10.973,10.973,0,0,0-6.45-2.3,10.973,10.973,0,0,0-6.45,2.3,13.793,13.793,0,0,0-2.466,2.3l-.317.4.317.4a13.794,13.794,0,0,0,2.466,2.3,10.973,10.973,0,0,0,6.45,2.3,10.973,10.973,0,0,0,6.45-2.3,13.791,13.791,0,0,0,2.466-2.3l.317-.4Zm-8.916,3.9a3.5,3.5,0,0,1-3.392-4.379l-.8-.135a4.308,4.308,0,0,0,1.263,4.172A11.351,11.351,0,0,1,3.579,111.4a13.59,13.59,0,0,1-1.893-1.677,13.593,13.593,0,0,1,1.893-1.677,11.353,11.353,0,0,1,2.726-1.484l.536.6a3.5,3.5,0,1,1,2.392,6.059Zm5.655-1.826a11.353,11.353,0,0,1-2.726,1.484,4.3,4.3,0,0,0,0-6.322,11.354,11.354,0,0,1,2.726,1.484,13.592,13.592,0,0,1,1.893,1.677A13.585,13.585,0,0,1,14.889,111.4Z"
                            transform="translate(0 -104.721)"
                            fill="#a8abbb"
                          />
                          <path
                            id="Path_458"
                            data-name="Path 458"
                            d="M170.855,172.405a2.334,2.334,0,1,0,.615-1l1.149,1.3Z"
                            transform="translate(-163.851 -168.086)"
                            fill="#a8abbb"
                          />
                        </g>
                      </g>
                    </svg>
                  </span>
                </div>
              </FormGroup>
              <FormGroup check>
                <Label className="check-box" check>
                  <Input type="checkbox" />
                  <span className="checkmark" />
                  Ingat Saya
                </Label>
                <Link to="/reset-password" className="forgot-link c-white">
                  Lupa Kata Sandi
                </Link>
              </FormGroup>
              <div className="button-holder">
                <SingleButton
                  type="submit"
                  onClick={submit}
                  content={{ text: 'MASUK KE AKUN SAYA', color: 'pink' }}
                />
              </div>
              <Label>
                Pengguna Baru?
                <Link to="/signup">Daftar Sekarang</Link>
              </Label>
            </div>
          </div>
        </div>
      </div>

      <SweetAlert
        warning
        show={notif}
        title="Gagal"
        // Text =  "Username atau Password yang anda masukkan salah"
        onConfirm={() => {
          setnotif(false);
          // window.location.href = "/profile"
        }}
        onCancel={() => {
          setnotif(false);
        }}
        onEscapeKey={() => {
          setnotif(false);
          // window.location.href = "/profile"
        }}
        onOutsideClick={() => {
          setnotif(false);
          // window.location.href = "/profile"
        }}
      >
        Username atau Password yang anda masukkan salah
      </SweetAlert>
    </>
  );
};

export default SignIn;
