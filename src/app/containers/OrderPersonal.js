import React, { useState, useEffect } from 'react';
import { Form, FormGroup, Input, Label } from 'reactstrap';
import { useParams, useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';
import Header from '../components/Header';
import Back from '../assets/images/icon-back.svg';
import Settings from '../assets/images/icon-more.svg';
import { getFormOrder, postFormOrder, getCategory } from '../services/order';
import LoadingFull from '../components/LoadingFull';
import { getCookie, setCookie } from '../helpers';

const OrderPersonal = () => {
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const { talentId } = useParams();
  const [orderDetail, setOrderDetail] = useState(false);
  const [category, setCategory] = useState();
  const [bookCat, setBookCat] = useState();
  const [projekNM, setProjekNM] = useState();
  const [briefNeed, setBriefNeed] = useState();
  const [to, setTo] = useState();
  const [from, setFrom] = useState();
  const [userID] = useState(Number(getCookie('UserId')));
  const { register, errors, handleSubmit } = useForm({
    criteriaMode: 'all',
  });

  const onSubmit = () => {
    setLoading(true);
    const data = {
      BookCategory: Number(bookCat) || '',
      ProjectNm: projekNM || '',
      TalentId: Number(talentId),
      BriefNeeds: briefNeed || '',
      From: from || '',
      To: to || '',
      UserId: userID,
      IsPublic: 1,
    };

    postFormOrder(data).then(resp => {
      if (resp.Status === 'OK') {
        setCookie('BookId', resp.BookId, 1);
        history.push(`/order-confirmation/${resp.BookId}`);
      }
    });
  };

  const handleOption = e => {
    setBookCat(e);
    const projectName = category.find(el => el.Id === Number(e));
    setProjekNM(projectName.CategoryNm);
  };
  const getCat = () => {
    getCategory().then(resp => {
      setCategory(resp);
    });
  };

  useEffect(() => {
    const query = {
      TalentId: talentId,
      UserId: userID,
    };
    getFormOrder(query).then(resp => {
      setOrderDetail(resp);
    });
    getCat();
  }, [talentId, userID]);
  return (
    <>
      {!orderDetail && <LoadingFull />}
      {loading && <LoadingFull />}
      <Header
        content={{
          title: 'Order Pesanan',
          BtnLeftIcon: Back,
          BtnRightIcon: Settings,
        }}
      />
      <div className="order order-personal">
        <div className="container">
          <div className="content-wrapper">
            <h2 className="title c-white">Tulis Pesan Kamu</h2>
            <div className="profile-image">
              <img
                src={orderDetail.TalentPhotos}
                className="image"
                alt={orderDetail.TalentNm}
              />
            </div>
            <h3 className="name c-white">{orderDetail.TalentNm}</h3>
            <Form onSubmit={handleSubmit(onSubmit)}>
              <FormGroup>
                <Label className="c-white title-textbox" for="example">
                  Pilih Tema Ucapan
                </Label>
                <Input
                  type="select"
                  name="select"
                  id="exampleSelect"
                  onChange={e => handleOption(e.target.value)}
                  required
                >
                  <option value="">Pilih tema ucapan Kamu</option>
                  {!!category &&
                    category
                      .filter(item => item.Id !== 53 && item.Id !== 54)
                      .map(item => (
                        <option key={item.Id} value={item.Id}>
                          {item.CategoryNm}
                        </option>
                      ))}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label className="c-white title-textbox" for="example">
                  Ucapan Untuk
                </Label>
                <Input
                  type="text"
                  name="ucapanTo"
                  placeholder="Masukkan nama... "
                  defaultValue={orderDetail.To}
                  className="textbox form-control"
                  onChange={e => {
                    setTo(e.target.value);
                  }}
                  innerRef={register({
                    required: true,
                  })}
                  invalid={errors.ucapanTo}
                />
                <ErrorMessage
                  errors={errors}
                  name="ucapanTo"
                  render={({ messages }) =>
                    messages
                      ? Object.entries(messages).map(([type, message]) => (
                          <p key={type} className="c-red">
                            {message}
                          </p>
                        ))
                      : null
                  }
                />
              </FormGroup>
              <FormGroup>
                <Label className="c-white title-textbox" for="example">
                  Ucapan Dari
                </Label>
                <Input
                  type="text"
                  name="ucapanFrom"
                  placeholder="Masukkan nama..."
                  className="textbox form-control"
                  innerRef={register({
                    required: '*Kolom harus diisi',
                  })}
                  onChange={e => {
                    setFrom(e.target.value);
                  }}
                  invalid={errors.ucapanFrom}
                />
                <ErrorMessage
                  errors={errors}
                  name="ucapanFrom"
                  render={({ messages }) =>
                    messages
                      ? Object.entries(messages).map(([type, message]) => (
                          <p key={type} className="c-red">
                            {message}
                          </p>
                        ))
                      : null
                  }
                />
              </FormGroup>
              <FormGroup>
                <Label className="c-white title-textbox" for="example">
                  Pesan
                </Label>
                <Input
                  type="textarea"
                  name="pesan"
                  id="textarea-ask"
                  className="textbox"
                  placeholder="Tulis pesan yang ingin diucapkan idola kamu..."
                  innerRef={register({
                    required: '*Kolom harus diisi',
                  })}
                  onChange={e => {
                    setBriefNeed(e.target.value);
                  }}
                  invalid={errors.pesan}
                />
                <ErrorMessage
                  errors={errors}
                  name="pesan"
                  render={({ messages }) =>
                    messages
                      ? Object.entries(messages).map(([type, message]) => (
                          <p key={type} className="c-red">
                            {message}
                          </p>
                        ))
                      : null
                  }
                />
                <Label className="c-white title-textbox" for="example">
                  * Pesan yang ingin disampaikan tidak bersifat komersial dan
                  promosi
                </Label>
              </FormGroup>
              <input
                style={{ width: '100%', color: 'white' }}
                className="btn-pink mb-4"
                type="submit"
                value="Lanjut"
              />
            </Form>
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderPersonal;
