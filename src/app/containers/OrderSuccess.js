/* eslint-disable react/no-unescaped-entities */

import React from 'react';

import Header from '../components/Header';

import Check from '../assets/images/icon-check-success.svg';

const OrderSuccess = () => (
  <div>
    <Header content={{ title: 'Sukses' }} />
    <div className="order order-success">
      <div className="container">
        <div className="content-wrapper">
          <img src={Check} className="icon-check" alt="" />
          <h2 className="title c-white">Yeay, berhasil</h2>
          <p className="subtitle c-white">Kode Kamu sudah berhasil terkirim.</p>
          <div className="suggestion-holder">
            <p className="c-white">Here is suggestion..</p>
            <p className="c-white">
              If you always late to meetings, book Limbad to say "mmmm Hey Guys,
              Fenny told me to tell you she is running late and will be there
              shortly." Then every time you are late, send the video, and they
              will be so surprised, they wont care.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default OrderSuccess;
