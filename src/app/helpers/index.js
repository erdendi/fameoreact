// eslint-disable-next-line camelcase
import { bake_cookie, read_cookie, delete_cookie } from 'sfcookies';
// import { appConfig } from '../Constants';

export function getApiBaseUrl() {
  return process.env.REACT_APP_API_URL;
}

// export function getAzureApiBaseUrl() {
//   return appConfig.API_BASE_URL_AZURE;
// }

export function getCookie(a) {
  // var b = document.cookie.match('(^|[^;]+)\\s*' + a + '\\s*=\\s*([^;]+)');
  // return b ? b.pop() : '';
  return read_cookie(a);
}

export function setCookie(name, value) {
  // var expires = "";
  // if (days) {
  //     var date = new Date();
  //     date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
  //     expires = "; expires=" + date.toUTCString();
  // }
  // document.cookie = name + "=" + (value || "") + expires + "; path=/";

  bake_cookie(name, value);
}
export function eraseCookie(name) {
  // document.cookie = name+'=; Max-Age=-99999999;';
  // Cookies.Rem
  // document.cookie = "userId=; expires=Thu, 01 Jan 1970 00:00:00 UTC;"
  delete_cookie(name);
}
