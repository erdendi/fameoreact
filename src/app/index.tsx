/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import { GlobalStyle } from '../styles/global-styles';

// import { HomePage } from './containers/HomePage/Loadable';
import Home from './containers/Home';
// import About from './containers/About';
import SplashScreen from './containers/SplashScreen';
// import Intro from './containers/Intro';
// import Intro2 from './containers/Intro2';
import { NotFoundPage } from './containers/NotFoundPage/Loadable';
import { SearchQuestion } from './containers/SearchQuestion';
import { SearchVideo } from './containers/SearchVideo';
import { SearchTalent } from './containers/SearchTalent';
import ForgotPassword from './containers/ForgotPassword';
import ResetPassword from './containers/ResetPassword';

import About from './containers/About';
import Intro from './containers/Intro';
import Intro2 from './containers/Intro2';
import SignUp from './containers/SignUp';
import SignIn from './containers/SignIn';
import Permissions from './containers/Permissions';
import TaskBar from './components/TaskBar';
import Detail from './containers/Detail';
import FanQuestions from './containers/FanQuestions';
import { Profile } from './containers/Profile/Loadable';
import ProfileOrderDetail from './containers/ProfileOrderDetail';
import ProfileOrderReady from './containers/ProfileOrderReady';
import ProfileOrderReview from './containers/ProfileOrderReview';
import ProfileUpload from './containers/ProfileUpload';
import ProfileSettings from './containers/ProfileSettings';
import ProfileUpdate from './containers/ProfileUpdate';
import TalentProfile from './containers/TalentProfile.jsx';
import OrderInformation from './containers/OrderInformation';
import OrderRecord from './containers/OrderRecord';
import Tabs from './containers/Tabs';
import Test from './containers/Test';
import OrderValidation from './containers/OrderValidation';
import OrderSuccess from './containers/OrderSuccess';
import OrderWithdrawSuccess from './containers/OrderWithdrawSuccess';
import OrderVerification from './containers/OrderVerification';
import OrderPersonal from './containers/OrderPersonal';
import OrderConfirmation from './containers/OrderConfirmation';
import OrderConfirmationTotal from './containers/OrderConfirmationTotal';
import OrderMethodPayment from './containers/OrderMethodPayment';
import OrderGenerateVirtual from './containers/OrderGenerateVirtual';
import OrderRincian from './containers/OrderRincian';
import OrderRincianHowPay from './containers/OrderRincianHowPay';
import OrderPaymentSuccess from './containers/OrderPaymentSuccess';
import OrderPromoCode from './containers/OrderPromoCode';
import DetailsQA from './containers/DetailsQA';
import OrderPaymentNotification from './containers/OrderPaymentNotification';
import OrderPaymentNotificationDetail from './containers/OrderPaymentNotificationDetail';
import WithdrawFunds from './containers/WithdrawFunds';
import QuestionList from './components/QuestionsList';
import OrderDetailRecord from './containers/OrderDetailRecord';
import OrderVideoReview from './containers/OrderVideoReview';
import OrderVideoFinish from './containers/OrderVideoFinish';
import TermsConditions from './containers/TermsConditions.jsx';
import Help from './containers/Help';
// import MyCoin from './containers/MyCoin';
// import HistoryCoin from './containers/HistoryCoin';
import Faq from './containers/Faq';
// import AboutFameoCoin from './containers/AboutFameoCoin';
// import Store from './containers/Store';
// import DetailStore from './containers/DetailStore';
// import Sticker from './containers/Sticker';
// import DetailSticker from './containers/DetailSticker';
// import LiveStreaming from './containers/LiveStreaming';
import AskTalent from './containers/AskTalent';
// import StartLiveStreaming from './containers/StartLiveStreaming';
// import TalentLiveStreaming from './containers/TalentLiveStreaming';
// import Reports from './containers/Reports';
// import TalentAnswer from './containers/TalentAnswer';
// import TalentAnswerFinish from './containers/TalentAnswerFinish';
import TermOfWithdraw from './containers/TermsOfWithdraw.jsx';
import ProfileChangePassword from './containers/ProfileChangePassword';
import { DetailVideo } from './containers/DetailVideo/Loadable';
import { Refund } from './containers/Refund/Loadable';
import { OrderList } from './containers/OrderList/Loadable';

import 'bootstrap/dist/css/bootstrap.min.css';
import './stylesheets/main.scss';

export function App() {
  return (
    <BrowserRouter>
      <Helmet titleTemplate="%s - Fameo" defaultTitle="Fameo">
        <meta name="description" content="A Fameo application" />
      </Helmet>

      <Switch>
        {/* <Route exact path={process.env.PUBLIC_URL + '/'} component={HomePage} /> */}
        <Route path="/" exact component={SplashScreen} />
        <Route path="/home" exact component={Home} />
        <Route path="/about" component={About} />
        <Route path="/signup" component={SignUp} />
        <Route exact path="/signin" component={SignIn} />
        <Route path="/intro" component={Intro} />
        <Route path="/intro2" component={Intro2} />
        <Route path="/permissions" component={Permissions} />
        <Route path="/taskbar" component={TaskBar} />
        <Route path="/detail/:talentId" component={Detail} />
        <Route path="/video/:videoId" component={DetailVideo} />
        <Route path="/fan" component={FanQuestions} />
        <Route path="/profile" component={Profile} />
        <Route path="/orderList/:userId" component={OrderList} />
        <Route path="/orderDetail" component={ProfileOrderDetail} />
        <Route path="/orderReview/:bookId" component={ProfileOrderReview} />
        <Route path="/orderReady/:bookId" component={ProfileOrderReady} />
        <Route path="/upload/:bookId" component={ProfileUpload} />
        <Route path="/settings" component={ProfileSettings} />
        <Route path="/update" component={ProfileUpdate} />
        <Route path="/talent" component={TalentProfile} />
        <Route path="/orderInfo/:bookId" component={OrderInformation} />
        <Route path="/orderRecord" component={OrderRecord} />
        <Route path="/search" component={SearchVideo} />
        <Route path="/search-talent" component={SearchTalent} />
        <Route path="/tabs" component={Tabs} />
        <Route path="/test" component={Test} />
        <Route path="/order-validation" component={OrderValidation} />
        <Route path="/order-success" component={OrderSuccess} />
        <Route path="/order-verification" component={OrderVerification} />
        <Route path="/order-personal/:talentId" component={OrderPersonal} />
        <Route
          path="/order-confirmation/:bookId"
          component={OrderConfirmation}
        />
        <Route
          path="/order-confirmation-total"
          component={OrderConfirmationTotal}
        />
        <Route
          path="/order-method-payment/:bookId"
          component={OrderMethodPayment}
        />
        <Route
          path="/order-generate-virtual-account/:bookId"
          component={OrderGenerateVirtual}
        />
        <Route
          path="/order-rincian/:bookId/:banktype"
          component={OrderRincian}
        />
        <Route
          path="/order-rincian-how-payment/:bookId"
          component={OrderRincianHowPay}
        />
        <Route path="/order-payment-success" component={OrderPaymentSuccess} />
        <Route
          path="/order-withdraw-success/:Nominal"
          component={OrderWithdrawSuccess}
        />
        <Route path="/order-promo-code/:price" component={OrderPromoCode} />
        <Route path="/details-qa/:QuestionId" component={DetailsQA} />
        <Route path="/search-qa" component={SearchQuestion} />
        <Route path="/qlist" component={QuestionList} />
        <Route
          path="/order-payment-notification"
          component={OrderPaymentNotification}
        />
        <Route
          path="/order-payment-notification-detail"
          component={OrderPaymentNotificationDetail}
        />
        <Route path="/withdraw-funds" component={WithdrawFunds} />
        <Route
          path="/order-detail-record/:bookId"
          component={OrderDetailRecord}
        />
        <Route
          path="/order-video-review/:bookId"
          component={OrderVideoReview}
        />
        <Route
          path="/order-video-finish/:bookId"
          component={OrderVideoFinish}
        />
        <Route path="/terms-conditions" component={TermsConditions} />
        <Route path="/terms-withdraw" component={TermOfWithdraw} />
        <Route path="/help" component={Help} />
        {/* <Route path="/my-coin" component={MyCoin} /> */}
        {/* <Route path="/history-coin" component={HistoryCoin} /> */}
        <Route path="/faq" component={Faq} />
        {/* <Route path="/about-fameo-coin" component={AboutFameoCoin} /> */}
        {/* <Route path="/store" component={Store} /> */}
        {/* <Route path="/detail-store" component={DetailStore} /> */}
        {/* <Route path="/sticker" component={Sticker} /> */}
        {/* <Route path="/detail-sticker" component={DetailSticker} /> */}
        {/* <Route path="/live-streaming" component={LiveStreaming} /> */}
        <Route path="/asktalent/:talentId" component={AskTalent} />
        {/* <Route path="/start-live-streaming" component={StartLiveStreaming} /> */}
        {/* <Route path="/talent-live-streaming" component={TalentLiveStreaming} /> */}
        {/* <Route path="/reports" component={Reports} /> */}
        {/* <Route path="/talent-answer" component={TalentAnswer} /> */}
        {/* <Route path="/talent-answer-finish" component={TalentAnswerFinish} /> */}
        <Route path="/reset-password" component={ResetPassword} />
        <Route path="/ForgotPassword/:token" component={ForgotPassword} />
        <Route path="/change-password" component={ProfileChangePassword} />
        <Route path="/refund/:id/:orderNo" component={Refund} />
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </BrowserRouter>
  );
}
