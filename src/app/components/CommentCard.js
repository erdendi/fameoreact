   
import React from 'react';
import PropTypes from 'prop-types';

const Comment = ({ commentContent }) => (
  <div className="comment-card">
    <div className="image-wrapper">
      <img
        src={commentContent.UserProfPicLink}
        className="profile-picture"
        alt={commentContent.name}
      />
    </div>

    <div className="text-wrapper">
      <p className="name">{commentContent.Commentby}</p>
      <p className="date-time">
        {commentContent.CommentedDate.slice(0, 10)} at{' '}
        {commentContent.CommentedDate.slice(11, 16)}
      </p>
      <p className="comment">{commentContent.CommentMsg}</p>
    </div>
  </div>
);
Comment.propTypes = {
  commentContent: PropTypes.object.isRequired,
};
export default Comment;
