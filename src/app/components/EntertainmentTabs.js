import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classNames from 'classnames';
import 'swiper/css/swiper.css';
import NormalCard from './NormalCard';
import CustomButton from './Button';
import Loading from './Loading';

const EntertainmentTabs = ({ items, propsID, childLoad, isLoad }) => {
  const [activeTab, setActiveTab] = useState(61);
  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  const handleChange = e => {
    childLoad && childLoad(e);
    propsID(e);
  };

  function ReturnTabs({ itemsData }) {
    return (
      <div className="section-entertainment">
        <div className="tab-wrapper">
          <Nav tabs>
            <div className="d-wrap-100">
              {itemsData.data.map(itemSwiper => (
                <NavItem className="tab-item" key={itemSwiper.id}>
                  <NavLink
                    className={classNames('tab-link', {
                      active: activeTab === itemSwiper.id,
                    })}
                    onClick={() => {
                      toggle(itemSwiper.id);
                      handleChange(itemSwiper.id);
                      // console.log(categoryId);
                    }}
                  >
                    <div className="image-holder">
                      {/* <div className="icon"> */}
                      <img src={itemSwiper.icon} alt={itemSwiper.iconName} />
                      {/* </div> */}
                    </div>
                    <span className="tab-title">{itemSwiper.tabTitle}</span>
                  </NavLink>
                </NavItem>
              ))}
            </div>
          </Nav>

          <TabContent activeTab={activeTab}>
            {/* <div className="content-container"> */}
            {isLoad && <Loading />}
            {!isLoad &&
              itemsData.data.map(item => (
                <TabPane tabId={item.id} key={item.id}>
                  <p className="tab-pane-title"> {item.tabTitle}</p>
                  <div className="card-listing">
                    {itemsData.card.map((itemTalent, j) => (
                      // eslint-disable-next-line react/no-array-index-key
                      <div className="card-holder" key={j}>
                        {/* {item.isliveCard ? <LiveCard content={item.contentSwiperCard} /> : ''} */}
                        <NormalCard content={itemTalent} />
                        {/* {item.isClipCardWithIcon ? <ClipCardWithIcon content={item.contentSwiperCard} /> : ''} */}
                      </div>
                    ))}
                  </div>
                  <a href="/search-talent">
                    <CustomButton
                      content={{ text: 'See All', color: 'pink' }}
                    />
                  </a>
                </TabPane>
              ))}
            {/* </div> */}
          </TabContent>
        </div>
      </div>
    );
  }
  ReturnTabs.propTypes = {
    itemsData: PropTypes.object.isRequired,
  };
  return (
    <div>
      <ReturnTabs itemsData={items} />
    </div>
  );
};

EntertainmentTabs.propTypes = {
  items: PropTypes.object.isRequired,
  propsID: PropTypes.func.isRequired,
  childLoad: PropTypes.func.isRequired,
  isLoad: PropTypes.func.isRequired,
};
export default EntertainmentTabs;
