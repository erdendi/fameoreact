/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/prop-types */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-unused-expressions */

import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Button = ({ content, onClick, like }) => {
  const handleClick = e => {
    onClick && onClick(e);
  };

  return (
    <>
      <div
        type="submit"
        onClick={handleClick}
        className={classnames('btn', {
          'btn-pink c-white': content.color === 'pink',
          'btn-white c-violet': content.color === 'white',
          'btn-border-violet c-white': content.color === 'violet',
          'btn-border-green c-green': content.color === 'green',
          'btn-border-pink c-pink': content.color === 'transparent-pink',
          'btn-gradient-pink c-white': content.color === 'gradient-pink',
          'btn-gradient-violet c-white': content.color === 'gradient-violet',
          // "btn-border-violet c-white": content.color === 'transparent-violet',
        })}
      >
        {content.icon ? (
          <img src={content.icon} className="btn-icon" alt="" />
        ) : (
          ''
        )}
        {like}
        {content.text}
      </div>
    </>
  );
};
Button.propTypes = {
  content: PropTypes.object.isRequired,
  handleClick: PropTypes.func.isRequired,
  like: PropTypes.number.isRequired,
};

export default Button;
