/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-unused-expressions */
/* eslint-disable global-require */

import React from 'react';
import PropTypes from 'prop-types';

const contentSwiperData = {
  heading: 'Video Terbaru',
  headingColor: 'white',
  linkName: 'Lihat Semua',
  url: '/',
  isClipCardWithIcon: true,
  thumbnail: require('../assets/images/img1.png'),
  name: 'Dimas',
  description: 'Ucapan Untuk Dimas',
  event: 'Selasa',
  duration: '5 mins',
  time: '07:30 PM',
  date: '3 Maret 2020',
  likeIcon: true,
  count: '12',
  number: '+30',
  giftIcon: true,
  isTitle: true,
  text: 'Ucapan untuk ',
};
const ClipCardWithIcon = ({ content, isShow, propsToDetail, imagesIcon }) => {
  const getIdTalent = () => {
    propsToDetail && propsToDetail(content.TalentId);
  };
  const showModal = () => {
    isShow && isShow(content);
  };
  return (
    <div className="custom-card clip-card">
      <div className="icon-wrapper">
        {imagesIcon === 'kado' ? (
          <div className="icon icon-gift">
            <img
              src={require('../assets/images/icon-present.svg')}
              alt="gift icon"
            />
          </div>
        ) : (
          ''
        )}

        {imagesIcon === 'reaction' && (
          <div className="icon icon-fun">
            <img
              src={require('../assets/images/icon-smile.svg')}
              alt="fun icon"
            />
          </div>
        )}

        {imagesIcon === 'tanya' && (
          <div className="icon icon-question-mark">
            <img
              src={require('../assets/images/icon-question-mark.svg')}
              alt="question mark icon"
            />
          </div>
        )}

        {contentSwiperData.chatIcon && (
          <div className="icon icon-chat">
            <img
              src={require('../assets/images/icon-question.svg')}
              alt="chat icon"
            />
          </div>
        )}

        {imagesIcon === 'kado' && (
          <div className="icon icon-like">
            <img
              src={require('../assets/images/icon-nice.svg')}
              alt="like icon"
            />

            <span style={{ fontSize: '14px' }}>{content.TotalLikes}</span>
          </div>
        )}
        {imagesIcon === 'reaction' && (
          <div className="icon icon-like">
            <img
              src={require('../assets/images/icon-nice.svg')}
              alt="like icon"
            />

            <span style={{ fontSize: '14px' }}>{content.TotalLikes}</span>
          </div>
        )}

        {contentSwiperData.likeBadge ? (
          <div className="icon like-badge">
            <span>{content.TotalLikes}</span>
            <p>likes</p>
          </div>
        ) : (
          ''
        )}

        {contentSwiperData.numberBadge ? (
          <div className="icon number-badge">
            <p>{content.TotalLikes}</p>
          </div>
        ) : (
          ''
        )}
      </div>
      <div className="play-holder" onClick={showModal}>
        <img
          src={require('../assets/images/icon-play-white.svg')}
          className="play-circle"
          alt="play button"
        />
      </div>
      <img
        src={
          content.Thumbnails ||
          content.ProfImg ||
          content.TalentProfPicLink ||
          content.ThumbLink
        }
        className="image-thumbnail"
        alt="thumbnail"
        onError={e => {
          e.target.onerror = null;
          e.target.src = content.ProfImg;
        }}
      />

      <div className="text-holder pb-4" onClick={() => getIdTalent()}>
        <p>
          {!!content.TalentNm &&
            (content.TalentNm.split(' ').slice(0, -1).join(' ') ||
              content.TalentName.split(' ').slice(0, -1).join(' '))}
        </p>
        <p className="text">
          {imagesIcon === 'reaction' && 'Video from '}
          {imagesIcon === 'kado' && `Video for ${content.To}`}
          {imagesIcon === 'tanya' && `Video for ${content.To}`}
          {content.CustomerName ||
            content.UserName ||
            content.CustName ||
            content.CategoryNm}
        </p>
      </div>

      {/* // : contentSwiperData.isDescription ? (
      //   <div className="text-holder pb-4">
      //     <p>{contentSwiperData.name}</p> <p className="text"> {contentSwiperData.desc} </p>
      //   </div>
      // ) : (
      //   <div className="text-holder text-profile">
      //     <p>{contentSwiperData.name}</p> <p>{contentSwiperData.job}</p>
      //   </div>
      // ) */}
    </div>
  );
};
ClipCardWithIcon.propTypes = {
  content: PropTypes.object.isRequired,
  isShow: PropTypes.func.isRequired,
  propsToDetail: PropTypes.func.isRequired,
  imagesIcon: PropTypes.string.isRequired,
};
export default ClipCardWithIcon;
