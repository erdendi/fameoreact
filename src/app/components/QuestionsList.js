import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Nice from '../assets/images/icon-nice.svg';
import Chat from '../assets/images/icon-chat.svg';

function RenderQuestions({ content }) {
  return (
    <div className="question-list-wrapper">
      {content &&
        content.map((content, i) => (
          <div key={content.i} className="question-wrapper">
            <Link to={`/details-qa/${content.QuestionId}`}>
              <div className="first-row">
                <div className="best-voted-owner">
                  <h5 className="owner-name">
                    Pertanyaan Untuk {content.TalentName}
                  </h5>
                </div>
              </div>
              <div className="second-row">
                <p className="best-question">{content.Question}</p>
              </div>
              <div className="third-row">
                <div>
                  <img
                    src={content.UserProfPicLink}
                    className="round-image"
                    alt="qa"
                  />
                  <Link
                    to={`/details-qa/${content.QuestionId}`}
                    className="see-question question-by"
                  >
                    {content.UserName.slice(0, 12)}
                  </Link>
                </div>
                <div className="action-list">
                  <button className="btn-vote">
                    <img src={Nice} className="fan-nice" alt="fameo-nice" />
                    <span className="fan-votes text-center">
                      {content.TotalLike} vote
                    </span>
                  </button>
                  <Link to="/talent-answer">
                    <img src={Chat} className="fan-chat" alt="fameo-chat" />
                  </Link>
                </div>
              </div>
            </Link>
          </div>
        ))}
    </div>
  );
}

const QuestionList = ({ question }) => <RenderQuestions content={question} />;
RenderQuestions.propTypes = {
  content: PropTypes.array.isRequired,
};

QuestionList.propTypes = {
  question: PropTypes.array.isRequired,
};

export default QuestionList;
