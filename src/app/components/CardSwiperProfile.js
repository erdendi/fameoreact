/* eslint-disable react/no-array-index-key */
/* eslint-disable react/jsx-props-no-spreading */

import React from 'react';
import PropTypes from 'prop-types';
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css';
import Heading from './Heading';
import NormalCardProfile from './NormalCardProfile';

const params = {
  slidesPerView: 2.3,
  spaceBetween: 5,
  rebuildOnUpdate: true,

  breakpoints: {
    320: {
      slidesPerView: 1.8,
    },
    360: {
      slidesPerView: 2.1,
    },
    375: {
      slidesPerView: 2.3,
    },
  },
};

const CardSwiperProfile = ({ contentSwiper, contents, userID }) => (
  <div>
    <Heading content={contentSwiper.headingData} userID={userID} />
    <div className="card-swiper">
      <Swiper {...params}>
        {contents.map((item, i) => (
          <div key={i} className="slider-content">
            {/* {contentSwiper.headingData.isliveCard ? <LiveCard content={item.contentSwiperCard} /> : ''} */}
            {contentSwiper.isNormalCard ? (
              <NormalCardProfile content={item} />
            ) : (
              ''
            )}
            {/* {contentSwiper.headingDataem.isClipCardWithIcon ? <ClipCardWithIcon content={item.contentSwiperCard} /> : ''} */}
          </div>
        ))}
      </Swiper>
    </div>
  </div>
);
CardSwiperProfile.propTypes = {
  contents: PropTypes.array.isRequired,
  contentSwiper: PropTypes.object.isRequired,
  userID: PropTypes.string.isRequired,
};

export default React.memo(CardSwiperProfile);
