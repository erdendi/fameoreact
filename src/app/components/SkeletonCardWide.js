import React from 'react';
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import Logo from '../assets/images/logo512.png';

const SkeletonCardWide = ({ totalData }) => {
  return (
    <>
      {Array(totalData)
        .fill()
        .map((item, index) => (
          <>
            <SkeletonTheme color="#ddd" highlightColor="#eaeaea" key={index}>
              <div
                style={{
                  width: '100%',
                  padding: '0.25rem',
                  float: 'left',
                  display: 'block',
                  position: 'relative',
                }}
              >
                <img
                  src={Logo}
                  className="align-self-center"
                  alt="logo"
                  style={{
                    position: 'absolute',
                    width: '130px',
                    top: 'calc(50% - 65px)',
                    left: '30%',
                  }}
                />
                <Skeleton width="100%" height={140} duration={1} />
              </div>
            </SkeletonTheme>
          </>
        ))}
    </>
  );
};

export default SkeletonCardWide;
