/* eslint-disable global-require */
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const NotificationCard = ({ notificationContent }) => (
  <div className="notification-card">
    <div className="image-wrapper">
      <img
        src={require('../assets/images/icon-info.svg')}
        className="icon-info"
        alt="information icon"
      />
    </div>

    <div className="text-wrapper">
      <p className="title">{notificationContent.title}</p>
      <p
        className={classnames('description', {
          'c-purple': notificationContent.descriptionColor === 'purple',
          'c-light-pink': notificationContent.descriptionColor === 'light-pink',
          'c-pink': notificationContent.descriptionColor === 'pink',
        })}
      >
        {notificationContent.description}
      </p>
      <p className="time">{notificationContent.time} ago</p>
    </div>
  </div>
);
NotificationCard.propTypes = {
  notificationContent: PropTypes.object.isRequired,
};

export default NotificationCard;
