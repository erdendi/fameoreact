   
import React from 'react';
import PropTypes from 'prop-types';

const NotificationDetail = ({ content }) => (
  <div className="notification-detail">
    <h3>Info Detail</h3>

    <div className="detail-item">
      <h4>Status</h4>
      <p className="active">{content.status}</p>
    </div>

    <div className="detail-item">
      <h4>Waktu</h4>
      <p>
        {content.date}, {content.time}
      </p>
    </div>

    <div className="detail-item">
      <h4>Penerima</h4>
      <p>
        {content.name}
        <br />
        <small>{content.card}</small>
      </p>
    </div>

    <div className="detail-item">
      <h4>Nominal</h4>
      <p>{content.amount}</p>
    </div>
  </div>
);

NotificationDetail.propTypes = {
  content: PropTypes.object.isRequired,
};

export default NotificationDetail;
