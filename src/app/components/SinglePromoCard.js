   
import React from 'react';
import PropTypes from 'prop-types';
import SingleButton from './Button';
import Calendar from '../assets/images/calendar.svg';

const SinglePromoCard = ({ content, actionProps }) => {
  const month = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'Mei',
    'Jun',
    'Jul',
    'Agu',
    'Sep',
    'Okt',
    'Nov',
    'Des',
  ];
  const d = new Date(content.EndDate);
  const time = `${d.getDate()}, ${
    month[parseInt(d.getMonth())]
  } ${d.getFullYear()}`;

  const postProps = () => {
    actionProps(content.VoucherCd) && actionProps();
  };

  return (
    <div className="promo-wrapper">
      <div className="content-holder">
        <h2 className="c-grape">{content.TalentNm}</h2>
        <p className="code">
          Code:
          {content.VoucherCd}
        </p>
      </div>
      <div className="content-holder">
        <div>
          <p className="discount">{content.discount}% Discount</p>
          <p className="date">
            <img src={Calendar} alt="" /> Valid until {time}
          </p>
        </div>
        <div className="button-holder">
          <SingleButton
            onClick={postProps}
            content={{ text: 'Pakai', color: 'pink' }}
          />
        </div>
      </div>
    </div>
  );
};

SinglePromoCard.propTypes = {
  content: PropTypes.object.isRequired,
  actionProps: PropTypes.func.isRequired,
};

export default SinglePromoCard;
