/* eslint-disable global-require */
/* eslint-disable react/no-unescaped-entities */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const LiveCard = ({ content }) => (
  <div className="custom-card live-card">
    <div className="front col">
      <div className="top">
        <p className="live-tag">Live</p>
        <div className="play-holder">
          <img
            src="../assets/images/icon-play-white.svg"
            className="play-circle"
            alt="play button"
          />
        </div>
        <img
          src={content.thumbnail}
          className="image-thumbnail"
          alt="thumbnail"
        />
      </div>
      <div className="bottom">
        <p className="name">{content.name}</p>
        <p className="description">"{content.description}"</p>
        <p className="duration">
          Live since
          {content.duration} ago
        </p>
      </div>
    </div>

    <div className="back col">
      <div className="profile-wrapper">
        <img
          src={content.thumbnail}
          className="image-thumbnail-back"
          alt="thumbnail"
        />
        <p className="name">{content.name}</p>
      </div>
      <p className="description">{content.description}</p>
      <p className="details">
        {content.event}, {content.date} {content.time}
      </p>
      <Link to={content.url} className="btn-book">
        <img
          src="../assets/images/menu2.png"
          className="btn-logo"
          alt="camera icon"
        />
        Book Now
      </Link>
    </div>
  </div>
);
LiveCard.propTypes = {
  content: PropTypes.object.isRequired,
};
export default LiveCard;
