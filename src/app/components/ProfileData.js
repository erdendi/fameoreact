   
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { formatPrice } from 'utils/price';
import Crown from '../assets/images/crown.png';
import fameoLogo from '../assets/images/fameo-logo.svg';

const ProfileData = ({ content, talent, saldo }) => (
  <div className="profile-wrapper">
    <div className="container px-0">
      <div className="profile-data">
        {/* "profile-image-wrapper with-notif" */}
        <div
          className={classNames('profile-image-wrapper', {
            'with-notif': content.notif === true,
          })}
        >
          <img
            src={talent.ProfPicLink || fameoLogo}
            className="round-image"
            alt={talent.FirstName}
          />
        </div>
        <div className="user-data-wrapper">
          <p className="user-name">
            <span className="circle-crown">
              <img src={Crown} alt="" />
            </span>{' '}
            Hi, {talent.FirstName}
          </p>
          <div className="link-wrapper">
            {/* {
                talent.RoleId === 2 ?
                  <div>
                    <Link to="/my-coin" className="profile-link-top-up">
                      TOP UP KOIN FAMEO <img src={Rightr} className="arrow-r-small" alt=""/>
                    </Link>
                    <Link to="/" className="profile-link-member">
                      <img src={Crown} alt=""/> YOU ARE MEMBER <img src={Rightr} className="arrow-r-small" alt=""/>
                    </Link>
                  </div>
                  :
                  <div className="wrapper-btn-live">
                    <Link to="/talent-live-streaming" className="btn-live">
                      <div className="live-video-wrapper">
                        <img src={require('../assets/images/icon-film.svg')} alt="fameo-nice" />
                        <span className="lbl-live text-center">LIVE VIDEO</span>
                      </div>
                    </Link>
                    <Link to="/reports" className="btn-live">
                    <div className="live-video-wrapper">
                      <img src={require('../assets/images/report-talent.svg')} alt="fameo-nice" />
                      <span className="lbl-live text-center">REPORTS</span>
                    </div>
                  </Link>
                </div>
              } */}
          </div>
        </div>
      </div>
    </div>
    <div className="container">
      {talent.RoleId === 3 && saldo.IsUnderAgency === false ? (
        <div className="total-balance-wrapper">
          <div className="balance-data">
            <h5 className="lbl-total">Total Saldo</h5>
            <h3 className="lbl-amount">{formatPrice(saldo.Income) || 0}</h3>
          </div>
          <Link to="/withdraw-funds" className="btn-pink">
            TARIK DANA
          </Link>
          {/* {saldo.IsUnderAgency === false ?
            : '' } */}
        </div>
      ) : (
        ''
      )}
    </div>
  </div>
);

ProfileData.propTypes = {
  content: PropTypes.object.isRequired,
  talent: PropTypes.object.isRequired,
  saldo: PropTypes.object.isRequired,
};

export default ProfileData;
