/* eslint-disable global-require */
/* eslint-disable no-nested-ternary */

import React from 'react';
import PropTypes from 'prop-types';

const ClipCardWithIcon = ({ content }) => (
  <div className="custom-card clip-card">
    <div className="icon-wrapper">
      {content.giftIcon ? (
        <div className="icon icon-gift">
          <img
            src={require('../assets/images/icon-present.svg')}
            alt="gift icon"
          />
        </div>
      ) : (
        ''
      )}

      {content.funIcon ? (
        <div className="icon icon-fun">
          <img
            src={require('../assets/images/icon-smile.svg')}
            alt="fun icon"
          />
        </div>
      ) : (
        ''
      )}

      {content.questionMarkIcon ? (
        <div className="icon icon-question-mark">
          <img
            src={require('../assets/images/icon-question-mark.svg')}
            alt="question mark icon"
          />
        </div>
      ) : (
        ''
      )}

      {content.chatIcon ? (
        <div className="icon icon-chat">
          <img
            src={require('../assets/images/icon-question.svg')}
            alt="chat icon"
          />
        </div>
      ) : (
        ''
      )}

      {content.likeIcon ? (
        <div className="icon icon-like">
          <img
            src={require('../assets/images/icon-nice.svg')}
            alt="like icon"
          />
          <span>{content.count}</span>
        </div>
      ) : (
        ''
      )}

      {content.likeBadge ? (
        <div className="icon like-badge">
          <span>{content.count}</span>
          <p>likes</p>
        </div>
      ) : (
        ''
      )}

      {content.numberBadge ? (
        <div className="icon number-badge">
          <p>{content.number}</p>
        </div>
      ) : (
        ''
      )}
    </div>
    <div className="play-holder">
      <img
        src={require('../assets/images/icon-play-white.svg')}
        className="play-circle"
        alt="play button"
      />
    </div>
    <img src={content.thumbnail} className="image-thumbnail" alt="thumbnail" />

    {content.isTitle ? (
      <div className="text-holder pb-4">
        <p className="text">
          {content.text} {content.name}
        </p>
      </div>
    ) : content.isDescription ? (
      <div className="text-holder pb-4">
        <p>{content.name}</p> <p className="text"> {content.desc} </p>
      </div>
    ) : (
      <div className="text-holder text-profile">
        <p>{content.name}</p> <p>{content.job}</p>
      </div>
    )}
  </div>
);

ClipCardWithIcon.propTypes = {
  content: PropTypes.object.isRequired,
};
export default ClipCardWithIcon;
