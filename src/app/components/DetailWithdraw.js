import React from 'react';
import PropTypes from 'prop-types';

const Detail = ({ content }) => (
  <div className="detail-item-wrapper">
    <h5 className="detail-title">{content.TalentNm}</h5>
    <div className="d-flex align-items-center">
      {/* {content.image ? <img src={content.image} className="mr-2" alt="fameo-detail" /> : ''} */}
      <h2 className="detail-data mb-0">{content.TalentNm}</h2>
    </div>
  </div>
);
Detail.propTypes = {
  content: PropTypes.object.isRequired,
};

export default Detail;
