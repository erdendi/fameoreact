/* eslint-disable react/no-array-index-key */

/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
/* eslint-disable global-require */
/* eslint-disable radix */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, FormGroup, Input, Button } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';
import Heading from './Heading';
import CustomButton from './Button';
import CommentCard from './CommentCard';
import { getCookie } from '../helpers';

const FanComment = ({
  content,
  data,
  comment,
  submit,
  setMessage,
  handleClick,
  totalLike,
}) => {
  const [RoleId] = useState(parseInt(getCookie('RoleId')));
  const [notif, setNotif] = useState(false);
  const handlePop = () => {
    setNotif(true);
  };

  return (
    <>
      <div className="section-fan-comment">
        <div className="fan-question">
          <Heading
            content={{ heading: 'Pertanyaan Fans', headingColor: 'white' }}
          />
          <div className="card-question">
            <div className="profile-holder">
              <img
                src={content.UserProfPicLink}
                className="profile-picture"
                alt={content.UserName}
              />
              <p className="profile-name">{content.UserName}</p>
            </div>
            <div className="text-holder">
              <p className="question-description">
                Pertanyaan untuk {content.TalentName}
              </p>
              <p className="question-statement">{content.Question}</p>
            </div>
          </div>
          {/* {content.hasVoting ? */}
          {totalLike.map((content, i) => (
            <CustomButton
              content={data.buttonDetails}
              like={content.TotalLike}
              onClick={handleClick}
            />
          ))}
          <div className="comments">
            {RoleId === 2 || !RoleId ? (
              <Form onSubmit={event => submit(event)}>
                <FormGroup>
                  <Input
                    type="text"
                    name="comment"
                    id="comment"
                    className="input-box"
                    placeholder="Say something nice"
                    onChange={e => {
                      setMessage(e.target.value);
                    }}
                  />
                  {RoleId ? (
                    <Button color="link">
                      <img
                        src={require('../assets/images/icon-chat-gradient.svg')}
                        alt="chat icon"
                      />
                    </Button>
                  ) : (
                    <Button color="link" onClick={handlePop}>
                      <img
                        src={require('../assets/images/icon-chat-gradient.svg')}
                        alt="chat icon"
                      />
                    </Button>
                  )}
                </FormGroup>
              </Form>
            ) : (
              ''
            )}
            <div className="comment-listing">
              {Array.isArray(comment) &&
                comment.map((item, i) => (
                  <CommentCard commentContent={item} key={i} />
                ))}

              {/* <CustomButton content={{ text: 'MORE', color: 'transparent-violet' }} /> */}
            </div>
          </div>
        </div>
      </div>
      {/* <SweetAlertNotification content={AlertNotificationType}>

      </SweetAlertNotification> */}
      <SweetAlert
        warning
        confirmBtnText="Login"
        cancelBtnText="No"
        show={notif}
        title=""
        icon="failed"
        onConfirm={() => {
          setNotif(false);
          window.location.href = '/signin';
        }}
        onCancel={() => {
          setNotif(false);
        }}
        onEscapeKey={() => {
          setNotif(false);
          window.location.href = '/signin';
        }}
        onOutsideClick={() => {
          setNotif(false);
          window.location.href = '/signin';
        }}
      >
        <p style={{ fontSize: '13pt' }}>Silahkan login terlebih dahulu</p>
      </SweetAlert>
    </>
  );
};

FanComment.propTypes = {
  content: PropTypes.object.isRequired,
  comment: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  submit: PropTypes.func.isRequired,
  setMessage: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  totalLike: PropTypes.func.isRequired,
};

export default FanComment;
