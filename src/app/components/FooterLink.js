   
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const FooterLink = ({ content }) => (
  <p className="footer-text pt-3 text-center">
    {content.text}
    <Link to={content.url} className="footer-link">
      {content.linkText}
    </Link>
  </p>
);
FooterLink.propTypes = {
  content: PropTypes.object.isRequired,
};
export default FooterLink;
