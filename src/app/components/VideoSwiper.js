/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/void-dom-elements-no-children */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */

import React from 'react';
// import { Link } from 'react-router-dom';
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css';

const params = {
  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable: 'true',
  },
  slidesPerView: 1,
  slidesPerGroup: 1,
  spaceBetween: 5,
};

const VideoSwiper = ({ content }) => (
  <div className="swiper-video">
    <div className="content-container">
      <Swiper {...params}>
        {content.map((item, i) => (
          // eslint-disable-next-line react/no-array-index-key
          <div key={i}>
            <div className="slider-content">
              {/* <video width="100%" height="100%" src={item.url} controls autoPlay>
                  </video> */}
              <img width="100%" height="100%" src={item.Img} />
            </div>
          </div>
        ))}

        {/* <div key='0'>
            <div className="slider-content">
              <img width="100%" height="100%" src="https://fameostoragesa.blob.core.windows.net/photos/HomeBanner.jpg" >
              </img>
            </div>
          </div>
          <div key='1'>
            <div className="slider-content">
              <img width="100%" height="100%" src="https://fameostoragesa.blob.core.windows.net/photos/HomeBanner1.jpg" >
              </img>
            </div>
          </div>
          <div key='2'>
            <div className="slider-content">
              <img width="100%" height="100%" src="https://fameostoragesa.blob.core.windows.net/photos/HomeBanner2.jpg" >
              </img>
            </div>
          </div> */}
      </Swiper>
    </div>
  </div>
);

export default VideoSwiper;
