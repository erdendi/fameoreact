/* eslint-disable react/no-array-index-key */
/* eslint-disable react/jsx-props-no-spreading */

/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
// import { Link } from 'react-router-dom'
import Swiper from 'react-id-swiper';
import PropTypes from 'prop-types';

import Icon from '../assets/images/img-idol-1.png';
// import Ratings from '../assets/images/icon-ratings.svg'
import StartOn from '../assets/images/feedback-star.svg';
import StartOff from '../assets/images/feedback-star-2.svg';

const swiperData = [
  {
    image: Icon,
    name: 'John Doe',
    message:
      'Sed nec finibus felis. Mauris vulputate nisl sed massa dapibus, in pretium.',
    votes: '12',
  },
  {
    image: Icon,
    name: 'Jane Doe',
    message:
      'Sed nec finibus felis. Mauris vulputate nisl sed massa dapibus, in pretium.',
    votes: '12',
  },
];

const params = {
  slidesPerView: 1.1,
  slidesPerGroup: 1.1,
  spaceBetween: 5,
};

function SwiperTalentFeedback({ content }) {
  return (
    <div className="swiper">
      <Swiper {...params}>
        {content.map((item, i) => (
          <div key={i} className="feedback-slide-wrapper">
            <div className="talent-rating-wrapper">
              <div className="fan-data">
                <img
                  src={item.image}
                  className="fan-image"
                  alt="fameo-user-icon"
                />
                <h5 className="fan-name text-white">{item.name}</h5>
              </div>
              <div className="rating-wrapper">
                <div className="start">
                  <img src={StartOn} />{' '}
                </div>
                <div className="start">
                  <img src={StartOn} />{' '}
                </div>
                <div className="start">
                  <img src={StartOn} />{' '}
                </div>
                <div className="start">
                  <img src={StartOn} />{' '}
                </div>
                <div className="start">
                  <img src={StartOff} />{' '}
                </div>
              </div>
            </div>
            <p className="talent-feedback">{item.message}</p>
          </div>
        ))}
      </Swiper>
    </div>
  );
}

const TalentFeedback = () => (
  <div className="talent-feedback-wrapper">
    <div className="container">
      <SwiperTalentFeedback content={swiperData} />
    </div>
  </div>
);

SwiperTalentFeedback.propTypes = {
  content: PropTypes.array.isRequired,
};

export default TalentFeedback;
