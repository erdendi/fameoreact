   
import React from 'react';
import PropTypes from 'prop-types';

const Detail = ({ content }) => (
  <>
    <div className="detail-item-wrapper">
      <h5 className="detail-title">Nama Talent</h5>
      <div className="d-flex align-items-center">
        {/* {content.TalentPhotos ? <img src={content.TalentPhotos} className="mr-2" alt="fameo-detail" /> : ''} */}
        <h2 className="detail-data mb-0">{content.TalentNm}</h2>
      </div>
    </div>
    <div className="detail-item-wrapper">
      <h5 className="detail-title">Nomor Pesanan</h5>
      <div className="d-flex align-items-center">
        <h2 className="detail-data mb-0">{content.OrderNo}</h2>
      </div>
    </div>
    <div className="detail-item-wrapper">
      <h5 className="detail-title">Judul Project</h5>
      <div className="d-flex align-items-center">
        <h2 className="detail-data mb-0">{content.ProjectNm}</h2>
      </div>
      <hr className="hr" />
      <h5 className="detail-title">Ucapan Untuk</h5>
      <div className="d-flex align-items-center">
        <h2 className="detail-data mb-0">{content.To}</h2>
      </div>
      <hr className="hr" />
      <h5 className="detail-title">Ucapan Dari</h5>
      <div className="d-flex align-items-center">
        <h2 className="detail-data mb-0">{content.From}</h2>
      </div>
      <hr className="hr" />
      <h5 className="detail-title">Pesan</h5>
      <div className="d-flex align-items-center">
        <h2 className="detail-data mb-0">{content.BriefNeeds}</h2>
      </div>
    </div>
  </>
);

Detail.propTypes = {
  content: PropTypes.array.isRequired,
};

export default Detail;
