import React, { useRef, useEffect } from 'react';
import videojs from 'video.js';
import 'videojs-plus';
import 'videojs-plus/dist/plugins/quality-hls';

import Div from './Styled';
// require('videojs-contrib-quality-levels');
// require('videojs-contrib-hls');
// require('videojs-youtube');

const defaultPlayerOptions = {
  autoplay: 'play',
  muted: false,
  // aspectRatio: '9:16',
  // aspectRatio: '16:9',
  mobileView: false,
  loop: true,
  // seeking: false,
  preload: 'none',
  // controls: false,
  fill: true,
  // techOrder: ['youtube'],
  youtube: { iv_load_policy: 1 },
};

export function Player({
  children,
  playerOptions,
  onPlayerInit,
  onPlayerDispose,
}) {
  const containerRef = useRef(null);

  useEffect(() => {
    if (containerRef.current) {
      const videoEl = containerRef.current.querySelector('video');
      const player = videojs(videoEl, {
        ...defaultPlayerOptions,
        ...playerOptions,
      });

      // used to move children component into player's elment
      // your may not thest script
      const playerEl = player.el();
      const flag = player.getChild('PlayToggleLayer').el();
      for (const child of containerRef.current.children) {
        if (child !== playerEl) {
          playerEl.insertBefore(child, flag);
        }
      }
      // ----
      onPlayerInit && onPlayerInit(player);

      // for debug purpose
      window.player = player;

      return () => {
        onPlayerDispose && onPlayerDispose(null);
        player.dispose();
      };
    }
  }, [onPlayerInit, onPlayerDispose, playerOptions]);

  return (
    <Div>
      <div className="player" ref={containerRef}>
        {/* <video data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "https://www.youtube.com/watch?v=xjS6SftYQaQ"}], "youtube": { "iv_load_policy": 1 } }' /> */}
        <video />
        {children}
      </div>
    </Div>
  );
}
