import React from 'react';
import PropTypes from 'prop-types';
import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css';
import Heading from './Heading';
import LiveCard from './LiveCard';
import NormalCard from './NormalCard';

const params = {
  slidesPerView: 2.3,
  spaceBetween: 12,
  rebuildOnUpdate: true,

  breakpoints: {
    320: {
      slidesPerView: 1.8,
    },
    360: {
      slidesPerView: 2.1,
    },
    375: {
      slidesPerView: 2.3,
    },
  },
};

const CardSwiper = props => {
  // const [talents, setTalents] = useState([]);
  // const [error, setError] = useState(false);

  // const getLimitTalent = async() =>{
  //   let params = {
  //     categoryid: 25,
  //     limit: 6
  //   }
  //   await getCategoryByID(params)
  //   .then(resp => {
  //     setTalents(resp.ListTalentCategoryModel);
  //   })
  //   .catch(err => {
  //     setError(true);
  //     console.log(err);
  //   });
  // }
  // useEffect(() => {
  //   // getLimitTalent()
  // }, []);

  const { contentSwiper, dataTalent } = props;

  return (
    <div>
      <Heading content={contentSwiper.headingData} userID="" />
      <div className="card-swiper">
        <Swiper {...params}>
          {dataTalent.map((item, i) => (
            <div key={i} className="slider-content">
              {contentSwiper.headingData.isliveCard ? (
                <LiveCard content={item.contentSwiperCard} />
              ) : (
                ''
              )}
              {contentSwiper.isNormalCard ? <NormalCard content={item} /> : ''}
              {/* {contentSwiper.headingDataem.isClipCardWithIcon ? <ClipCardWithIcon content={item.contentSwiperCard} /> : ''} */}
            </div>
          ))}
        </Swiper>
      </div>
    </div>
  );
};
CardSwiper.propTypes = {
  contentSwiper: PropTypes.object.isRequired,
  dataTalent: PropTypes.array,
};

export default CardSwiper;
