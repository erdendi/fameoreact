   
import React from 'react';
import * as LoadingGif from '../assets/images/loading.gif';

function LoadingScreen() {
  return (
    <div className="blank-page-fixed">
      <div className="img-container">
        <img src={LoadingGif} alt="loading" width="200" />
      </div>
    </div>
  );
}

export default LoadingScreen;
