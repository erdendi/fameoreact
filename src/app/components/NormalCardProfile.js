import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { getCookie } from '../helpers';

const NormalCard = ({ content }) => {
  const [userID] = useState(Number(getCookie('UserId')));

  // used for the customer page
  if (content.BookedBy === userID) {
    if (content.Status === 0) {
      return (
        // /order-rincian-how-payment/:bookId/:banktype
        //  <Link to={`orderInfo/${content.Id}`}>
        <Link to={`order-rincian-how-payment/${content.Id}`}>
          <div className="custom-card normal-card">
            <img
              src={
                content.TalentPhotos ||
                content.ProfImg ||
                content.ThumbLink ||
                content.UserImage
              }
              className="image-thumbnail"
              alt={content.TalentNm}
            />
            <div className="text-holder">
              <p className="job">{content.CustName}</p>
              <p className="job">{content.ProjectNm}</p>
              <p className="job">{content.Profesion}</p>
              <p className="name">{content.TalentNm}</p>
            </div>
          </div>
        </Link>
      );
    }
    if (content.Status === 1) {
      return (
        // /order-rincian-how-payment/:bookId/:banktype
        //  <Link to={`orderInfo/${content.Id}`}>
        <Link to={`order-confirmation/${content.Id}`}>
          <div className="custom-card normal-card">
            <img
              src={
                content.TalentPhotos ||
                content.ProfImg ||
                content.ThumbLink ||
                content.UserImage
              }
              className="image-thumbnail"
              alt={content.TalentNm}
            />
            <div className="text-holder">
              <p className="job">{content.CustName}</p>
              <p className="job">{content.ProjectNm}</p>
              <p className="job">{content.Profesion}</p>
              <p className="name">{content.TalentNm}</p>
            </div>
          </div>
        </Link>
      );
    }
    if (content.Status === 5) {
      return (
        <Link to={`orderReady/${content.Id}`}>
          <div className="custom-card normal-card">
            <img
              src={
                content.TalentPhotos ||
                content.ProfImg ||
                content.ThumbLink ||
                content.UserImage
              }
              className="image-thumbnail"
              alt={content.TalentNm}
            />
            <div className="text-holder">
              <p className="job">{content.CustName}</p>
              <p className="job">{content.ProjectNm}</p>
              <p className="job">{content.Profesion}</p>
              <p className="name">{content.TalentNm}</p>
            </div>
          </div>
        </Link>
      );
    }
    if (content.Status > 7) {
      return (
        <Link to={`orderInfo/${content.Id}`}>
          <div className="custom-card normal-card">
            <img
              src={
                content.TalentPhotos ||
                content.ProfImg ||
                content.ThumbLink ||
                content.UserImage
              }
              className="image-thumbnail"
              alt={content.TalentNm}
            />
            <div className="text-holder">
              <p className="job">{content.CustName}</p>
              <p className="job">{content.ProjectNm}</p>
              <p className="job">{content.Profesion}</p>
              <p className="name">{content.TalentNm}</p>
            </div>
          </div>
        </Link>
      );
    }

    return (
      <Link to={`orderInfo/${content.Id}`}>
        <div className="custom-card normal-card">
          <img
            src={
              content.TalentPhotos ||
              content.ProfImg ||
              content.ThumbLink ||
              content.UserImage
            }
            className="image-thumbnail"
            alt={content.TalentNm}
          />
          <div className="text-holder">
            <p className="job">{content.CustName}</p>
            <p className="job">{content.ProjectNm}</p>
            <p className="job">{content.Profesion}</p>
            <p className="name">{content.TalentNm}</p>
          </div>
        </div>
      </Link>
    );
  }

  // used for talent page

  if (content.Status === 3) {
    return (
      // /order-rincian-how-payment/:bookId/:banktype
      <Link to={`orderInfo/${content.Id}`}>
        {/* // <Link to={`order-rincian-how-payment/${content.Id}/`}> */}
        <div className="custom-card normal-card">
          <img
            src={
              content.TalentPhotos ||
              content.ProfImg ||
              content.ThumbLink ||
              content.UserImage
            }
            className="image-thumbnail"
            alt={content.TalentNm}
          />
          <div className="text-holder">
            <p className="job">{content.CustName}</p>
            <p className="job">{content.ProjectNm}</p>
            <p className="job">{content.Profesion}</p>
            <p className="name">{content.TalentNm}</p>
          </div>
        </div>
      </Link>
    );
  }
  if (content.Status === 4) {
    return (
      <Link to={`order-detail-record/${content.Id}`}>
        {/* // <Link to={`order-rincian-how-payment/${content.Id}/`}> */}
        <div className="custom-card normal-card">
          <img
            src={
              content.TalentPhotos ||
              content.ProfImg ||
              content.ThumbLink ||
              content.UserImage
            }
            className="image-thumbnail"
            alt={content.TalentNm}
          />
          <div className="text-holder">
            <p className="job">{content.CustName}</p>
            <p className="job">{content.ProjectNm}</p>
            <p className="job">{content.Profesion}</p>
            <p className="name">{content.TalentNm}</p>
          </div>
        </div>
      </Link>
    );
  }
  if (content.Status > 4) {
    return (
      <Link to={`order-detail-finish/${content.Id}`}>
        {/* // <Link to={`order-rincian-how-payment/${content.Id}/`}> */}
        <div className="custom-card normal-card">
          <img
            src={
              content.TalentPhotos ||
              content.ProfImg ||
              content.ThumbLink ||
              content.UserImage
            }
            className="image-thumbnail"
            alt={content.TalentNm}
          />
          <div className="text-holder">
            <p className="job">{content.CustName}</p>
            <p className="job">{content.ProjectNm}</p>
            <p className="job">{content.Profesion}</p>
            <p className="name">{content.TalentNm}</p>
          </div>
        </div>
      </Link>
    );
  }

  return (
    <Link to={`orderInfo/${content.Id}`}>
      {/* // <Link to={`order-rincian-how-payment/${content.Id}/`}> */}
      <div className="custom-card normal-card">
        <img
          src={
            content.TalentPhotos ||
            content.ProfImg ||
            content.ThumbLink ||
            content.UserImage
          }
          className="image-thumbnail"
          alt={content.TalentNm}
        />
        <div className="text-holder">
          <p className="job">{content.CustName}</p>
          <p className="job">{content.ProjectNm}</p>
          <p className="job">{content.Profesion}</p>
          <p className="name">{content.TalentNm}</p>
        </div>
      </div>
    </Link>
  );
};
NormalCard.propTypes = {
  content: PropTypes.object.isRequired,
};
export default NormalCard;
