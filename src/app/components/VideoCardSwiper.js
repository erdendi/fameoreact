import React, { useState, useEffect } from 'react';
// import Swiper from 'react-id-swiper';
import 'swiper/css/swiper.css';
import {
  Modal,
  ModalHeader,
  ModalBody,
  // ModalFooter, Button
} from 'reactstrap';
import { useHistory } from 'react-router-dom';
import SweetAlertNotification from 'react-bootstrap-sweetalert';
import { SocialIcon } from 'react-social-icons';
// import Heading from './Heading';
// import LiveCard from './LiveCard';
// import NormalCard from './NormalCard';
import ClipCardWithIcon from './ClipCardWithIcon';
// import {Link} from 'react-router-dom';
import { sendQuestion } from '../services/question';
import { getCookie } from '../helpers';
import { LikesVideo } from '../services/video';

import VideoA from '../assets/images/detail/left_arrow_video.png';
// import VideoB from '../assets/images/detail/content1.png';
import VideoC from '../assets/images/detail/close.png';
import VideoD from '../assets/images/video/share.png';
import VideoE from '../assets/images/video/chat.png';
// import VideoF from '../assets/images/video/chats.png';
import VideoG from '../assets/images/video/nice.png';
import VideoH from '../assets/images/video/charz.svg';
import VideoI from '../assets/images/video/origami.png';
import { Player } from './Player';

const playerOptions = {};

const CardSwiper = ({ contents }) => {
  const history = useHistory();
  const [question, setQuestion] = useState();
  const [userID] = useState(Number(getCookie('UserId')));
  const [warningnotif, setwarningnotif] = useState(false);

  const [show, setShow] = useState(false);

  const [modalShare, setModalShare] = useState(false);
  const toggleShare = () => {
    setModalShare(!modalShare);
  };

  const [player, setPlayer] = useState(null);
  const [video, setVideo] = useState(null);
  useEffect(() => {
    if (player && video) {
      player.unload({ loading: true });
      player.src(video.sources);
    }
  }, [video, player]);

  function myFunction(content) {
    const element = document.getElementById('taskbar');
    if (element.getAttribute('class') === 'taskbar w-100') {
      element.classList.add('hidden');
    } else {
      element.classList.remove('hidden');
      // stopvideo();
    }
  }

  const askbtnshow = e => {
    history.push(`/asktalent/${e}`);

    // di Comment Sementara sampe fitur talent answer video udh jadi
    // if(show == false)
    // {
    //   setShow(true);
    // }
    // else
    // {
    //   setShow(false);
    // }

    // setShow(true);
    // var element = document.getElementById("custom-ask-on-video");
    // if (element.getAttribute("class") === "custom-ask-on-video-hidden") {
    //   element.classList.add("show");
    // } else {
    //   element.classList.remove("show");
    // }
  };

  const LikesVideosData = e => {
    if (isNaN(userID)) {
      setwarningnotif(true);
    } else {
      const params = {
        UserId: userID,
        FileId: e,
      };
      LikesVideo(params).then(resp => {
        if (resp.Status === 'OK') {
          document.getElementById('TotalLikes').innerHTML = resp.CountLikes;
        }
      });
    }
  };

  const [modal, setModal] = useState(false);
  const [dataModal, setDataModal] = useState(false);

  const sendQA = e => {
    const params = {
      UserId: userID,
      FileId: e,
      Question: question,
    };
    sendQuestion(params).then(resp => {
      if (resp.Status === 'OK') {
        setShow(false);
      }
    });
  };

  const toggle = data => {
    if (data !== undefined) {
      const videoObj = {
        title: data.TalentNm || '',
        // poster:
        //   'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg',
        sources: [
          {
            src: data.Link,
            type: 'video/mp4',
          },
        ],
      };

      setVideo(videoObj);
    }

    setDataModal(data);
    setModal(!modal);
  };

  useEffect(() => {
    window.scroll(0, 0);
  }, []);
  return (
    <>
      <div>
        <div className="d-wrap">
          {contents &&
            contents
              .slice(0, 10)
              .filter(content => Boolean(content.IsPublic) === true)
              .map((content, i) => (
                <div
                  key={i}
                  className="slider-content"
                  onClick={function () {
                    toggle(content);
                    myFunction(content);
                  }}
                >
                  <ClipCardWithIcon content={content} imagesIcon="kado" />
                </div>
              ))}
        </div>

        {dataModal && (
          <>
            <Modal isOpen={modal} toggle={toggle} id="ModalVideoPlayer">
              <ModalBody>
                <div className="box-video--">
                  <div className="row">
                    <div className="col-2 col-md-2">
                      <div
                        className="btn-close--"
                        onClick={function () {
                          toggle();
                          myFunction();
                        }}
                      >
                        <img src={VideoA} alt="" />
                      </div>
                    </div>
                    <div className="col-6 col-md-6">
                      <div className="media">
                        <img
                          src={dataModal.Thumbnails}
                          className="align-self-center mr-3"
                          alt=""
                        />
                        <div className="media-body">
                          <h4>{dataModal.TalentNm}</h4>
                          {/* {content2.ListTalentVideo && content2.ListTalentVideo.map((item, i) => {
                      return (
                        <p key={i}>{item.CustomerName}</p>
                      )
                    })} */}
                        </div>
                      </div>
                    </div>
                    <div className="col-4 col-md-4 text-right">
                      <div
                        onClick={function () {
                          toggle();
                          myFunction();
                        }}
                      >
                        <img className="vid-logo-det" src={VideoC} alt="" />
                      </div>
                    </div>
                  </div>
                </div>

                {/* {content2.ListTalentVideo && content2.ListTalentVideo.map((item, i) => {
            return ( */}
                <div className="wrapper_video">
                  {/* <video
                  width="100%"
                  height="100%"
                  src={dataModal.Link}
                  className="video_video"
                  id="video"
                  type="video/mp4"
                  onClick={stopvideo}
                />
                <div
                  className="playpause_video"
                  id="playpause_video_id"
                  onClick={playvideo}
                /> */}
                  <Player
                    playerOptions={playerOptions}
                    onPlayerInit={setPlayer}
                    onPlayerDispose={setPlayer}
                  />
                </div>
                {/* )
          })} */}

                <div className="box-video--bottom">
                  <div className="row">
                    <div className="col-2 col-md-2">
                      <div className="btn-bottom-icon">
                        <img src={VideoD} alt="" onClick={toggleShare} />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-2 col-md-2">
                      <div
                        onClick={() => askbtnshow(dataModal.TalentId)}
                        className="btn-bottom-icon"
                      >
                        <img src={VideoE} style={{ padding: '6px' }} alt="" />
                      </div>
                    </div>
                  </div>
                  {/* <div className="row">
                <div className="col-2 col-md-2">
                  <div className="btn-bottom-icon">
                    <img src={VideoF} alt="" />
                  </div>
                </div>
              </div> */}
                  <div className="row">
                    <div className="col-2 col-md-2">
                      {/* <div className="btn-bottom-icon">
                    <img className="m-0" src={VideoG} alt="" />
                    <div>{dataModal.TotalLikes}</div>
                  </div> */}
                      <div
                        onClick={() => LikesVideosData(dataModal.FileId)}
                        className="btn-bottom-icon"
                      >
                        <img className="m-0" src={VideoG} alt="" />
                        <div style={{ color: 'white' }}>
                          <label id="TotalLikes">{contents.TotalLikes}</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* <div id="custom-seekbar">
                <span id="custom-seekbar-span" />
                <div id="custom-seekbar-div" />
              </div> */}
                {show && (
                  <div
                    id="custom-ask-on-video"
                    className="custom-ask-on-video-hidden show"
                  >
                    <div className="col-12 col-md-12">
                      <div className="line----" onClick={askbtnshow} />
                      <p className="text-center">
                        <img className="btn-chat----" src={VideoH} alt="" />
                        Pertanyaan untuk <b> {dataModal.TalentNm}</b>
                      </p>
                      <input
                        type="text"
                        placeholder="Ajukan pertanyaan Kamu"
                        onChange={e => setQuestion(e.target.value)}
                      />
                      <span onClick={() => sendQA(dataModal.FileId)}>
                        <img src={VideoI} alt="" />
                      </span>
                    </div>
                  </div>
                )}
              </ModalBody>
            </Modal>
            <Modal
              isOpen={modalShare}
              toggle={toggleShare}
              contentClassName="modal-share"
            >
              <ModalHeader toggle={toggleShare} charCode="X">
                Share Video on
              </ModalHeader>
              <ModalBody>
                <div className="modal-body-container">
                  <div className="modal-icon">
                    <SocialIcon
                      url={`https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.fameoapp.com%2Fvideo%2F${dataModal.FileId}`}
                    />
                  </div>
                  <div className="modal-icon">
                    <SocialIcon
                      network="whatsapp"
                      url={`https://wa.me/?text=https%3A%2F%2Fwww.fameoapp.com%2Fvideo%2F${dataModal.FileId}`}
                    />
                  </div>
                </div>
              </ModalBody>
            </Modal>
          </>
        )}
      </div>

      <SweetAlertNotification
        warning
        show={warningnotif}
        confirmBtnText="Login"
        title=""
        onConfirm={() => {
          setwarningnotif(false);
          window.location.href = '/signin';
        }}
        onCancel={() => {
          setwarningnotif(false);
        }}
        onEscapeKey={() => {
          setwarningnotif(false);
        }}
        onOutsideClick={() => {
          setwarningnotif(false);
        }}
      >
        {' '}
        <p style={{ fontSize: '13pt' }}>Silahkan login terlebih dahulu</p>
      </SweetAlertNotification>
    </>
  );
};

export default CardSwiper;
