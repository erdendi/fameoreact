/* eslint-disable react/no-array-index-key */

import React from 'react';
import PropTypes from 'prop-types';
import NotificationCard from './NotificationCard';

const NotificationCardListing = ({ content }) => (
  <div className="notification-card-listing">
    {content.data.map((item, i) => (
      <NotificationCard notificationContent={item.notifCard} key={i} />
    ))}
  </div>
);

NotificationCardListing.propTypes = {
  content: PropTypes.array.isRequired,
};

export default NotificationCardListing;
