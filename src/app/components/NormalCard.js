   
import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

const NormalCard = ({ content }) => (
  <NavLink
    to={`detail/${content.TalentId}/${content.TalentNm.trim()
      .replace('  ', '-')
      .replace(' ', '-')}`}
  >
    {/* <Link to={`detail/${content.TalentId}/${content.TalentNm.trim().replace('  ','-').replace(' ','-')}`}> */}
    <div className="custom-card normal-card">
      <img
        src={content.LinkImg || content.ProfImg || content.ThumbLink}
        className="image-thumbnail"
        alt={content.TalentNm}
      />
      <div className="text-holder">
        <p className="name">{content.TalentNm}</p>
        <p className="job">{content.Profesion}</p>
      </div>
    </div>
  </NavLink>
);
NormalCard.propTypes = {
  content: PropTypes.object.isRequired,
};
export default NormalCard;
