/* eslint-disable no-unused-vars */
/* eslint-disable react/button-has-type */

import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Star from '../assets/images/icon-star.svg';
import Nice from '../assets/images/icon-nice.svg';
import Chat from '../assets/images/icon-chat.svg';

const BestVotedQuestion = ({ comment }) => (
  <div className="best-voted-component">
    {comment.slice(0, 1).map((item, i) => (
      <div className="best-voted-wrapper">
        <div className="container best-voted-block">
          <div className="question-wrapper">
            <div className="first-row">
              <div className="best-voted-owner">
                <img
                  src={item.UserProfPicLink}
                  className="round-image"
                  alt="fameo-question"
                />
                <h5 className="owner-name">{item.UserName}</h5>
              </div>
              <div className="best-voted-badge">
                <img src={Star} className="star-icon" alt="fameo-star" />
                BEST VOTED
                <img src={Star} className="star-icon" alt="fameo-star" />
              </div>
            </div>
            <div className="second-row">
              <p className="best-question">{item.Question}</p>
            </div>
            <div className="third-row">
              <Link to="/" className="see-question">
                Lihat Pertanyaan
              </Link>
              <div className="action-list">
                <button className="btn-vote">
                  <img src={Nice} className="fan-nice" alt="fameo-nice" />
                  <span className="fan-votes text-center">
                    {item.TotalLike}
                  </span>
                </button>
                <Link to="/">
                  <img src={Chat} className="fan-chat" alt="fameo-chat" />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    ))}
  </div>
);
BestVotedQuestion.propTypes = {
  comment: PropTypes.object.isRequired,
};
export default BestVotedQuestion;
