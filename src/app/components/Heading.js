   
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

const Heading = ({ content, userID = '' }) => (
  <div className="section-heading">
    <div className="d-flex justify-content-between">
      <p
        className={classnames('section-title', {
          'section-title-white': content.headingColor === 'white',
          'section-title-violet': content.headingColor === 'violet',
          'section-title-uppercase': content.headingLetters === 'uppercase',
        })}
      >
        {content.heading}
      </p>
      {userID !== '' && (
        <Link to={`/orderList/${userID}`} className="see-all-link">
          {content.linkName}
        </Link>
      )}
    </div>
  </div>
);
Heading.propTypes = {
  content: PropTypes.object.isRequired,
  userID: PropTypes.string,
};

export default Heading;
