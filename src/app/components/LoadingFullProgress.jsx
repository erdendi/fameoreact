   
import React from 'react';
import * as LoadingGif from '../assets/images/loading.gif';
import { Progress } from 'reactstrap';
import styled from 'styled-components/macro';

function LoadingFullProgress({ progressValue = 0 }) {
  return (
    <Div>
      <div className="blank-page-fixed">
        <div className="img-container">
          <img src={LoadingGif} alt="loading" width="200" />
        </div>
      </div>
      {progressValue > 0 && (
        <div className="progress-wrapper">
          <Progress animated value={Math.floor(progressValue)}>
            {Math.floor(progressValue)}%
          </Progress>
        </div>
      )}
    </Div>
  );
}

export const Div = styled.div`
  .progress-wrapper {
    width: 370px;
    position: fixed;
    top: 54%;
    height: 20px;
    z-index: 9991;
  }
  .progress-bar {
    background-color: #fd2281;
  }
`;

export default LoadingFullProgress;
