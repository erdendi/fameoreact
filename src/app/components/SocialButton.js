/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const SocialButton = ({ children }) => (
  <Link to="#" className="social-link">
    <span className="social-icon">{children}</span>
  </Link>
);

SocialButton.propTypes = {
  children: PropTypes.string.isRequired,
};
export default SocialButton;
