import { GithubRepoFormState } from 'app/containers/GithubRepoForm/types';
import { ThemeState } from 'styles/theme/types';
// import { ResetPasswordState } from 'app/containers/ResetPassword/types';
import { SearchTalentState } from 'app/containers/SearchTalent/types';
import { SearchQuestionState } from 'app/containers/SearchQuestion/types';
import { DetailState } from 'app/containers/Detail/types';
import { DetailVideoState } from 'app/containers/DetailVideo/types';
import { RefundState } from 'app/containers/Refund/types';
import { SearchVideoState } from 'app/containers/SearchVideo/types';
import { ProfileState } from 'app/containers/Profile/types';
import { OrderListState } from 'app/containers/OrderList/types';
// [IMPORT NEW CONTAINERSTATE ABOVE] < Needed for generating containers seamlessly

/* 
  Because the redux-injectors injects your reducers asynchronously somewhere in your code
  You have to declare them here manually
  Properties are optional because they are injected when the components are mounted sometime in your application's life. 
  So, not available always
*/
export interface RootState {
  theme?: ThemeState;
  githubRepoForm?: GithubRepoFormState;
  // resetPassword?: ResetPasswordState;
  searchTalent?: SearchTalentState;
  searchQuestion?: SearchQuestionState;
  detail?: DetailState;
  detailVideo?: DetailVideoState;
  refund?: RefundState;
  searchVideo?: SearchVideoState;
  profile?: ProfileState;
  orderList?: OrderListState;
  // [INSERT NEW REDUCER KEY ABOVE] < Needed for generating containers seamlessly
}
